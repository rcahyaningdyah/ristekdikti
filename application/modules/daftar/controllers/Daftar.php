<?php
	class Daftar extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->load->model(array('users_m','kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','kementrian_m','lembaga_m','puslitbang_m'));
			$this->form_validation->set_message('required', ' %s harap di isi.');
			$this->form_validation->set_message('valid_email','Harap memasukkan %s valid');
			$this->form_validation->set_message('numeric','Harap memasukkan %s berupa angka');
			$this->form_validation->set_message('valid_url', 'Harap memasukkan %s valid');
		}

		function index(){
			$data['title']	 = "Daftar Identitas Pengisi";
			$data['bab'] = $this->bab_m->get_bab_1();	
			// $data['register'] = $this->bab_m->get_bab_daftar();
			$data['content_css'] = "";
			$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
			$data['content'] = $this->load->view('daftar_step1', $data, TRUE);

			$this->load->view('frame-daftar',$data);

		}

		function step1(){
			$data['title']	 = "Daftar Identitas Pengisi";
			$data['bab2']	= $this->pertanyaan_m->get_pertanyaan_bab2();
			$data['bab'] = $this->bab_m->get_bab_2();	
			$data['lembaga']		= $this->lembaga_m->get_all_file();
			$data['kementrian']		= $this->kementrian_m->get_all_file();
			$data['puslitbang']		= $this->puslitbang_m->get_all_file();
			// $data['register'] = $this->bab_m->get_bab_daftar();

			$data['content_css'] = "";
			$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
			$data['content'] = $this->load->view('daftar_step1', $data, TRUE);

			$this->load->view('frame-daftar',$data);
			
		}


		function step2(){
			$temp_id = $this->uri->segment(3);
			if ($temp_id){
				$data['temp_id'] = $temp_id;
			}
			$data['title']	 		= "Daftar Identitas Pengisi";
			// $data['bab2']			= $this->pertanyaan_m->get_pertanyaan_bab2();
			$data['bab'] 			= $this->bab_m->get_bab_1();		
			
			$data['content_css'] 	= "";
			$data['content_js'] 	= $this->load->view('daftar_js',$data, TRUE);
			$data['content'] 		= $this->load->view('daftar_step2', $data, TRUE);
			$this->load->view('frame-daftar',$data);
		}

		function step3(){
			$temp_id = $this->uri->segment(3);
			if ($temp_id){
				$data['temp_id'] = $temp_id;
			}
			$data['title']	 = "Daftar Identitas Pengisi";
			$data['bab'] = $this->bab_m->get_bab_12();		
			$data['content_css'] = "";
			$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
			$data['content'] = $this->load->view('daftar_step3', $data, TRUE);
			$this->load->view('frame-daftar',$data);
		}

		function simpan_step1(){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_8', 'Nama Kementrian', 'required');
			$this->form_validation->set_rules('q_9','Nama Lembaga','required');	
			$this->form_validation->set_rules('q_10','Nama Pusat/Balai','required');
			$this->form_validation->set_rules('q_66','Nama Kepala Pusat/Balai','required');
			$this->form_validation->set_rules('q_11','Alamat Pusat/Balai','required');
			$this->form_validation->set_rules('q_12', 'Nomor Telepon Pusat/Balai', 'numeric');
			$this->form_validation->set_rules('q_13', 'Alamat Website', 'required');
			$this->form_validation->set_rules('q_14', 'Nomor Fax Pusat/Balai', 'numeric');	
			
			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				
				$data['title']	 = "Daftar Identitas Pengisi";
				$data['bab'] = $this->bab_m->get_bab_2();
				$data['lembaga']		= $this->lembaga_m->get_all_file();
				$data['lembaga']		= $this->lembaga_m->get_all_file();
				$data['kementrian']		= $this->kementrian_m->get_all_file();
				$data['puslitbang']		= $this->puslitbang_m->get_all_file();
				$data['content_css'] = "";
				$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
				$data['content'] = $this->load->view('daftar_step2', $data, TRUE);
				$this->load->view('frame-daftar',$data);
			}
			else{
				/*$temp_id = $this->uri->segment(3);
				if ($temp_id){
					$data['temp_id'] = $temp_id;
				}*/
				//$data = $_POST;
				$q_8 = $this->input->post('q_8');
				$q_8_name = $this->kementrian_m->get_name_by_id($q_8);
				$q_8_save = $q_8_name[0]['nama_kementrian'];
			
				$data = array (
						"q_8" => $q_8_save,
						"q_9" => $this->input->post('q_9'),
						"q_10" => $this->input->post('q_10'),
						"q_66" => $this->input->post('q_66'),
						"q_11" => $this->input->post('q_11'),
						"q_12" => $this->input->post('q_12'),
						"q_13" => $this->input->post('q_13'),
						"q_14" => $this->input->post('q_14')
					);
				
				
				$insert = $this->mongo_db->insert('jawaban_temp', $data); 
				if ($insert){						
					$get_email = $this->jawaban_temp_m->get_email($email);
					foreach ($get_email as $data){
						$temp_id = $data['_id'];
						redirect('daftar/step2/'.$temp_id);
					}
					
				}
				else {
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('daftar/step1/'.$temp_id);
				}
			}
		}

		function simpan_step2(){			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_1','Nama Pengisi','required');	
			$this->form_validation->set_rules('q_2','Jabatan Pengisi','required');
			$this->form_validation->set_rules('q_3','NIP Pengisi','numeric|required');
			$this->form_validation->set_rules('q_4', 'Nomor Telepon Pengisi', 'numeric');
			$this->form_validation->set_rules('q_5', 'Nomor HP pengisi', 'numeric');
			$this->form_validation->set_rules('q_6', 'Nomor Fax pengisi', 'numeric');
			$this->form_validation->set_rules('q_7', 'Email', 'valid_email|required');
			// $this->form_validation->set_rules('q_8', 'Tangal Pengisian', 'required');
			
			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				//$data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);
            	$temp_id = $this->uri->segment(3);
				if ($temp_id){
					$data['temp_id'] = $temp_id;
				}
            	$data['title']	 = "Daftar Identitas Pengisi";
				$data['bab'] = $this->bab_m->get_bab_1();
				
				$data['content_css'] = "";
				$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
				$data['content'] = $this->load->view('daftar_step1', $data, TRUE);
				$this->load->view('frame-daftar',$data);
			}
			else{
				$temp_id = $this->uri->segment(3);
				if ($temp_id){
					$data['temp_id'] = $temp_id;
				}

				$email = $this->input->post('q_7');
				$cek_email = $this->jawaban_temp_m->get_email($email);
				if ($cek_email){
					//email sudah ada, kembali ke halaman awal pendaftaran
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Email yang anda gunakan sudah terdaftar, silakan masukkan email lain.</div>');
					redirect('daftar/step2/'.$temp_id);
				}
				else{
					//insert ke collection jawaban_temp
					$data = $_POST;
					$data = array(
							//"created_at" => new MongoDate(),
							"q_1" => $this->input->post('q_1'),
							"q_2" => $this->input->post('q_2'),
							"q_3" => $this->input->post('q_3'),
							"q_4" => $this->input->post('q_4'),
							"q_5" => $this->input->post('q_5'),
							"q_6" => $this->input->post('q_6'),
							"q_7" => $this->input->post('q_7')
						
						);

					$string_temp = (string)$temp_id;
			
					$update = $this->jawaban_temp_m->update_temp($string_temp, $data);
					if ($update){
						redirect('daftar/step3/'.$temp_id);
					}
					else{
						$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
						redirect('daftar/step2');
					}
					
				}
			}
		}

		


		function simpan_step3(){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_65','Konfirmasi Melakukan Kegiatan Litbang','required');	

			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');
			
			if ($this->form_validation->run() == FALSE){
				$temp_id = $this->uri->segment(3);
				if ($temp_id){
					$data['temp_id'] = $temp_id;
				}
				$data['title']	 = "Daftar Identitas Pengisi";
				$data['bab'] = $this->bab_m->get_bab_12();
				
				$data['content_css'] = "";
				$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
				$data['content'] = $this->load->view('daftar_step3', $data, TRUE);
				$this->load->view('frame-daftar',$data);
			}
			else{
				$data = $_POST;

				if($data['q_65'] == "Ya"){
					$temp_id = $this->uri->segment(3);				
					$string_temp = (string)$temp_id;
				
					$update = $this->jawaban_temp_m->update_temp($string_temp, $data);
					
					//insert ke collection user tapi password dikosongin dan status belum dikonfirmasi
					$data_temp = $this->jawaban_temp_m->get_temp($string_temp);
					foreach($data_temp as $data){}
					$fullname = $data['q_1'];
					$arr_fullname = explode(' ', trim($fullname));
					$username = $arr_fullname[0];
					$email =  $data['q_7'];
					$data_user = array(
						"joined_at" => new MongoDate(),
						"q_1"		=> $fullname,
						"q_2"		=> $data['q_2'],
						"q_3"		=> $data['q_3'],
						"q_4"		=> $data['q_4'],
						"q_5"		=> $data['q_5'],
						"q_6"		=> $data['q_6'],	
						"q_7"		=> $email,
						"q_8"		=> $data['q_8'],
						"q_9"		=> $data['q_9'],
						"q_10"		=> $data['q_10'],
						"q_11"		=> $data['q_11'],
						"q_12"		=> $data['q_12'],
						"q_13"		=> $data['q_13'],
						"q_14"		=> $data['q_14'],
						"username"	=> $username,
						"password"	=> "",
						"role"		=> "pegawai",
						"status"	=> "Belum dikonfirmasi"									
					);
					
					$insert_user = $this->mongo_db->insert('users', $data_user);

					//jika semua data update ke collection jawaban_temp
					if ($update){
						$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Terima Kasih, telah melakukan pendaftaran. Konfirmasi pendaftaran akan dikirimkan melalui Email.</div>');
						redirect('daftar/selesai');
					}
					else {
						$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Terjadi Kesalahan saat melakukan penyimpanan</div>');
						redirect('daftar/selesai');
					}
				}
				else{
					//echo "tidak ada";
					$temp_id = $this->uri->segment(3);
					if ($temp_id){
						$data['temp_id'] = $temp_id;
					}				
					$string_temp = (string)$temp_id;
				
					$update = $this->jawaban_temp_m->update_temp($string_temp, $data);
					if ($update){
						$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Satuan Kerja Anda Tidak melakukan kegiatan LITBANG, tidak diperlukan untuk mengisi kuesioner ini. Terima Kasih.</div>');
						redirect('daftar/selesai');
					}

					
				}				
			}

		}

		function selesai(){
			$data['title']	 = "Daftar Identitas Pengisi";
			$data['bab'] = $this->bab_m->get_bab_12();
			$data['content_css'] = "";
			$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
			$data['content'] = $this->load->view('daftar_selesai', $data, TRUE);
			$this->load->view('frame-daftar',$data);
		}

		function get_email(){
			$email = "rachmaamma@gmail.com";
			$get_email = $this->jawaban_temp_m->get_email($email);
			echo "<pre>";var_dump($get_email);

			foreach ($get_email as $data){
				echo $data['_id'];
			}
		}

		function get_lembaga_bykementrian($id_kementrian){
			$kelompok_lembaga= $this->lembaga_m->get_lembaga_by_id($id_kementrian);
			foreach ($kelompok_lembaga as $data){
				$value[$data['_id']] = $data['nama_lembaga'];
			}
			echo json_encode(array('kelompok_lembaga' => $value));
		}

		function get_puslitbang_bylembaga($id_lembaga){
			// $id_lembaga = "BPP_Kemendagri";
			$puslitbang_lembaga = $this->puslitbang_m->get_data_by_id($id_lembaga);

			foreach ($puslitbang_lembaga as $data){
				foreach ($data['puslitbang'] as $val){
					$value[$val] = $val;
				}
				
			}
			echo json_encode(array('puslitbang_lembaga' => $value));
		}


		function forgot_pass(){
			$data['title']	 = "Lupa Password";
			$data['content_css'] = "";
			$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
			$data['content'] = $this->load->view('daftar_forgot_pass', $data, TRUE);
			$this->load->view('frame-daftar',$data);
		}

		function find_account(){
			$email = $this->input->post('email');
			$get_user = $this->users_m->get_user($email);
			if (!$get_user){
				/*tidak ditemukan alamat email*/
				$this->session->set_flashdata('pesan',' <div class="alert alert-warning "><a class="close" data-dismiss="alert">×</a>Email yang Anda cari tidak ditemukan. Silakan ulangi lagi.</div>');
					redirect('daftar/forgot_pass');
			}
			else{
				/*ditemukan alamat email*/

				/*get _id users */
				$id = (string)$get_user[0]["_id"];
				$email = $get_user[0]['q_7'];
				$nama = $get_user[0]['q_1'];

				$this->confirm_forgot_pass($id,$email,$nama);		
						

			}
		}

		function confirm_forgot_pass($id,$email,$nama){
			$data['title']	 = "Konfirmasi Reset Password";
			$data['content_css'] = "";
			$data['id'] = $id;
			$data['email'] = $email;
			$data['nama'] = $nama;
			$data['content_js'] = $this->load->view('daftar_js',$data, TRUE);
			$data['content'] = $this->load->view('daftar_confirm_forgot_pass', $data, TRUE);
			$this->load->view('frame-daftar',$data);
		}

		function do_reset_pass(){
			$id = $this->uri->segment(3);
			/*generate password baru*/
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+,.?";
    			$password = substr( str_shuffle( $chars ), 0, 8 );
    			$email = $this->input->post('email');
				$nama = $this->input->post('nama');



    			/*inisiasi librari email*/
    			$this->load->library('email');

				$config['protocol']	='smtp';
				$config['smtp_host']='ssl://smtp.googlemail.com';
				$config['smtp_port']='465';
				$config['smtp_user']='rcahyaningdyah@gmail.com';
				$config['smtp_pass']='Rahadian010692';
				$config['charset'] = "iso-8859-1";
				$config['mailtype'] = "html";
				$config['newline'] = "\r\n";

				$this->email->initialize($config);

				$this->email->from('rcahyaningdyahgmail.com', 'Admin Sistem');
				$this->email->to($email); 
				$this->email->subject('Konfirmasi Reset Password Kuesioner Online Kemenristekdikti');
					$message="Hay $nama,<br><p>
							Reset Password Anda berhasil dilakukan. Untuk masuk ke sistem 
							silahkan login dengan, <br/>
							Username : ".$email."<br/>".
							"Password : ".$password."</p>";
							
				$this->email->message($message);

				if ($this->email->send()){
					$data_pass = array(
						"password" => sha1($password)
					);
					
					$update = $this->users_m->update_user($id, $data_pass);

										
					$this->session->set_flashdata('pesan',' <div class="alert alert-success "><a class="close" data-dismiss="alert">×</a>Reset Password berhasil dilakukan. Silakan cek Email untuk melakukan Login.</div>');
					redirect('daftar/forgot_pass');
				}
				else {
					$this->session->set_flashdata('pesan',' <div class="alert alert-warning "><a class="close" data-dismiss="alert">×</a>Terjadi Kesalahan pada sistem. Silakan ulangi beberapa saat lagi.</div>');
					redirect('daftar/forgot_pass');
				}	
		}

	}
?>
