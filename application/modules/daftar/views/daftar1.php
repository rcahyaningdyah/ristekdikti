 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Form Identitas</strong> Pengisi Kuesioner  </h2>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
         <?php echo validation_errors(); ?>

      
         <?php 
             $message = $this->session->flashdata('success');
              if(isset($message)) {
        ?>
              <div class="alert alert-success media fade in">
              <?php  echo $message; ?>
               </div>
            <?php  } ?>
       
                           
            <form class="form-horizontal" id="wizard_example" action="<?php echo site_url('daftar/cek') ?>" method="POST">
            <?php foreach ($register as $key) {
                        $id = $key['_id'];
                        $pertanyaan = $this->pertanyaan_m->get_pertanyaan($id);
                 ?>
                   <fieldset id="bab-1">
                    <legend><?php echo ucwords($key['topik'])?></legend>
                    <div class="row">
                        <div class="col-lg-6">
                              <?php foreach ($pertanyaan as $key=>$value) { ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                        <div class="col-sm-9">
                                            <?php if (count(array_filter($value['bentuk_form'])) == 0) {
                                            ?>
                                            <input type="<?php foreach($value['bentuk_form'] as $a=>$b){echo $a;} ?>" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placehorder="<?php echo $value['kalimat_tanya'] ?>"  >
                                            
                                            <?php  echo form_error($value['_id']) ?>
                                            <?php }else{ 
                                                foreach ($value['bentuk_form'] as $k=>$v) {?>
                                                    <select class="form-control" name="<?php echo $value['_id']?>" data-parsley-group="block0" data-search="true">
                                                        <?php  foreach ($v as $data){?>
                                                        <option value="<?php echo $data?>"><?php echo $data?></option>
                                                       <?php }?>
                                                     </select>
                                                 <?php } ?>  
                                            <?php }?>
                                        </div>
                                    </div>                                       
                                <?php } // end foreach pertanyaan ?>
                        </div>
                    </div>
                </fieldset><!--end Bab 1-->
               <?php }?>
            </form>
        </div>
    </div>
</div>

<script>
    var sfw;
    $(document).ready(function () {
        sfw = $("#wizard_example").stepFormWizard({});
    })
    $(window).load(function () {
        /* only if you want use mcustom scrollbar */
        $(".sf-wizard fieldset").mCustomScrollbar({
            theme: "dark-3",
            scrollButtons: {
                enable: true
            }
        });
        /* ***************************************/

        /* this function call can help with broken layout after loaded images or fonts */
        sfw.refresh();
    });
</script>   