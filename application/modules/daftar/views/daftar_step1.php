 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Form Identitas</strong> Pengisi Kuesioner  </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Daftar Identitas Pengisi</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
            <ul class="nav nav-tabs nav-primary">
                <li class="active" ><a href="<?php echo site_url('daftar') ?>" ><i class="icon-docs"></i>Asal Institusi</a></li>
                <li><a href="<?php echo site_url('daftar/step2') ?>" ><i class="icon-docs"></i>Identitas Pengisi</a></li>
                <li ><a href="<?php echo site_url('daftar/step3')?>" ><i class="icon-docs"></i>Konfirmasi Kegiatan Litbang</a></li>
            </ul>

          <div class="content">
                <div class="row">
                    <div class="col-md-6">
                         <br/>
                        <?php
                         $message = $this->session->flashdata('pesan_error');
                          if(isset($message))
                          {
                            echo $message;
                          } 
                        ?>
                        
                        <?php 
                              foreach ($bab as $data){
                                  $pertanyaan = $this->pertanyaan_m->get_pertanyaan($data["_id"]);

                                  $part1 = array_slice($pertanyaan, 0,5);
                                  $second = array_slice($part1, 0,1);
                                  $fourth = array_slice($part1, 1);

                                  $part2 = array_slice($pertanyaan, 5);
                                  $third = array_slice($part2, 0,1);
                                  $first1 = array_slice($part2, 1);
                                  
                        ?>
                              <form action="<?php echo site_url('daftar/simpan_step1') ?>" method="POST">
                                  <div class="form-horizontal ">
                                      <?php
                                          foreach ($first1 as $key=>$value){
                                           // echo $value['_id'];
                                      ?>
                                            <div class="form-group">
                                               <label class="col-md-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                               <div class="col-sm-9">
                                                    <?php 
                                                        foreach ($value['bentuk_form'] as $k => $v){
                                                           if (empty($v)){   
                                                            //get data dari collection lain
                                                              if ($value['_id'] == "q_8"){    
                                                              // get dari collection kementrian   
                                                    ?>
                                                                <!-- select dari kementrian -->
                                                              <select class="form-control" name="q_8" id="q_8" data-parsley-group="block0"   data-search="true"> 
                                                                   <option  value="<?php echo set_select($value['_id'])?>"> <?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Kementrian";
                                                                    } ?></option>
                                                                  <?php foreach ($kementrian as $data){
                                                                   ?>
                                                                      <option id="<?php echo $data['_id'] ?>" value="<?php echo $data['_id'] ?>"> <?php echo $data['nama_kementrian'] ?> </option>
                                                                  <?php      
                                                                          } 
                                                                  ?>
                                                              </select>
                                                    <?php 
                                                            } //end if q_8
                                                            else {
                                                              // get dari collection lembaga
                                                  ?>    
                                                                <!-- select dari lembaga -->
                                                                 <select class="form-control" name="q_9" id="q_9" data-parsley-group="block0"  data-search="true"> 
                                                             
                                                                     <option  value="<?php echo set_select($value['_id'])?>"> <?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Lembaga atau Badan";
                                                                    } ?></option>
                                                               
                                                            </select>

                                                    <?php 
                                                                } // en else q_9   
                                                            } //end if value $v
                                                    ?>          
                                                    <?php 
                                                           
                                                        } // end foreach bentuk_form 
                                                      ?>
                                                       
                                               </div>
                                            </div>  
                                      <?php
                                          } // end foreach first1
                                      ?>

                                      <?php 
                                          foreach ($second as $key=>$value){
                                      ?>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                                <div class="col-md-9">
                                                      <?php 
                                                          foreach ($value['bentuk_form'] as $k=>$v) {
                                                            if(empty($v)){
                                                      ?>
                                                              <!-- dropdown select pusat/upt -->
                                                            <select class="form-control" name="q_10" id="q_10" data-parsley-group="block0"  data-search="true">                                                              
                                                                    <option  value="<?php echo set_select($value['_id'])?>"> <?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Puslitbang/Balai/UPT";
                                                                    } ?></option>                                                           
                                                            </select>
                                                      <?php
                                                            } //end if empty $v
                                                         }// end foreach bentuk form
                                                      ?>
                                                       
                                                </div>
                                            </div>
                                      <?php } // end foreach second?>


                                      <?php foreach ($third as $key=>$value) { 
                                         // echo $value['_id'];
                                      ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                            <div class="col-sm-9">
                                       <?php foreach ($value['bentuk_form'] as $a=>$b) {
                                            if ($a == "text"){?>
                                                <input type="text" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>">      
                                            <?php }
                                            elseif ($a=="textarea"){?>
                                                <textarea id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>"></textarea>
                                            <?php }
                                            elseif ($a=="date"){ ?>
                                                <input type="date" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>">
                                            <?php }
                                            else{
                                                foreach ($value['bentuk_form'] as $k=>$v) {
                                                  if (empty($v)){
                                            ?>  
                                                    <!-- dropdown select pusat/upt -->
                                                       <select class="form-control" name="q_10" id="q_10" data-parsley-group="block0"  data-search="true"> 
                                                             
                                                                     <option  value="<?php echo set_select($value['_id'])?>"> <?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Puslitbang/Balai/UPT";
                                                                    } ?></option>
                                                               
                                                            </select>

                                                <?php 
                                                  } // end if empty
                                                  else {
                                                ?>
                                                         <select class="form-control" name="<?php echo $value['_id']?>" data-parsley-group="block0" data-search="true">
                                                            <option value="<?php echo set_select($value['_id'])?>">Pilih Salah satu</option>
                                                            <?php  foreach ($v as $data){?>
                                                                <option value="<?php echo $data?>"><?php echo $data?></option>
                                                            <?php }?>
                                                         </select>

                                                <?php
                                                  } // end else empty
                                                 ?>
                                                 <?php } // end foreach bentuk_form ?> 
                                            <?php } // end else bentuk_form ?>
                                           
                                        <?php }?>
                                            </div>
                                        </div>                                       
                                    <?php } // end foreach third ?>


                                    <?php foreach ($fourth as $key=>$value) { 
                                          //echo $value['_id'];
                                      ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                            <div class="col-sm-9">
                                       <?php foreach ($value['bentuk_form'] as $a=>$b) {
                                            if ($a == "text"){?>
                                                <input type="text" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>">      
                                            <?php }
                                            elseif ($a=="textarea"){?>
                                                <textarea id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>"></textarea>
                                            <?php }
                                            elseif ($a=="date"){ ?>
                                                <input type="date" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>">
                                            <?php }
                                            else{
                                                foreach ($value['bentuk_form'] as $k=>$v) {
                                                  if (empty($v)){
                                            ?>  
                                                    <!-- dropdown select pusat/upt -->
                                                       <select class="form-control" name="q_10" id="q_10" data-parsley-group="block0"  data-search="true"> 
                                                             
                                                                    <option  value="<?php echo set_select($value['_id'])?>"> <?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Puslitbang/Balai/UPT";
                                                                    } ?></option>
                                                               
                                                            </select>

                                                <?php 
                                                  } // end if empty
                                                  else {
                                                ?>
                                                         <select class="form-control" name="<?php echo $value['_id']?>" data-parsley-group="block0" data-search="true">
                                                            <option value="<?php echo set_select($value['_id'])?>">Pilih Salah satu</option>
                                                            <?php  foreach ($v as $data){?>
                                                                <option value="<?php echo $data?>"><?php echo $data?></option>
                                                            <?php }?>
                                                         </select>

                                                <?php
                                                  } // end else empty
                                                 ?>
                                                 <?php } // end foreach bentuk_form ?> 
                                            <?php } // end else bentuk_form ?>
                                            
                                        <?php }?>
                                            </div>
                                        </div>                                       
                                    <?php } // end foreach fourth ?>
                                      <?php  echo form_error($value['_id']) ?>
                                      <div class="form-group " style="margin-left: 258px;">
                                            <button type="submit"   class="btn btn-primary">Simpan</button>
                                      </div>  
                                      <div class="form-group ">
                                           <button  type="button" onclick="goBack()" class="btn btn-success btn-embossed"><< Kembali</button>
                                            <button type="button" onclick="javascript:history.forward();" class="btn btn-success pull-right">Berikutnya >></button>
                                      </div>
                                  </div>
                              </form>
                        <?php
                              } // end foreach bab
                        ?>
                    </div>
                   
                </div>                
          </div>               
        </div>


        </div>
    </div>
</div>
<script type="text/javascript">
function goBack() {
    window.history.back();
}

$(document).ready(function(){
       
        $("#q_8").change(function()
        {
           var kementrian_id = $('#q_8').val();
           $("#q_9").html('<option value=""> Pilih Lembaga </option>');
           $("select#q_9").val("").prop('selected', true);
           if (kementrian_id != ""){
                var post_url = "<?php echo site_url('daftar/daftar/get_lembaga_bykementrian') ?>/" + kementrian_id;
                $.ajax({
                    type : "POST",
                    url : post_url,
                    dataType: 'json',
                    success : function(data)
                    {
                        str = '<option value=""> Pilih Lembaga </option>';
                        // console.log(data);
                        $.each(data.kelompok_lembaga, function(keyx, valx){
                            // console.log(keyx+' '+valx);
                            str += '<option value="'+keyx+'">'+valx+'</option>'
                        });
                        // console.log(str);
                        $("#q_9").html(str);

                    } // end success
                }); // end ajax
           }

        }); // end change  q_9

       $("#q_9").change(function(){
            var kelompok = $('#q_9').val();
            $("#q_10").html('<option value=""> Pilih Puslitbang </option>');
            $("select#q_10").val("").prop('selected', true);
            if (kelompok != ""){
                var post_url = "<?php echo site_url('daftar/daftar/get_puslitbang_bylembaga') ?>/" + kelompok;
                $.ajax({
                    type : "POST",
                    url : post_url,
                    dataType: 'json',
                    success : function(data)
                    {
                        str = '<option value=""> Pilih Puslitbang </option>';
                         console.log(data);
                         $("#q_10").empty();
                        $.each(data.puslitbang_lembaga, function(keyx, valx){
                            // console.log(keyx+' '+valx);
                            str += '<option value="'+keyx+'">'+valx+'</option>'
                        });
                        // console.log(str);
                        $("#q_10").html(str);

                    } // end success
                }); // end ajax
            }
        }); // end change q_31
    });
</script>