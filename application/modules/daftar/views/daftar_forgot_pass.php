 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Lupa Password</strong>   </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Lupa Password</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Temukan Akun Anda</strong> </h3>
        </div>

        <div class="panel-content">
        	<div class="row">
        		 <div class="col-md-6 ">
        		 		<?php
		                   $message = $this->session->flashdata('pesan');
		                    if(isset($message))
		                    {
		                      echo $message;
		                    } 
		                  ?>
        		 		<form action="<?php echo site_url('daftar/find_account') ?>" method="POST">
                                 <div class="form-horizontal ">
                                 	<div class="form-group">
                                            <label class="col-md-3 control-label"> Masukkan Alamat Email Anda</label>
                                            <div class="col-sm-9">
                                            	<input type="text" class="form-control col-sm-9" name="email" autocomplete="off" placeholder="Masukkan Alamat Email Anda">
                                            </div>
                                    </div>

                                    <div class="form-group " style="margin-left: 258px;">
                                         <button type="submit"   class="btn btn-primary">Cari </button>
                                    </div>
                                 </div>
                         </form>
        		 </div>
        	</div>
        </div>
    </div>
</div>