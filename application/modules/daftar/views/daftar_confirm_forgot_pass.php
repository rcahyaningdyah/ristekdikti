 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Lupa Password</strong>   </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Lupa Password</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Konfirmasi Reset Password</strong> </h3>
        </div>

        <div class="panel-content">
        	<div class="row">
        		 <div class="col-md-6 ">
        		 		<?php
		                   $message = $this->session->flashdata('pesan');
		                    if(isset($message))
		                    {
		                      echo $message;
		                    } 
		                  ?>
                      <p class="strong"> Anda akan melanjutkan melakukan Reset Password </p>
          		 		<form action="<?php echo site_url('daftar/do_reset_pass/'.$id) ?>" method="POST">
                         <div class="form-horizontal ">
                         	<div class="form-group">
                                <label class="col-md-3 control-label">Alamat Email Anda</label>
                                <div class="col-sm-9">
                                	<input type="text" class="form-control col-sm-9" readonly name="email" autocomplete="off" value="<?php echo $email ?>">
                                  <input type="hidden" name="email" value="<?php echo $email ?>" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama Akun Anda</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control col-sm-9" readonly name="nama" autocomplete="off" value="<?php echo $nama ?>">
                                   <input type="hidden" name="nama" value="<?php echo $nama ?>" >
                                </div>
                            </div>

                            <div class="form-group " style="margin-left: 258px;">
                                 <button type="submit"   class="btn btn-primary">Lanjut </button>
                                  <a href="<?php echo site_url('daftar/forgot_pass') ?>" class="btn btn-danger">Batal </a>
                            </div>
                            
                         </div>
                 </form>
        		 </div>
        	</div>
        </div>
    </div>
</div>