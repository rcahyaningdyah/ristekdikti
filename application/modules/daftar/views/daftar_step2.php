 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Form Identitas</strong> Pengisi Kuesioner  </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Daftar Identitas Pengisi</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
            <ul class="nav nav-tabs nav-primary ">
                <li class="active" ><a href="#" ><i class="icon-docs"></i>Asal Institusi</a></li>
                <li class="active"><a href="#"><i class="icon-docs"></i>Identitas Pengisi</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Konfirmasi Kegiatan Litbang</a></li>
            </ul>

            <div class="content">
                <div class="row">
                    <div class="col-md-6 ">
                         <br/>
                        <?php
                         $message = $this->session->flashdata('pesan_error');
                          if(isset($message))
                          {
                            echo $message;
                          } 
                        ?>
                       
                        <?php 
                            foreach ($bab as $data){
                                $pertanyaan = $this->pertanyaan_m->get_pertanyaan($data["_id"]);
                        ?>
                             <?php //echo validation_errors(); ?>

                            <form action="<?php echo site_url('daftar/simpan_step2/'.$temp_id) ?>" method="POST">
                                 <div class="form-horizontal ">
                                    <?php foreach ($pertanyaan as $key=>$value) { ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                            <div class="col-sm-9">
                                                <?php if (count(array_filter($value['bentuk_form'])) == 0) {
                                                ?>
                                                <input type="<?php foreach($value['bentuk_form'] as $a=>$b){echo $a;} ?>" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>"  >
                                               
                                                <?php  echo form_error($value['_id']) ?>
                                                <?php }else{ 
                                                    foreach ($value['bentuk_form'] as $k=>$v) {?>
                                                        <select class="form-control" name="<?php echo $value['_id']?>" data-parsley-group="block0" data-search="true">
                                                            <?php  foreach ($v as $data){?>
                                                            <option value="<?php echo $data?>"><?php echo $data?></option>
                                                           <?php }?>
                                                         </select>
                                                     <?php } ?>  
                                                <?php }?>
                                            </div>
                                        </div>                                       
                                    <?php } // end foreach pertanyaan
                                        //onclick="javascript:history.forward(); untuk next selanjuutnyaaa
                                     ?>
                                      <br/>
                                        <div class="form-group " style="margin-left: 258px;">
                                             <button type="submit"   class="btn btn-primary">Simpan</button>
                                        </div> 
                                        <div class="form-group"> 
                                             <button type="button" onclick="javascript:history.forward();" class="btn btn-success btn-embossed pull-right">Berikutnya >></button>
                                        </div>                                     
                                </div> 
                            </form>                               
                        <?php
                            } // end foreach bab 
                         ?>
                    </div>
                   
                </div>                
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
function goBack() {
    window.history.back();
}
</script>