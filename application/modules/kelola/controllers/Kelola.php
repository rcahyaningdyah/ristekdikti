<?php
	class Kelola extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session','email'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','users_m'));
			if ($this->session->userdata('role')!= 'admin'){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Tata Kelola";
			$data['jawaban_temp'] 	= $this->users_m->get_data_konfirmasi();
			$id_lembaga = $this->session->userdata('lembaga');
			$data['user'] = $this->users_m->get_pegawai_by_admin($id_lembaga);
			$data['content'] = $this->load->view('home_kelola', $data, TRUE);
			$this->load->view('frame-template-admin',$data);
		}

		function send_email(){
			$id = $this->uri->segment(3);
		
			$string_id = (string)$id;
			$data_user = $this->users_m->get_user_byId($string_id);
			//var_dump($data_user); exit;

			foreach ($data_user as $data){
				$nama = $data['q_1'];
				$email = $data['q_7'];				
			}

			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+,.?";
    		$password = substr( str_shuffle( $chars ), 0, 8 );
    		
    		$this->load->library('email');

			$config['protocol']	='smtp';
			$config['smtp_host']='ssl://smtp.googlemail.com';
			$config['smtp_port']='465';
			$config['smtp_user']='rcahyaningdyah@gmail.com';
			$config['smtp_pass']='Rahadian010692';
			$config['charset'] = "iso-8859-1";
			$config['mailtype'] = "html";
			$config['newline'] = "\r\n";

			$this->email->initialize($config);

			$this->email->from('rcahyaningdyahgmail.com', 'Admin Sistem');
			$this->email->to($email); 
			$this->email->subject('Konfirmasi Pendaftaran Kuesioner Online Kemenristekdikti');
				$message="Hay $nama,<br><p>
						Terimakasih sudah melakukan pendaftaran Sistem, . Untuk masuk ke sistem
						silahkan login dengan 
						Username : ".$email."<br/>".
						"Password : ".$password."</p>";
						
			$this->email->message($message);

			if ($this->email->send()){
				//get data temp dari jawaban_temp berdasar temp_id
				$string_id = (string)$id;
				$data_temp = $this->jawaban_temp_m->get_temp($string_id);
				foreach ($data_temp as $data){}

				//insert ke collection users ====> diubah jadi update
				$fullname = $data['q_1'];
				$arr_fullname = explode(' ', trim($fullname));
				$username = $arr_fullname[0];
				$email =  $data['q_7'];
				$data_user = array(
					"username"      => $username,
					"password"		=> sha1($password),
					"status"		=> "Sudah Dikonfirmasi"
					);

				//get users_id dari collection users
				$get_user = $this->users_m->get_user($email);
				
				foreach ($get_user as $user_data){}
				$user_id = $user_data['_id']; 

				$update_user = $this->users_m->update_user($user_id, $data_user);

				//hapus data di collection jawaban_temp karena sudah dikonfirmasi

				
				if ($update_user){
					$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Konfirmasi berhasil dilakukan.</div>');
					redirect('kelola');
				}
				else{
					echo "gagal";
				}

				//echo "sukses";
			}
			else {
				//echo "gagal";
				$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
				redirect('kelola');
			}	
		}


		function tolak_konfirmasi(){
			$user_id = $this->uri->segment(3);
			
			//merubah status di collection user menjadi tidak di konfirmasi
			$data = array(
					"status" => "Tidak Dikonfirmasi"
				);
			$tolak_konfirmasi = $this->users_m->update_tolak_konfirmasi($user_id, $data);

			if ($tolak_konfirmasi){
				echo "sukses di tolak";
			}
			else{
				echo "gagal";
			}
		}


		function coba(){
			$string_id = "56665b86c4365bdc19000029";
			$hapus = $this->jawaban_temp_m->delete_by_id($string_id);
			var_dump($hapus);
			if ($hapus){
				echo "berhasil";
			}
			else{
				echo "gagal";
			}
			exit;

			$tes = array();

			$data = array (
				"nama" => "Rachma",
				"email"	=> "rcahyaningdyah@gmail.com"
				);
			$a = array_fill(0, 2, $data);

			

			$array = new SplFixedArray(5);

			$array[] = $data;

			var_dump($array);
		}



		function cek(){
			$data['title']	 = "Tata Kelola";
			$data['kuesioner'] 	= $this->kuesioner_m->get_all_file();
			$data['bab'] 	    = $this->bab_m->get_all_file();
			$data['pertanyaan']	= $this->pertanyaan_m->get_all_file();
			$data['content'] = $this->load->view('tes', $data, TRUE);
			$this->load->view('frame-template-admin',$data);
		}

		function detail_kuesioner(){
			$id_kuesioner = $this->uri->segment(3);
			$data['title']	 = "Detail Kuesioner";
			$data['kuesioner'] 	= $this->kuesioner_m->get_all_file();
			$data['bab'] 	    = $this->bab_m->get_all_file();
			$data['pertanyaan']	= $this->pertanyaan_m->get_all_file();
			$data['content'] = $this->load->view('detail_kuesioner', $data, TRUE);
			$this->load->view('frame-template-admin',$data);
		}

		function get_pertanyaan(){
			
			$id_bab = $this->input->post('select');
			$arr = $this->pertanyaan_m->get_pertanyaan($id_bab);
			echo json_encode($arr);
		}

		function tanya(){
			$id = "bab-6";
			$arr = $this->pertanyaan_m->get_pertanyaan($id);
			// echo "<pre>"; var_dump($arr); 
			foreach ($arr as $key=>$data){
				foreach ($data as $key=>$value){

					if (is_array($value)){
						echo $key."<br/>";
						foreach ($value as $k=>$val){

							echo $k."<br/>";
							if (is_array($val)){
								foreach ($val as $k=>$v){
									echo $v." ";
									echo "<br/>";
								}
							}
						}
					}
					else{
						echo $key.":".$value."<br/>";
					}
				}
				echo "<br/>";
				//echo "<pre>"; var_dump($data);
			}
			
		}


		function add_kuesioner(){

			$this->form_validation->set_rules('created_by', 'Name', 'trim|xss_clean|callback_alpha_space_only');
	        $this->form_validation->set_rules('created_at', 'Emaid ID', 'trim|valid_email');
	        $this->form_validation->set_rules('masa_berlaku_awal', 'Subject', 'trim|xss_clean');
	        $this->form_validation->set_rules('masa_berlaku_akhir', 'Message', 'trim|xss_clean');
	        $this->form_validation->set_rules('target', 'Message', 'trim|xss_clean');
	  			
			$data_kuesioner = array(
					'_id' => $this->input->post('kuesioner_id'),
					'created_by' => $this->input->post('created_by') ,
					'created_at' => $this->input->post('created_at'),
					'masa_berlaku_awal' => $this->input->post('masa_berlaku_awal'),
					'masa_berlaku_akhir' => $this->input->post('masa_berlaku_akhir'),
					'untuk' => $this->input->post('untuk')
					 );
			
			$this->mongo_db->insert('kuesioner', $data_kuesioner); 
			redirect('Kelola');
		}

		function add_pertanyaan(){

			$this->form_validation->set_rules('bentuk_form', 'form', 'trim|xss_clean|callback_alpha_space_only');
	        $this->form_validation->set_rules('kalimat_tanya', 'tanya', 'trim|valid_email');
	        $this->form_validation->set_rules('wajib', 'wajib', 'trim|xss_clean');
	  
			$data_pertanyaan = array(
					// '_id' => getNextSequence('kuesioner_id'),
					'bentuk_form' => $this->input->post('bentuk_form') ,
					'kalimat_tanya' => $this->input->post('kalimat_tanya'),
					'wajib' => $this->input->post('wajib')
					 );
			
			$this->mongo_db->insert('pertanyaan', $data_pertanyaan); 
			redirect('Kelola');
		}

		function add_bab(){
			$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|xss_clean|callback_alpha_space_only');
	        $this->form_validation->set_rules('topik', 'topik', 'trim|valid_email');
	  
			$data_bab = array(
					// '_id' => getNextSequence('kuesioner_id'),
					'deskripsi' => $this->input->post('deskripsi') ,
					'topik' => $this->input->post('topik')
					 );
			
			$this->mongo_db->insert('bab', $data_bab); 
			redirect('Kelola');
		}

		
	}
?>