<?php 
	class Belanja extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session','email'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','users_m','lembaga_m','belanja_lembaga_m'));
			$this->form_validation->set_message('required', ' %s harap di isi.');
			$this->form_validation->set_message('numeric','Harap memasukkan %s berupa angka');
			if ($this->session->userdata('role')!= 'admin'){
	            redirect ('login');
	        }
		}

		
		function index(){
			//cek id lembaga dan user id
			$belanja = $this->belanja_lembaga_m->cek_data_lembaga();
			
			if ($belanja){

				//halaman lihat belanja
				foreach ($belanja as $data){
					$data_belanja = array (
						"q_35" => $data['q_35'],
						"q_36" => $data['q_36'],
						"q_37" => $data['q_37'],
						"q_38" => $data['q_38']
						);
				}
				$data['title']	 = "Kelola Belanja Litbang";			
				$data['data_belanja'] = $data_belanja;
				$data['content'] = $this->load->view('view_belanja', $data, TRUE);
				$this->load->view('frame-template-admin',$data);
			}
			else {
				//belum pernah insert
				$this->isi_belanja();
			}

		}

		function isi_belanja(){
			//isi data belanja
			$data['title']	 = "Kelola Belanja Litbang";
			$data['bab'] 	 = $this->bab_m->get_bab_5();
			$this->session->set_flashdata('pesan_success',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Belanja Litbang belum di isi, silakan isi formulir.</div>');
			$data['content'] = $this->load->view('home_belanja', $data, TRUE);
			$this->load->view('frame-template-admin',$data);
		}

		function simpan_belanja(){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_35','Gaji/Upah + Tunjangan Peneliti','numeric|required');	
			$this->form_validation->set_rules('q_36','Gaji/Upah + Tinjangan Staf Pendukung','numeric|required');
			$this->form_validation->set_rules('q_37','Belanja Modal','numeric|required');
			$this->form_validation->set_rules('q_38', 'Belanja DIPA', 'numeric|required');

			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){				

				$data['title']	 = "Kelola Belanja Litbang";
				//$data['data_belanja'] = $data_belanja;
				$data['bab'] 	 = $this->bab_m->get_bab_5();
				$data['content'] = $this->load->view('home_belanja', $data, TRUE);
				$this->load->view('frame-template-admin',$data);

			}
			else{
				$q_35 = array();  $q_36 = array();  $q_37 = array();  $q_38 = array();  
				$q_35[] = $this->input->post('q_35');
				$q_36[] = $this->input->post('q_36');
				$q_37[] = $this->input->post('q_37');
				$q_38[] = $this->input->post('q_38');
				
				$data_belanja = array(
					"id_user" => $this->session->userdata('user_id'),
					"q_9"  => $this->session->userdata('lembaga'),
					"q_35" => array_map('doubleval',$q_35),
					"q_36" => array_map('doubleval',$q_36),
					"q_37" => array_map('doubleval',$q_37),
					"q_38" => array_map('doubleval',$q_38)
					);


				$cek = $this->belanja_lembaga_m->cek_data_lembaga();
				if (!$cek){
					//insert di collection belanja lembaga
					$this->mongo_db->insert('belanja_lembaga', $data_belanja); 

					//cek di collection jawaban
					$lembaga = $this->session->userdata('lembaga');
					$cek_jawaban = $this->jawaban_m->get_kegiatan_by_lembaga($lembaga);

					if ($cek_jawaban){
						foreach ($cek_jawaban as $data){
							$data_jawaban = array(
									"q_35" => array_map('doubleval',$q_35),
									"q_36" => array_map('doubleval',$q_36),
									"q_37" => array_map('doubleval',$q_37),
									"q_38" => array_map('doubleval',$q_38)
								);
								$id_jawaban = $data['_id'];
								$string_id = (string)$id_jawaban; 
								
								//update data jawaban
								$update = $this->jawaban_m->update_jawaban($string_id, $data_jawaban);

						}
						//redirect ke halaman view 
						$this->session->set_flashdata('pesan_success',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Belanja Litbang berhasil ditambahkan.</div>');	
						redirect('kelola/belanja');
					}
					else{
						echo "belum ada di collection jawaban";
					}

				}
				else{
					//data sudah ada di collection belanja lembaga 
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>Data Belanja Litbang sudah ada.</div>');	
					redirect('kelola/belanja');
				}
			}
		}


		function edit_belanja(){
			//get data belanja lembaga dari collection belanja lembaga
			$get_belanja = $this->belanja_lembaga_m->cek_data_lembaga();

			//halaman edit belanja
			$data['title']	 = "Kelola Belanja Litbang";
			$data['bab'] 	 = $this->bab_m->get_bab_5();
			$data['data_belanja'] = $get_belanja;
			$data['content'] = $this->load->view('edit_belanja', $data, TRUE);
			$this->load->view('frame-template-admin',$data);
		}

		function update_belanja(){
			//get id_belanja
			$id_belanja = $this->uri->segment('4');
			$string_id = (string) $id_belanja;

			$q_35 = array();  $q_36 = array();  $q_37 = array();  $q_38 = array();  
			$q_35[] = $this->input->post('q_35');
			$q_36[] = $this->input->post('q_36');
			$q_37[] = $this->input->post('q_37');
			$q_38[] = $this->input->post('q_38');
			
			$data_belanja = array(
				"id_user" => $this->session->userdata('user_id'),
				"q_9"  => $this->session->userdata('lembaga'),
				"q_35" => array_map('doubleval',$q_35),
				"q_36" => array_map('doubleval',$q_36),
				"q_37" => array_map('doubleval',$q_37),
				"q_38" => array_map('doubleval',$q_38)
				);

			$update = $this->belanja_lembaga_m->update_lembaga($string_id, $data_belanja);

			if ($update){
				//jika update berhasil, maka update di collection jawaban
				//cek di collection jawaban
				$lembaga = $this->session->userdata('lembaga');
				$cek_jawaban = $this->jawaban_m->get_kegiatan_by_lembaga($lembaga);

				if ($cek_jawaban){
					foreach ($cek_jawaban as $data){
						$data_jawaban = array(
								"q_35" => array_map('doubleval',$q_35),
								"q_36" => array_map('doubleval',$q_36),
								"q_37" => array_map('doubleval',$q_37),
								"q_38" => array_map('doubleval',$q_38)
							);
							$id_jawaban = $data['_id'];
							$string_id = (string)$id_jawaban; 
							
							//update data jawaban
							$update = $this->jawaban_m->update_jawaban($string_id, $data_jawaban);

					}
					//redirect ke halaman view 
					$this->session->set_flashdata('pesan_success',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Perubahan data Belanja Litbang berhasil disimpan.</div>');	
					redirect('kelola/belanja');
				}
				else{
					echo "belum ada di collection jawaban";
				}
			}
			else{
				//jika update tidak berhasil
				$this->session->set_flashdata('pesan_error',' <div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>Terjadi kesalahan dalam menyimpan data.</div>');	
				redirect('kelola/belanja');
			}

		}


		function edit_belanja1(){
			//get id lembaga frame-template-admin
			$lembaga_id = $this->session->userdata['lembaga'];
			
			//get lembaga yang sama di collection users
			$get_lembaga = $this->users_m->get_data_by_lembaga($lembaga_id);

			foreach ($get_lembaga as $data){
				if ($data['role'] == "pegawai"){
					$pegawai_id = $data['_id'];
					$data_jawaban = $this->jawaban_m->get_data_by_userid($pegawai_id);
					if ($data_jawaban){
						$data_belanja = array (
							"q_35" => $data_jawaban[0]['q_35'],
							"q_36" => $data_jawaban[0]['q_36'],
							"q_37" => $data_jawaban[0]['q_37'],
							"q_38" => $data_jawaban[0]['q_38']
						);
					}

				}			
			}
			
			if (empty($data_belanja)){
				//isi data belanja
				$data['title']	 = "Kelola Belanja Litbang";
				$data['bab'] 	 = $this->bab_m->get_bab_5();	
				$this->session->set_flashdata('pesan_success',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Belanja Litbang belum di isi, silakan isi formulir.</div>');	
				$data['content'] = $this->load->view('home_belanja', $data, TRUE);
				$this->load->view('frame-template-admin',$data);
			}
			else {
				//halaman edit belanja
				$data['title']	 = "Kelola Belanja Litbang";
				$data['bab'] 	 = $this->bab_m->get_bab_5();
				$data['data_belanja'] = $data_belanja;
				$data['content'] = $this->load->view('edit_belanja', $data, TRUE);
				$this->load->view('frame-template-admin',$data);
			}
		}




	}

?>