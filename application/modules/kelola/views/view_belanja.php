 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Belanja Litbang</strong>   </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Belanja Litbang</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->
<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3> Data Belanja Litbang </h3>
        </div>

        <div class="panel-content">
          <div class="col-md-9">
              <?php
                 $message = $this->session->flashdata('pesan_error');
                  if(isset($message))
                  {
                    echo $message;
                  } 
                ?>
              <?php
                 $message = $this->session->flashdata('pesan_success');
                  if(isset($message))
                  {
                    echo $message;
                  } 
              ?>

              <?php //echo $this->session->userdata('lembaga'); 
                    //echo "<br>"; echo $this->session->userdata('user_id');
                     ?>
             <table class=" table table-striped ">
                  <thead>
                      <tr>
                          <td> <strong> No </strong></td>
                          <td> <strong> Jenis Belanja </strong> </td>
                          <td> <strong> Jumlah Dana (Dalam Rupiah) </strong></td>
                      </tr>
                  </thead>

                  <tbody>
                     <?php 
                          $no = $this->uri->segment(4)+1 ; 
                       
                          foreach ($data_belanja as $key=> $value){

                      ?>
                          <tr>
                              <td>  <?php echo $no; $no = $no+1;?></td>
                              <td>
                                    <?php                                       
                                      $pertanyaan = $this->pertanyaan_m->get_kalimat_tanya($key);
                                      foreach ($pertanyaan as $data){
                                        echo $data['kalimat_tanya'];
                                      }
                                    ?>
                              </td>
                              <td> 
                                  <?php 
                                    foreach ($value as $arr){
                                        
                                        echo "Rp " . number_format( $arr , 2 , ',' , '.' ).",-"; 
                                    } 
                                  ?>                                
                              </td>
                          </tr>
                      <?php
                          } // end foreach
                      ?>
                  </tbody>
              </table>
             
              <a href="<?php echo site_url('kelola/belanja/edit_belanja') ?>" class="btn btn-primary"><i class="fa fa-edit"></i>Edit </a>
          </div>
           
        </div>
     </div>
</div>