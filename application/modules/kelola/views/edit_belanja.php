<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Belanja Litbang</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Belanja Litbang </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-basket-loaded"></i> <strong>Edit Belanja Litbang </strong> </h3>
        </div>

        <div class="panel-content">
        	<div class="col-md-9">
        		<?php
	               $message = $this->session->flashdata('pesan_error');
	                if(isset($message))
	                {
	                  echo $message;
	                } 
	              ?>
	            <?php
	               $message = $this->session->flashdata('pesan_success');
	                if(isset($message))
	                {
	                  echo $message;
	                } 
	              ?>
	       		<?php
	       			foreach ($data_belanja as $data){
	       				  // $pertanyaan = $this->pertanyaan_m->get_pertanyaan($data["_id"]);
	       				 
	       		?>
	       		     	   <form action="<?php echo site_url('kelola/belanja/update_belanja/'.$data['_id']) ?>" method="POST">
							     <div class="form-horizontal ">
							    	<div class="form-group">
							    		<label class="col-md-3 control-label">Gaji/Upah + Tunjangan Peneliti</label>
							    		<div class="col-sm-9">
							    			<input type="text" class="form-control " name="q_35" value="<?php foreach ($data['q_35'] as $val) {echo $val;} ?>">
							    		</div>	                        		
							    	</div>    

							    	<div class="form-group">
							    		<label class="col-md-3 control-label">Gaji/Upah + Tunjangan Teknisi dan Staf Pendukung</label>
							    		<div class="col-sm-9">
							    			<input type="text" class="form-control " name="q_36" value="<?php foreach ($data['q_36'] as $val) {echo $val;} ?>">
							    		</div>	                        		
							    	</div>   

							    	<div class="form-group">
							    		<label class="col-md-3 control-label">Belanja Modal (tanah, gedung dan bangunan, kendaraan, mesin, dan peralatan)</label>
							    		<div class="col-sm-9">
							    			<input type="text" class="form-control " name="q_37" value="<?php foreach ($data['q_37'] as $val) {echo $val;} ?>">
							    		</div>	                        		
							    	</div>

							    	<div class="form-group">
							    		<label class="col-md-3 control-label">Belanja sumber DIPA</label>
							    		<div class="col-sm-9">
							    			<input type="text" class="form-control " name="q_38" value="<?php foreach ($data['q_38'] as $val) {echo $val;} ?>">
							    		</div>	                        		
							    	</div> 

							    	<div class="form-group " style="margin-left: 278px;">
							                 <button type="submit"   class="btn btn-primary">Simpan</button>
							        </div>   

							    </div> 
							</form>  
	       		<?php
	       			}
	       		?>
	       	</div>
        </div>
    </div>
</div>