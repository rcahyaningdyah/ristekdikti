<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Tata Kelola</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Tata Kelola </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->
<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-users"></i> <strong>Tata Kelola User </strong> </h3>
        </div>

        <div class="panel-content">
            <ul class="nav nav-tabs nav-primary">
                <li class="active" ><a href="#tab1" data-toggle="tab" ><i class="icon-docs"></i>Data Konfirmasi User</a></li>
                <li ><a href="#tab2" data-toggle="tab"><i class="icon-docs"></i>Data Semua Users</a></li>
                
            </ul>

            <div class="tab-content ">
                <div class="tab-pane fade active in" id="tab1">
                    <?php
                     $message = $this->session->flashdata('pesan');
                      if(isset($message))
                      {
                        echo $message;
                      } 
                    ?>
                    <table class="table table-dynamic table-tools">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Data Pegawai</th>
                                <th>Tanggal Akses </th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $no = $this->uri->segment(4)+1 ; 
                                foreach ($jawaban_temp as $data){
                            ?>
                            <tr>
                                <td>
                                    <?php echo $no; $no = $no+1;?>
                                </td>

                                <td>                                    
                                        <?php
                                                echo "Nama Lengkap : " .$data['q_1']."<br/>";
                                                echo "Email        : " .$data['q_7']."<br/>";                                                                      
                                        ?>
                                    <a href="" data-toggle="modal" data-target="#konfirmasi"> Detail >> </a> 
                                     <!-- Modal -->
                                    <div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <?php 
                                                $konfirm = $this->jawaban_temp_m->get_email($data['q_7']);
                                                foreach ($konfirm as $det){
                                                    
                                             ?>
                                            <div class="modal-header">
                                                <button type="button" class="close " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title text-info " id="myModalLabel">Detail Informasi <strong> <?php echo $det['q_1']; ?></strong></h4>
                                                <hr/>
                                            </div>
                                            <div class="modal-body" style="margin-top: -56px;">
                                                <?php foreach ($det as $key=>$value){ ?>
                                                <dl class="dl-horizontal">
                                                    <?php 
                                                       if ($key == "_id"){

                                                       }
                                                       else {                                                           
                                                    ?>   
                                                      <dt>
                                                            <?php 
                                                                $pertanyaan  = $this->pertanyaan_m->get_kalimat_tanya($key);
                                                                foreach ($pertanyaan as $p){
                                                                    echo $p['kalimat_tanya'];
                                                                }
                                                            ?>
                                                       </dt>
                                                      <dd style="margin-bottom: -18px;"><?php echo $value ?></dd>
                                                    <?php } ?>
                                                </dl>
                                                <?php
                                                        } //end foreach det
                                                    } //end foreach konfirm
                                                ?>
                                            </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>                      
                                </td>   
                                <td>
                                    <?php 
                                         $tgl = $data['joined_at']->sec; 
                                         $date = date('d/m/Y', $tgl);
                                         $tanggal = explode("/", $date);
                                        $year= $tanggal[2];
                                        $month = $tanggal[1];
                                        $date = $tanggal[0];
                                        switch ($month){
                                            case 1 : $month ='Januari'; break;
                                            case 2 : $month ='Februari'; break;
                                            case 3 : $month = 'Maret'; break;
                                            case 4 : $month = 'April'; break;
                                            case 5 : $month = 'Mei'; break;
                                            case 6 : $month = 'Juni'; break;
                                            case 7 : $month = 'Juli'; break;
                                            case 8 : $month = 'Agustus'; break;
                                            case 9 : $month = 'September'; break;
                                            case 10 : $month = 'Oktober'; break;
                                            case 11 : $month = 'November'; break;
                                            case 12 : $month = 'Desember'; break;
                                        } 
                                       
                                       echo $date.' '.$month.' '.$year;
                                    ?>
                                </td> 

                                <td>
                                
                                    <a href="<?php echo site_url('kelola/send_email/'.$data["_id"]) ?>" class="btn btn-primary" > <i class="icon-paper-plane"> </i> Konfirmasi </a>
                                    <a href="<?php echo site_url('kelola/tolak_konfirmasi/'.$data["_id"]) ?>" class="btn btn-danger"> <i class="icon-trash"> </i>Tolak </a>
                                    
                                </td>                   
                            </tr>
                            <?php   } ?>
                        </tbody>
                    </table>   
                </div>

                  <div class="tab-pane fade  " id="tab2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Data Pegawai</th>                                
                               
                            </tr>
                        </thead>
                         <tbody>
                            <?php 
                                $no = $this->uri->segment(4)+1 ; 
                                foreach ($user as $data){
                            ?>
                            <tr>
                                <td>
                                    <?php echo $no; $no = $no+1;?>
                                </td>

                                <td>
                                  
                                    <?php
                                        //echo $data['_id']; -->untuk link nya   
                                       echo "Email : ".$data['q_7']."<br/>";
                                       echo "Username : ".$data['username']."<br/>";
                                       echo "Fullname : ".$data['q_1']."<br/>"; 
                                      // echo $data['_id'];                            
                                    ?>
                                    <a href="" data-toggle="modal" data-target="#detailUser"> Detail User >> </a> 
                                       
                                    <!-- Modal -->
                                    <div class="modal fade" id="detailUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                      <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <?php 

                                                $detail = $this->users_m->get_user($data['q_7']);
                                                foreach ($detail as $det){
                                                    
                                             ?>
                                            <div class="modal-header">
                                                <button type="button" class="close " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title text-info " id="myModalLabel">Detail Informasi <strong> <?php echo $det['q_1']; ?></strong></h4>
                                                <hr/>
                                            </div>
                                            <div class="modal-body" style="margin-top: -56px;">
                                                <?php foreach ($det as $key=>$value){ ?>
                                                <dl class="dl-horizontal">
                                                    <?php 
                                                       if ($key == "_id" ){

                                                       }
                                                       else {                                                           
                                                    ?>   
                                                      <dt>
                                                            <?php 
                                                                $pertanyaan  = $this->pertanyaan_m->get_kalimat_tanya($key);
                                                                foreach ($pertanyaan as $p){
                                                                    echo $p['kalimat_tanya'];
                                                                }

                                                            ?>
                                                       </dt>
                                                      <dd style="margin-bottom: -18px;"><?php echo $value ?></dd>
                                                    <?php } ?>
                                                </dl>
                                                <?php
                                                        } //end foreach det
                                                    } //end foreach konfirm
                                                ?>
                                            </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>                       
                                </td>                   
                            </tr>
                            <?php   } ?>
                        </tbody>
                    </table>
                </div>                             
            </div>       
        </div>
	</div>
</div>

<script type="text/javascript">
	$('#created_at').datepicker('enable');
	
});
</script>