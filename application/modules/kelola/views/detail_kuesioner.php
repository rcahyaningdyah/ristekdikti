<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Tata Kelola</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li > <a href="">Tata Kelola </a> </li>
      <li class="active"> Detail Kuesioner </li>
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
	<div class="col-lg-12">
		
			<div class="panel-header panel-controls">
				  <h3><i class="icon-layers"></i><strong>Edit Kuesioner</strong> </h3>
			</div>

			<div class="panel-content" style="min-height:500px;">
    
        <div class="container">
                    
  
        <div class="control-group" id="fields">
            <div class="controls"> 
                <form role="form" autocomplete="off">
                 <div style="width: 25%;float: left;" >
                        <input class="form-control" placeholder="Name this snippet" name="voca" type="text" value="Dynamic Form Fields - Add & Remove BS3">
                    </div>
                    </br>
                    </br>
                    <div style="width: 25%;float: left;">
                        <input class="form-control" placeholder="Name this snippet" name="vocatrans" type="text" value="Dynamic Form Fields - Add & Remove BS3">
                    </div>
                    </br>
                    </br>

                <div class="row">
                   
                    <div class="voca">
                    <div class="col-md-3">
                        <input class="form-control" placeholder="Name this snippet" name="voca" type="text" value="Dynamic Form Fields - Add & Remove BS3">
                    </div>
                    <div class="col-md-3">
                        <input class="form-control" placeholder="Name this snippet" name="vocatrans" type="text" value="Dynamic Form Fields - Add & Remove BS3">
                    </div>
                    <div class="col-md-1">
                        <select class="form-control">
                          <option>1</option>
                          <option>2</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-success btn-add" >
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add more
                    </button>
                    </div>
                    </div>    
                    
                </form>
            <br>

            </div>
        </div>

</div>

        <div class="col-md-9">
           <a class="btn btn-primary" role="button" data-toggle="collapse" href="#tambahbab" aria-expanded="false" aria-controls="collapseExample">
              Tambah Bab/Pertanyaan
          </a>
          <div class="collapse" id="tambahbab">
              <div class="col-xs-12">
                  <form class="form-inline" id="form_bab" action="<?php echo site_url('') ?>">
                    <div class="form-group  col-xs-6">
                        <label class="control-label">Pilih Bab</label>
                        <select id="select" class="form-control" name="select" >
                           <option >Pilih Bab</option>
                          <?php foreach ($bab as $data) {
                             
                            ?>                      
                            <option  value="<?php echo $data['_id']?>"><?php  echo $data['_id']."-".$data['deskripsi']; ?></option>                     
                          <?php } ?>
                        </select>
                    </div>
                   
                  </form>         
              </div>
                
              <div class="col-xs-12" >
                  <div id="isi" > </div>
                  <div id="isian"> </div>
              </div>
                       
           <!--    <div class="col-xs-12" id="fields" >
                  <div class="control-group" >
                      <div class="controls"> 
                          <form role="form" autocomplete="off">                            
                              <div class="row">                       
                                  <div class="voca">
                                      <div class="col-md-5">
                                          <label> Tambah Pertanyaan </label>
                                          <select class="form-control">

                                              <option value="Y">Wajib</option>
                                              <option value="T">Tidak</option>
                                          </select>
                                      </div>
                                      <button id="tambah" type="button" class="btn btn-success btn-add" style="margin-top:25px;">
                                          <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add more
                                      </button>
                                  </div>
                              </div>    
                              
                          </form>
                      <br>

                        </div>
                  </div>
              </div>    
               -->
             

            </div>
            
          </div>
        
       

        <div class="col-md-3">
            <h2 class="text-info">List Bab </h2>
            <div class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-bab"> 
               Tambah Bab Baru
            </div>
             
            <ul class="bab1" style="overflow-y: scroll;  width: 300px; height: 180px;">
                <?php foreach ($bab as $key => $value){ ?>
                  <li>
                     <strong><?php echo ucfirst($value['_id']); ?> </strong> - <?php echo ucfirst($value['deskripsi']);?>
                    <input type="hidden" value="<?php $value['_id'];?>">
                  </li>
                <?php }; ?>
            </ul>
            <hr> 
            <h2 class="text-info">List Pertanyaan</h2>
            <div class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-pertanyaan">
              Tambah Pertanyaan Baru
            </div>
            <ul class="pertanyaan" style="overflow-y: scroll;  width: 300px; height: 180px;">
              <?php foreach ($pertanyaan as $key => $value) { ?>
                <li> 
                    <strong> <?php echo ucfirst($value['_id']); ?> </strong> -
                  <?php  
                      echo ucfirst($value['kalimat_tanya']);
                  ?>
                  <input type="hidden" value="<?php $value['_id'];?>"> 
                </li>
              <?php }; ?>
            </ul>
            
           
            <div class="modal fade" id="modal-pertanyaan" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <form action="<?php echo site_url('Kelola/add_pertanyaan'); ?>" method="post">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                      <h4 class="modal-title"><strong>Form Tambah</strong>Pertanyaan</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="field-1" class="control-label">Bentuk Form</label>
                            <select class="form-control" name="bentuk_form" value="">
                            <option value="text">Text</option>
                            <option value="select">Select</option>
                          </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="field-1" class="control-label">Kalimat Tanya</label>
                            <input type="text" name="kalimat_tanya" class="form-control" id="kalimat_tanya" value="" placeholder="Kalimat tanya ">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="field-4" class="control-label">Wajib</label>
                            <select class="form-control" name="wajib" value="">
                            <option value="Y">Ya</option>
                            <option value="T">Tidak</option>
                          </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer text-center">
                      <button type="submit" class="btn btn-primary btn-embossed bnt-square" ><i class="fa fa-check"></i>Save</button>
                    </div>
                    </form>
                  </div>
                </div>
            </div>
            <div class="modal fade" id="modal-bab" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <form action="<?php echo site_url('Kelola/add_bab'); ?>" method="post">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Form Tambah</strong>Pertanyaan</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="field-1" class="control-label">Deskripsi</label>
                            <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="" placeholder="Deskripsi">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="field-1" class="control-label">Topik</label>
                          <input type="text" name="topik" class="form-control" id="topik" value="" placeholder="Deskripsi">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-primary btn-embossed bnt-square" ><i class="fa fa-check"></i>Save</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
        </div>
         
         
      </div>
   
  </div>
</div>

<script type="text/javascript">

  $( document ).ready(function() {
   // $('#fields').hide();
   $('#select').on('change',function(e){
        var url = "<?php echo site_url('kelola/get_pertanyaan/')?>" ;
        var select = document.getElementById("select");
        var option = select.options[select.selectedIndex].value;
        var response = "";
        $("#isi").text(option);
        // $("#div1").html(url+'/'+option);
        // $('#isi').val($(this).val());

        $.ajax({
            type: 'post',
            url: url,
            data: $("#form_bab").serialize(),
            success: function(response) {
              $('#fields').show();
              var json_obj = $.parseJSON(response);//parse JSON 
              var html;
              console.log(json_obj);
              $.each( json_obj, function( keys, values ) {
                  $.each(values, function(key, value){
                      if(value.isArray){
                         html = key;
                       }
                      else{
                        html = html+' '+key+' '+value;
                      }
                  });
               });
             $('#isian').html(html);
            }
        });

      });

      $(document).on('click', '.btn-add', function(e)
      {
            e.preventDefault();

            var controlForm = $('.controls form:first'),
                currentEntry = $(this).parents('.voca:first'),
                newEntry = $(currentEntry.clone()).appendTo(controlForm);

            newEntry.find('input').val('');
            controlForm.find('.btn-add:not(:last)')
                .removeClass('btn-default').addClass('btn-danger')
                .removeClass('btn-add').addClass('btn-remove')
                
                .html('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Remove   ');
        }).on('click', '.btn-remove', function(e)
        {
        $(this).parents('.voca:first').remove();

        e.preventDefault();
        return false;
      });
  });


</script>