<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Tata Kelola</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Tata Kelola </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-header panel-controls">
				
			</div>

			<div class="panel-content" style="min-height:500px;">
				<ul  class="nav nav-tabs ">
					<!-- <li  class="active"><a href="#tab1" data-toggle="tab"><i class="icon-docs"></i> Tata Kelola Kuisioner</a></li> -->
					<li class="active"><a href="#tab2" data-toggle="tab"><i class="icon-user"></i> Tata Kelola User</a></li>
					
				</ul>

				<div class="tab-content tab-head">
					<div class="tab-pane fade " id="tab2">
							
					</div>
<!-- 
					<div class="tab-pane fade active in" id="tab1">
						<div class="btn btn-primary" data-toggle="modal" data-target="#modal-responsive">
	                           Tambah Kuesioner
	                        </div>
							<div class="modal fade" id="modal-responsive" aria-hidden="true">
					            <div class="modal-dialog modal-lg">
					              <div class="modal-content">
					              	<form action="<?php echo site_url('Kelola/add_kuesioner'); ?>" method="post">
					                <div class="modal-header">
					                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
					                  <h4 class="modal-title"><strong>Form Tambah</strong>Kuesioner</h4>
					                </div>
					                <div class="modal-body">
					                  <div class="row">
					                  <div class="col-md-6">
					                      <div class="form-group">
					                        <label for="field-1" class="control-label">Kuesioner ke</label>
					                        <input type="text" name="kuesioner_id" class="form-control" id="kuesioner_id" value="" placeholder="Admin">
					                      </div>
					                    </div>
					                    <div class="col-md-6">
					                      <div class="form-group">
					                        <label for="field-1" class="control-label">Pembuat Kuesioner</label>
					                        <input type="text" name="created_by" class="form-control" id="created_by" value="" placeholder="Admin">
					                      </div>
					                    </div>
					                    <div class="col-md-6">
					                      <div class="form-group">       
						                    <label class="form-label">Tanggal Pembuatan Kuesioner</label>
						                    <div class="prepend-icon" id="createat">
						                      <input type="text" name="created_at" id="created_at" value="" class="datetimepicker form-control" placeholder="Pilih tanggal...">
						                      <i class="icon-calendar"></i>
						                    </div>
						                  </div>
					                    </div>
					                  </div>
					                  <div class="row">
					                    <div class="col-md-12">
					                      <div class="form-group">
					                         <label class="form-label">Masa Berlaku Kuesioner:</label>
						                        <div class="input-daterange b-datepicker input-group" id="datepicker">
						                            <input type="text" name="masa_berlaku_awal" id="masa_berlaku_awal" class="input-sm form-control" name="start" placeholder="Mulai dari..."/>
						                            <span class="input-group-addon">s/d</span>
						                            <input type="text" name="masa_berlaku_akhir" id="masa_berlaku_akhir" class="input-sm form-control" name="end" placeholder="Sampai..."/>
						                        </div>
					                      </div>
					                    </div>
					                  </div>
					                  <div class="row">
					                    <div class="col-md-4">
					                      <div class="form-group">
					                        <label for="field-4" class="control-label">Target</label>
					                        <input type="text" name="untuk" class="form-control" id="untuk" placeholder="Semua Kementrian">
					                      </div>
					                    </div>
					                  </div>
					                </div>
					                <div class="modal-footer text-center">
					                  <button type="submit" class="btn btn-primary btn-embossed bnt-square" ><i class="fa fa-check"></i>Save</button>
					                </div>
					                </form>
					              </div>
					            </div>
					        </div>
						<table class="table">
							<thead>
								<tr>									
									<td> Nama Kuesioner </td>
									<td> Pembuat Kuesioner</td>
									<td> Tanggal Pembuatan </td>									
									<td> Awal Berlaku </td>
									<td> Akhir Berlaku</td>
									<td> Tujuan Kuesioner</td>
								</tr>
							</thead>

							<tbody>
								<?php foreach ($kuesioner as $data){ 
										?>
										<tr>
										<?php //foreach ($data as $key=>$val){ ?>	
											<td><a href="<?php echo site_url('kelola/detail_kuesioner/'.$data['_id']) ?>" ><?php echo ucfirst($data['_id']) ?> </a></td>
											<td><?php echo ucfirst($data['created_by'])?> </td>
											<td><?php echo $data['created_at']?> </td>
											<td><?php echo $data['masa_berlaku_awal']?> </td>
											<td><?php echo $data['masa_berlaku_akhir']?> </td>
											<td><?php echo ucfirst($data['untuk'])?> </td>
										<?php // } ?>
										</tr>
								<?php	
									  }
								 ?>

							</tbody>
						</table>
					</div>
 -->
				</div>
			</div>
		</div>
	</div>
</div>