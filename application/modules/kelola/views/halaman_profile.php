 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Profil</strong> Admin  </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Profil</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">    

        <div class="panel-content">
            <ul class="nav nav-tabs nav-primary">
                <li class="active" ><a href="#tab1" data-toggle="tab" ><i class="icon-user"></i>Detail Profil</a></li>
                <li ><a href="#tab2" data-toggle="tab" ><i class="icon-key"></i>Ganti Password</a></li>                
            </ul>

            <div class="tab-content ">
                <div class="tab-pane fade active in " id="tab1">
                     <?php
                       $message = $this->session->flashdata('sukses');
                        if(isset($message))
                        {
                          echo $message;
                        } 
                      ?>
                       <?php
                       $message = $this->session->flashdata('gagal');
                        if(isset($message))
                        {
                          echo $message;
                        } 
                      ?>
                    <?php 
                      foreach ($detail_user as $data){                     
                     
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <h3> <strong> Identitas Pegawai </strong></h3>
                             <dl class="dl-horizontal">
                                <dt> Nama Lengkap </dt>
                                <dd> <?php echo $data['q_1'] ?> </dd>
                                <dt> Jabatan </dt>
                                <dd> <?php echo $data['q_2'] ?></dd>
                                <dt> NIP</dt>
                                <dd> <?php echo $data['q_3'] ?></dd>
                                <dt> No. Telepon</dt>
                                <dd> <?php echo $data['q_4'] ?></dd>
                                <dt> No. Handphone</dt>
                                <dd> <?php echo $data['q_5'] ?></dd>
                                <dt> Fax</dt>
                                <dd> <?php echo $data['q_6'] ?></dd>
                                <dt> Email </dt>
                                <dd> <?php echo $data['q_7'] ?></dd>
                                <dt> Tanggal Daftar</dt>
                                <dd> 
                                    <?php 
                                          $tgl = $data['joined_at']->sec; 
                                         $date = date('d/m/Y', $tgl);
                                         $tanggal = explode("/", $date);
                                        $year= $tanggal[2];
                                        $month = $tanggal[1];
                                        $date = $tanggal[0];
                                        switch ($month){
                                            case 1 : $month ='Januari'; break;
                                            case 2 : $month ='Februari'; break;
                                            case 3 : $month = 'Maret'; break;
                                            case 4 : $month = 'April'; break;
                                            case 5 : $month = 'Mei'; break;
                                            case 6 : $month = 'Juni'; break;
                                            case 7 : $month = 'Juli'; break;
                                            case 8 : $month = 'Agustus'; break;
                                            case 9 : $month = 'September'; break;
                                            case 10 : $month = 'Oktober'; break;
                                            case 11 : $month = 'November'; break;
                                            case 12 : $month = 'Desember'; break;
                                        } 
                                       
                                       echo $date.' '.$month.' '.$year;
                                    ?>
                                 </dd>
                             </dl>
                        </div>

                         <div class="col-md-6">
                            <h3> <strong> Asal Institusi </strong> </h3>
                             <dl class="dl-horizontal">
                                <dt> Nama Kementrian </dt>
                                <dd> <?php echo $data['q_8'] ?> </dd>
                                <dt> Nama Lembaga/Badan </dt>
                                <dd> <?php echo $data['q_9'] ?></dd>
                                <dt> Nama Pusat/Balai</dt>
                                <dd> <?php echo $data['q_10'] ?></dd>
                                <dt style="width: 200px;margin-left: -38px;"> Nama Kepala Pusat/Balai/UPT</dt>
                                <dd> <?php echo $data['q_66'] ?></dd>
                                <dt> Alamat</dt>
                                <dd> <?php echo $data['q_11'] ?></dd>
                                <dt>  No. Telepon</dt>
                                <dd> <?php  if ($data['q_12']) {echo $data['q_12'];} else {echo "-";} ?></dd>
                                <dt> Alamat Website</dt>
                                <dd> <?php echo $data['q_13'] ?></dd>
                                <dt> Fax</dt>
                                <dd> 
                                    <?php
                                     if ($data['q_14'] == null) {echo "- " ;} else {echo $data['q_14'];} ?>
                                </dd>
                             </dl>
                        </div>

                    </div>
                    <?php  } // end foreach detail user ?>
                </div>

                <div class="tab-pane fade " id="tab2">
                   
                    <form action="<?php echo site_url('./kelola/Profil/ubah_password ') ?>" method="POST">
                          <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Password Sekarang </label>
                                <div class="col-sm-6">
                                    <input type="password" name="pass_lama"  class="form-control col-sm-4"  value="<?php echo set_value('pass_lama') ?>" placeholder="Password Sekarang" >
                                     <?php  echo form_error('pass_lama') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Password Baru </label>
                                <div class="col-sm-6">
                                    <input type="password" name="password"  class="form-control col-sm-4"  value="<?php echo set_value('password') ?>" placeholder="Password Baru">
                                     <?php  echo form_error('password') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> Konfirmasi Password Baru </label>
                                <div class="col-sm-6">
                                    <input type="password" name="konfirm_pass_baru"  class="form-control col-sm-4"  value="<?php echo set_value('konfirm_pass_baru') ?>" placeholder="Konfirm Password Baru">
                                     <?php  echo form_error('konfirm_pass_baru') ?>
                                </div>
                            </div>
                            <div class="form-group " >
                                 <button type="submit"   class="btn btn-primary " style="margin-left:335px;">Simpan</button>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>