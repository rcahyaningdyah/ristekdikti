 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Tambah </strong> Lembaga  </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li><a href="<?php echo site_url('administrator/institusi') ?>">Institusi</a> </li>
      <li><a href="<?php echo site_url('administrator/institusi/detail_kementrian/'.$id_kementrian) ?>">Detail Kementrian</a> </li>
      <li class="active">Tambah Lembaga</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
        	<div class="col-md-8">
        		 <?php
                   $message = $this->session->flashdata('pesan');
                    if(isset($message))
                    {
                      echo $message;
                    } 
                  ?>
                   	<?php 
        	
		        		foreach ($kementrian as $data){
		        	?>
		        			<div class="col-xs-12">
		        				<dl class="dl-horizontal">
		        					<dt> Nama Kementrian </dt> <dd> <?php echo $data['nama_kementrian'] ?> </dd>
		        					<dt> Alamat Kementrian </dt> <dd> <?php echo $data['alamat_kementrian'] ?></dd>
		        				</dl>
		        			</div>
		        	<?php 
		        		} // end foreach kementrian
		        	?>
        		<form action="<?php echo site_url('administrator/institusi/simpan_lembaga/'.$id_kementrian) ?>" method="POST">
	        		<div class="form-horizontal ">
	        			<div class="form-group">
	        				<label class="col-md-3 control-label">Nama Lembaga </label>
	        				 <div class="col-sm-9">
	        				 	<input type="text" class="form-control col-sm-3" name="nama_lembaga" value="<?php echo set_value('nama_lembaga') ?>" id="nama_lembaga">
	        				 	<?php  echo form_error('nama_lembaga') ?>	
	        				 </div>
	        				
	        			</div>
	        			<div class="form-group">
	        				<label class="col-md-3 control-label">Nama Inisial Lembaga (ditulis dalam SATU KATA saja) </label>
	        				 <div class="col-sm-9">
	        				 	<input type="text" class="form-control col-sm-3" name="inisial_lembaga" value="<?php echo set_value('inisial_lembaga') ?>" id="inisial_lembaga">
	        				 	<?php  echo form_error('inisial_lembaga') ?>	
	        				 </div>
	        				
	        			</div>
	        			<div class="form-group">
	        				<label class="col-md-3 control-label">Alamat Lembaga </label>
	        				<div class="col-sm-9">
	        					<textarea class="form-control col-sm-6" name="alamat_lembaga"  value="<?php echo set_value('alamat_lembaga') ?>" id="alamat_lembaga">  </textarea>
	        						<?php  echo form_error('alamat_lembaga') ?>	
	        				</div>
	        				
	        			</div>
	        			<div class="form-group " style="margin-left: 258px;">
	                         <button type="submit"   class="btn btn-primary">Simpan</button>
	                    </div> 

	        		</div>
	        	</form>
        	</div>
        	
        </div>
    </div>
</div>