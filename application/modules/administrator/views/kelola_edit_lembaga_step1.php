 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Edit </strong> Admin Lembaga  </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li><a href="<?php echo site_url('administrator/kelola') ?>">Tata Kelola</a> </li>
      <li class="active">Edit Admin Lembaga</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
            <?php
             $message = $this->session->flashdata('pesan_error');
              if(isset($message))
              {
                echo $message;
              } 
            ?>
           
            <?php 
              foreach ($user_admin as $data){
               
            ?>
              <form action="<?php echo site_url('administrator/kelola/update_admin_step1/'.$data['_id'])?>"  method="POST">
              <div class="form-horizontal">
            <?php
                foreach ($data as $key => $value){
                  if (strpos($key, 'q_') !== false){
                    // get kalimat tanya dari key
                    $pertanyaan = $this->pertanyaan_m-> get_kalimat_tanya($key); 
                                            
            ?>
                      <div class="form-group">
                          <label class="col-md-3 control-label"><?php echo $pertanyaan[0]['kalimat_tanya'];?> </label> 
                          <div class="col-sm-9">
                              <?php 
                                  if ($key == "q_8"){
                              ?>
                                      <!-- select dari kementrian -->
                                      <select class="form-control" name="q_8" id="q_8" data-parsley-group="block0"   data-search="true"> 
                                          <!--  <option > Pilih Kementrian </option> -->
                                          <?php foreach ($kementrian as $data){
                                           ?>
                                              <option value="<?php echo $value ?>"> <?php if ($value) { echo $value;} else {echo set_select($key);} ?> </option>
                                              <option id="<?php echo $data['_id'] ?>" value="<?php echo $data['_id'] ?>"> <?php echo $data['nama_kementrian'] ?> </option>
                                          <?php      
                                                  } 
                                          ?>
                                      </select>
                              <?php 
                                  } //end if q_8 
                                  elseif ($key == "q_9"){
                              ?>
                                      <!-- select dari lembaga -->
                                      <select class="form-control" name="q_9" id="q_9" data-parsley-group="block0"  data-search="true">
                                          <option value="<?php echo $value ?>"> <?php if ($value) { echo $value;} else {echo set_select($key);} ?> </option>                                                  
                                            <option > Pilih Lembaga </option>                                                   
                                     </select>
                              <?php 
                                  } //end else q_9
                                  elseif ($key == "q_10"){
                              ?>
                                    <!-- dropdown select pusat/upt -->
                                   <select class="form-control" name="q_10" id="q_10" data-parsley-group="block0"  data-search="true">
                                        <option value="<?php echo $value ?>"> <?php if ($value) { echo $value;} else {echo set_select($key);} ?> </option>                                                      
                                        <option > Pilih Puslitbang/UPT/Balai </option>                                                       
                                  </select>

                              <?php
                                } //end else q_10
                                else {
                              ?>
                                  <input type="text" class="form-control col-sm-6" id="<?php echo $key ?>" value="<?php if ($value){echo $value;} else {echo set_value($key);} ?>" name="<?php echo $key?>" placehorder="<?php  ?>">      
                              <?php
                                } //end else
                              ?>
                                <?php  echo form_error($key) ?>
                          </div>
                      </div>
            <?php
                   
                  } // end if
                } // end foreach data
            ?>
                </div>
                <button class="btn btn-primary" type="submit" style="margin-left: 258px;">Simpan</button>
              </form>
            <?php
              } // end foreach user admin                         
            ?>                       
        </div>
    </div>
</div>

<script type="text/javascript">
function goBack() {
    window.history.back();
}

$(document).ready(function(){
       
        $("#q_8").change(function()
        {
           var kementrian_id = $('#q_8').val();
           $("#q_9").html('<option value=""> Pilih Lembaga </option>');
           $("select#q_9").val("").prop('selected', true);
           if (kementrian_id != ""){
                var post_url = "<?php echo site_url('daftar/daftar/get_lembaga_bykementrian') ?>/" + kementrian_id;
                $.ajax({
                    type : "POST",
                    url : post_url,
                    dataType: 'json',
                    success : function(data)
                    {
                        str = '<option value=""> Pilih Lembaga </option>';
                        // console.log(data);
                        $.each(data.kelompok_lembaga, function(keyx, valx){
                            // console.log(keyx+' '+valx);
                            str += '<option value="'+keyx+'">'+valx+'</option>'
                        });
                        // console.log(str);
                        $("#q_9").html(str);

                    } // end success
                }); // end ajax
           }

        }); // end change  q_9

       $("#q_9").change(function(){
            var kelompok = $('#q_9').val();
            $("#q_10").html('<option value=""> Pilih Puslitbang </option>');
            $("select#q_10").val("").prop('selected', true);
            if (kelompok != ""){
                var post_url = "<?php echo site_url('daftar/daftar/get_puslitbang_bylembaga') ?>/" + kelompok;
                $.ajax({
                    type : "POST",
                    url : post_url,
                    dataType: 'json',
                    success : function(data)
                    {
                        str = '<option value=""> Pilih Puslitbang </option>';
                         console.log(data);
                         $("#q_10").empty();
                        $.each(data.puslitbang_lembaga, function(keyx, valx){
                            // console.log(keyx+' '+valx);
                            str += '<option value="'+keyx+'">'+valx+'</option>'
                        });
                        // console.log(str);
                        $("#q_10").html(str);

                    } // end success
                }); // end ajax
            }
        }); // end change q_31
    });
</script>