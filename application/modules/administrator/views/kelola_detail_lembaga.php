<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Detail Lembaga</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li > <a href="<?php echo site_url('administrator/kelola') ?>"> Tata Kelola </a> </li>
      <li class="active">Detail Lembaga </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-users"></i> <strong>Tata Kelola User </strong> </h3>
        </div>

        <div class="panel-content">
        	<h3 class="text-info"><strong>User Detail</strong> </h3>
        	<?php
        		foreach ($user_lembaga as $data){
        			if ($data['role'] == 'admin'){
        	?>
        			<div class="col-xs-12">
        				<div class="col-xs-4">
        					<dl class="dl-horizontal">
							  <dt>Nama Admin Lembaga</dt> <dd><?php echo $data['q_1'] ?></dd>
							  <dt> NIP</dt> <dd> <?php echo $data['q_3'] ?> </dd>
							  <dt> Jabatan </dt> <dd> <?php echo $data['q_2'] ?></dd>
							  <dt> Telepon </dt> <dd><?php echo $data['q_4'] ?></dd>
							  <dt> HP </dt> <dd> <?php echo $data['q_5'] ?></dd>
							  <dt> Fax</dt> <dd> <?php echo $data['q_6'] ?></dd>
							</dl>
        				</div>

        				<div class="col-xs-4">
        					<dl class="dl-horizontal">
							  <dt>Nama Kementrian</dt> <dd><?php echo $data['q_8'] ?></dd>
							  <dt> Nama Lembaga/Badan </dt> <dd> <?php echo $data['q_9'] ?> </dd>
							  <dt> Nama Balai/UPT </dt> <dd> <?php echo $data['q_10'] ?></dd>
							  <dt> Alamat </dt> <dd><?php echo $data['q_11'] ?></dd>
							  <dt> Telepon </dt> <dd> <?php echo $data['q_12'] ?></dd>
							  <dt> Website</dt> <dd> <?php echo $data['q_13'] ?></dd>
							  <dt> Fax</dt> <dd> <?php echo $data['q_14'] ?></dd>
							</dl>
        				</div>
        			</div>
        			
        	<?php		
        			}// end if role admin
        		} // end foreach user_lembaga      	
        	?>
    			<table class="table table-dynamic table-tools">
    				<thead>
    					<tr> 
    						<td> No </td>
    						<td> Nama </td>
    						<td> NIP </td>
    						<td> Jabatan</td>
    						<td> Telepon</td>
    						<td> HP</td>
    						<td> Fax</td>
    						<td> Email</td>
    					</tr>
    				</thead>

    				<tbody>
    					<?php 

                          	$no = $this->uri->segment(4)+1 ; 
    						foreach ($user_lembaga as $data){
    							if ($data['role'] == "pegawai"){
    					?>
    								<tr> 
    									<td><?php echo $no; $no = $no+1;?> </td>
    									<td> <?php echo $data['q_1'] ?> </td>
    									<td> <?php echo $data['q_3'] ?></td>
    									<td> <?php echo $data['q_2'] ?></td>
    									<td> <?php echo $data['q_4'] ?></td>
    									<td> <?php echo $data['q_5'] ?></td>
    									<td> <?php echo $data['q_6'] ?></td>
    									<td> <?php echo $data['q_7'] ?></td>
    								</tr>
    					<?php 
    							} // end if pegawai
    						} //  end foreach
    					?>
    				</tbody>
    			</table>
        
        </div>
    </div>
</div>