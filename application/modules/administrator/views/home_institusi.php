<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Institusi</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Daftar Kementrian </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-basket-loaded"></i> <strong>Daftar Kementrian </strong> </h3>
        </div>

        <div class="panel-content">
             <?php
                   $message = $this->session->flashdata('pesan');
                    if(isset($message))
                    {
                      echo $message;
                    } 
                  ?>
        	<a href="<?php echo site_url('administrator/institusi/tambah_kementrian') ?>" class="btn btn-primary"> <i class="icon-plus"> </i> Tambah Kementrian </a>
        	<table class="table  table-dynamic table-tools">
        		<thead>
        			<tr> 
        				<td> No. </td>
        				<td> Nama Kementrian</td>
        				<td> Alamat </td>
        				<td colspan="2"> Aksi</td>
        			</tr>
        		</thead>

        		<tbody>
        			<?php
        				 $no = $this->uri->segment(3)+1 ; 
        				foreach ($kementrian as $data){
        					
        			?>
        				<tr>
        					<td> <?php echo $no; $no = $no+1;?> </td>
        					<td> <?php echo $data['nama_kementrian'] ?></td>
        					<td> <?php echo $data['alamat_kementrian']?></td>
        					<td> <a href="<?php echo site_url('administrator/institusi/detail_kementrian/'.$data['_id']) ?>" class="btn btn-primary"> <i class="icon-eye"> </i> Detail </a></td>
        				  <td> <a onclick="return confirm('Anda yakin akan menghapus data kementrian ini? Data yang dihapus meliputi data lembaga dan data puslitbang yang ada.')" href="<?php echo site_url('administrator/institusi/hapus_kementrian/'.$data['_id'])?>" class="btn btn-danger"> <i class="icon-trash"> </i> Hapus </a> </td>
        				</tr>
        			<?php		
        					
        				} // end foreach kementrian
        			?>
        		</tbody>
        	</table>
        </div>
    </div>
</div>