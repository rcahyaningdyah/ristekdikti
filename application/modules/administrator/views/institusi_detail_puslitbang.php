<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Detail Puslitbang</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li > <a href="<?php echo site_url('administrator/institusi') ?>"> Kementrian </a> </li>
      <li > <a href="<?php echo site_url('administrator/institusi/detail_kementrian/'.$id_kementrian) ?>"> Detail Kementrian </a> </li>
       <li > <a href="<?php echo site_url('administrator/institusi/detail_lembaga/'.$id_lembaga) ?>"> Detail Lembaga </a> </li>
      <li class="active">Detail Puslitbang </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->


<div class="row panel">
  <div class="col-lg-12">
    <div class="panel-header panel-controls">
            <h3><i class="icon-users"></i> <strong>Detail Puslitbang </strong> </h3>
        </div>

        <div class="panel-content">
          <div class="alert alert-info "> 
            Jika Anda ingin menghapus salah satu Puslitbang, maka klik <italic> Button </italic> berwarna merah. <br/>
            Jika Anda ingin mengubah data Puslitbang, maka isikan data Puslitbang sesuai dengan kolom input yang tersedia. Kemudian klik Button Simpan  
          </div>

          <?php
              foreach ($puslitbang as $data){
               
          ?>
                <form  action="<?php echo site_url('administrator/institusi/update_puslitbang/'.$data['_id'].'/'.$id_lembaga.'/'.$id_kementrian) ?>" method="POST">
                   <div class="form-horizontal ">
                      <?php  foreach ($data['puslitbang'] as $litbang){ ?>
                       <div class="form-group">
                            <label class="col-md-2 control-label">Nama Puslitbang </label>
                             <div class="col-md-10">
                                   <input type="text" class="form-control col-sm-3" value="<?php if (set_value('nama_puslitbang[]') == TRUE){echo set_value('nama_puslitbang[]');} else{echo $litbang;} ?>" name="nama_puslitbang[]" style="width:300px;">
                                   <a href="<?php echo site_url('administrator/institusi/hapus_puslitbang/'.$data['_id'].'/'.$litbang) ?>" class="btn btn-sm btn-danger"> <i class="icon-trash"> </i> Hapus Puslitbang </a>
                                   <?php  echo form_error('nama_puslitbang[]') ?> 
                             </div>
                            
                       </div>
                        

                      <?php   } // end freach litbang ?>
                      <div class="form-group " style="margin-left: 258px;">
                           <button type="submit"   class="btn btn-primary">Simpan</button>
                        </div>
                    </div> 
                </form>
          <?php
              
             } // end foreach puslitbang 
          ?>
        </div>
    </div>
</div>