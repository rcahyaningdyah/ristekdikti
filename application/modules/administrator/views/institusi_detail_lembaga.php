<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Detail Lembaga</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li > <a href="<?php echo site_url('administrator/institusi') ?>"> Kementrian </a> </li>
      <li > <a href="<?php echo site_url('administrator/institusi/detail_kementrian/'.$id_kementrian) ?>"> Detail Kementrian </a> </li>
      <li class="active">Detail Lembaga </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->


<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-users"></i> <strong>Detail Lembaga </strong> </h3>
        </div>

        <div class="panel-content">
        	<?php         	
        		foreach ($kementrian as $data){
        	?>
        			<div class="col-xs-12">
        				 <?php
		                   $message = $this->session->flashdata('pesan');
		                    if(isset($message))
		                    {
		                      echo $message;
		                    } 
		                  ?>
        				<dl class="dl-horizontal">
        					<dt> Nama Kementrian </dt> <dd> <?php echo $data['nama_kementrian'] ?> </dd>
        					<dt> Alamat Kementrian </dt> <dd> <?php echo $data['alamat_kementrian'] ?></dd>
        				</dl>
        			</div>
        	<?php 
        		} // end foreach kementrian
        	?>
        	<?php         	
        		foreach ($lembaga as $data){
        	?>
        			<div class="col-xs-12">        			
        				<dl class="dl-horizontal">
        					<dt> Nama Lembaga </dt> <dd> <?php echo $data['nama_lembaga'] ?> </dd>
        					<dt> Alamat Lembaga </dt> <dd> <?php echo $data['alamat_lembaga'] ?></dd>
        				</dl>
        			</div>
        	<?php 
        		} // end foreach kementrian
        	?>
        		<!-- Modal -->
			<div class="modal fade" id="editLembaga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Edit Kementrian</h4>
			      </div>
			      <div class="modal-body">
			      		
			      			<form action="<?php echo site_url('administrator/institusi/update_lembaga/'.$id_lembaga.'/'.$id_kementrian) ?>" method="POST">
				        		<div class="form-horizontal ">
				        		<?php 
					      			foreach ($lembaga as $data){
					      		?>
				        			<div class="form-group">
				        				<label class="col-md-3 control-label">Nama Lembaga </label>
				        				 <div class="col-sm-9">
				        				 	<input type="text" class="form-control col-sm-3" name="nama_lembaga" value="<?php echo $data['nama_lembaga'] ?>" id="nama_lembaga">
				        				 	<?php  echo form_error('nama_lembaga') ?>	
				        				 </div>
				        				
				        			</div>
				        			<div class="form-group">
				        				<label class="col-md-3 control-label"> Nama Inisial Lembaga (ditulis dalam SATU KATA saja) </label>
				        				 <div class="col-sm-9">
				        				 	<input type="text" class="form-control col-sm-3" name="inisial_lembaga" value="<?php echo $data['_id'] ?>" id="inisial_lembaga">
				        				 	<?php  echo form_error('inisial_lembaga') ?>	
				        				 </div>
				        				
				        			</div>
				        			<div class="form-group">
				        				<label class="col-md-3 control-label">Alamat Lembaga </label>
				        				<div class="col-sm-9">
				        					<textarea class="form-control col-sm-6" name="alamat_lembaga"  value="<?php echo $data['alamat_lembaga'] ?>" id="alamat_lembaga"> <?php echo $data['alamat_lembaga'] ?>  </textarea>
				        						<?php  echo form_error('alamat_lembaga') ?>	
				        				</div>
				        				
				        			</div>
				        			<div class="form-group " style="margin-left: 258px;">
				                         <button type="submit"   class="btn btn-primary">Simpan</button>
				                    </div> 
				                 <?php } // end foreach kementrian ?>
				        		</div>
				        	</form>

			      		
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
			      </div>
			    </div>
			  </div>
			</div>
			<!-- end modals -->
			<!-- modals untuk edit kementrian -->
        	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editLembaga" style="margin-left: 52px; margin-top: -10px;"> <i class="icon-pencil"> </i>Edit Lembaga </button>
        	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahPuslitbang" style="margin-top: -10px;"> <i class="icon-plus"> </i>Tambah Puslitbang </button>
        		<!-- Modal -->
			<div class="modal fade" id="tambahPuslitbang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Tambah Puslitbang</h4>
			      </div>
			      <div class="modal-body">
			      			<?php $id_puslitbang = $puslitbang[0]['_id']; ?>
			      			<form action="<?php echo site_url('administrator/institusi/simpan_puslitbang/'.$id_lembaga.'/'.$id_puslitbang) ?>" method="POST">
				        		<div class="form-horizontal ">
				        		
				        			<div class="form-group">
				        				<label class="col-md-3 control-label">Nama Puslitbang </label>
				        				 <div class="col-sm-9">
				        				 	<input type="text" class="form-control col-sm-3" name="nama_puslitbang"  id="nama_puslitbang">
				        				 	<?php  echo form_error('nama_puslitbang') ?>	
				        				 </div>
				        				
				        			</div>
				        			
				        			<div class="form-group " style="margin-left: 258px;">
				                         <button type="submit"   class="btn btn-primary">Simpan</button>
				                    </div> 
				                 
				        		</div>
				        	</form>

			      		
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			       <!--  <button type="button" class="btn btn-primary">Save changes</button> -->
			      </div>
			    </div>
			  </div>
			</div>
			<!-- end modals -->
			<!-- <a href="<?php echo site_url('administrator/institusi/tambah_puslitbang/'.$id_lembaga.'/'.$id_kementrian) ?>" class="btn  btn-primary" style="margin-top: -10px;"> <i class="icon-plus"> </i>Tambah Puslitbang</a> -->
			
			<h3> <strong>Daftar UPT/Pusat/Balai </strong> </h3>
			
			<table class="table table-dynamic table-tools">
				<thead>
					<tr> 
						<td> No</td>
						<td> Nama Puslitbang </td>
						<td> Aksi</td>
					</tr>
				</thead>

				<tbody>
					<?php 
						$no = $this->uri->segment(5)+1 ; 
						foreach ($puslitbang as $data){
					?>
						<tr>
							<td> <?php echo $no; $no = $no+1;?>   </td>
							<td> <?php foreach ($data['puslitbang'] as $litbang) {echo $litbang."<br/>"; }?></td>
							<td> <a href="<?php echo site_url('administrator/institusi/detail_puslitbang/'.$id_puslitbang.'/'.$id_lembaga.'/'.$id_kementrian) ?>" class="btn btn-primary"> <i class="icon-eye"> </i> Detail  </a></td>
						</tr>
					<?php } // end foreach puslitbang ?>
				</tbody>
			</table>
			
        </div>
    </div>
</div>