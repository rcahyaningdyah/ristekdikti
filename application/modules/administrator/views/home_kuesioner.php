<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Kuesioner</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Kuesioner </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
 	<div class="col-lg-12">
 		<div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Daftar Kegiatan Kuesioner </strong> </h3>
        </div>
         <div class="panel-content">
         	<table class="table table-dynamic table-tools">
         		<thead>
         			<tr>
         				<td> No. </td>
         				<td> Nama Kementrian </td>
         				<td> Nama Lembaga</td>
         				<td> Nama Balai</td>
                        <td> Nama Penelitian</td>
         				<td> Identitas Pengisi </td>
         				<td> Tanggal Pengisian </td>
         				<td> Aksi </td>
         			</tr>
         		</thead>

         		<tbody>
         			<?php 
         				 $no = $this->uri->segment(4)+1 ; 
         				foreach ($kegiatan as $data){

         					$string_id = (string) $data['id_user'];
         					$identitas_pengisi = $this->users_m->get_user_byId($string_id);
         			?>
         					<tr>
         						<td> <?php echo $no; $no = $no+1;?> </td>
         						<td> <?php echo $identitas_pengisi[0]['q_8']; ?> </td>
         						<td> <?php echo $identitas_pengisi[0]['q_9'] ?></td>
         						<td> <?php echo $identitas_pengisi[0]['q_10'] ?> </td>
                                <td> <?php echo $data['q_27']?></td>
         						<td> 
         							Nama Pengisi : <?php echo $identitas_pengisi[0]['q_1'] ?> <br/>
         							NIP : <?php echo $identitas_pengisi[0]['q_3'] ?> <br/>
         							Jabatan : <?php echo $identitas_pengisi[0]['q_2'] ?>
         						</td>
         						<td> 
         							<?php 
                                        $tgl = $data['created_at']->sec; 
                                         $date = date('d/m/Y', $tgl);
                                         $tanggal = explode("/", $date);
                                        $year= $tanggal[2];
                                        $month = $tanggal[1];
                                        $date = $tanggal[0];
                                        switch ($month){
                                            case 1 : $month ='Januari'; break;
                                            case 2 : $month ='Februari'; break;
                                            case 3 : $month = 'Maret'; break;
                                            case 4 : $month = 'April'; break;
                                            case 5 : $month = 'Mei'; break;
                                            case 6 : $month = 'Juni'; break;
                                            case 7 : $month = 'Juli'; break;
                                            case 8 : $month = 'Agustus'; break;
                                            case 9 : $month = 'September'; break;
                                            case 10 : $month = 'Oktober'; break;
                                            case 11 : $month = 'November'; break;
                                            case 12 : $month = 'Desember'; break;
                                        } 
                                       
                                       echo $date.' '.$month.' '.$year;
                                    ?>
         						</td>

         						<td>
                                	<a href="<?php echo site_url('administrator/kuesioner/detail_kuesioner/'.$data['_id']) ?>" class="btn btn-primary btn-emboss" > <i class="icon-eye"> </i>  Detail</a>
         						</td>
         					</tr>
         			<?php } // end foreach ?>
         		</tbody>
         	</table>
         </div>
 	</div>	
</div>