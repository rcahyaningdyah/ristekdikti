<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Laporan</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Laporan </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
			<h3><i class="icon-docs"></i><strong>Laporan</strong> Lembaga</h3>
		</div>

		<div class="panel-content" style="min-height:500px;">
			<table class="table table-dynamic table-tools">
				<thead>
					<tr> 
						<td> No </td>
						<td> Nama Lembaga</td>
						<td> Nama Kementrian</td>
						<td> Aksi</td>
					</tr>
				</thead>
				<tbody>
					<?php 
						 $no = $this->uri->segment(3)+1 ;
						foreach ($jawaban as $data){
					?>
						<tr>
							<td> <?php echo $no; $no = $no+1;?> </td>
							<td> <?php echo $data?></td>
							<td> 
								<?php
									$kementrian = $this->lembaga_m->get_lembaga_by_idlembaga($data);
									$id_kementrian = $kementrian[0]['id_kementrian'];
								
									$nama_kementrian = $this->kementrian_m->get_name_by_id($id_kementrian);
									echo $nama_kementrian[0]['nama_kementrian'];
								?>
							</td>
							<td> <a href="<?php echo site_url('administrator/laporan/laporan_lembaga/'.$data) ?>" class="btn btn-primary btn-emboss"> <i class="icon-eye"></i>Detail Laporan Lembaga</a> </td>
						</tr>
					<?php } // end foreach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>