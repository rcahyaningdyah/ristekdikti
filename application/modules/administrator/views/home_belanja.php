<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Belanja Litbang</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Belanja Litbang </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-basket-loaded"></i> <strong>Daftar Belanja Litbang </strong> </h3>
        </div>

        <div class="panel-content">
    	  	<?php
               $message = $this->session->flashdata('pesan');
                if(isset($message))
                {
                  echo $message;
                } 
            ?>
        	<table class="table table-dynamic table-tools">
        		<thead>
        			<tr>
        				<td> No </td>
        				<td> Identitas Pengisi &nbsp; </td>
        				<td> Nama Lembaga </td>
        				<td> Gaji/Upah + Tunjangan Peneliti</td>
        				<td> Gaji/Upah + Tunjangan Teknisi dan Staf Pendukung </td>
        				<td> Belanja Modal <br/>(tanah, gedung dan bangunan, kendaraan, mesin, dan peralatan)</td>
        				<td> Belanja sumber DIPA</td>
        			</tr>        			
        		</thead>

        		<tbody>
        			<?php 
        			  $no = $this->uri->segment(4)+1 ; 
        			  foreach ($belanja_lembaga as $data){ 
        			?>
        				<tr> 
        					<td> <?php echo $no; $no = $no+1;?>  </td>
        					<td> 
        						<?php
        						 	$string_id = (string) $data['id_user'];
        						 	$user = $this->users_m->get_user_byId($string_id);        						 	 
        						 ?>
        						 Nama : <?php echo $user[0]['q_1'] ?> <br/>
        						 NIP : <?php echo $user[0]['q_3'] ?> <br/>
        						 Jabatan : <?php echo $user[0]['q_2'] ?> <br/>
        						 Kementrian : <?php echo $user[0]['q_8'] ?> <br/>
        						 Lembaga/Badan    : <?php echo $user[0]['q_9']?> <br/>
        						 Balai/UPT	: <?php echo $user[0]['q_10'] ?>
        					</td>
        					<td> <?php echo $data['q_9'] ?> </td>
        					<td> 
        						<?php 
        							foreach ($data['q_35'] as $val){
        								echo "Rp " . number_format( $val , 2 , ',' , '.' ).",-"; 
        							}
        						?> 
        					</td>
        					<td> 
        						<?php 
        							foreach ($data['q_36'] as $val){
        								echo "Rp " . number_format( $val , 2 , ',' , '.' ).",-"; 
        							}
        						?> 
        					</td>
        					<td> 
        						<?php 
        							foreach ($data['q_37'] as $val){
        								echo "Rp " . number_format( $val , 2 , ',' , '.' ).",-"; 
        							}
        						?> 
        					</td>
        					<td> 
        						<?php 
        							foreach ($data['q_38'] as $val){
        								echo "Rp " . number_format( $val , 2 , ',' , '.' ).",-"; 
        							}
        						?> 
        					</td>
        				</tr>
        			<?php } ?>
        		</tbody>
        	</table>
        </div>
    </div>
</div>