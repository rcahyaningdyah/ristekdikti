<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Detail Kuesioner</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li > <a href="<?php echo site_url('administrator/kuesioner') ?>"> Kuesioner  </a> </li>
      <li class="active">Detail Kuesioner </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Detail Kuesioner "<?php echo $kegiatan[0]['q_27'] ?>"" </strong> </h3>
        </div>

        <div class="panel-content">
        	<?php 
        		 $no = $this->uri->segment(4)+1 ; 
        		foreach ($kegiatan as $data){

        			$string_id = (string)$data['id_user'];
        			$identitas_user = $this->users_m->get_user_byId($string_id);
        			
        	?>
        	<div class="row">
        		<div class="col-md-6">
        			<dl class="dl-horizontal">
        				<dt> Nama Kementrian  </dt>
	        			<dd> <?php echo $identitas_user[0]['q_8'] ?></dd>
	        			<dt> Nama Lembaga </dt>
	        			<dd>
	        				<?php
	        					$id_lembaga = $identitas_user[0]['q_9'];
	        					 $lembaga = $this->lembaga_m->get_lembaga_by_idlembaga($id_lembaga);
	        					 echo $lembaga[0]['nama_lembaga'];
	        				?>
	        			</dd>
	        			<dt> Nama Balai</dt>
	        			<dd> <?php echo $identitas_user[0]['q_10'] ?></dd>
	        			<dt> Tanggal Pengisian </dt>
	        			<dd> 
	        				 <?php 
	                                $tanggal_isi = date("d/m/Y",($data['created_at']->sec));
	                                $tanggal = explode("/", $tanggal_isi);
	                                $year= $tanggal[2];
	                                $month = $tanggal[1];
	                                $date = $tanggal[0];
	                                switch ($month){
	                                    case 1 : $month ='Januari'; break;
	                                    case 2 : $month ='Februari'; break;
	                                    case 3 : $month = 'Maret'; break;
	                                    case 4 : $month = 'April'; break;
	                                    case 5 : $month = 'Mei'; break;
	                                    case 6 : $month = 'Juni'; break;
	                                    case 7 : $month = 'Juli'; break;
	                                    case 8 : $month = 'Agustus'; break;
	                                    case 9 : $month = 'September'; break;
	                                    case 10 : $month = 'Oktober'; break;
	                                    case 11 : $month = 'November'; break;
	                                    case 12 : $month = 'Desember'; break;
	                                } 
	                                   
	                                echo $date.' '.$month.' '.$year;
	                            ?>
	        			</dd>
	        			<dt> Awal Kegiatan </dt>
	        			<dd> 
	        				<?php 
	                                $awal_kegiatan = date("d/m/Y",($data['q_33']->sec));
	                                $tanggal = explode("/", $awal_kegiatan);
	                                $year= $tanggal[2];
	                                $month = $tanggal[1];
	                                $date = $tanggal[0];
	                                switch ($month){
	                                    case 1 : $month ='Januari'; break;
	                                    case 2 : $month ='Februari'; break;
	                                    case 3 : $month = 'Maret'; break;
	                                    case 4 : $month = 'April'; break;
	                                    case 5 : $month = 'Mei'; break;
	                                    case 6 : $month = 'Juni'; break;
	                                    case 7 : $month = 'Juli'; break;
	                                    case 8 : $month = 'Agustus'; break;
	                                    case 9 : $month = 'September'; break;
	                                    case 10 : $month = 'Oktober'; break;
	                                    case 11 : $month = 'November'; break;
	                                    case 12 : $month = 'Desember'; break;
	                                } 
	                                 echo $date.' '.$month.' '.$year;                                 
	                            ?>
	        			</dd>
	        			<dt> Akhir Kegiatan </dt>
	        			<dd> 
	        				<?php 
	                                $akhir_kegiatan = date("d/m/Y",($data['q_34']->sec));
	                                $tanggal = explode("/", $akhir_kegiatan);
	                                $year= $tanggal[2];
	                                $month = $tanggal[1];
	                                $date = $tanggal[0];
	                                switch ($month){
	                                    case 1 : $month ='Januari'; break;
	                                    case 2 : $month ='Februari'; break;
	                                    case 3 : $month = 'Maret'; break;
	                                    case 4 : $month = 'April'; break;
	                                    case 5 : $month = 'Mei'; break;
	                                    case 6 : $month = 'Juni'; break;
	                                    case 7 : $month = 'Juli'; break;
	                                    case 8 : $month = 'Agustus'; break;
	                                    case 9 : $month = 'September'; break;
	                                    case 10 : $month = 'Oktober'; break;
	                                    case 11 : $month = 'November'; break;
	                                    case 12 : $month = 'Desember'; break;
	                                } 
	                                 echo $date.' '.$month.' '.$year;                                 
	                            ?>
	        			</dd>
        			</dl>
        		</div>

        		<div class="col-md-6">
        			<dl class="dl-horizontal">        			
	        			<dt> Jenis Penelitian </dt>
	        			<dd> <?php echo $data['q_29'] ?></dd>
	        			<dt> Kelompok Penelitian</dt>
	        			<dd> 
	        				<?php 
	        					echo $data['q_30']
	        				?>
	        			</dd>
	        			<dt> Bidang Penelitian</dt>
	        			<dd> 
	        				<?php        				
	        					echo $data['q_31']
	        				?>
	        			</dd>
	        			<dt> Sub Penelitian</dt>
	        			<dd> 
	        				<?php 
	        				 	 echo $data['q_32']
	        				?>
	        			</dd>
	        			<dt> Koordinator Penelitian</dt>
	        			<dd> <?php echo $data['q_28'] ?></dd>
	        		</dl>
        		</div>
        	</div>
        		
        		<strong>Personil Penelitian</strong> <br/>        
        		<em> <small> Tabel 1a.Sebutkan peneliti (fungsional peneliti, perekayasa, pranata nuklir, dan lain-lain) yang berperan sebagai peneliti dalam kegiatan litbang berdasarkan tingkat pendidikan dan bidang ilmu di unit
kerja Saudara tahun 2015 </small></em>
        		<table class="table">
        			<thead>
        				<tr>
        					<td> Nama </td>
        					<td> Jenis Kelamin </td>
        					<td> NIP</td>
        					<td> Tingkat Pendidikan</td>
        					<td> Bidang Ilmu</td>
        					<td> Fungsional</td>
        				</tr>
        			</thead>
        			<tbody>
        				<tr>        					
        					<td> 
        						<?php
        							foreach ($data['q_20'] as $kwn){
        								foreach ($kwn as $d){
        									if ($d == "WNI"){
        										//echo "wni";
        										foreach ($data['q_15'] as $nama){
			        								echo $nama."<br>";
			        							}

        									}
        									else {
        										echo "wna";
        									}
        								}
        							}

        							
        						 ?>
        					</td>
        					<td> 
        						<?php
        							/*foreach ($data['q_16'] as $jk){
        								foreach ($jk as $arr){
        									echo $arr."<br>";
        								}
        							}*/
        						 ?>
        					</td>
        					<td>
        						<?php
        							/*foreach ($data['q_17'] as $nip){
        								echo $nip."<br>";
        							}*/
        						 ?>
        					</td>
        					<td> 
        						<?php 
        							/*foreach ($data['q_18'] as $Pendidikan){
        								foreach ($Pendidikan as $arr){
        									echo $arr."<br>";
        								}
        							}*/
        						?>
        					</td>
        					<td>
        						<?php 
        							/*foreach ($data['q_25'] as $bid_ilmu){
        								foreach ($bid_ilmu as $arr){
        									echo $arr."<br>";
        								}
        							}*/
        						?>
        					</td>
        					<td>
        						<?php 
        							/*foreach ($data['q_19'] as $Fungsional){
        								foreach ($Fungsional as $arr){
        									echo $arr."<br>";
        								}
        							}*/
        						?>
        					</td>
        				</tr>
        			</tbody>
        		</table>

        		<em> <small> Tabel 1b. Sebutkan peneliti asing berdasarkan tingkat pendidikan dan bidang ilmu pada unit kerja Saudara tahun 2015 </em> </small> 
        		<table class="table">
        			<thead>
        				<tr>
        					<td> Nama </td>
        					<td> Kewarganegaraan</td>
        					<td> Asal Institusi</td>
        					<td> Jenis Kelamin</td>
        					<td> Tingkat Pendidikan</td>
        					<td> Bidang Ilmu</td>
        					<td> Fungsional </td>
        				</tr>
        			</thead>
        			<tbody>
        				
        			</tbody>
        		</table>
        	<?php } // end foreach ?>
      	</div>
	</div>
</div>