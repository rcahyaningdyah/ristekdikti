 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Tambah </strong> Institusi  </h2>
   <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li><a href="<?php echo site_url('administrator/institusi') ?>">Institusi</a> </li>
      <li class="active">Tambah Institusi</li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
        	<div class="col-md-8">
        		 <?php
                   $message = $this->session->flashdata('pesan');
                    if(isset($message))
                    {
                      echo $message;
                    } 
                  ?>
        		<form action="<?php echo site_url('administrator/institusi/simpan_kementrian') ?>" method="POST">
	        		<div class="form-horizontal ">
	        			<div class="form-group">
	        				<label class="col-md-3 control-label">Nama Kementrian </label>
	        				 <div class="col-sm-9">
	        				 	<input type="text" class="form-control col-sm-3" name="nama_kementrian" value="<?php echo set_value('nama_kementrian') ?>" id="nama_kementrian">
	        				 	<?php  echo form_error('nama_kementrian') ?>	
	        				 </div>
	        				
	        			</div>
	        			<div class="form-group">
	        				<label class="col-md-3 control-label">Alamat Kementrian </label>
	        				<div class="col-sm-9">
	        					<textarea class="form-control col-sm-6" name="alamat_kementrian"  value="<?php echo set_value('alamat_kementrian') ?>" id="alamat_kementrian">  </textarea>
	        						<?php  echo form_error('alamat_kementrian') ?>	
	        				</div>
	        				
	        			</div>
	        			<div class="form-group " style="margin-left: 258px;">
	                         <button type="submit"   class="btn btn-primary">Simpan</button>
	                    </div> 

	        		</div>
	        	</form>
        	</div>
        	
        </div>
    </div>
</div>