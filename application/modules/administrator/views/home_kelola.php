<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Tata Kelola</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Tata Kelola </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->
<div class="row panel">
	<div class="col-lg-12">
		<div class="panel-header panel-controls">
            <h3><i class="icon-users"></i> <strong>Tata Kelola User </strong> </h3>
        </div>

        <div class="panel-content">
                <?php
                   $message = $this->session->flashdata('pesan');
                    if(isset($message))
                    {
                      echo $message;
                    } 
                  ?>
            <a href="<?php echo site_url('administrator/kelola/tambah_admin') ?>" class="btn btn-primary"> <i class="icon-plus"> </i> Tambah Admin Lembaga </a>
            <table class="table table-dynamic table-tools">
                <thead>
                    <tr>
                        <td> No.</td>
                        <td> Nama Kementrian </td>
                        <td> Nama Lembaga </td>
                        <td> Identitas Admin </td>
                        <td> Aksi </td>
                    </tr>
                </thead>

                <tbody>
                    <?php 
                          $no = $this->uri->segment(4)+1 ; 
                        foreach ($user_admin as $data){                         
                          
                    ?>
                    <tr>
                        <td> <?php echo $no; $no = $no+1;?> </td>
                        <td><?php echo $data['q_8']; ?></td>
                        <td><?php echo $data['q_9']; ?></td>
                        <td>
                            Nama : <?php echo $data['q_1'] ?> <br/>
                            NIP : <?php echo $data['q_3']?> <br/>
                            Jabatan : <?php echo $data['q_2'] ?> <br/>
                            Telepon : <?php echo $data['q_4']?> <br/>
                            HP : <?php echo $data['q_5'] ?> <br/>
                            Fax : <?php echo $data['q_6'] ?> <br/>
                            Email : <?php echo $data['q_7'] ?> <br/>


                        </td>
                        <td> 
                            <a href="<?php echo site_url('administrator/kelola/detail_lembaga/'.$data['q_9']) ?>" class="btn btn-primary btn-emboss"> <i class="icon-eye"> </i> Details </a> 
                            <a href="<?php echo site_url('administrator/kelola/edit_lembaga_step1/'.$data['_id']) ?>" class="btn btn-success btn-emboss"> <i class="icon-pencil"> </i> Edit </a> 
                       </td>
                    </tr>
                    <?php  } // end foreach user_admin ?>
                </tbody>
            </table>    
        </div>
	</div>
</div>
