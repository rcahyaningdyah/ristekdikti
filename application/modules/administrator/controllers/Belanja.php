<?php
	class Belanja extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','users_m','lembaga_m','belanja_lembaga_m','kementrian_m','puslitbang_m'));
			$this->load->helper(array('form', 'url', 'file'));
			 if ($this->session->userdata('role')!= 'superuser'){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Belanja Litbang";
			$data['belanja_lembaga'] = $this->belanja_lembaga_m->get_all_file();
			$data['content'] = $this->load->view('home_belanja', $data, TRUE);
			
			$this->load->view('frame-template-superuser',$data);

		}

		
	}
?>