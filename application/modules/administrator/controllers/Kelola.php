<?php
	class Kelola extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','users_m','lembaga_m','belanja_lembaga_m','kementrian_m','puslitbang_m'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->form_validation->set_message('required', ' %s harap di isi.');
			$this->form_validation->set_message('valid_email','Harap memasukkan %s valid');
			$this->form_validation->set_message('numeric','Harap memasukkan %s berupa angka');
			$this->form_validation->set_message('valid_url', 'Harap memasukkan %s valid');
			 if ($this->session->userdata('role')!= 'superuser'){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Kelola";
			$data['user_admin'] = $this->users_m->get_data_role_admin();

			$data['content'] = $this->load->view('home_kelola', $data, TRUE);
			
			$this->load->view('frame-template-superuser',$data);

		}

		function detail_lembaga($id_lembaga){
			$id_lembaga = $this->uri->segment(4);
			$data['title'] = "Detail Lembaga";
			$data['user_lembaga'] = $this->users_m->get_data_by_lembaga($id_lembaga);

			$data['content'] = $this->load->view('kelola_detail_lembaga', $data, TRUE);

			$this->load->view('frame-template-superuser',$data);
		}

		function tambah_admin(){
			$data['title'] ="Tambah Admin";
			$data['bab'] = $this->bab_m->get_bab_1();
			$data['content'] = $this->load->view('kelola_tambah_admin', $data, TRUE);
			$this->load->view('frame-template-superuser', $data);
		}

		function tambah_admin_step2(){
			$temp_id = $this->uri->segment(4);
			if ($temp_id){
				$data['temp_id'] = $temp_id;
			}
			$data['title'] ="Tambah Admin";
			$data['bab'] = $this->bab_m->get_bab_2();
			$data['lembaga']		= $this->lembaga_m->get_all_file();
			$data['kementrian']		= $this->kementrian_m->get_all_file();
			$data['puslitbang']		= $this->puslitbang_m->get_all_file();
			$data['content'] = $this->load->view('kelola_tambah_admin_step2', $data, TRUE);
			$this->load->view('frame-template-superuser', $data);
		}

		function simpan_step1(){

			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_1','Nama Pengisi','required');	
			$this->form_validation->set_rules('q_2','Jabatan Pengisi','required');
			$this->form_validation->set_rules('q_3','NIP Pengisi','numeric|required');
			$this->form_validation->set_rules('q_4', 'Nomor Telepon Pengisi', 'numeric');
			$this->form_validation->set_rules('q_5', 'Nomor HP pengisi', 'numeric');
			$this->form_validation->set_rules('q_6', 'Nomor Fax pengisi', 'numeric');
			$this->form_validation->set_rules('q_7', 'Email', 'valid_email|required');

			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				            
            	$data['title']	 = "Tambah Admin";
				$data['bab'] = $this->bab_m->get_bab_1();
				
				$data['content'] = $this->load->view('kelola_tambah_admin', $data, TRUE);
				$this->load->view('frame-template-superuser', $data);
			}
			else{
				//cek email 
				$email = $this->input->post('q_7');
				$cek_email = $this->users_m->get_user($email);

				if ($cek_email){
					//email sudah ada, kembali ke halaman awal pendaftaran
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Email yang anda gunakan sudah terdaftar, silakan masukkan email lain.</div>');
					redirect('administrator/kelola/tambah_admin');
				}
				else{
					//insert ke collection user
					$fullname = $this->input->post('q_1');
					$arr_fullname = explode(' ', trim($fullname));
					$username = $arr_fullname[0];
					
					$data = array(
						//"created_at" => new MongoDate(),
						"joined_at" => new MongoDate(),
						"q_1" => $this->input->post('q_1'),
						"q_2" => $this->input->post('q_2'),
						"q_3" => $this->input->post('q_3'),
						"q_4" => $this->input->post('q_4'),
						"q_5" => $this->input->post('q_5'),
						"q_6" => $this->input->post('q_6'),
						"q_7" => $this->input->post('q_7'),
						"username" => $username
					);					
					
					$insert = $this->mongo_db->insert('users', $data); 
					
					if ($insert){
						$get_user = $this->users_m->get_user($email);
						foreach ($get_user as $data){
							$temp_id = $data['_id'];
							redirect('administrator/kelola/tambah_admin_step2/'.$temp_id);
						}
					}
					else{
						$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
						redirect('administrator/kelola/tambah_admin');
					}

				}
			}
		}

		function simpan_step2(){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_8', 'Nama Kementrian', 'required');
			$this->form_validation->set_rules('q_9','Nama Lembaga','required');	
			$this->form_validation->set_rules('q_10','Nama Pusat/Balai','required');
			$this->form_validation->set_rules('q_11','Alamat Pusat/Balai','required');
			$this->form_validation->set_rules('q_12', 'Nomor Telepon Pusat/Balai', 'numeric');
			$this->form_validation->set_rules('q_13', 'Alamat Website', 'required');
			$this->form_validation->set_rules('q_14', 'Nomor Fax Pusat/Balai', 'numeric');	
			
			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				$temp_id = $this->uri->segment(4);
				if ($temp_id){
					$data['temp_id'] = $temp_id;
				}
				$data['title'] ="Tambah Admin";
				$data['bab'] = $this->bab_m->get_bab_2();
				$data['lembaga']		= $this->lembaga_m->get_all_file();
				$data['kementrian']		= $this->kementrian_m->get_all_file();
				$data['puslitbang']		= $this->puslitbang_m->get_all_file();
				$data['content'] = $this->load->view('kelola_tambah_admin_step2', $data, TRUE);
				$this->load->view('frame-template-superuser', $data);
			}
			else{				
				$temp_id = $this->uri->segment(4);
				if ($temp_id){
					$data['temp_id'] = $temp_id;
				}

				$q_8 = $this->input->post('q_8');
				$q_8_name = $this->kementrian_m->get_name_by_id($q_8);
				$q_8_save = $q_8_name[0]['nama_kementrian'];

				$data = array (
						"q_8" => $q_8_save,
						"q_9" => $this->input->post('q_9'),
						"q_10" => $this->input->post('q_10'),
						"q_11" => $this->input->post('q_11'),
						"q_12" => $this->input->post('q_12'),
						"q_13" => $this->input->post('q_13'),
						"q_14" => $this->input->post('q_14'),
						"role" => "admin",
						"status" => "Sudah Dikonfirmasi"
						
					);
				
				$string_temp = (string)$temp_id;

				$update = $this->users_m->update_user($string_temp, $data);
				if ($update){
					// send email konfirmasi pass

					//get data email
					$get_user = $this->users_m->get_user_byId($string_temp);
					$email = $get_user[0]['q_7'];
					$nama = $get_user[0]['q_1'];

					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+,.?";
    				$password = substr( str_shuffle( $chars ), 0, 8 );

    				$this->load->library('email');

					$config['protocol']	='smtp';
					$config['smtp_host']='ssl://smtp.googlemail.com';
					$config['smtp_port']='465';
					$config['smtp_user']='rcahyaningdyah@gmail.com';
					$config['smtp_pass']='Rahadian010692';
					$config['charset'] = "iso-8859-1";
					$config['mailtype'] = "html";
					$config['newline'] = "\r\n";

					$this->email->initialize($config);

					$this->email->from('rcahyaningdyahgmail.com', 'Admin Sistem');
					$this->email->to($email); 
					$this->email->subject('Konfirmasi Pendaftaran Kuesioner Online Kemenristekdikti');
						$message="Hay $nama,<br><p>
								Terimakasih sudah melakukan pendaftaran Sistem, . Untuk masuk ke sistem
								silahkan login dengan 
								Username : ".$email."<br/>".
								"Password : ".$password."</p>";
								
					$this->email->message($message);

					if ($this->email->send()){
						
						$data_pass = array(
							"password" => sha1($password)
						);
						
						$update = $this->users_m->update_user($string_temp, $data_pass);
						
						$this->session->set_flashdata('pesan',' <div class="alert alert-success "><a class="close" data-dismiss="alert">×</a>Data admin lembaga berhasil dilakukan.</div>');
						redirect('administrator/kelola');
					}
					else{
						$this->session->set_flashdata('pesan',' <div class="alert alert-danger "><a class="close" data-dismiss="alert">×</a>Terjadi kesalahan saat mengirimkan email.</div>');
						redirect('administrator/kelola');

					}
				}
				else {
					$this->session->set_flashdata('pesan',' <div class="alert alert-danger "><a class="close" data-dismiss="alert">×</a>Terjadi kesalahan saat update data.</div>');
					redirect('administrator/kelola');
				}
			}

		}


		function edit_lembaga_step1($id_user){
			$id_user = $this->uri->segment(4);
			$data['title'] = "Edit Admin Lembaga";
			$string_id = (string)$id_user;
			$data['user_admin'] = $this->users_m->get_user_byId($string_id);
			$data['bab'] = $this->bab_m->get_bab_1();
			$data['lembaga']		= $this->lembaga_m->get_all_file();
			$data['kementrian']		= $this->kementrian_m->get_all_file();
			$data['puslitbang']		= $this->puslitbang_m->get_all_file();
			$data['content'] = $this->load->view('kelola_edit_lembaga_step1', $data, TRUE);

			$this->load->view('frame-template-superuser',$data);
		}

		function update_admin_step1($id_user){
			$id_user = $this->uri->segment('4');
			
			$this->load->library('form_validation');

			$this->form_validation->set_rules('q_1','Nama Pengisi','required');	
			$this->form_validation->set_rules('q_2','Jabatan Pengisi','required');
			$this->form_validation->set_rules('q_3','NIP Pengisi','numeric|required');
			$this->form_validation->set_rules('q_4', 'Nomor Telepon Pengisi', 'numeric');
			$this->form_validation->set_rules('q_5', 'Nomor HP pengisi', 'numeric');
			$this->form_validation->set_rules('q_6', 'Nomor Fax pengisi', 'numeric');
			$this->form_validation->set_rules('q_7', 'Email', 'valid_email|required');
			$this->form_validation->set_rules('q_8', 'Nama Kementrian', 'required');
			$this->form_validation->set_rules('q_9','Nama Lembaga','required');	
			$this->form_validation->set_rules('q_10','Nama Pusat/Balai','required');
			$this->form_validation->set_rules('q_11','Alamat Pusat/Balai','required');
			$this->form_validation->set_rules('q_12', 'Nomor Telepon Pusat/Balai', 'numeric');
			$this->form_validation->set_rules('q_13', 'Alamat Website', 'required');
			$this->form_validation->set_rules('q_14', 'Nomor Fax Pusat/Balai', 'numeric');	

			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				$this->edit_lembaga_step1($id_user);
			}
			else {

				$string_id = (string)$id_user;
				$data_user = $_POST;

				$update = $this->users_m->update_user($string_id, $data_user);

				if ($update){
					$this->session->set_flashdata('pesan',' <div class="alert alert-success "><a class="close" data-dismiss="alert">×</a>Perubahan data admin lembaga berhasil disimpan.</div>');
					redirect('administrator/kelola');
				}
				else{
					$this->session->set_flashdata('pesan',' <div class="alert alert-danger "><a class="close" data-dismiss="alert">×</a>Terjadi kesalahan saat menyimpan data.</div>');
					edirect('administrator/kelola');
				}
			}
		}
		
	}
?>