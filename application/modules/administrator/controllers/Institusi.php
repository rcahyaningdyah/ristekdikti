<?php
	class Institusi extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','users_m','lembaga_m','belanja_lembaga_m','kementrian_m','puslitbang_m'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->form_validation->set_message('required', ' %s tidak boleh kosong.');
			$this->form_validation->set_message('valid_email','Harap memasukkan %s valid');
			$this->form_validation->set_message('numeric','Harap memasukkan %s berupa angka');
			$this->form_validation->set_message('valid_url', 'Harap memasukkan %s valid');
			 if ($this->session->userdata('role')!= 'superuser'){
	            redirect ('login');
	        }
		}


		function index(){
			$data['title']	 = "Daftar Kementrian";
			$data['kementrian'] = $this->kementrian_m->get_all_file();

			$data['content'] = $this->load->view('home_institusi', $data, TRUE);
			
			$this->load->view('frame-template-superuser',$data);
		}

		function tambah_kementrian(){
			$data['title']	 = "Tambah Institusi";
			$data['kementrian'] = $this->kementrian_m->get_all_file();

			$data['content'] = $this->load->view('institusi_tambah', $data, TRUE);
			
			$this->load->view('frame-template-superuser',$data);
		}

		function simpan_kementrian(){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_kementrian','Nama kementrian','required');	
			$this->form_validation->set_rules('alamat_kementrian','Alamat kementrian','required');
			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				$this->tambah_kementrian();
			}
			else{
				$count_kementrian = $this->kementrian_m->count_kementrian();
				$id = $count_kementrian+1;
				$string_id = (string) $id;
				$data = array (
					"_id" => "K".$string_id,
					"nama_kementrian" => $this->input->post('nama_kementrian'),
					"alamat_kementrian" => $this->input->post('alamat_kementrian')
					);
			
				$insert_kementrian = $this->mongo_db->insert('kementrian',$data);
				if ($insert_kementrian){
					$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Kementrian berhasil ditambahkan.</div>');
					redirect('administrator/institusi');
				}	
				else{
					$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Kementrian gagal ditambahkan.</div>');
					redirect('administrator/institusi');
				}
				
			}
		}

		function detail_kementrian($id_kementrian){
			$id_kementrian = $this->uri->segment(4);
			
			$data['title'] = "Detail Kementrian";
			$data['id_kementrian'] = $id_kementrian;
			$data['kementrian'] = $this->kementrian_m->get_name_by_id($id_kementrian);
			$data['lembaga'] = $this->lembaga_m->get_lembaga_by_id($id_kementrian);
			$data['content'] = $this->load->view('institusi_detail_kementrian', $data, TRUE);

			$this->load->view('frame-template-superuser',$data);
		}


		function update_kementrian($id_kementrian){
			$id_kementrian = $this->uri->segment(4);

			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_kementrian','Nama kementrian','required');	
			$this->form_validation->set_rules('alamat_kementrian','Alamat kementrian','required');
			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Kementrian tidak lengkap, silakan cek kembali.</div>');
				redirect('administrator/institusi/detail_kementrian/'.$id_kementrian);
			}
			else{
				$count_kementrian = $this->kementrian_m->count_kementrian();
				$id = $count_kementrian+1;
				$string_id = (string) $id_kementrian;
				$data = array (
					
					"nama_kementrian" => $this->input->post('nama_kementrian'),
					"alamat_kementrian" => $this->input->post('alamat_kementrian')
					);
				echo $string_id; echo "<pre>"; var_dump($data);
			
				$update_kementrian = $this->kementrian_m->update_kementrian($string_id,$data);
				if ($update_kementrian){
					$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Perubahan Data Kementrian berhasil disimpan.</div>');
					redirect('administrator/institusi/detail_kementrian/'.$id_kementrian);
				}	
				else{
					$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Kementrian gagal diperbarui.</div>');
					redirect('administrator/institusi/detail_kementrian/'.$id_kementrian);
				}
				
			}
		}


		function tambah_lembaga($id_kementrian){
			$id_kementrian = $this->uri->segment(4);
			$data['title']	 = "Tambah Lembaga";
			$data['id_kementrian'] = $id_kementrian;
			$data['kementrian'] = $this->kementrian_m->get_name_by_id($id_kementrian);

			$data['content'] = $this->load->view('institusi_tambah_lembaga', $data, TRUE);
			
			$this->load->view('frame-template-superuser',$data);
		}

		function simpan_lembaga($id_kementrian){
			$id_kementrian = $this->uri->segment(4);

			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_lembaga','Nama Lembaga','required');	
			$this->form_validation->set_rules('inisial_lembaga','Inisial Lembaga','required');	
			$this->form_validation->set_rules('alamat_lembaga','Alamat Lembaga','required');
			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				$this->tambah_lembaga($id_kementrian);
			}
			else{

				//cek inisial hanya berisi satu kata
				if  ( preg_match('/\s/',$this->input->post('inisial_lembaga') )){
					$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Inisial lembaga hanya diperbolehkan SATU kata saja, silakan ulangi pengisian.</div>');
					redirect('administrator/institusi/tambah_lembaga/'.$id_kementrian);
				}
				else{
					$inisial = strtoupper($this->input->post('inisial_lembaga'));
					$cek_id = $this->lembaga_m->cek_id($inisial);

					if ($cek_id){
						$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Inisial lembaga sudah ada, silakan ulangi pengisian.</div>');
						redirect('administrator/institusi/tambah_lembaga/'.$id_kementrian);
					}
					else{
						$data_insert = array(
							"_id" 				=> $this->input->post('inisial_lembaga'),
							"nama_lembaga" 		=> $this->input->post('nama_lembaga'),
							"alamat_lembaga"	=> $this->input->post('alamat_lembaga'),
							"id_kementrian"     => $id_kementrian
						);

						$insert_lembaga = $this->mongo_db->insert('lembaga',$data_insert);
						if ($insert_lembaga){
							$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Lembaga berhasil ditambahkan.</div>');
							redirect('administrator/institusi/detail_kementrian/'.$id_kementrian);
						}	
						else{
							$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Lembaga gagal ditambahkan.</div>');
							redirect('administrator/institusi/tambah_lembaga/'.$id_kementrian);
						}
					}	
				}

			} // end else validation
		}

		function hapus_lembaga($id_lembaga){
			$id_lembaga = $this->uri->segment(4);
			$string_id = (string) $id_lembaga;
			$kementrian = $this->lembaga_m->get_lembaga_by_idlembaga($id_lembaga);
			$id_kementrian = $kementrian[0]['id_kementrian'];

			$get_puslitbang = $this->puslitbang_m->get_data_by_id($string_id);

			foreach ($get_puslitbang as $data){
				$string_litbang = (string) $data['_id'];
				$hapus_puslitbang = $this->puslitbang_m->delete_by_id($string_litbang);
			}

			$delete = $this->lembaga_m->delete_by_id($string_id);

			if ($delete){
				$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Lembaga berhasil dihapus.</div>');
				redirect('administrator/institusi/detail_kementrian/'.$id_kementrian);
				//echo "berhasil hapus";
			}
			else{
				$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Lembaga gagal dihapus.</div>');
				redirect('administrator/institusi/detail_kementrian/'.$id_kementrian);
				//echo "gagal hapus";
			}
		}

		function hapus_kementrian($id_kementrian){
			$id_kementrian = $this->uri->segment(4);
			$string_id = (string) $id_kementrian;

			//delete lembaga yang punya idkementrian sama
			$get_lembaga = $this->lembaga_m->get_lembaga_by_id($id_kementrian);

			foreach ($get_lembaga as $data){
				$id_lembaga = $data['_id']; 
				$string_lembaga = (string) $id_lembaga;
				$get_puslitbang = $this->puslitbang_m->get_data_by_id($id_lembaga);
				foreach ($get_puslitbang as $litbang){
					$string_id = (string) $litbang['_id'];
					$hapus_puslitbang = $this->puslitbang_m->delete_by_id($string_id);
				}
				$hapus_lembaga = $this->lembaga_m->delete_by_id($string_lembaga);
			}

			$delete = $this->kementrian_m->delete_by_id($string_id);
			
			if ($delete){
				$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Kementrian, Lembaga dan Puslitbang berhasil dihapus.</div>');
				redirect('administrator/institusi');
			}
			else{
				$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Kementrian, Lembaga dan Puslitbang gagal dihapus</div>');
				redirect('administrator/institusi');
			}
		}


		function detail_lembaga($id_lembaga){
			$id_lembaga = $this->uri->segment(4);
			$data['title'] = "Detail Lembaga";
			$data['id_lembaga'] = $id_lembaga;

			$lembaga = $this->lembaga_m->get_lembaga_by_idlembaga($id_lembaga);
			$data['lembaga'] = $lembaga;
			$data['id_kementrian'] = $lembaga[0]['id_kementrian'];
			$data['kementrian'] = $this->kementrian_m->get_name_by_id($lembaga[0]['id_kementrian']);
			$data['puslitbang'] = $this->puslitbang_m->get_data_by_id($id_lembaga);

			$data['content'] = $this->load->view('institusi_detail_lembaga', $data, TRUE);

			$this->load->view('frame-template-superuser',$data);
		}

		function update_lembaga($id_lembaga,$id_kementrian){
			$id_lembaga = $this->uri->segment(4);
			$id_kementrian = $this->uri->segment(5);

			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_lembaga','Nama Lembaga','required');	
			$this->form_validation->set_rules('inisial_lembaga','Inisial Lembaga','required');	
			$this->form_validation->set_rules('alamat_lembaga','Alamat Lembaga','required');
			
			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

			if ($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Lembaga tidak lengkap, silakan cek kembali.</div>');
				redirect('administrator/institusi/detail_lembaga/'.$id_lembaga);
			}
			else{
				//cek inisial hanya berisi satu kata
				if  ( preg_match('/\s/',$this->input->post('inisial_lembaga') )){
					$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Inisial lembaga hanya diperbolehkan SATU kata saja, silakan ulangi pengisian.</div>');
					redirect('administrator/institusi/detail_lembaga/'.$id_lembaga);
				}
				else{
					//update
					$inisial = strtoupper($this->input->post('inisial_lembaga'));
					
					$string_lembaga = (string) $id_lembaga;
					$data = array(
						"_id" 				=> $this->input->post('inisial_lembaga'),
						"nama_lembaga" 		=> $this->input->post('nama_lembaga'),
						"alamat_lembaga"	=> $this->input->post('alamat_lembaga'),
						"id_kementrian"     => $id_kementrian
					);


					$update_lembaga = $this->lembaga_m->update_lembaga($string_lembaga, $data);
					if ($update_lembaga){
						$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Perubahan data lembaga berhasil disimpan.</div>');
						redirect('administrator/institusi/detail_lembaga/'.$id_lembaga);
					}	
					else{
						$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Perubahan data lembaga gagal disimpan.</div>');
						redirect('administrator/institusi/detail_lembaga/'.$id_lembaga);
					}
				}
			} // end else validation
		}

		function simpan_puslitbang($id_lembaga,$id_puslitbang){
			$id_lembaga = $this->uri->segment(4);
			$id_puslitbang = $this->uri->segment(5);

			$get_puslitbang = $this->puslitbang_m->get_data_by_id($id_lembaga);

			$nama_puslitbang = ucwords($this->input->post('nama_puslitbang'));

			$puslitbang = $get_puslitbang[0]['puslitbang'];

			$data_puslitbang = array_push($puslitbang, $nama_puslitbang);

			$string_id = (string) $id_puslitbang;

			$data_update = array(
				'id_lembaga' => $id_lembaga,
				'puslitbang' => $puslitbang
				);

			$update_puslitbang = $this->puslitbang_m->update_puslitbang($string_id, $data_update);

			if ($update_puslitbang){
				$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Puslitbang berhasil ditambahkan.</div>');
				redirect('administrator/institusi/detail_lembaga/'.$id_lembaga);
			}
			else {
				$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Puslitbang gagal ditambahkan.</div>');
				redirect('administrator/institusi/detail_lembaga/'.$id_lembaga);
			}
		}


		function detail_puslitbang($id_puslitbang, $id_lembaga, $id_kementrian){
			$id_puslitbang = $this->uri->segment(4);
			$id_lembaga = $this->uri->segment(5);
			$id_kementrian = $this->uri->segment(6);
			$data['title'] = "Detail Puslitbang";
			$data['id_lembaga'] = $id_lembaga;
			$data['id_kementrian'] = $id_kementrian;
			$data['puslitbang'] = $this->puslitbang_m->get_data_by_id($id_lembaga);
			$data['content'] = $this->load->view('institusi_detail_puslitbang', $data, TRUE);

			$this->load->view('frame-template-superuser',$data);
		}


		function hapus_puslitbang($id_puslitbang,$nama_puslitbang){
			$id_puslitbang = $this->uri->segment(4);
			$nama_puslitbang = $this->uri->segment(5);

			$string_puslitbang = urldecode($nama_puslitbang);

			//get data puslitbang berdasar id puslitbang
			$puslitbang = $this->puslitbang_m-> get_all_by_id($id_puslitbang);
			$arr_puslitbang = $puslitbang[0]['puslitbang'];	
			echo $string_puslitbang;		
			
			$a = array_search($string_puslitbang, $arr_puslitbang);
			echo $a; reset($arr_puslitbang); unset($arr_puslitbang[$a]);
			 $data_update = array(
				 	'id_lembaga' => $puslitbang[0]['id_lembaga'],
				 	'puslitbang' => $arr_puslitbang

				 	);
			//update data puslitbang
			 $string_id = (string) $id_puslitbang;
			 $update_puslitbang = $this->puslitbang_m->update_puslitbang($string_id, $data_update);
			 if ($update_puslitbang){
			 	$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Puslitbang berhasil dihapus.</div>');
				redirect('administrator/institusi/detail_lembaga/'.$puslitbang[0]['id_lembaga']);
			 }
			 else{
			 	$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Data Puslitbang gagal dihapus.</div>');
				redirect('administrator/institusi/detail_lembaga/'.$puslitbang[0]['id_lembaga']);
			 }
		
			
		}

		function update_puslitbang($id_puslitbang,$id_lembaga,$id_kementrian){
			$id_puslitbang = $this->uri->segment(4);
			$id_lembaga = $this->uri->segment(5);
			$id_kementrian = $this->uri->segment(6);

			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_puslitbang[]','Nama Puslitbang','required');


			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger " style="margin-top:10px">', '</div>');
			
			if ($this->form_validation->run() == FALSE) {
				$this->detail_puslitbang($id_puslitbang, $id_lembaga, $id_kementrian);
			}
			else{
				$data_puslitbang = $this->puslitbang_m->get_all_by_id($id_puslitbang);
			
				foreach($data_puslitbang as $data){
					
					$data_update = array(
						"id_lembaga" => $data['id_lembaga'],
						"puslitbang" => $_POST['nama_puslitbang']
					);
					$string_id = (string) $id_puslitbang;
					$update = $this->puslitbang_m->update_puslitbang($string_id, $data_update);
					if ($update){
						$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Perubahan Data Puslitbang berhasil disimpan.</div>');
						redirect('administrator/institusi/detail_lembaga/'.$data['id_lembaga']);
					}
					else{
						$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Perubahan Data Puslitbang gagal disimpan.</div>');
						redirect('administrator/institusi/detail_lembaga/'.$data['id_lembaga']);
					}
				} // end foreach data_puslitbang
			}

			
			
		}

	}
?>