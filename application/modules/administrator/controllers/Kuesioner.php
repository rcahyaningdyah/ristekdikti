<?php
	class Kuesioner extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->model(array('kementrian_m','kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','users_m','lembaga_m','bidang_penelitian_m','penelitian_m','sub_penelitian_m','ilmu_m','kelompok_ilmu_m'));
			$this->load->helper(array('form', 'url', 'file'));
			 if ($this->session->userdata('role')!= 'superuser'){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Kuesioner";
			$data['lembaga'] = $this->lembaga_m->get_all_file();
			$data['kegiatan']= $this->jawaban_m->get_all_file();
			$data['content'] = $this->load->view('home_kuesioner', $data, TRUE);			
			$this->load->view('frame-template-superuser',$data);

		}

		// function detail_kuesioner($id_kegiatan){
		// 	$id_kegiatan = $this->uri->segment(4);
		// 	$data['title'] = "Detail Kuesioner";
		// 	//get data kegiatan
		// 	$string_id = (string) $id_kegiatan;
		// 	$data['kegiatan'] = $this->jawaban_m->kegiatan_byid($string_id);
		// 	$data['content'] = $this->load->view('kuesioner_detail', $data, TRUE);			
		// 	$this->load->view('frame-template-superuser',$data);
		// }

		
	}
?>