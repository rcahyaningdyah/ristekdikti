<?php
	class Laporan extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->model(array('kuesioner_m','kementrian_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','users_m','lembaga_m','bidang_penelitian_m','penelitian_m','sub_penelitian_m','ilmu_m','kelompok_ilmu_m'));
			$this->load->helper(array('form', 'url', 'file'));
			 if ($this->session->userdata('role')!= 'superuser'){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Laporan";
			//get lembaga yang sudah mengisi kuesione
			$data['jawaban'] = $this->jawaban_m-> get_distinct_lembaga();
			$data['content'] = $this->load->view('home_laporan', $data, TRUE);
			
			$this->load->view('frame-template-superuser',$data);

		}

		function laporan_lembaga($id_lembaga){
			$id_lembaga = $this->uri->segment('4');
			
			$data['title'] = "Detail Laporan Lembaga";
			$data['id_lembaga'] = $id_lembaga;
			$data['get_lembaga'] = $this->users_m->get_data_by_lembaga($id_lembaga);
			$data['nama_kementrian'] = $data['get_lembaga'][0]['q_8'];
			$data['content'] = $this->load->view('laporan_lembaga', $data, TRUE);
			$this->load->view('frame-template-superuser',$data);
		}

		function cetak($id_lembaga){
			$id_lembaga = $this->uri->segment('4');
			//get lembaga yang sama di collection users
			$data['get_lembaga'] = $this->users_m->get_data_by_lembaga($id_lembaga);
			$data['title']	 = "Laporan";
			$data['id_lembaga'] = $id_lembaga;
			$data_admin = $this->users_m->get_data_admin_by_lembaga($id_lembaga);
			$id_admin = $data_admin[0]['_id'];
			$data['id_admin'] = (string) $id_admin;
			
			$data['content'] = $this->load->view('laporan_cetak', $data, TRUE);
			
			$this->load->view('frame-template-superuser',$data);
		}

		
	}
?>