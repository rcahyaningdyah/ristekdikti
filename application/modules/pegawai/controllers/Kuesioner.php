<?php
	class Kuesioner extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->load->model(array('belanja_lembaga_m','jawaban_temp_m','kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','users_m','lembaga_m','bidang_penelitian_m','penelitian_m','sub_penelitian_m','ilmu_m','kelompok_ilmu_m'));
			if ($this->session->userdata('role')!= 'pegawai'){
	            redirect ('login');
	        }
	        $this->form_validation->set_message('required', ' %s harap di isi.');
			$this->form_validation->set_message('valid_email','Harap memasukkan %s valid');
			$this->form_validation->set_message('numeric','Harap memasukkan %s berupa angka');
			$this->form_validation->set_message('valid_url', 'Harap memasukkan %s valid');
			$this->form_validation->set_message('max_length', '%s: Maksimal panjang karakter adalah %s');
		}

		function index($id=''){
			$user_id = $this->session->userdata('user_id');
			$user=(string)$user_id;
			$data['title']	 = "Kuesioner";
			$data['kegiatan_temp']= $this->jawaban_temp_m->get_all_kegiatan($user);
			$data['kegiatan']= $this->jawaban_m->get_all_kegiatan($user);
			// echo "<pre>";var_dump($data);exit;
			$data['content'] = $this->load->view('form_kuesioner', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function kuesioner1($id=''){
			$data['title']	 = "Isi Kuesioner 1";
			$data['bab_4'] = $this->bab_m->get_bab_4();		
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['bidang_penelitian'] = $this->bidang_penelitian_m->get_all_file();
			$data['kelompok_penelitian'] = $this->penelitian_m->get_all_file();
			$data['sub_penelitian'] = $this->sub_penelitian_m->get_all_file();
			$data['content'] = $this->load->view('kuesioner_bab1', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner1($id=''){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_27','Nama Kegiatan','required');
			$this->form_validation->set_rules('q_28','Nama Koordinator Kegiatan','required');
			$this->form_validation->set_rules('q_29','Jenis Penelitian','required');
			$this->form_validation->set_rules('q_30','Jenis Kelompok Penelitian','required');
			$this->form_validation->set_rules('q_31','Bidang Penelitian','required');
			$this->form_validation->set_rules('q_32','Sub Bidang Penelitian','required');
			$this->form_validation->set_rules('q_32-other', 'Sub Bidang Penelitian lain');
			$this->form_validation->set_rules('q_33','Awal Penelitian','required');
			$this->form_validation->set_rules('q_34','Akhir penelitian','required');

			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:10px">', '</div>');

			if ($this->form_validation->run() == FALSE) {
				$data['title']	 = "Isi Kuesioner 1";
				$data['bab_4'] = $this->bab_m->get_bab_4();		
				$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
				$data['bidang_penelitian'] = $this->bidang_penelitian_m->get_all_file();
				$data['kelompok_penelitian'] = $this->penelitian_m->get_all_file();
				$data['sub_penelitian'] = $this->sub_penelitian_m->get_all_file();
				$data['content_js'] = $this->load->view('kuesioner_js',$data, TRUE);
				$data['content'] = $this->load->view('kuesioner_bab1', $data, TRUE);
				$this->load->view('frame-template-pegawai',$data);
				
			}else{
				$cek_kegiatan = $this->jawaban_temp_m->cek_kegiatan();

				if ($cek_kegiatan==null){
					$q_30 = $this->input->post('q_30');
					$q_30_name = $this->penelitian_m->get_name_by_id($q_30);
					$q_30_save = $q_30_name[0]['nama_kelompok']; 

					$q_31 = $this->input->post('q_31');
					$q_31_name = $this->bidang_penelitian_m->get_nama_by_id($q_31);
					$q_31_save = $q_31_name[0]['nama_bidang'];
					

					$q_32 = $this->input->post('q_32');
					$arr = explode("-", $q_32);
					$q_32_name = $arr[1];
					$a = $this->input->post('q_32-other');
					if ($a == null) { $a = $q_32; } else {$a = $this->input->post('q_32-other');}
					$data = array (
						'id_user' => $this->session->userdata('user_id'),
						'id_lembaga' => $this->session->userdata('lembaga'),
						'q_27' => $this->input->post('q_27'),
						'q_28' => $this->input->post('q_28'),
						'q_29' => $this->input->post('q_29'),
						'q_30' => $q_30_save,
						'q_31' => $q_31_save,
						'q_32' => $a,
						//'q_32-other' =>$this->input->post('q_32-other'),
						'q_33' => (new MongoDate(strtotime($this->input->post('q_33')))),
						'q_34' => (new MongoDate(strtotime($this->input->post('q_34')))),
						);
					
					$insert_kegiatan = $this->mongo_db->insert('jawaban_temp',$data);
					$nama_kegiatan = $this->input->post('q_27');
					$get_id = $this->jawaban_temp_m->get_kegiatan($nama_kegiatan);
					
					foreach ($get_id as $data) {}
						$id_kegiatan = $data['_id'];
						redirect('./pegawai/kuesioner/kuesioner2/'.$id_kegiatan);
				}else{
						$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
						redirect('./pegawai/kuesioner/kuesioner1');
					}
				
			}
		}

		function kuesioner2($id=''){
			$id_kegiatan = $this->uri->segment(4);

			if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
			$data['title']	 = "Isi Kuesioner 2";
			$data['bab_3'] = $this->bab_m->get_bab_3();		
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['sub_kelompok_ilmu'] = $this->kelompok_ilmu_m->get_all_file();
			$data['content_js'] = $this->load->view('kuesioner_js',$data, TRUE);
			$data['content'] = $this->load->view('kuesioner_bab2', $data, TRUE);

			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner2($id=''){
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_15[]','Nama Personil Litbang','required');
			$this->form_validation->set_rules('q_16[]','Jenis Kelamin','required');
			$this->form_validation->set_rules('q_17[]','NIP','max_length[18]|numeric|required');
			$this->form_validation->set_rules('q_18[]','Tingkat Pendidikan','required');
			$this->form_validation->set_rules('q_19[]','Jabatan Penelitian','required');
			$this->form_validation->set_rules('q_20[]','Warga Negara','required');
			$this->form_validation->set_rules('q_21[]','Negara Asal','required');
			$this->form_validation->set_rules('q_22[]','Jenis Institusi Asal','required');
			$this->form_validation->set_rules('q_23[]','NamaInstitusi Asal','required');
			$this->form_validation->set_rules('q_25[]','Sub Bidang Ilmu','required');
			$this->form_validation->set_rules('q_26[]','Lama waktu penelitian Setiap Minggu','required');

			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:10px">', '</div>');
			if ($this->form_validation->run() == FALSE) {
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
				$data['title']	 = "Isi Kuesioner 2";
				$data['bab_3'] = $this->bab_m->get_bab_3();		
				$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
				$data['sub_kelompok_ilmu'] = $this->kelompok_ilmu_m->get_all_file();
				$data['content_js'] = $this->load->view('kuesioner_js',$data, TRUE);
				$data['content'] = $this->load->view('kuesioner_bab2', $data, TRUE);
				$this->load->view('frame-template-pegawai',$data);
			}else{
				//cek id kegiatan 
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id'] = $id_kegiatan;
				}

				//get bidang ilmu
				$q_25_name=array();
				$q_25 = $this->input->post('q_25[]');
				foreach ($q_25 as $id_ilmu) {
					$b = $this->kelompok_ilmu_m->get_name_by_id($id_ilmu);
					$q_25_name[]= $b[0]['nama_kelompok'];
					
				}
				
				// echo "<pre>";var_dump($q_25_name);exit();
				$q_16=array(); $q_18=array(); $q_19=array(); $q_20=array(); $q_21=array(); $q_22=array(); $q_25=array(); 
				$q_16[] = $this->input->post('q_16[]');
				$q_18[] = $this->input->post('q_18[]');
				$q_19[] = $this->input->post('q_19[]');
				$q_20[] = $this->input->post('q_20[]');
				$q_21[] = $this->input->post('q_21[]');
				$q_22[] = $this->input->post('q_22[]');
				$q_25_1[] = $q_25_name;
				$data=array(
					'created_at' => new MongoDate(),
					'q_15' => $this->input->post('q_15[]'),
					'q_16' => $q_16,
					'q_17' => $this->input->post('q_17[]'),
					'q_18' => $q_18,
					'q_19' => $q_19,
					'q_20' => $q_20,
					'q_21' => $q_21,
					'q_22' => $q_22,
					'q_23' => $this->input->post('q_23[]'),
					'q_25' => $q_25_1,
					'q_26' => $this->input->post('q_26[]')
					);
				// echo "<pre>";var_dump($data);exit;
				$string_id=(string)$id_kegiatan;
				$update_answer = $this->jawaban_temp_m->update_jawaban($string_id,$data);
				if($update_answer){
					redirect('./pegawai/kuesioner/kuesioner3/'.$string_id);
				}else{
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('./pegawai/kuesioner/kuesioner2/'.$string_id);
				}
			}
		}

		function  kuesioner3($id=''){
			$id_kegiatan = $this->uri->segment(4);
			if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
			$data['title']	 = "Isi Kuesioner 3";
			$data['bab'] = $this->bab_m->get_bab_6();		
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['content'] = $this->load->view('kuesioner_bab3', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner3($id=''){
			// echo "<pre>";var_dump($_POST);exit;
			$this->load->library('form_validation');
			$this->form_validation->set_rules('q_39[]','Sumber Instansi Belanja Kerjasama','required');
			$this->form_validation->set_rules('q_40[]','Jenis Instansi Belanja Kerjasama','required');
			$this->form_validation->set_rules('q_41[]','Nama Instansi','required');
			$this->form_validation->set_rules('q_42[]','Jumlah Dana','required');

			$this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:10px">', '</div>');
			if ($this->form_validation->run() == FALSE) {
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
				$data['title']	 = "Isi Kuesioner 3";
				$data['bab'] = $this->bab_m->get_bab_6();		
				$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
				$data['content'] = $this->load->view('kuesioner_bab3', $data, TRUE);
				$this->load->view('frame-template-pegawai',$data);
			}else{
				//cek id kegiatan 
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id'] = $id_kegiatan;
				}
				$data=array(
					'q_39' => $this->input->post('q_39[]'),
					'q_40' => $this->input->post('q_40[]'),
					'q_41' => $this->input->post('q_41[]'),
					'q_42' => array_map('doubleval', $this->input->post('q_42[]'))
					);
				// echo "<pre>";var_dump($data);exit;
				$string_id=(string)$id_kegiatan;
				
				$update_answer = $this->jawaban_temp_m->update_jawaban($string_id,$data);
				if($update_answer){
					redirect('./pegawai/kuesioner/kuesioner4/'.$string_id);
				}else{
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('./pegawai/kuesioner/kuesioner3/'.$string_id);
				}
			}
		}

		function kuesioner4($id=''){
			$id_kegiatan = $this->uri->segment(4);
			if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
			$data['title']	 = "Isi Kuesioner 4";
			$data['bab'] = $this->bab_m->get_bab_7();		
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['content'] = $this->load->view('kuesioner_bab4', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner4($id=''){
				//cek id kegiatan 
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id'] = $id_kegiatan;
				}
				$data=array(
					'q_43' => $this->input->post('q_43[]'),
					'q_44' => $this->input->post('q_44[]'),
					'q_45' => array_map('doubleval', $this->input->post('q_45[]'))
					);;
				$string_id=(string)$id_kegiatan;
			//echo "<pre>";var_dump($data);exit;
				$update_answer = $this->jawaban_temp_m->update_jawaban($string_id,$data);
				if($update_answer){
					redirect('./pegawai/kuesioner/kuesioner5/'.$string_id);
				}else{
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('./pegawai/kuesioner/kuesioner4/'.$string_id);
				}
		}

		function  kuesioner5($id=''){
			$id_kegiatan = $this->uri->segment(4);
			if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
			$data['title']	 = "Isi Kuesioner 5";
			$data['bab'] = $this->bab_m->get_bab_8();	
			$data['kegiatan'] = $this->jawaban_m->get_kegiatan_by_id($id_kegiatan);	
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['content'] = $this->load->view('kuesioner_bab5', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner5(){
			// var_dump($_POST);exit;
				//cek id kegiatan 
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id'] = $id_kegiatan;
				}
				//get identitas penulis base on delimter input
				$penulis_jurnal =array();
				$penulis=$this->input->post('q_48[]');
				foreach ($penulis as $key => $value) {
					$penulis_jurnal[] = explode(';',$value);//show as array
					
				}
				$gender_penulis=array();
				$gender=$this->input->post('q_49[]');
				foreach ($gender as $key => $value) {
					$gender_penulis[] = explode(';',$value);
				}
				$tingkat_penulis=array();
				$tingkat=$this->input->post('q_50[]');
				foreach ($tingkat as $key => $value) {
					$tingkat_penulis[] = explode(';',$value);
				}
				
				$data=array(
					'q_46' => $this->input->post('q_46[]'),
					'q_47' => $this->input->post('q_47[]'),
					'q_48' => $penulis_jurnal,
					'q_49' => $gender_penulis,
					'q_50' => $tingkat_penulis,
					'q_51' => $this->input->post('q_51[]'),
					'q_52' => $this->input->post('q_52[]'),
					'q_53' => $this->input->post('q_53[]'),
					'q_54' => $this->input->post('q_54[]')
					);
				$string_id=(string)$id_kegiatan;
				// echo "<pre>";var_dump($data);exit;
				$update_answer = $this->jawaban_temp_m->update_jawaban($string_id,$data);
				if($update_answer){
					redirect('./pegawai/kuesioner/kuesioner6/'.$string_id);
				}else{
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('./pegawai/kuesioner/kuesioner5/'.$string_id);
				}
		}

		function kuesioner6($id=''){
			$id_kegiatan = $this->uri->segment(4);
			if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}

			$data['title']	 = "Isi Kuesioner 6";
			$data['bab'] = $this->bab_m->get_bab_9();		
			$data['kegiatan'] = $this->jawaban_m->get_kegiatan_by_id($id_kegiatan);	
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['content'] = $this->load->view('kuesioner_bab6', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner6($id=''){
				//cek id kegiatan 
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id'] = $id_kegiatan;
				}

				$data=array(
					'q_55' => $this->input->post('q_55[]'),
					'q_56' => $this->input->post('q_56[]'),
					'q_57' => $this->input->post('q_57[]'),
					'q_58' => $this->input->post('q_58[]')
					);
				$string_id=(string)$id_kegiatan;
				// echo "<pre>";var_dump($data);exit;
				$update_answer = $this->jawaban_temp_m->update_jawaban($string_id,$data);
				if($update_answer){
					redirect('./pegawai/kuesioner/kuesioner7/'.$string_id);
				}else{
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('./pegawai/kuesioner/kuesioner6/'.$string_id);
				}

		}

		function kuesioner7($id=''){
			$id_kegiatan = $this->uri->segment(4);
			if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
			$data['title']	 = "Isi Kuesioner 7";
			$data['bab'] = $this->bab_m->get_bab_10();		
			$data['kegiatan'] = $this->jawaban_m->get_kegiatan_by_id($id_kegiatan);	
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['content'] = $this->load->view('kuesioner_bab7', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner7(){
				//cek id kegiatan 
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id'] = $id_kegiatan;
				}
				$data=array(
					'q_59' => $this->input->post('q_59[]'),
					'q_60' => $this->input->post('q_60[]'),
					'q_61' => $this->input->post('q_61[]'),
					'q_62' => $this->input->post('q_62[]'),
					'q_63' => $this->input->post('q_63[]')
					);
				$string_id=(string)$id_kegiatan;
				
				$update_answer = $this->jawaban_temp_m->update_jawaban($string_id,$data);
				if($update_answer){
					redirect('./pegawai/kuesioner/kuesioner8/'.$string_id);
				}else{
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('./pegawai/kuesioner/kuesioner7/'.$string_id);
				}
			
		}

		function kuesioner8($id=''){
			$id_kegiatan = $this->uri->segment(4);
			if ($id_kegiatan){
					$data['id_keg'] = $id_kegiatan;
				}
			$data['title']	 = "Isi Kuesioner 8";
			$data['bab'] = $this->bab_m->get_bab_11();		
			$data['pertanyaan'] = $this->pertanyaan_m->get_pertanyaan($id);
			$data['content'] = $this->load->view('kuesioner_bab8', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function simpan_kuesioner8(){
			
				//cek id kegiatan 
				$id_kegiatan = $this->uri->segment(4);
				if ($id_kegiatan){
					$data['id'] = $id_kegiatan;
				}

				$data=$_POST;
				$string_id=(string)$id_kegiatan;
				
				$update_answer = $this->jawaban_temp_m->update_jawaban($string_id,$data);
				if($update_answer){	
					//cek belanja lembaga
					$belanja_lembaga = $this->belanja_lembaga_m->get_lembaga_by_id($this->session->userdata('lembaga'));	
					
					if ($belanja_lembaga){
						foreach ($belanja_lembaga as $data_belanja){
							
							$arr_b = array(
									"q_35" => $data_belanja['q_35'],
									"q_36" => $data_belanja['q_36'],
									"q_37" => $data_belanja['q_37'],
									"q_38" => $data_belanja['q_38'],
								);

						}	
						
						$insert_belanja =$this->jawaban_temp_m->update_jawaban($string_id,$arr_b);
						
					}

					//get jawaban di jawaban_temp
					$data_temp = $this->jawaban_temp_m->get_temp($string_id);
					$arr = array();		

					

					foreach ($data_temp as $temp){
						$data_temp = array_slice($temp, 1);
						
						if(array_key_exists('q_64', $data_temp)){
							//insert ke jawaban
							$insert_jawaban = $this->mongo_db->insert('jawaban',$data_temp);

							//delete jawaban_temp
							$delete_jawaban_temp = $this->jawaban_temp_m->delete_by_id($string_id);
						}											

					}

					
					$this->session->set_flashdata('pesan_success',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Terimakasih. Kuesioner telah berhasil disimpan</div>');
					redirect('./pegawai/kuesioner');
				}else{
					$this->session->set_flashdata('pesan_error',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Error occuring when saving data.</div>');
					redirect('./pegawai/kuesioner/kuesioner7/'.$string_id);
				}
			
		}

		function hapus_kegiatan_temp($temp_id){
			$temp_id = $this->uri->segment(4);
			$string_temp = (string) $temp_id;
			$delete_temp = $this->jawaban_temp_m->delete_by_id($string_temp);
			if ($delete_temp){
				$this->session->set_flashdata('pesan',' <div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Data Kuesioner berhasil dihapus, silakan Tambah Kegiatan baru.</div>');
				redirect('./pegawai/kuesioner/');
			}
			else{
				$this->session->set_flashdata('pesan',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Terjadi kesalahan menghapus data.</div>');
				redirect('./pegawai/kuesioner/');
			}
		}

		function update_kegiatan($id=''){
			$id = $this->uri->segment(4);		
			// echo "<pre>";var_dump($data['details']);exit;
			$data['title']	 = "Q-online Litbang Kemenristek";
			$data['bab_4'] = $this->bab_m->get_bab_4();	
			$data['details']= $this->jawaban_m->kegiatan_byid($id);
			$data['content'] = $this->load->view('update_kuesioner_bab1', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function tes(){
			$kelompok_penelitian = $this->penelitian_m->get_all_file();
			foreach ($kelompok_penelitian as $data){				
				echo $data['nama_kelompok']."<br/><br/>";
				$bidang_penelitian = $this->bidang_penelitian_m->get_data_by_id($data['_id']);
				foreach ($bidang_penelitian as $bid){
					echo $bid['nama_bidang']."<br/>";
					$sub_penelitian = $this->sub_penelitian_m->get_data_by_id($bid['_id']);
					foreach ($sub_penelitian as $sub){
						foreach ($sub['penelitian'] as $key => $value){
							echo $value."<br/>";
						}						
					}
				}	
			}
		}

		function get_bidang_penelitian($id_penelitian){
			//$id_penelitian = "penelitian_1";
			$bidang_penelitian = $this->bidang_penelitian_m->get_data_by_id($id_penelitian);

			foreach ($bidang_penelitian as $data){
				$value[$data['_id']] = $data['nama_bidang'];
			}
			echo json_encode(array('bidang_penelitian' => $value));
			
			
		}

		function get_sub_penelitian($id_bidpen){
			//$id_bidpen = $this->uri->segment(3);
			//$id_bidpen = "bidpen_9";
			$sub_pen = $this->sub_penelitian_m->get_data_by_id($id_bidpen);

			foreach ($sub_pen as $data){
				foreach ($data['penelitian'] as $val){
					$value[$val] = $val;
				}
				//$value[$data['_id']] = $data['penelitian'];
			}
			echo json_encode(array('sub_penelitian' => $value));
		}

		function get_sub_ilmu($id_ilmu){
			// $id_ilmu = "ilmu_9";
			$sub_ilmu = $this->kelompok_ilmu_m->get_data_by_id($id_ilmu);
			// echo "<pre>";var_dump($sub_ilmu);exit;
			foreach ($sub_ilmu as $data){
				$value[$data['_id']] = $data['nama_kelompok'];
			}
			echo json_encode(array('sub_kelompok_ilmu' => $value));
		}



	}
?>