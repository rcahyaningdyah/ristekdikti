<?php
	class Profil extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','lembaga_m','users_m'));
			$this->form_validation->set_message('matches', ' %s tidak sesuai dengan %s');
			if ($this->session->userdata('role')!= 'pegawai'){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Profil Pegawai";
			//get data profil dari user berdasar user_id
			$data['detail_user'] = $this->users_m->get_user_byId($this->session->userdata('user_id'));
			$data['content'] = $this->load->view('halaman_profile', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}

		function ubah_password(){
			$this->load->library('form_validation');
                $this->form_validation-> set_rules('pass_lama','Password Lama','required');
                $this->form_validation-> set_rules('password','Password Baru','required|matches[konfirm_pass_baru]');
                $this->form_validation-> set_rules('konfirm_pass_baru','Konfirmasi Password Baru','required');

                $this->form_validation->set_error_delimiters('<div class="alert media fade in alert-danger" style="margin-top:41px">', '</div>');

           if ($this->form_validation->run() == FALSE){
           		$data['title']	 = "Q-online Litbang Kemenristek";
				//get data profil dari user berdasar user_id
				$data['detail_user'] = $this->users_m->get_user_byId($this->session->userdata('user_id'));
				$data['content'] = $this->load->view('halaman_profile', $data, TRUE);
				$this->load->view('frame-template-pegawai',$data);
           }
           else{
           		
                $pass_lama = $this->input->post('pass_lama');
                $pass_baru = $this->input->post('password');  
                $konf_pass_baru = $this->input->post('konfirm_pass_baru');
                //cek password baru dengan konfirm_pass_baru
                if ($pass_baru == $konf_pass_baru){
                		$cek_pass = $this->users_m->cek_password_by_user($pass_lama);

                    if ( $cek_pass == true){
                        //update password di collection users
                        $data = array(
                        		"password" => sha1($pass_baru)
                        	);
                        $this->users_m->update_password_by_user($data);

                         $this->session->set_flashdata('sukses','<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a> Perubahan password anda berhasil disimpan. </div>');
                          redirect('pegawai/Profil');
                    }
                    else {
                       // echo "kesalahan pada pass lama";
                         $this->session->set_flashdata('gagal','<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Terjadi kesalahan dengan password lama, silakan ulangi kembali. </div>');
                         redirect('pegawai/Profil');
                    }
                }
                else{
                   // echo "pass baru dan konfirm harus sama";
                     $this->session->set_flashdata('gagal','<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a> Konfirmasi password harus sama dengan password baru!</div>');
                     redirect('pegawai/Profil');
                }
           }
            
		}
	}
?>