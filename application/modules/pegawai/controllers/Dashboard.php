<?php
	class Dashboard extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			 if ($this->session->userdata('role')!= 'pegawai'){
	            redirect ('login');
	        }
		}

		function index(){
			// echo "tes";
			$data['title']	 = "Dashboard Pegawai";
			$data['content'] = $this->load->view('home_pegawai_v', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);
		}
	}
?>
