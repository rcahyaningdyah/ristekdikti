<?php
	class Pegawai extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m'));
			if ($this->session->userdata('role')!= 'pegawai'){
	            redirect ('login');
	        }
		}

		function index(){
			// echo "tes";
			$data['title']	 = "Pegawai";
			$data['bab'] = $this->bab_m->get_bab();
			$data['register'] = $this->bab_m->get_bab_daftar();
			$data['content'] = $this->load->view('pegawai_v', $data, TRUE);
			$this->load->view('frame-template-pegawai',$data);

		}

		

	}
?>
