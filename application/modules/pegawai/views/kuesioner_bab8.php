 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Form Tanggapan</strong> dan Saran Sistem Kuesioner  </h2>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
            <ul class="nav nav-tabs nav-primary">
                <li ><a href="#" ><i class="icon-docs"></i>Kegiatan Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Data Personil Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Kerjasama</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Ekstramural</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Jurnal Luaran Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Buku Teks Luaran Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>HKI Litbang</a></li>
                <li class="active"><a href="#" data-toggle="tab8"><i class="icon-docs"></i>Tanggapan dan Saran</a></li>
            </ul>
   

        <div class="content" id="<?php echo base_url('./pegawai/Kuesioner/Kuesioner8')?>"> 
            <div class="row"><br>
             <?php
                         $message = $this->session->flashdata('pesan');
                          if(isset($message))
                          {
                            echo $message;
                          } 
                        ?>
                    <?php foreach ($bab as $key) {
                                 $id = $key['_id'];
                                $pertanyaan = $this->pertanyaan_m->get_pertanyaan($id);
                    ?>
                        <div class="form-horizontal">
                            <form action="<?php echo site_url('./pegawai/kuesioner/simpan_kuesioner8/'.$id_keg) ?>" method="POST">
                                <h2 id="reference" name="reference" class="heading-reference">Tanggapan dan Saran</h2>
                                <p>Form Tanggapan dan saran ini hanya bersifat opsional. Jika anda tidak memiliki tanggapan dan saran, silakan langsung tekan tombol simpan.</p>
                            <?php foreach ($pertanyaan as $key=>$value) { ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                    <div class="col-sm-10">
                                        <?php foreach ($value['bentuk_form'] as $a=>$b) {
                                            $nama = $value['_id'];
                                            if ($a == "text"){?>
                                                <input type="text" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>">
                                            <?php }elseif ($a=="textarea"){?>
                                                <textarea style="width:485px; height:106px;" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>" placehorder="<?php echo $value['kalimat_tanya'] ?>"></textarea>
                                            <?php }elseif ($a=="date"){ ?>
                                                <input type="date" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>">
                                            <?php }else{foreach ($value['bentuk_form'] as $k=>$v) {?>
                                                
                                                <select class="form-control" name="<?php echo $value['_id']?>[]" data-parsley-group="block0" data-search="true">
                                                <option value="<?php echo set_select('$nama[]')?>">Pilih Salah satu</option>
                                                    <?php  foreach ($v as $data){?>
                                                        <option value="<?php echo $data?>"><?php echo $data?></option>
                                                    <?php }?>
                                                 </select>
                                             <?php } ?> 
                                            <?php }?>
                                                <?php  echo form_error('$nama[]') ?>
                                        <?php }?>  
                                        </div>
                                    </div>                                       
                            <?php } // end foreach pertanyaan 
                            ?>
                                <button type="button" data-toggle="modal" data-target="#colored-header" class="btn btn-primary" style="margin-left: 458px;">Simpan</button>
                                <div class="modal fade" id="colored-header" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                                      <h4 class="modal-title"><strong>Konfirmasi</strong> Survey Litbang</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p>Apakah anda yakin inputan yang di isikan sudah benar?</p>
                                      <div class="p-t-20 m-b-20 p-l-40">
                                        <p>Silahkan cek kembali form kegiatan anda dengan memilih tombol <strong>Kembali</strong></p><hr>
                        
                                        <p>Jika Anda sudah yakin dengan inputan anda silahkan pilih tombol <strong>Simpan Survey</strong></p><hr>
        
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Kembali</button>
                                      <button type="submit" class="btn btn-primary btn-embossed" >Simpan Survey</button>
                                    </div>
                                  </div>
                                </div>
                              </div><!--End Modal-->
                            </form>
                        </div>
                    <?php }?>
                     <div class="form-group">
                        <button  type="button" onclick="goBack()" class="btn btn-success btn-embossed"><< Kembali</button>
                         
                    </div>
                </div>
             </div>
             <div class="content">
             </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function goBack() {
    window.history.back();
}
</script>