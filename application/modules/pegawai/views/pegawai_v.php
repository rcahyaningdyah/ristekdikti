<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Step From Wizard - example 4</title>

    <!-- bootstrap for better look example, but not necessary -->
    <link rel="stylesheet" href="<?php echo base_url('./assets/plugins/bootstrap/css/bootstrap.css')?>" type="text/css" media="screen, projection">

    <!-- Step Form Wizard plugin -->
    <link rel="stylesheet" href="<?php echo base_url('./assets/plugins/step-form-wizard/css/step-form-wizard.css')?>" type="text/css" media="screen, projection">
    <script src="<?php echo base_url('./assets/plugins/step-form-wizard/js/step-form-wizard.js')?>"></script>

    <!-- nicer scroll in steps -->
    <link rel="stylesheet" href="<?php echo base_url('./assets/plugins/mcustom-scrollbar/mcustom_scrollbar.min.css')?>">
    <script src="<?php echo base_url('./assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')?>"></script>


    <script>
        var sfw;
        $(document).ready(function () {
            sfw = $("#wizard_example").stepFormWizard({});
        })
        $(window).load(function () {
            /* only if you want use mcustom scrollbar */
            $(".sf-wizard fieldset").mCustomScrollbar({
                theme: "dark-3",
                scrollButtons: {
                    enable: true
                }
            });
            /* ***************************************/

            /* this function call can help with broken layout after loaded images or fonts */
            sfw.refresh();
        });
    </script>

    <style>
        pre {margin: 45px 0 60px;}
        h2 {margin: 60px 0 30px 0;}
        p {margin-bottom: 10px;}
    </style>
</head>

<body>
 <div class="row">
    <div class="col-md-12">
        <h2>Form Identitas Pengisi</h2>
        <form id="wizard_example" action="">
        <?php foreach ($register as $key) {
                    $id = $key['_id'];
                    $pertanyaan = $this->pertanyaan_m->get_pertanyaan($id);
             ?>
               <fieldset id="bab-1">
                <legend><?php echo $key['topik']?></legend>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                          <?php foreach ($pertanyaan as $key => $value) { ?>
                            <label for="nama"><?php echo $value['kalimat_tanya'];?></label>
                            <?php if (count(array_filter($value['bentuk_form'])) == 0) {
                                   ?>
                                <input type="" class="form-control" id="exampleInputEmail1" name="<?php echo $value['kalimat_tanya']?>" placehorder="" required data-parsley-group="block0">
                                <?php }else{ 
                                    foreach ($value['bentuk_form'] as $k=>$v) {?>
                                        <select class="form-control" name="<?php echo $value['kalimat_tanya']?>" data-parsley-group="block0">
                                            <?php  foreach ($v as $data){?>
                                            <option value="<?php echo $data?>"><?php echo $data?></option>
                                           <?php }?>
                                         </select>
                                     <?php } ?>  
                                <?php }?>
                            <?php
                                }?>
                        </div>
                    </div>
                </div>
            </fieldset><!--end Bab 1-->
           <?php }?>
        </form>
    </div>
</body>
</html>
