<div class="row">
   <div class="col-md-12">
   		<div class="col-md-12">
   			<div class="panel">
   				<div class="panel-header">
                  <?php // echo $this->session->userdata('lembaga'); ?>
   					<h3> <strong> Selamat Datang,</strong> </h3>
   				</div>
   				<div class="panel-content">
   					<img id="img1" src="<?php echo site_url('./assets/images/logo/logo-ristekdiktimini.png')?>" />
                  <h1 style="display:inline">Kuesioner Litbang Pemerintah-Kemenristekdikti</h1>
   					<p>Kuesioner online ini berisi pertanyaan yang berkaitan dengan seluruh kegiatan penelitian dan pengembangan
   					 (selanjutnya disebut litbang) yang dilaksanakan sepanjang tahun anggaran 2015, 
   					 terhitung mulai 1 Januari 2015 sampai dengan 31 Desember 2015. Yang dimaksud dengan 
   					 “seluruh kegiatan” adalah semua kegiatan baik dari anggaran sendiri atau yang didanai oleh instansi pemerintah lain, pihak industri dan luar negeri.</p> 
   					<p>Kegiatan penelitian yang didanai melalui skema insentif Ristek maupun insentif Dikti atau skema Grant lainnya dari berbagai pihak, termasuk ke dalam kegiatan penelitian yang dimaksud dalam kuesioner ini.</p>
   				   <p> <h3><strong> Bacalah Petunjuk Pengisian Kuesioner sebelum Anda melakukan pengisian Kuesioner.  </strong> </h3>
                        Buku Petunjuk Pengisian Kuesioner dapat di UNDUH pada  <a target = '_blank' href="<?php echo site_url('/assets/manual-book/MANUAL_BOOK_KUESIONER_ONLINE_RISTEKDIKTI.pdf') ?>"> <strong>LINK</strong> </a> berikut. 
                   </p>
               </div>
   			</div>
   			<div class="panel">
   				<div class="panel-header">
   					<h3> <strong>Tujuan Survey Litbang</strong> </h3>
   				</div>
   				<div class="panel-content">
						<p>Tujuan umum dari survei ini adalah mengumpulkan data litbang, sektor pemerintah untuk mengetahui gambaran aktivitas dan kinerja litbang sebagai bahan masukan dalam penyusunan kebijakan  pembangunan Iptek Nasional.  Tujuan khususnya adalah untuk mengembangkan basis data Iptek di Kementerian/Lembaga Pemerintah Non-Kementerian.</p>
						<p>Kegiatan Litbang yang dimaksud dalam kuesioner ini adalah seluruh kegiatan litbang yang dilakukan  di lingkungan unit kerja Saudara dengan sumber pendanaan baik yang berasal dari pemerintah maupun dari pihak lain melalui kontrak. Di samping itu, survei ini juga mendata tenaga peneliti yang terlibat kegiatan litbang, tenaga teknisi dan penunjang serta gaji/upah di unit kerja Saudara. Pengertian beberapa hal yang dimaksud dalam kuesioner ini dapat dilihat dalam Petunjuk Pengisian Kuesioner.</p>
   				</div>
   			</div>
            <div class="panel">
               <div class="panel-header">
                  <h3> <strong>Kerahasiaan</strong> </h3>
               </div>
               <div class="panel-content">
                  <p>SEMUA INFORMASI DAN ISIAN YANG TERKANDUNG DALAM KUESIONER INI AKAN DIPERLAKUKAN DENGAN PRINSIP KERAHASIAAN, DAN HANYA AKAN DIGUNAKAN UNTUK KEPENTINGAN ANALISIS.</p>
                  <p>INFORMASI SPESIFIK TENTANG IDENTITAS RESPONDEN ATAU UNIT KERJA TIDAK AKAN DIINFORMASIKAN KEPADA UMUM ATAU DIPUBLIKASIKAN DALAM BENTUK APAPUN. IDENTITAS INI HANYA AKAN DIGUNAKAN UNTUK KEPENTINGAN KORESPONDENSI.</p>
               </div>
            </div>
            <div class="panel">
               <div class="panel-header">
                  <h3> <strong>Hal-hal yang perlu diperhatikan</strong> </h3>
               </div>
               <div class="panel-content">
                  <p>•  Daftar pertanyaan ini diisi oleh pimpinan unit kerja atau pihak yang ditunjuk dan karenanya bertanggungjawab atas semua isian dalam kuesioner ini.</p>
                     <div style="text-indent:50px">
                     <p>Adapun pertanyaan kuesioner dibagi dalam 8 Bagian:</p>
                     <div style="text-indent:80px">
                        <p>1. Data Kegiatan Litbang</p>
                        <p>2. Data Personil Kegiatan Litbang</p>
                        <p>3. Data Belanja Kerjasama</p>
                        <p>4. Data Belanja Ekstramural</p>
                        <p>5. Data Jurnal Keluaran Kegiatan Litbang</p>
                        <p>6. Data Buku Keluaran Kegiatan Litbang</p>
                        <p>7. Data HKI Keluaran Kegiatan Litbang</p>
                     <p>8. Tanggapan dan Saran</p></div></div>
                  <p>•  Survei ini mengacu dan berdasarkan atas realisasi anggaran dan aktivitas tahun 2016.</p>
                  <p>•  Agar kuesioner ini dapat terisi dengan benar, mohon definisi dan  batasan istilah sebagaimana termuat dalam “Lembar Petunjuk Pengisian Kuesioner” dibaca terlebih dahulu.</p>
                  <p>•  Jika ada saran dan/atau komentar, silakan tulis pada halaman yang telah disediakan.</p>
                  <p>•  Setelah terisi lengkap, mohon agar kuesioner ini dikembalikan ke alamat penyelenggara survey selambat-lambatnya tanggal……….</p>
               </div>
            </div>
   		</div>


   </div>
</div>


 

     