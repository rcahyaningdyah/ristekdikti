<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('./assets/js/clone-form-td.js')?>"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<div class="row">
  <div class="col-md-12">
    <h2>Form Kuesioner<strong> Litbang Kemenristekdikti</strong></h2>
    <div class="col-lg-12 portlets">
              <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="icon-check"></i> <strong>Kuesioner Litbang Bagian I</strong> <small>dari Sektor Pemerintahan</small></h3>
                  <p>Berikut adalah pertanyaan mengenai personil litbang di unit kerja Saudara.  Isilah forml yang disediakan dengan data yang sesuai dengan kondisi nyata pada unit kerja Saudara.
          Apabila diperlukan, Saudara dapat menambah form dengan memilih tanda + pada bagian bawah. Penjelasan dari masing-masing pertanyaan dapat dilihat pada bagian PETUNJUK PENGISIAN KUESIONER.
          </p>
                </div>
                <div class="panel-content">
                   <div class="row">
                    <h3 style="color:#2D69C9;text-decoration:underline">I. Personil Litbang tahun 2015</h3>
                      <div class="col-md-12">
                          <div class="form-group m-b-30">
                            <p>1. Sebutkan peneliti  (fungsional peneliti, perekayasa, pranata nuklir, dan lain-lain) yang berperan sebagai peneliti dalam kegiatan litbang berdasarkan tingkat pendidikan dan bidang ilmu di unit kerja Saudara tahun 2015</p>
                            <form role="form" class="form-validation" novalidate="novalidate">
                              <div id="entry1" class="clonedInput">
                              <h2 id="reference" name="reference" class="heading-reference">Personil 1</h2>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label class="control-label">NIP</label>
                                    <div class="append-icon">
                                      <input id="nip" type="text" name="nip" class="form-control" minlength="3" placeholder="NIP Pranata Litbang" required="" aria-required="true">
                                      <i class="icon-user"></i>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label class="control-label">Nama</label>
                                    <div class="append-icon">
                                      <input type="text" name="lastname" class="form-control" minlength="4" placeholder="Nama pengisi Kuesioner" required="" aria-required="true">
                                      <i class="icon-user"></i>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Kelamin</label>
                                      <select class="form-control"  data-placeholder="Pilih Jenis kelamin">
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Jenjang Pendidikan</label>
                                      <select class="form-control"  data-placeholder="Jenjang Pendidikan">
                                        <option value="L">S1</option>
                                        <option value="P">S2</option>
                                        <option value="P">S3</option>
                                      </select>
                                  </div>
                                </div>
                              </div>
                               <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Bidang Ilmu</label>
                                      <select class="form-control"  data-placeholder="Pilih Jenis kelamin">
                                        <option value="L">Logic</option>
                                        <option value="P">Mathematics</option>
                                        <option value="P">Physics</option>
                                        <option value="P">Astronomys</option> 
                                        <option value="P">Chemistry</option>
                                        <option value="P">Life Science</option>
                                        <option value="P">Earth and space</option>
                                        <option value="P">Agricultural Science</option>
                                        <option value="P">Medical Science</option>
                                        <option value="P">Technology Science</option>
                                        <option value="P">Demography</option>
                                        <option value="P">Economic Science</option>
                                        <option value="P">Geography</option>
                                        <option value="P">History</option>
                                        <option value="P">Juridical Science & Law</option>
                                        <option value="P">Political Science</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Peneliti</label>
                                     <div class="append-icon">
                                          <input type="text" name="lastname" class="form-control" minlength="4" placeholder="Peneliti" required="" aria-required="true">
                                          <i class="icon-user"></i>
                                      </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Perekayas</label>
                                     <div class="append-icon">
                                          <input type="text" name="lastname" class="form-control" minlength="4" placeholder="Perekayasa" required="" aria-required="true">
                                          <i class="icon-user"></i>
                                      </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Pranata Nuklir</label>
                                     <div class="append-icon">
                                          <input type="text" name="lastname" class="form-control" minlength="4" placeholder="Pranata Nuklir" required="" aria-required="true">
                                          <i class="icon-user"></i>
                                      </div>
                                  </div>
                                </div>
                              </div>
                              <p>
                        <button type="button" id="btnAdd" name="btnAdd" class="btn btn-info">add section</button>
                          <button type="button" id="btnDel" name="btnDel" class="btn btn-danger">remove section above</button>
                        </p>
                              </div>
                           </form>
                          </div>
                   </div>
                </div>
              </div>
              </div>

                <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="icon-check"></i> <strong>Kuesioner Litbang Bagian II</strong> <small>dari Sektor Pemerintahan</small></h3>
                  <p>Kuesioner ini berisikan mengenai AnggranBelanja Litbang pada tahun 2015</p>
                </div>
                <div class="panel-content">
                   <div class="row">
                    <h3 style="color:#2D69C9;text-decoration:underline">I. Belanja Litbang</h3>
                      <div class="col-md-12">
                          <div class="form-group m-b-30">
                            <p>1. Sebutkan peneliti  (fungsional peneliti, perekayasa, pranata nuklir, dan lain-lain) yang berperan sebagai peneliti dalam kegiatan litbang berdasarkan tingkat pendidikan dan bidang ilmu di unit kerja Saudara tahun 2015</p>
                            <form role="form" class="form-validation" novalidate="novalidate">
                              <!-- <div id="entry1" class="clonedInput"> -->
                              <h2 id="reference" name="reference" class="heading-reference">Isian 1</h2>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label class="control-label">Judul Penelitian</label>
                                    <div class="append-icon">
                                      <input id="judul_penelitian" type="text" name="judul_penelitian" class="form-control" minlength="3" placeholder="Judul Penelitian" required="" aria-required="true">
                                      <i class="icon-user"></i>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label class="control-label">Nama Ketua Peneliti/Koordinator</label>
                                    <div class="append-icon">
                                      <input type="Koordinator" name="lastname" class="form-control" minlength="4" placeholder="Nama peneliti/koordinator" required="" aria-required="true">
                                      <i class="icon-user"></i>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Peneliti</label>
                                      <div class="append-icon">
                                          <input type="jumlah_peneliti" name="lastname" class="form-control" minlength="4" placeholder="JUmlah Peneliti" required="" aria-required="true">
                                        </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Penelitian</label>
                                      <select class="form-control"  data-placeholder="Jenjang Pendidikan">
                                        <option value="L">Dasar</option>
                                        <option value="P"> Terapan</option>
                                        <option value="P">Eksperimental</option>
                                      </select>
                                  </div>
                                </div>
                              </div>
                               <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Bidang Penelitian</label>
                                      <select class="form-control"  data-placeholder="Pilih Jenis kelamin">
                                        <option value="L">Natural science</option>
                                        <option value="P">Enginering dan technology</option>
                                        <option value="P">Agricultural dan Environmental</option>
                                        <option value="P">Medical Science</option> 
                                        <option value="P">Chemistry</option>
                                        <option value="P">Social Science</option>
                                        <option value="P">Humanities</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Dana</label>
                                     <div class="append-icon">
                                          <input type="text" name="dana" class="form-control" minlength="4" placeholder="Peneliti" required="" aria-required="true">
                                         
                                      </div>
                                  </div>
                                </div>
                              </div>
                          
                              <p>
                        <button type="button" id="btnAdd" name="btnAdd" class="btn btn-info">add section</button>
                          <button type="button" id="btnDel" name="btnDel" class="btn btn-danger">remove section above</button>
                        </p>
                              <!-- </div> -->
                           </form>
                      </div>
                      </div>
                   </div>
                </div>
              </div>

               <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="icon-check"></i> <strong>Kuesioner Litbang Bagian III</strong> <small>dari Sektor Pemerintahan</small></h3>
                  <p>Kuesioner ini berisikan mengenai Keluaran hasil penelitilian dan pengembangan pada tahun 2015</p>
                </div>
                <div class="panel-content">
                   <div class="row">
                    <h3 style="color:#2D69C9;text-decoration:underline">I. Luaran Litbang</h3>
                      <div class="col-md-12">
                          <div class="form-group m-b-30">
                            <p>1. Sebutkan publikasi pada jurnal internasional tahun 2014-2015</p>
                            <form role="form" class="form-validation" novalidate="novalidate">
                              <!-- <div id="entry1" class="clonedInput"> -->
                              <h2 id="reference" name="reference" class="heading-reference">Jurnal 1</h2>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label class="control-label">Judul</label>
                                    <div class="append-icon">
                                      <input id="judul_jurnak" type="text" name="judul_penelitian" class="form-control" minlength="3" placeholder="Judul Jurnal" required="" aria-required="true">
                                      <i class="icon-user"></i>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label class="control-label">Penulis Pertama</label>
                                    <div class="append-icon">
                                      <input type="penulis1" name="lastname" class="form-control" minlength="4" placeholder="Nama penulis pertama" required="" aria-required="true">
                                      <!-- <i class="icon-user"></i> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Kelamin</label>
                                      <select class="form-control"  data-placeholder="Jenjang Pendidikan">
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Tinkat Pendidikan</label>
                                      <select class="form-control"  data-placeholder="Jenjang Pendidikan">
                                        <option value="L">S1</option>
                                        <option value="P">S2</option>
                                        <option value="P">S3</option>
                                      </select>
                                  </div>
                                </div>
                              </div>
                               <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Nama Jurnal</label>
                                         <div class="append-icon">
                                        <input type="penulis1" name="lastname" class="form-control" minlength="4" placeholder="Nama penulis pertama" required="" aria-required="true">
                                        <!-- <i class="icon-user"></i> -->
                                      </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">ISSN</label>
                                     <div class="append-icon">
                                          <input type="text" name="dana" class="form-control" minlength="4" placeholder="Peneliti" required="" aria-required="true">
                                         
                                      </div>
                                  </div>
                                </div>
                              </div>
                            <div class="row">
                              <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Tahun Terbit</label>
                                     <div class="append-icon">
                                          <input type="text" name="dana" class="form-control" minlength="4" placeholder="Peneliti" required="" aria-required="true">
                                         
                                      </div>
                                  </div>
                                </div>
                            </div>
                              <p>
                        <button type="button" id="btnAdd" name="btnAdd" class="btn btn-info">add section</button>
                          <button type="button" id="btnDel" name="btnDel" class="btn btn-danger">remove section above</button>
                        </p>
                              <!-- </div> -->
                           </form>
                      </div>
                      </div>
                   </div>
                </div>
              </div>
            </div>
             <div class="text-center  m-t-20">
              <a href="<?php echo site_url('pegawai/Dashboard'); ?>" type="submit" id="submit-form" class="btn btn-embossed btn-primary" data-style="expand-left">
                  Submit
                </a>
              <!-- <button type="reset" class="cancel btn btn-embossed btn-default m-b-10 m-r-0">Cancel</button> -->
            </div>
  </div>
</div>
  