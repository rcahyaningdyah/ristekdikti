 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Form Data</strong> Kegiatan </h2>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
            <ul class="nav nav-tabs nav-primary">
               <li class="active"><a href="#" ><i class="icon-docs"></i>Kegiatan Litbang</a></li>
                <li><a href="#" ><i class="icon-docs"></i>Data Personil Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Kerjasama</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Ekstramural</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Jurnal Luaran Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Buku Teks Luaran Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>HKI Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Tanggapan dan Saran</a></li>
            </ul>
   

        <div class="content"> 
            <div class="row"><br>
             <?php
                 $message = $this->session->flashdata('pesan_error');
                  if(isset($message))
                  {
                    echo $message;
                  } 


                ?>

                <?php 
                
                ?>
                    <?php foreach ($bab_4 as $key) {
                                $id = $key['_id'];
                                $pertanyaan = $this->pertanyaan_m->get_pertanyaan($id);
                    ?>
                    <?php //echo validation_errors(); ?>
                        <div class="form-horizontal">
                            <form action="<?php echo site_url('./pegawai/kuesioner/simpan_kuesioner1')?>" method="POST">
                            <?php foreach ($pertanyaan as $key=>$value) {  ?>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                    <div class="col-sm-5">
                                        <?php foreach ($value['bentuk_form'] as $a=>$b) {
                                            if ($a == "text"){?>
                                                <input type="text" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>">      
                                            <?php }
                                            elseif ($a=="textarea"){?>
                                                <textarea id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>"></textarea>
                                            <?php }
                                            elseif ($a=="date"){ ?>
                                                <input type="date" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>" placeholder="Isikan <?php echo $value['kalimat_tanya'] ?>">
                                            <?php }
                                            else{
                                                foreach ($value['bentuk_form'] as $k=>$v) {
                                                    if (empty($v)){
                                                        //get data dari collection lain
                                                        if ($value['_id'] == "q_30"){
                                            ?>
                                                            <!-- select kelompok penelitian -->
                                                            <select class="form-control" name="q_30" id="q_30" data-parsley-group="block0"   data-search="true"> 
                                                                 
                                                                 <option  value="<?php echo set_select($value['_id'])?>"> <?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Kelompok Penelitian";
                                                                    } ?></option>
                                                                <?php foreach ($kelompok_penelitian as $data){
                                                                 ?>
                                                                    <option id="<?php echo $data['_id'] ?>" value="<?php echo $data['_id'] ?>"> <?php echo $data['nama_kelompok'] ?> </option>
                                                                <?php      
                                                                        } 
                                                                ?>
                                                            </select>
                                            <?php        }
                                                        elseif ($value['_id'] == "q_31"){
                                            ?>
                                                            <!-- select bidang penelitian -->
                                                            <select class="form-control" name="q_31" id="q_31" data-parsley-group="block0"  data-search="true"> 
                                                             
                                                                    <option value="<?php echo set_select($value['_id'])?>"><?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Bidang Penelitian";
                                                                    } ?>  </option>
                                                               
                                                            </select>
                                            <?php                
                                                        }
                                                        else{
                                            ?>
                                                            <!-- select  sub bidang penelitian -->
                                                              <select class="form-control" name="q_32" id="q_32" data-parsley-group="block0" data-search="true"> 
                                                             
                                                                    <option value="<?php echo set_select($value['_id'])?>"><?php if (set_select($value['_id'])) {
                                                                    echo set_select($value['_id']);
                                                                    } else {
                                                                        echo "Pilih Sub Bidang Penelitian";
                                                                    } ?>    </option>
                                                               
                                                            </select>

                                                            <input type="text" placeholder="Jika memilih other, isi Bidang Penelitian Anda Disini"  style="visibility:hidden;" class="form-control col-sm-6" id="q_32-other" value="<?php echo set_value($value['_id']) ?>" name="<?php echo $value['_id']?>-other">
                                            <?php                
                                                        }

                                            ?>

                                            <?php            
                                                    } // end if empty $v
                                                    else {
                                                        //get data pilihan
                                            ?>
                                                    <div class="input-group">
                                                        <select class="form-control" name="<?php echo $value['_id']?>" data-parsley-group="block0" data-search="true">
                                                            <option value="<?php echo set_select($value['_id'])?>">Pilih Salah satu</option>
                                                            <?php  foreach ($v as $data){?>
                                                                <option value="<?php echo $data?>"><?php echo $data?></option>
                                                            <?php }?>
                                                         </select>
                                                         <!-- icon modal untuk informasi jenis penelitian -->
                                                         <a class="input-group-addon" style="color:#319DB5"><i class="fa fa-info-circle" data-toggle="modal" data-target="#colored-header"></i></a>
                                                    </div>
                                            <?php                                                       
                                                    } // end else
                                            ?>                                                
                                                
                                             <?php } // end foreach bentuk_form ?> 
                                            <?php } // end else bentuk_form ?>
                                            <?php  echo form_error($value['_id']) ?>
                                        <?php }?>  
                                    </div>
                                </div>                                       
                            <?php } // end foreach pertanyaan 
                            ?>
                            <br/>
                            <div class="form-group" style="margin-left: 458px;"> 
                                 <button type="submit" class="btn btn-primary ">Simpan</button>
                            </div>
                            <!--Modal Informasi untuk jenis penelitian-->
                            <div class="modal fade" id="colored-header" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                                      <h4 class="modal-title"><strong>Jenis</strong> Penelitian</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p>Klasifikasi Jenis Penelitian:</p>
                                      <div class="p-t-20 m-b-20 p-l-40">
                                        <p><strong>Penelitian Dasar</strong></p>
                                        <p>adalah kegiatan penelitian teoritis atau eksperimental yang dilakukan untuk memperoleh pengetahuan baru tentang prinsip-prinsip dasar (the underlying foundations) dari fenomena atau fakta yang teramati, tanpa memikirkan penerapannya.</p><hr>
                                        <p><strong>Penelitian Terapan</strong></p>
                                        <p>juga merupakan kegiatan penelitian teoritis atau eksperimental yang orisinal yang dilakukan untuk memperoleh pengetahuan baru. Namun kegiatan investigatif ini diarahkan untuk tujuan praktis tertentu.</p><hr>
                                        <p><strong>Penelitian Eksperimental</strong></p>
                                        <p>adalah kegiatan sistematik dengan menggunakan pengetahuan yang sudah ada, yang diperoleh melalui penelitian atau pengalaman praktis.</p>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Save changes</button>
                                    </div>
                                  </div>
                                </div>
                              </div><!--End Modal-->
                            </form>
                        </div>
                    <?php }?>
                       <div class="form-group"> 
                             <button type="button" onclick="javascript:history.forward();" class="btn btn-success btn-embossed pull-right">Berikutnya >></button>
                        </div>
                   
                </div>
             </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
      
        $("#q_30").change(function()
        {
           var bidang_id = $('#q_30').val();
           $("#q_31").html('<option value=""> Pilih Bidang Penelitian </option>');
           $("select#q_31").val("").prop('selected', true);
           if (bidang_id != ""){
                var post_url = "<?php echo site_url('pegawai/kuesioner/get_bidang_penelitian') ?>/" + bidang_id;
                $.ajax({
                    type : "POST",
                    url : post_url,
                    dataType: 'json',
                    success : function(data)
                    {
                        str = '<option value=""> Pilih Bidang Penelitian </option>';
                         console.log(data);
                         console.log("lalalala");
                      
                        $.each(data.bidang_penelitian, function(keyx, valx){
                            // console.log(keyx+' '+valx);
                            str += '<option value="'+keyx+'">'+valx+'</option>'
                        });
                        // console.log(str);
                        $("#q_31").html(str);

                    } // end success
                }); // end ajax
           }


        }); // end change  q_30

        $("#q_31").change(function(){
            var kelompok = $('#q_31').val();
            $("#q_32").html('<option value=""> Pilih Sub Bidang Penelitian </option>');
            $("select#q_32").val("").prop('selected', true);
            if (kelompok != ""){
                var post_url = "<?php echo site_url('pegawai/kuesioner/get_sub_penelitian') ?>/" + kelompok;
                $.ajax({
                    type : "POST",
                    url : post_url,
                    dataType: 'json',
                    success : function(data)
                    {
                        console.log('success');
                        str = '<option value=""> Pilih Sub Bidang Penelitian </option>';
                         console.log(data);
                         $("#q_32").empty();
                        $.each(data.sub_penelitian, function(keyx, valx){
                            console.log(keyx+' '+valx);
                            str += '<option value="'+keyx+'">'+valx+'</option>'
                        });
                        // console.log(str);
                        $("#q_32").html(str);
                        console.log('ulala')
                    } // end success
                }); // end ajax
            }
        }); // end change q_31

        $("#q_32").change(function(){
            if($("#q_32 option:selected:contains('Other')").length){
                console.log($("#q_32 option:selected").text());
                $("#q_32-other").css('visibility', 'visible');
            }
        });// end change q_32

    });    
</script>