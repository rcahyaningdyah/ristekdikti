<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Step From Wizard - example 4</title>

    <!-- bootstrap for better look example, but not necessary -->
    <link rel="stylesheet" href="<?php echo base_url('./assets/plugins/bootstrap/css/bootstrap.css')?>" type="text/css" media="screen, projection">

    <!-- Step Form Wizard plugin -->
    <link rel="stylesheet" href="<?php echo base_url('./assets/plugins/step-form-wizard/css/step-form-wizard.css')?>" type="text/css" media="screen, projection">
    <script src="<?php echo base_url('./assets/plugins/step-form-wizard/js/step-form-wizard.js')?>"></script>

    <!-- nicer scroll in steps -->
    <link rel="stylesheet" href="<?php echo base_url('./assets/plugins/mcustom-scrollbar/mcustom_scrollbar.min.css')?>">
    <script src="<?php echo base_url('./assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')?>"></script>


   
<div class="header">
  <h2><strong>Daftar Kegiatan Litbang</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active">Daftar Kegiatan </li>
     
    </ol>
  </div>
</div>

<!-- END BREADCRUMB AND TITLE ON PAGE-->
<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><strong>Riwayat Kegiatan Litbang </strong> </h3>
        </div>
        <div class="panel-content">
            <?php
               $message = $this->session->flashdata('pesan');
                if(isset($message))
                {
                  echo $message;
                } 
              ?>


              <?php 
             
                foreach ($kegiatan_temp as $key=>$value){
                    if (!array_key_exists('q_64', $value)){
                ?>
                     <div class="alert alert-danger">
                        <a class="close" data-dismiss="alert">×</a>
                       Maaf, waktu Anda sudah habis dan belum menyelesaikan seluruh Kuesioner. <br/>
                       Hapus data Anda dengan klik <a href="<?php echo site_url('pegawai/Kuesioner/hapus_kegiatan_temp/'.$value['_id']) ?>"> TAUTAN </a>  berikut, kemudian pilih Tambah Kegiatan. <br/>
                      
                    </div>
                <?php
                    }
                    else{
                        echo "sudah semua diisi".$value['q_27']." <br/>";
                    }
                }
              ?>
             <a href="<?php echo base_url('./pegawai/Kuesioner/kuesioner1')?>" class="btn btn-primary btn-embossed" style="margin-bottom: 16px;"><i class="icon-plus"> </i>Tambah Kegiatan</a>
           
            <table class="table table-dynamic table-tools" id="table-editable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kegiatan</th>
                        <th>Tanggal Isi</th>
                        <th>Awal Kegiatan</th>
                        <th>Akhir Kegiatan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $no = $this->uri->segment(4)+1 ;

                        foreach ($kegiatan as $key => $value) {?>

                    <tr>
                        <td><?php echo $no; $no = $no+1;?></td>
                        <td><?php echo $value['q_27']?></td>
                        <td>
                            <?php 
                                if (array_key_exists('created_at', $value)){
                                    $tanggal_isi = date("d/m/Y",($value['created_at']->sec));

                                    $tanggal = explode("/", $tanggal_isi);
                                    $year= $tanggal[2];
                                    $month = $tanggal[1];
                                    $date = $tanggal[0];
                                    switch ($month){
                                        case 1 : $month ='Januari'; break;
                                        case 2 : $month ='Februari'; break;
                                        case 3 : $month = 'Maret'; break;
                                        case 4 : $month = 'April'; break;
                                        case 5 : $month = 'Mei'; break;
                                        case 6 : $month = 'Juni'; break;
                                        case 7 : $month = 'Juli'; break;
                                        case 8 : $month = 'Agustus'; break;
                                        case 9 : $month = 'September'; break;
                                        case 10 : $month = 'Oktober'; break;
                                        case 11 : $month = 'November'; break;
                                        case 12 : $month = 'Desember'; break;
                                    } 
                                       
                                    echo $date.' '.$month.' '.$year;
                                }
                                else{
                                    echo "-";
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                $awal_kegiatan = date("d/m/Y",($value['q_33']->sec));
                                $tanggal = explode("/", $awal_kegiatan);
                                $year= $tanggal[2];
                                $month = $tanggal[1];
                                $date = $tanggal[0];
                                switch ($month){
                                    case 1 : $month ='Januari'; break;
                                    case 2 : $month ='Februari'; break;
                                    case 3 : $month = 'Maret'; break;
                                    case 4 : $month = 'April'; break;
                                    case 5 : $month = 'Mei'; break;
                                    case 6 : $month = 'Juni'; break;
                                    case 7 : $month = 'Juli'; break;
                                    case 8 : $month = 'Agustus'; break;
                                    case 9 : $month = 'September'; break;
                                    case 10 : $month = 'Oktober'; break;
                                    case 11 : $month = 'November'; break;
                                    case 12 : $month = 'Desember'; break;
                                } 
                                 echo $date.' '.$month.' '.$year;                                 
                            ?>
                        </td>
                        <td>
                            <?php 
                                $akhir_kegiatan = date("d/m/Y",($value['q_34']->sec));
                                 $tanggal = explode("/", $akhir_kegiatan);
                                $year= $tanggal[2];
                                $month = $tanggal[1];
                                $date = $tanggal[0];
                                switch ($month){
                                    case 1 : $month ='Januari'; break;
                                    case 2 : $month ='Februari'; break;
                                    case 3 : $month = 'Maret'; break;
                                    case 4 : $month = 'April'; break;
                                    case 5 : $month = 'Mei'; break;
                                    case 6 : $month = 'Juni'; break;
                                    case 7 : $month = 'Juli'; break;
                                    case 8 : $month = 'Agustus'; break;
                                    case 9 : $month = 'September'; break;
                                    case 10 : $month = 'Oktober'; break;
                                    case 11 : $month = 'November'; break;
                                    case 12 : $month = 'Desember'; break;
                                } 
                                 echo $date.' '.$month.' '.$year;
                            ?>
                        </td>
                        <td><a target="_blank" href="<?php echo site_url('api/cetak_kegiatan/'.$value['_id']) ?>" class="btn btn-primary btn-emboss"><i class="icon-eye"></i>Detail Laporan Kegiatan</a></td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
         
        </div>
    </div>
</div>

</head>     
</html>
