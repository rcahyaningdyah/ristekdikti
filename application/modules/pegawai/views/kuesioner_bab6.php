 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Form Buku Teks</strong> Luaran Litbang  </h2>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
            <ul class="nav nav-tabs nav-primary">
                <li ><a href="#" ><i class="icon-docs"></i>Kegiatan Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Data Personil Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Kerjasama</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Ekstramural</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Jurnal Luaran Litbang</a></li>
                <li class="active"><a href="#" ><i class="icon-docs"></i>Buku Teks Luaran Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>HKI Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Tanggapan dan Saran</a></li>
            </ul>
   

        <div class="content"> 
            <div class="row"><br>
                <?php
                 $message = $this->session->flashdata('pesan_error');
                  if(isset($message))
                  {
                    echo $message;
                  } 
                ?>
                    <?php foreach ($bab as $key) {
                                 $id = $key['_id'];
                                $pertanyaan = $this->pertanyaan_m->get_pertanyaan($id);
                    ?>
                        <div class="form-horizontal">
                            <form action="<?php echo site_url('./pegawai/kuesioner/simpan_kuesioner6/'.$id_keg) ?>" method="POST">
                            <div id="buku1" class="clonedInput">
                                <h2 id="reference" name="reference" class="heading-reference">Buku Teks Luaran Litbang</h2>
                                    <p>Form Buku luaran Litbang bersifat opsional. Jika kegiatan litbang ini tidak memiliki luaran Buku, silakan langsung tekan tombol simpan.</p>
                                    <?php foreach ($pertanyaan as $key=>$value) { ?>
                                         <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $value['kalimat_tanya'];?> </label>
                                            <div class="col-sm-6">
                                                <?php foreach ($value['bentuk_form'] as $a=>$b) {
                                                    $nama = $value['_id'];
                                                    if ($a == "text"){
                                                        if ($value['_id'] == "q_56") {?>
                                                        <input type="text" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>">
                                                        <p>Pisahkan inputan dengan delimeter <strong> titik koma ( ; )</strong></p>
                                                        <p>Contoh : data1;data2;data ke-n..</p>
                                                    <?php }else{?>
                                                            <input type="text" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>">
                                                   <?php }
                                                    }elseif ($a=="textarea"){?>
                                                        <textarea id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>"></textarea>
                                                    <?php }
                                                    elseif ($a=="date"){ ?>
                                                        <input type="date" class="form-control col-sm-6" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>">
                                                    <?php }
                                                    else{foreach ($value['bentuk_form'] as $k=>$v) {?>
                                                        
                                                        <select class="form-control" name="<?php echo $value['_id']?>[]" data-parsley-group="block0" data-search="true">
                                                        <option value="<?php echo set_select('$nama[]')?>">Pilih Salah satu</option>
                                                            <?php  foreach ($v as $data){?>
                                                                <option value="<?php echo $data?>"><?php echo $data?></option>
                                                            <?php }?>
                                                         </select>
                                                     <?php } ?> 
                                                    <?php }?>
                                                        <?php  echo form_error('$nama[]') ?>
                                                <?php }?>
                                            </div>
                                        </div>                                      
                                    <?php } // end foreach pertanyaan 
                                    ?>
                            </div>
                                <button type="submit" class="btn btn-primary" style="margin-left: 458px;">Simpan</button>
                            </form>
                        </div>
                    <?php }?>
                    <div class="class-md-6" style="float:center">
                        <button id="buttonTambah" type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i>TAMBAH FORM</button>
                        <button id="buttonReset" type="button" class="btn btn-default addButton"><i class="fa fa-minus"></i>RESET</button>
                    </div>

                    <div class="form-group">
                        <button  type="button" onclick="goBack()" class="btn btn-success btn-embossed"><< Kembali</button>
                         <button type="button" onclick="javascript:history.forward();" class="btn btn-success btn-embossed pull-right">Berikutnya >></button>
                    </div>
                </div>
             </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function goBack() {
    window.history.back();
}

$(document).ready(function(){
    untuk_di_duplikat = $( "#buku1" ).html();
    // console.log(untuk_di_duplikat);
    
    $( "#buttonTambah" ).click(function() {
      // alert(untuk_di_duplikat);
      $(untuk_di_duplikat).fadeIn('slow').appendTo('#buku1');
      $('select').select2();
        i++;
    });
    $( "#buttonReset" ).click(function() {
      // alert(untuk_di_duplikat);
      $( "#buku1" ).html('');
    });

});
</script>