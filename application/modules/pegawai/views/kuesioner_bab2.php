 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Form Identitas</strong> Pengisi Kuesioner  </h2>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row panel">
    <div class="col-lg-12">
        <div class="panel-header panel-controls">
            <h3><i class="icon-docs"></i> <strong>Lengkapi formulir berikut ini </strong> </h3>
        </div>

        <div class="panel-content">
            <ul class="nav nav-tabs">
                <li ><a href="#" ><i class="icon-docs"></i>Kegiatan Litbang</a></li>
                <li  class="active"><a href="#" ><i class="icon-docs"></i>Data Personil Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Kerjasama</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Belanja Ekstramural</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Jurnal Luaran Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Buku Teks Luaran Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>HKI Litbang</a></li>
                <li ><a href="#" ><i class="icon-docs"></i>Tanggapan dan Saran</a></li>
            </ul>
   

        <div class="content"> 
            <div class="row"><br>
             <?php
                 $message = $this->session->flashdata('pesan_error');
                  if(isset($message))
                  {
                    echo $message;
                  } 
                ?>
                    <?php foreach ($bab_3 as $key) {
                            $id = $key['_id'];
                            $pertanyaan = $this->pertanyaan_m->get_pertanyaan($id);
                    ?>
                        <div class="form-horizontal">
                            <form action="<?php echo site_url('./pegawai/kuesioner/simpan_kuesioner2/'.$id_keg) ?>" class="#surveyForm" method="POST">
                            <div id="personil1" class="clonedInput">
                                <h2 id="reference" name="reference" class="heading-reference">Personil Litbang</h2>
                                <?php foreach ($pertanyaan as $key=>$value) { ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" id="kalimat_tanya"><?php echo $value['kalimat_tanya'];?> </label>
                                        <div class="col-sm-6">
                                           <?php foreach ($value['bentuk_form'] as $a=>$b) {
                                                    $nama = $value['_id'];
                                                if ($a == "text"){?>
                                                    <input type="text" class="form-control col-sm-4" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>">
                                                <?php }elseif ($a=="textarea"){?>
                                                    <textarea id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>"></textarea>
                                                <?php }elseif ($a=="date"){ ?>
                                                    <input type="date" class="form-control col-sm-4" id="<?php echo $value['_id'] ?>" value="<?php echo set_value('$nama[]') ?>" name="<?php echo $value['_id']?>[]" placehorder="<?php echo $value['kalimat_tanya'] ?>">
                                                <?php }else{
                                                    foreach ($value['bentuk_form'] as $k=>$v) {
                                                        if (empty($v)){
                                                        //get data dari collection lain
                                                        if ($value['_id'] == "q_25"){
                                                    ?>
                                                            <!-- select Bidang Ilmu -->
                                                            <select class="form-control" name="q_25[]" id="q_25" data-parsley-group="block0"   data-search="true"> 
                                                                 <option > Pilih Bidang Ilmu </option>
                                                                <?php foreach ($sub_kelompok_ilmu as $data){
                                                                 ?>
                                                                    <option id="<?php echo $data['_id']?>" value="<?php echo $data['_id'] ?>"> <?php echo $data['nama_kelompok'] ?> </option>
                                                                <?php      
                                                                        } 
                                                                ?>
                                                            </select>
                    
                                                    <?php            
                                                     }
                                                    } // end if empty $v
                                                    else {
                                                        if ($value['_id'] == "q_19"){
                                                    ?>
                                                        <div class="input-group">
                                                        <select class="form-control" name="<?php echo $value['_id']?>[]" data-parsley-group="block0" data-search="true">
                                                            <option value="<?php echo set_select($value['_id'])?>">Pilih Salah satu</option>
                                                            <?php  foreach ($v as $data){?>
                                                                <option value="<?php echo $data?>"><?php echo $data?></option>
                                                            <?php }?>
                                                         </select>
                                                         <!-- icon modal untuk informasi jenis penelitian -->
                                                         <a class="input-group-addon" style="color:#319DB5"><i class="fa fa-info-circle" data-toggle="modal" data-target="#colored-header"></i></a>
                                                    </div>

                                            <?php }else{
                                                //get data pilihan?>

                                                        <select class="form-control" name="<?php echo $value['_id']?>[]" data-parsley-group="block0" data-search="true">
                                                            <option value="<?php echo set_select($value['_id'])?>">Pilih Salah satu</option>
                                                            <?php  foreach ($v as $data){?>
                                                                <option value="<?php echo $data?>"><?php echo $data?></option>
                                                            <?php }?>
                                                         </select>
                                            <?php                                                       
                                                   } } // end else
                                            ?> 
                                                 <?php } ?> 
                                                <?php }?>
                                                    <?php  echo form_error('$nama[]') ?>
                                            <?php }?>                                       
                                            </div>
                                        </div>                               
                                <?php } // end foreach pertanyaan 
                                ?>
                                <hr>
                            </div>
                            <button type="submit" class="btn btn-primary " style="margin-left: 458px;">Simpan</button>
                             <!--Modal Informasi untuk jabatan penelitian-->
                            <div class="modal fade" id="colored-header" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                                      <h4 class="modal-title"><strong>Jabatan</strong> Penelitian</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p>Klasifikasi Jabatan Penelitian:</p>
                                      <div class="p-t-20 m-b-20 p-l-40">
                                        <p><strong>Peneliti</strong></p>
                                        <p>Peneliti adalah para profesional yang terlibat dalam pembuatan konsep atau penciptaan pengetahuan baru, produk, proses, metoda, dan sistem serta profesional yang terlibat dalam pengelolaan proyek litbang.</p><hr>
                                        <p><strong>Teknisi</strong></p>
                                        <p>Teknisi adalah orang yang dalam melaksanakan tugas utamanya membutuhkan pengetahuan dan pengalaman teknis dalam satu atau lebih bidang engineering, Ilmu Pengetahuan Alam (IPA), atau Ilmu Pengetahuan Sosial dan Kemasyarakatan (IPSK).</p><hr>
                                        <p><strong>Staff Pendukung</strong></p>
                                        <p>para tukang/juru terlatih maupun tak terlatih, sekretaris dan juru tulis yang terlibat dalam proyek litbang atau secara langsung terkait dengan suatu proyek litbang</p>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Save changes</button>
                                    </div>
                                  </div>
                                </div>
                              </div><!--End Modal-->
                            </form>
                        </div>
                    <?php }?>
                    <div class="class-md-6">
                        <button id="buttonTambah" type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i>TAMBAH FORM</button>
                        <button id="buttonReset" type="button" class="btn btn-default addButton"><i class="fa fa-minus"></i>RESET</button>                      
                        
                    </div>
                    <div class="form-group">
                        <button  type="button" onclick="goBack()" class="btn btn-success btn-embossed"><< Kembali</button>
                         <button type="button" onclick="javascript:history.forward();" class="btn btn-success btn-embossed pull-right">Berikutnya >></button>
                    </div>
                </div>
             </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function goBack() {
    window.history.back();
}

$(document).ready(function(){
    untuk_di_duplikat = $( "#personil1" ).html();
    // console.log(untuk_di_duplikat);
    
    $( "#buttonTambah" ).click(function() {
      // alert(untuk_di_duplikat);
      $(untuk_di_duplikat).fadeIn('slow').appendTo('#personil1');
      $('select').select2();
        // i++;
    });
    $( "#buttonReset" ).click(function() {
      // alert(untuk_di_duplikat);
      $( "#personil1" ).html('');
    });

});
</script>