 <script type="text/javascript">
	var id_lembaga = "<?php echo $this->session->userdata('lembaga') ?>";
	var id_user = "<?php echo $this->session->userdata('user_id') ?>";
	var site_url = "<?php echo site_url('api/cetak_kegiatan') ?>"
</script>
 <!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Laporan</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li ><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
      <li class="active"> Laporan </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->

<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-header panel-controls">
				<h3><i class="icon-docs"></i><strong>Kegiatan</strong> Penelitian</h3>
			</div>

			<div class="panel-content" style="min-height:500px;">
				<a href="<?php echo site_url('report/cetak') ?>" class="btn btn-primary"><i class="icon-printer"></i>Cetak Laporan Hasil Kuesioner Lembaga </a>
				<br>
				<table class="table table-dynamic table-tools">
		          <thead>
		            <tr>
		              <th class="text-center">#</th>
		              <th class="text-center">Nama Kegiatan</th>
		              <th class="text-center">Tanggal Pengisian</th>
		              <th class="text-center">Laporan Kegiatan</th>
		            </tr>
		          </thead>
		          <tbody id="daftar-kegiatan">
		            <!-- <tr>
		              <td class="text-center">1</td>
		              <td>Lembaga 1</td>
		            </tr> -->
		          </tbody>
		        </table>
				
			</div>
		</div>
	</div>
</div>
<script src='<?php echo site_url('assets/plugins/pdfmake-master')?>/build/pdfmake.min.js'></script>
<script src='<?php echo site_url('assets/plugins/pdfmake-master')?>/build/vfs_fonts.js'></script>
<script>
function myFunction() {
    window.print();
}
$(function () {

	cetak_daftar_kegiatan = new get_daftar_kegiatan_by_lembaga(id_lembaga);
	console.log(cetak_daftar_kegiatan);		
	i = 1;
	$.each( cetak_daftar_kegiatan.kegiatan, function( key, value ) {
	  	str = '';
	  	tanggal_pengisian = '';
	  	// console.log(value.created_at.sec);
	  	if (typeof value.created_at.sec != 'undefined') {
	  		var currentDate = new Date(value.created_at.sec*1000)
		    var day = currentDate.getDate()
		    var month = currentDate.getMonth() + 1
		    var year = currentDate.getFullYear()
		    var tanggal_pengisian = day+'/'+month+'/'+year;	
	  	};
	  	
	  	str += '<tr><td class="text-center">'+i+'</td><td>'+value.q_27+'</td><td class="text-center">'+tanggal_pengisian+'</td><td class="text-center"><a target="_blank" href="'+site_url+'/'+value._id.$id+'" class="btn btn-primary"><i class="icon-printer"></i>Cetak Laporan Kegiatan</a></td></tr>'
	  	$('#daftar-kegiatan').append(str);
	  	i++;
	});
})
</script>