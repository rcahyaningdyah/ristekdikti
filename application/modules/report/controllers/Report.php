<?php 
	class Report extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','jawaban_temp_m','users_m','lembaga_m'));
			$this->load->helper(array('form', 'url', 'file'));
			if (($this->session->userdata('role')!= 'admin') ){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Laporan";
			
			$data['content'] = $this->load->view('home_report', $data, TRUE);
			
			$this->load->view('frame-template-admin',$data);
		}

		function tes(){
			$temp_id = $this->uri->segment(3);
			if ($temp_id){
				echo $temp_id;
			}
			else{
				echo "tidak ada uri";
			}
		}
		

		function lembaga(){
			$data['title']	 = "Laporan";
			
			$data['content'] = $this->load->view('lembaga_report', $data, TRUE);
			
			$this->load->view('frame-template-admin',$data);
		}

		function cetak(){
			//get id lembaga frame-template-admin
			$lembaga_id = $this->session->userdata['lembaga'];
			
			//get lembaga yang sama di collection users
			$data['get_lembaga'] = $this->users_m->get_data_by_lembaga($lembaga_id);
			$data['title']	 = "Laporan";
			
			$data['content'] = $this->load->view('cetak', $data, TRUE);
			
			$this->load->view('frame-template-admin',$data);
		
		}

	
	}
?>