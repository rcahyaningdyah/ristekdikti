<script type="text/javascript">
  var id_lembaga = "<?php echo $this->session->userdata('lembaga') ?>";
  var id_user = "<?php echo $this->session->userdata('user_id') ?>";
</script>
<!-- BEGIN BREADCRUMB AND TITLE ON PAGE -->
<div class="header">
  <h2><strong>Dashboard</strong> </h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li class="active"><a href="<?php echo site_url('') ?>">Depan</a>
      </li>
     
    </ol>
  </div>
</div>
<!-- END BREADCRUMB AND TITLE ON PAGE-->
 <div class="row">
 	<div class="col-lg-12">
 		<div class="panel">

 			<div class="panel-header panel-controls">
 				<h3><i class="icon-bar-chart"></i><strong>Kumpulan</strong> Survey </h3>
 			</div>

 			<div class="panel-content" style="min-height:500px;">
      <?php echo $this->session->userdata('lembaga'); ?>
 				<div class="tab_left">
 					<ul  class="nav nav-tabs ">
                      <li class="active"><a href="#tab3" data-toggle="tab"><i class="icon-docs"></i>Perbandingan anggaran Intramural dan Ekstramural</a></li>
                      <li><a href="#tab1" data-toggle="tab"><i class="icon-docs"></i>Jumlah Kegiatan per Jenis</a></li>
                      <li><a href="#tab2" data-toggle="tab"><i class="icon-docs"></i>Jenis dan Bidang penelitian</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane fade active in" id="tab3">
                        <div id="grafik3"></div>
                      </div>
                      <div class="tab-pane fade " id="tab1">
                  			<div id="grafik1" ></div>
                      </div>
                      <div class="tab-pane fade " id="tab2">
                        <div id="grafik2"></div>          
                      </div>
                    </div>
 				</div>
 			</div>
 		</div>
 	</div>
</div>
<script type="text/javascript">
  $(function () {
      grafik3 = new get_total_anggaran(id_lembaga);
      grafik1 = new get_kegiatan_bulan(id_lembaga);
      grafik2 = new get_bidang_jenis_kegiatan(id_lembaga);
      

      // README - GRAFIK 1 -> Jenis Kegiatan perbulan 
      // Array yang disiapkan : Dasar, Terapan, Eksperimental -> 12
      $('#grafik1').highcharts({
        title: {
            text: 'Jumlah Kegiatan per Bulan'
        },
        subtitle: {
              text: 'Source: <a href="http://ristek.go.id">Kemenristekdikti</a>'
          },
        xAxis: {
            categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        },
        labels: {
            items: [{
                html: 'Total Kegiatan',
                style: {
                    left: '50px',
                    top: '18px',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }]
        },
        series: [{
            type: 'column',
            name: 'Dasar',
            data: grafik1.dasar
        }, {
            type: 'column',
            name: 'Terapan',
            data: grafik1.terapan
        }, {
            type: 'column',
            name: 'Eksperimental',
            data: grafik1.eksperimental
        }, {
            type: 'spline',
            name: 'Average',
            data: grafik1.average,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            }
        }]
      });

      // README - GRAFIK 2 -> Jenis dan bidang penelitian
      // Array yang disiapkan : array per bidang dan jenis kegiatan, dalam berntuk presentase aja
      var colors = Highcharts.getOptions().colors,
      categories = ['Dasar', 'Terapan', 'Eksperimental'],
        data = [{
            y: grafik2.jumlahDasar,
            color: colors[0],
            drilldown: {
                name: 'Dasar',
                categories: grafik2.namaDasar,
                data: grafik2.perDasar,
                color: colors[0]
            }
        }, {
            y: grafik2.jumlahTerapan,
            color: colors[1],
            drilldown: {
                name: 'Terapan',
                categories: grafik2.namaTerapan,
                data: grafik2.perTerapan,
                color: colors[1]
            }
        }, {
            y: grafik2.jumlahEksperimental,
            color: colors[2],
            drilldown: {
                name: 'Eksperimental',
                categories: grafik2.namaEksperimental,
                data: grafik2.perEksperimental,
                color: colors[2]
            }
        }],
        browserData = [],
        versionsData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;
      // Build the data arrays
      for (i = 0; i < dataLen; i += 1) {

          // add browser data
          browserData.push({
              name: categories[i],
              y: data[i].y,
              color: data[i].color
          });

          // add version data
          drillDataLen = data[i].drilldown.data.length;
          for (j = 0; j < drillDataLen; j += 1) {
              brightness = 0.2 - (j / drillDataLen) / 5;
              versionsData.push({
                  name: data[i].drilldown.categories[j],
                  y: data[i].drilldown.data[j],
                  color: Highcharts.Color(data[i].color).brighten(brightness).get()
              });
          }
      }
      // Create the chart
      $('#grafik2 ').highcharts({
          chart: {
              type: 'pie'
          },
          title: {
              text: 'Jenis dan Bidang Kegiatan'
          },
          subtitle: {
              text: 'Source: <a href="http://ristek.go.id/">Ristekdikti</a>'
          },
          yAxis: {
              title: {
                  text: 'Total percent market share'
              }
          },
          plotOptions: {
              pie: {
                  shadow: false,
                  center: ['50%', '50%']
              }
          },
          tooltip: {
              valueSuffix: '%'
          },
          series: [{
              name: 'Presentase',
              data: browserData,
              size: '60%',
              dataLabels: {
                  formatter: function () {
                      return this.y > 5 ? this.point.name : null;
                  },
                  color: '#ffffff',
                  distance: -30
              }
          }, {
              name: 'Jumlah',
              data: versionsData,
              size: '80%',
              innerSize: '60%',
              dataLabels: {
                  formatter: function () {
                      // display only if larger than 1
                      return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + ' -' : null;
                  }
              }
          }]
      });

      // README - GRAFIK 3 -> Anggaran Intramural dan Ekstramural
      // Array yang disiapkan : jumlah anggaran intramural dan ekstramural per jenisnya 
      //Intramural : belanja DIPA, belana Kerjasama
      //Ekstramural : belanja ekstramural, belanja Upah
      $('#grafik3').highcharts({
          chart: {
              type: 'column'
          },
          title: {
              text: 'Total Anggaran Intramural dan Ekstramural'
          },
          subtitle: {
              text: 'Source: <a href="http://ristek.go.id">Kemenristekdikti</a>'
          },
          xAxis: {
              // Jenis Kegiatannya
              categories: ['Intramural', 'Ekstramural']
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Total Anggaran'
              },
              stackLabels: {
                  enabled: true,
                  style: {
                      fontWeight: 'bold',
                      color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                  }
              }
          },
          legend: {
              align: 'right',
              x: -30,
              verticalAlign: 'top',
              y: 25,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
              borderColor: '#CCC',
              borderWidth: 1,
              shadow: false
          },
          tooltip: {
              formatter: function () {
                  return '<b>' + this.x + '</b><br/>' +
                      this.series.name + ': ' + this.y + '<br/>' +
                      'Total: ' + this.point.stackTotal;
              }
          },
          plotOptions: {
              column: {
                  stacking: 'normal'
                  
              }
          },
          series: [{
              name: 'Belanja DIPA',
              data: grafik3.dipa
          }, {
              name: 'Belanja Kerjasama',
              data: grafik3.kerjasama
          },{
              name: 'Belanja Ekstramural',
              data: grafik3.ekstramural
          }, {
              name: 'Belanja Upah',
              data: grafik3.upah
          }]
      });
  });
</script>