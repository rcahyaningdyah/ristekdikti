<?php
	class Dashboard extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			 if ($this->session->userdata('role')!= 'admin'){
	            redirect ('login');
	        }
		}

		function index(){
			$data['title']	 = "Dashboard";
			
			$data['content'] = $this->load->view('home_dashboard', $data, TRUE);
			
			$this->load->view('frame-template-admin',$data);

		}

		function tes(){
			 $this->load->view('tes');
		}
	}
?>