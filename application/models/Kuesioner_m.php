<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kuesioner_m extends CI_Model {

	private $collection='kuesioner';
	 var $field = '';
    var $query = '';
    
    public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

  	function get_all_file()
    {
      $this->mongo_db->addIndex('kuesioner', array('title' => -1));
      return
      $this->mongo_db
      ->get($this->collection);
    }

    function get_file_by_id(){
       return $this->mongo_db
      ->where(['_id' => new MongoId($id)])
      ->get($this->collection);
    }

   
}
