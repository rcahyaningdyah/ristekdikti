<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Belanja_lembaga_m extends CI_Model {
		private $collection='belanja_lembaga';
	 	var $field = '';
    	var $query = '';
    
	    public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}

	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('belanja_lembaga', array('_id' => -1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
	    }

	    function get_lembaga_by_id($id_lembaga){
	    	 $this->mongo_db->addIndex('belanja_lembaga', array('_id' => 1));
      		 return $this->mongo_db
     			 ->where(['q_9' => $id_lembaga])
      			->get($this->collection);
	    }

	    function cek(){
	    	 $this->mongo_db->addIndex('belanja_lembaga', array('_id' => 1));
      		 return $this->mongo_db
     			 ->where(['id_user' => new Mongoid("5674c014c4365b0420000029")])
      			->get($this->collection);
	    }


	    function cek_data_lembaga(){
	    	$string_id = (string)$this->session->userdata('user_id');
	    	$query = array(
	    		'$and'=> array(
	    			array("id_user"=>new Mongoid($string_id)),
	    			array("q_9" => $this->session->userdata('lembaga'))
	    			));
	      		return $this->mongo_db
	     		 ->where($query)
	      		->get($this->collection);
	    }

	    function update_lembaga($string_id, $data){
	    	 return $this->mongo_db
          ->where(['_id' => new MongoId($string_id)])
          ->set($data)
          ->update($this->collection); 
	    }
	}
?>