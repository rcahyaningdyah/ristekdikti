<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Ilmu_m extends CI_Model {
		private $collection='kelompok_ilmu';
		var $field = '';
	    var $query = '';

		public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}


	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('kelompok_ilmu', array('_id' => 1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
    	}

    	
	}

?>