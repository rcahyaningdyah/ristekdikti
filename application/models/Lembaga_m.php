<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Lembaga_m extends CI_Model {
		private $collection='lembaga';
		 var $field = '';
	    var $query = '';


	    public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}

	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('lembaga', array('_id' => -1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
	    }

	    function get_lembaga_by_id($id_kementrian){
	    	$this->mongo_db->addIndex('lembaga', array('_id' => 1));
	    	return $this->mongo_db
	    	 ->orderBy(array('_id'=>1))
	    	->where(['id_kementrian' => $id_kementrian])
	    	->get($this->collection);
	    }

	    function get_lembaga_by_nama($lembaga_id){
	    	$this->mongo_db->addIndex('lembaga', array('_id' => 1));
	    	return $this->mongo_db
	    	 ->orderBy(array('_id'=>1))
	    	->where(['_id' => $lembaga_id])
	    	->get($this->collection);
	    }

	    function get_lembaga_by_idlembaga($id_lembaga){
	    	$this->mongo_db->addIndex('lembaga', array('_id' => 1));
	    	return $this->mongo_db
	    	 ->orderBy(array('_id'=>1))
	    	->where(['_id' => $id_lembaga])
	    	->get($this->collection);
	    }

	    function delete_by_id($string_id){
	        $id =array('_id' => $string_id);
	        return $this->mongo_db
	           ->where($id)
	           ->delete('lembaga');
   		 }

   		function cek_id($id){
   			   return 
	      $this->mongo_db
	      ->where(['_id' => $id]) 
	      ->get($this->collection);
   		}


   		function update_lembaga($string_id, $data){
   			  return $this->mongo_db
          ->where(['_id' => $string_id])
          ->set($data)
          ->update($this->collection);   
   		}



	}

?>