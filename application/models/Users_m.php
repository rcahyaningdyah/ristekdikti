<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_m extends CI_Model {

	private $collection='users';
	 var $field = '';
    var $query = '';
    
    public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

   function cek_login($email, $pass)
    {
        $data = ['q_7' => $email,
                 'password' => $pass];

        return $this->mongo_db
        ->where($data)
        ->get($this->collection);
    }

    function cek_role($username, $pass){
        $data = ['username' => $username];
        $data1 = ['password' => $pass];

        $query = $this->collection->findOne(array('username' => $data), array('password' => $data1));
        return $this->mongo_db
        ->where($query)
        ->get($this->collection);
    }

    function get_user($email){
         $this->mongo_db->addIndex('users', array('_id' => 1));
       return $this->mongo_db
      ->where(['q_7' => $email])
      ->get($this->collection);
    }

    function get_data_konfirmasi(){
        $status = "Belum dikonfirmasi";
         $this->mongo_db->addIndex('users', array('_id' => 1));
       return $this->mongo_db
      ->where(['status' => $status])
      ->get($this->collection);
    }

    function get_user_konfirm(){
         $status = "Sudah Dikonfirmasi";
         $this->mongo_db->addIndex('users', array('_id' => 1));
       return $this->mongo_db
      ->where(['status' => $status])
      ->get($this->collection);
    }

    function update_user($string_id, $data_user){
         return $this->mongo_db
          ->where(['_id' => new MongoId($string_id)])
          ->set($data_user)
          ->update($this->collection);   
    }


    function get_user_byId($string_id){
           $this->mongo_db->addIndex('users', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => new MongoId($string_id)])
      ->get($this->collection);
    }


    function cek_password_by_user($pass_lama){
      $pass = sha1($pass_lama);
       return
      $this->mongo_db
      ->where(['_id' => $this->session->userdata('user_id')],['password' => $pass])
      ->get($this->collection);
    }

    function update_password_by_user($data){
         return $this->mongo_db
          ->where(['_id' => new MongoId($this->session->userdata('user_id'))])
          ->set($data)
          ->update($this->collection);  
    }

    function update_tolak_konfirmasi($user_id, $data){
        return $this->mongo_db
          ->where(['_id' => new MongoId($user_id)])
          ->set($data)
          ->update($this->collection); 
    }


    function get_data_by_lembaga($lembaga_id){
         $this->mongo_db->addIndex('users', array('_id' => -1));
       return $this->mongo_db
      ->where(['q_9' => $lembaga_id])
      ->get($this->collection);
    }


    function get_data_role_admin(){
        return
        $this->mongo_db
      ->where(['role' => 'admin'])
      ->get($this->collection);
    }


    function get_data_role_pegawai(){
        return
        $this->mongo_db
      ->where(['role' => 'pegawai'])
      ->get($this->collection);
    }

    function get_data_admin_by_lembaga($id_lembaga){
      $query = array(
              '$and'=>array(
                  array('q_9' => $id_lembaga),
                array('role' => 'admin')
                )
              );
      
       return
        $this->mongo_db
      ->where($query)
      ->get($this->collection);
    }

    function get_pegawai_by_admin($id_lembaga){
       $query = array(
              '$and'=>array(
                  array('q_9' => $id_lembaga),
                  array('role' => 'pegawai'),
                  array('status' => 'Sudah Dikonfirmasi')
                )
              );
        return
        $this->mongo_db
      ->where($query)
      ->get($this->collection);
    }

   
}
