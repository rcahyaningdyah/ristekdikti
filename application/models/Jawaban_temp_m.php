<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jawaban_temp_m extends CI_Model {

	private $collection='jawaban_temp';
	 var $field = '';
    var $query = '';
    
    public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

  	function get_all_file()
    {
      $this->mongo_db->addIndex('jawaban_temp', array('_id' => -1));
      return
      $this->mongo_db
      ->get($this->collection);
    }

     function get_all_kegiatan($user){
      return
      $this->mongo_db
      ->where(['id_user' => new Mongoid($user)])
      ->get($this->collection);
    }

    function get_email($email){
        $this->mongo_db->addIndex('jawaban_temp', array('_id' => 1));
       return $this->mongo_db
      ->where(['q_7' => $email])
      ->get($this->collection);
    }

     function cek_kegiatan(){
      return
      $this->mongo_db
      ->where(['user_id' => $this->session->userdata('user_id')],['q_29' => " "])
      ->get($this->collection);
    }

    function get_temp($string_id){
        $this->mongo_db->addIndex('jawaban_temp', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => new MongoId($string_id)])
      ->get($this->collection);
    }


    function update_temp($string_temp, $data){
        return $this->mongo_db
          ->where(['_id' => new MongoId($string_temp)])
          ->set($data)
          ->update($this->collection);    
    }

    function delete_by_email($email){
         $data =array('email' =>$email);
        return $this->mongo_db
        ->where($id_meta)
        ->delete('jawaban_temp');
    } 


    function delete_by_id($string_id){
        $id =array('_id' =>new MongoId($string_id));
        return $this->mongo_db
           ->where($id)
           ->delete('jawaban_temp');
    }

    function get_kegiatan($nama_kegiatan){
      $this->mongo_db->addIndex('jawaban_temp', array('_id' => 1));
       return $this->mongo_db
      ->where(['q_27' => $nama_kegiatan])
      ->get($this->collection);
    }

    function update_jawaban($string_id,$data){
      return $this->mongo_db
          ->where(['_id' => new MongoId($string_id)])
          ->set($data)
          ->update($this->collection);  
    }




   
}
