<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pertanyaan_m extends CI_Model {

	private $collection='pertanyaan';
	 var $field = '';
    var $query = '';
    
    public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

  	function get_all_file()
    {
      $this->mongo_db->addIndex('pertanyaan', array('_id' => -1));
      return
      $this->mongo_db
      ->get($this->collection);
    }

   function get_pertanyaan($id){
      $this->mongo_db->addIndex('pertanyaan', array('_id' => 1));
       return $this->mongo_db
       ->orderBy(array('_id'=>1))
      ->where(['id_bab' => $id])
      ->get($this->collection);
   }

   function get_pertanyaan_bab2(){
    return
    $this->mongo_db
    ->orderBy(array('_id'=>1))
    ->where(['id_bab' => 'bab_2'],['_id'=>['$in'=>['q_9','q_10','q_11','q_12','q_13','q_14','q_15']]])
    ->get($this->collection);
   }


   function get_kalimat_tanya($id_pertanyaan){
       $this->mongo_db->addIndex('pertanyaan', array('_id' => 1));
       return $this->mongo_db
       ->orderBy(array('_id'=>1))
      ->where(['_id' => $id_pertanyaan])
      ->get($this->collection);
   }
}
