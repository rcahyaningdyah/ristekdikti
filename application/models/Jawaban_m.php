<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jawaban_m extends CI_Model {

  private $collection='jawaban';
   var $field = '';
    var $query = '';
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('mongo_db');
    }

    function get_all_file()
    {
      $this->mongo_db->addIndex('jawaban', array('_id' => -1));
      return
      $this->mongo_db
      ->get($this->collection);
    }

    function cek_user(){
      return 
      $this->mongo_db
      ->where(['user_id' => $this->session->userdata('user_id')],['q_1' =>" "]) 
      ->get($this->collection);
    }

    function get_all_kegiatan($user){
      return
      $this->mongo_db
      ->where(['id_user' => new Mongoid($user)])
      ->get($this->collection);
    }

    function kegiatan_byid($id){
      return 
      $this->mongo_db
      ->where(['_id' => new Mongoid($id)])
      ->get($this->collection);
    }

     function get_all_data()
    {
      $this->mongo_db->addIndex('jawaban', array('title' => -1));
      return
      $this->mongo_db
      ->get($this->collection);
    }

    function cek_kegiatan(){
      return
      $this->mongo_db
      ->where(['user_id' => $this->session->userdata('user_id')],['q_29' => " "])
      ->get($this->collection);
    }

    function get_kegiatan($nama_kegiatan){
      $this->mongo_db->addIndex('jawaban', array('_id' => 1));
       return $this->mongo_db
      ->where(['q_27' => $nama_kegiatan])
      ->get($this->collection);
    }

    function update_jawaban($string_id,$data){
      return $this->mongo_db
          ->where(['_id' => new MongoId($string_id)])
          ->set($data)
          ->update($this->collection);  
    }

    function get_data_belanja_byid($lembaga_id){
      return $this->mongo_db
        ->where(['id_lembaga'=> $lembaga_id])
        ->get($this->collection);
    }

     function belanja_kegiatan($lembaga_id){
      // $m= new MongoClient('localhost');
      $m = new MongoClient("mongodb://gamaboxristek:gamabox15juni2015@202.46.15.201/ristek");
      $db = $m->ristek;
      $collection = $db->jawaban;
      $belanja = [['$match'=>['id_lembaga' => $lembaga_id]],['$unwind' => '$q_42'],['$unwind' => '$q_45'],['$unwind' => '$q_35'],
              ['$unwind' => '$q_36'],['$unwind' => '$q_37'],['$unwind' => '$q_38'],
                ['$group'=>
                     [
                       '_id'=> null,
                       'BelanjaDipa'=> ['$last' => '$q_38'],
                       'BelanjaKerjasama'=> ['$sum'=> [ '$add'=> [ '$q_42']]],
                       'BelanjaEkstramural'=> ['$sum'=> [ '$add'=> [ '$q_45']]],
                       'BelanjaUpah'=> ['$last'=>  ['$add'=> [ '$q_35', '$q_36', '$q_37' ] ]],                   
                     ]
                ]
           ];
      // $belanja_lembaga = $this->mongo_db->where(array("id_lembaga" =>$lembaga_id))->get($this->collection);
      // echo "<pre>";var_dump($belanja_lembaga);exit;
      $belanja_group = $collection->aggregate($belanja);
    return $belanja_group;
    }

    function all_belanja_kegiatan(){
      // $m = new MongoClient('localhost');
      $m = new MongoClient("mongodb://gamaboxristek:gamabox15juni2015@202.46.15.201/ristek");
      $db = $m->ristek;
      $collection = $db->jawaban;
      $belanja_group = $collection->aggregate([['$unwind' => '$q_42'],['$unwind' => '$q_45'],
                   ['$group'=>
                       [   
                          '_id'=> null,
                         'BelanjaKerjasama'=> ['$sum'=> [ '$add'=> [ '$q_42']]],
                         'BelanjaEkstramural'=> ['$sum'=> [ '$add'=> [ '$q_45']]],
                         
                       ]
            ]
         ]);
      return $belanja_group;
    }

    //----Kegiatan per lembaga----//
    //count kegiatan dasar
    function count_dasar_des($lembaga_id){
      $awal = new MongoDate(strtotime('2015-12-01'));
      $akhir= New MongoDate(strtotime('2015-12-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_des($lembaga_id){
       $awal = new MongoDate(strtotime('2015-12-01'));
      $akhir= New MongoDate(strtotime('2015-12-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_des($lembaga_id){
       $awal = new MongoDate(strtotime('2015-12-01'));
      $akhir= New MongoDate(strtotime('2015-12-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_jan($lembaga_id){
      $awal = new MongoDate(strtotime('2015-01-01'));
      $akhir= New MongoDate(strtotime('2015-01-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }
    function count_terapan_jan($lembaga_id){
      $awal = new MongoDate(strtotime('2015-01-01'));
      $akhir= New MongoDate(strtotime('2015-01-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

     function count_eksperimental_jan($lembaga_id){
      $awal = new MongoDate(strtotime('2015-01-01'));
      $akhir= New MongoDate(strtotime('2015-01-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_feb($lembaga_id){
      $awal = new MongoDate(strtotime('2015-02-01'));
      $akhir= New MongoDate(strtotime('2015-02-29'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_feb($lembaga_id){
      $awal = new MongoDate(strtotime('2015-02-01'));
      $akhir= New MongoDate(strtotime('2015-02-29'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_feb($lembaga_id){
      $awal = new MongoDate(strtotime('2015-02-01'));
      $akhir= New MongoDate(strtotime('2015-02-29'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_mar($lembaga_id){
      $awal = new MongoDate(strtotime('2015-03-01'));
      $akhir= New MongoDate(strtotime('2015-03-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }


    function count_terapan_mar($lembaga_id){
      $awal = new MongoDate(strtotime('2015-03-01'));
      $akhir= New MongoDate(strtotime('2015-03-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }


    function count_eksperimental_mar($lembaga_id){
      $awal = new MongoDate(strtotime('2015-03-01'));
      $akhir= New MongoDate(strtotime('2015-03-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }


    function count_dasar_apr($lembaga_id){
      $awal = new MongoDate(strtotime('2015-04-01'));
      $akhir= New MongoDate(strtotime('2015-04-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_apr($lembaga_id){
      $awal = new MongoDate(strtotime('2015-04-01'));
      $akhir= New MongoDate(strtotime('2015-04-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_apr($lembaga_id){
      $awal = new MongoDate(strtotime('2015-04-01'));
      $akhir= New MongoDate(strtotime('2015-04-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_mei($lembaga_id){
      $awal = new MongoDate(strtotime('2015-05-01'));
      $akhir= New MongoDate(strtotime('2015-05-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_mei($lembaga_id){
      $awal = new MongoDate(strtotime('2015-05-01'));
      $akhir= New MongoDate(strtotime('2015-05-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_mei($lembaga_id){
      $awal = new MongoDate(strtotime('2015-05-01'));
      $akhir= New MongoDate(strtotime('2015-05-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }
    
    function count_dasar_jun($lembaga_id){
      $awal = new MongoDate(strtotime('2015-06-01'));
      $akhir= New MongoDate(strtotime('2015-06-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_jun($lembaga_id){
      $awal = new MongoDate(strtotime('2015-06-01'));
      $akhir= New MongoDate(strtotime('2015-06-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_jun($lembaga_id){
      $awal = new MongoDate(strtotime('2015-06-01'));
      $akhir= New MongoDate(strtotime('2015-06-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_jul($lembaga_id){
      $awal = new MongoDate(strtotime('2015-07-01'));
      $akhir= New MongoDate(strtotime('2015-07-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_jul($lembaga_id){
      $awal = new MongoDate(strtotime('2015-07-01'));
      $akhir= New MongoDate(strtotime('2015-07-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_jul($lembaga_id){
      $awal = new MongoDate(strtotime('2015-07-01'));
      $akhir= New MongoDate(strtotime('2015-07-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_aug($lembaga_id){
      $awal = new MongoDate(strtotime('2015-08-01'));
      $akhir= New MongoDate(strtotime('2015-08-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_aug($lembaga_id){
      $awal = new MongoDate(strtotime('2015-08-01'));
      $akhir= New MongoDate(strtotime('2015-08-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_aug($lembaga_id){
      $awal = new MongoDate(strtotime('2015-08-01'));
      $akhir= New MongoDate(strtotime('2015-08-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_sep($lembaga_id){
      $awal = new MongoDate(strtotime('2015-09-01'));
      $akhir= New MongoDate(strtotime('2015-09-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_sep($lembaga_id){
      $awal = new MongoDate(strtotime('2015-09-01'));
      $akhir= New MongoDate(strtotime('2015-09-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_sep($lembaga_id){
      $awal = new MongoDate(strtotime('2015-09-01'));
      $akhir= New MongoDate(strtotime('2015-09-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_okt($lembaga_id){
      $awal = new MongoDate(strtotime('2015-10-01'));
      $akhir= New MongoDate(strtotime('2015-10-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_terapan_okt($lembaga_id){
      $awal = new MongoDate(strtotime('2015-10-01'));
      $akhir= New MongoDate(strtotime('2015-10-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_eksperimental_okt($lembaga_id){
      $awal = new MongoDate(strtotime('2015-10-01'));
      $akhir= New MongoDate(strtotime('2015-10-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function count_dasar_nov($lembaga_id){
      $awal = new MongoDate(strtotime('2015-11-01'));
      $akhir= New MongoDate(strtotime('2015-11-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

     function count_terapan_nov($lembaga_id){
      $awal = new MongoDate(strtotime('2015-11-01'));
      $akhir= New MongoDate(strtotime('2015-11-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

     function count_eksperimental_nov($lembaga_id){
      $awal = new MongoDate(strtotime('2015-11-01'));
      $akhir= New MongoDate(strtotime('2015-11-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    ),
                array(
                  "id_lembaga"=>$lembaga_id)
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    //-----Semua count Kegiatan-----//
    //count kegiatan dasar
    function all_count_dasar_des(){
      $awal = new MongoDate(strtotime('2015-12-01'));
      $akhir= New MongoDate(strtotime('2015-12-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_des(){
       $awal = new MongoDate(strtotime('2015-12-01'));
      $akhir= New MongoDate(strtotime('2015-12-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_des(){
       $awal = new MongoDate(strtotime('2015-12-01'));
      $akhir= New MongoDate(strtotime('2015-12-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_jan(){
      $awal = new MongoDate(strtotime('2015-01-01'));
      $akhir= New MongoDate(strtotime('2015-01-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }
    function all_count_terapan_jan(){
      $awal = new MongoDate(strtotime('2015-01-01'));
      $akhir= New MongoDate(strtotime('2015-01-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_jan(){
      $awal = new MongoDate(strtotime('2015-01-01'));
      $akhir= New MongoDate(strtotime('2015-01-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_feb(){
      $awal = new MongoDate(strtotime('2015-02-01'));
      $akhir= New MongoDate(strtotime('2015-02-29'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    ),
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_feb(){
      $awal = new MongoDate(strtotime('2015-02-01'));
      $akhir= New MongoDate(strtotime('2015-02-29'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_feb(){
      $awal = new MongoDate(strtotime('2015-02-01'));
      $akhir= New MongoDate(strtotime('2015-02-29'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_mar(){
      $awal = new MongoDate(strtotime('2015-03-01'));
      $akhir= New MongoDate(strtotime('2015-03-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }


    function all_count_terapan_mar(){
      $awal = new MongoDate(strtotime('2015-03-01'));
      $akhir= New MongoDate(strtotime('2015-03-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }


    function all_count_eksperimental_mar(){
      $awal = new MongoDate(strtotime('2015-03-01'));
      $akhir= New MongoDate(strtotime('2015-03-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }


    function all_count_dasar_apr(){
      $awal = new MongoDate(strtotime('2015-04-01'));
      $akhir= New MongoDate(strtotime('2015-04-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_apr(){
      $awal = new MongoDate(strtotime('2015-04-01'));
      $akhir= New MongoDate(strtotime('2015-04-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_apr(){
      $awal = new MongoDate(strtotime('2015-04-01'));
      $akhir= New MongoDate(strtotime('2015-04-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_mei(){
      $awal = new MongoDate(strtotime('2015-05-01'));
      $akhir= New MongoDate(strtotime('2015-05-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_mei(){
      $awal = new MongoDate(strtotime('2015-05-01'));
      $akhir= New MongoDate(strtotime('2015-05-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_mei(){
      $awal = new MongoDate(strtotime('2015-05-01'));
      $akhir= New MongoDate(strtotime('2015-05-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }
    
    function all_count_dasar_jun(){
      $awal = new MongoDate(strtotime('2015-06-01'));
      $akhir= New MongoDate(strtotime('2015-06-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_jun(){
      $awal = new MongoDate(strtotime('2015-06-01'));
      $akhir= New MongoDate(strtotime('2015-06-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_jun(){
      $awal = new MongoDate(strtotime('2015-06-01'));
      $akhir= New MongoDate(strtotime('2015-06-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_jul(){
      $awal = new MongoDate(strtotime('2015-07-01'));
      $akhir= New MongoDate(strtotime('2015-07-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_jul(){
      $awal = new MongoDate(strtotime('2015-07-01'));
      $akhir= New MongoDate(strtotime('2015-07-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_jul(){
      $awal = new MongoDate(strtotime('2015-07-01'));
      $akhir= New MongoDate(strtotime('2015-07-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_aug(){
      $awal = new MongoDate(strtotime('2015-08-01'));
      $akhir= New MongoDate(strtotime('2015-08-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_aug(){
      $awal = new MongoDate(strtotime('2015-08-01'));
      $akhir= New MongoDate(strtotime('2015-08-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_aug(){
      $awal = new MongoDate(strtotime('2015-08-01'));
      $akhir= New MongoDate(strtotime('2015-08-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_sep(){
      $awal = new MongoDate(strtotime('2015-09-01'));
      $akhir= New MongoDate(strtotime('2015-09-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_sep(){
      $awal = new MongoDate(strtotime('2015-09-01'));
      $akhir= New MongoDate(strtotime('2015-09-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_sep(){
      $awal = new MongoDate(strtotime('2015-09-01'));
      $akhir= New MongoDate(strtotime('2015-09-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_okt(){
      $awal = new MongoDate(strtotime('2015-10-01'));
      $akhir= New MongoDate(strtotime('2015-10-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_terapan_okt(){
      $awal = new MongoDate(strtotime('2015-10-01'));
      $akhir= New MongoDate(strtotime('2015-10-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_eksperimental_okt(){
      $awal = new MongoDate(strtotime('2015-10-01'));
      $akhir= New MongoDate(strtotime('2015-10-31'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    function all_count_dasar_nov(){
      $awal = new MongoDate(strtotime('2015-11-01'));
      $akhir= New MongoDate(strtotime('2015-11-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Dasar"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

     function all_count_terapan_nov(){
      $awal = new MongoDate(strtotime('2015-11-01'));
      $akhir= New MongoDate(strtotime('2015-11-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Terapan"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

     function all_count_eksperimental_nov(){
      $awal = new MongoDate(strtotime('2015-11-01'));
      $akhir= New MongoDate(strtotime('2015-11-30'));
      $query = array(
            '$and'  => array(
                array(
                   "q_33"     => array(
                        '$gt'=>$awal,'$lt'=>$akhir
                    )
                ),
                array(
                    "q_29"  =>"Eksperimental"
                    )
                )
            );
     return $this->mongo_db
        ->where($query)
        ->count($this->collection);
    }

    


   function get_data_by_userid($user_id){
        return 
      $this->mongo_db
      ->where(['id_user' => new Mongoid($user_id)])
      ->get($this->collection);
   }

   function get_all_kegiatan_dasar(){
       return $this->mongo_db
      ->where(["q_29"=>"Dasar"])
      ->get($this->collection);
   }

   function get_all_kegiatan_terapan(){
       return $this->mongo_db
      ->where(["q_29"=>"Terapan"])
      ->get($this->collection);
   }

   function get_all_kegiatan_eksperimental(){
       return $this->mongo_db
      ->where(["q_29"=>"Eksperimental"])
      ->get($this->collection);
   }

   function kegiatan_dasar($lembaga_id){
      // var_dump($lembaga_id);
      return $this->mongo_db
      ->where(array('$and' => array(
          array("id_lembaga" => $lembaga_id),
          array("q_29" => "Dasar")
        )))
      // ->where(['$and'=>["q_29"=>"Dasar"],["id_lembaga"=>$lembaga_id]])
      ->get($this->collection);
   }

   function kegiatan_terapan($lembaga_id){
      return $this->mongo_db
      ->where(array('$and' => array(
          array("id_lembaga" => $lembaga_id),
          array("q_29" => "Terapan")
        )))
      // ->where(["q_29"=>"Terapan"])
      ->get($this->collection);
   }

   function kegiatan_eksperimental($lembaga_id){
      return $this->mongo_db
      ->where(array('$and' => array(
          array("id_lembaga" => $lembaga_id),
          array("q_29" => "Eksperimental")
        )))
      // ->where(["q_29"=>"Eksperimental"])
      ->get($this->collection);
   }

   function get_kegiatan_by_id($id_kegiatan){
      $string_id = (string)$id_kegiatan;
      return $this->mongo_db
      ->where(["_id"=>new Mongoid($string_id)])
      ->get($this->collection);
   }

   function get_kegiatan_by_lembaga($id_lembaga){
        return $this->mongo_db
      ->where(["id_lembaga"=>$id_lembaga])
      ->get($this->collection);
   }

   function get_distinct_lembaga(){
        $m = new MongoClient("185.28.22.104");
        $db = $m->ristek;
        $collection = $db->jawaban;
        $lembaga = $collection->distinct("id_lembaga");
        return $lembaga;
   }

  
   
}
