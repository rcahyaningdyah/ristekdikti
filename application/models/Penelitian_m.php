<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Penelitian_m extends CI_Model {
		private $collection='penelitian';
		var $field = '';
	    var $query = '';

		public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}


	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('penelitian', array('_id' => 1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
    	}

    	function get_name_by_id($id_penelitian){
    		  return
       			 $this->mongo_db
      		->where(['_id' => $id_penelitian])
      		->get($this->collection);
    	}


    	
	}

?>