<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Kelompok_ilmu_m extends CI_Model {
		private $collection='sub_kelompok_ilmu';
		var $field = '';
	    var $query = '';

		public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}


	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('sub_kelompok_ilmu', array('_id' => 1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
    	}

    	function get_data_by_id($id_ilmu){
    		$this->mongo_db->addIndex('sub_kelompok_ilmu', array('_id' => 1));
	       return $this->mongo_db
	       ->orderBy(array('_id'=>1))
	      ->where(['kelompok_id' => $id_ilmu])
	      ->get($this->collection);
    	}

    	function get_name_by_id($id_ilmu){
    		  return
       			 $this->mongo_db
      		->where(['_id' => $id_ilmu])
      		->get($this->collection);
    	}

    	
	}

?>