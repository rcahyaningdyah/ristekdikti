<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Bidang_penelitian_m extends CI_Model {
		private $collection='bidang_penelitian';
		var $field = '';
	    var $query = '';

		public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}


	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('bidang_penelitian', array('_id' => -1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
    	}

    	function get_data_by_id($id_penelitian){
    		$this->mongo_db->addIndex('bidang_penelitian', array('_id' => 1));
	       return $this->mongo_db
	       ->orderBy(array('_id'=>1))
	      ->where(['id_kelpen' => $id_penelitian])
	      ->get($this->collection);
    	}


    	function get_nama_by_id($id_bidpen){
    		$this->mongo_db->addIndex('bidang_penelitian', array('_id' => 1));
	       return $this->mongo_db
	       ->orderBy(array('_id'=>1))
	      ->where(['_id' => $id_bidpen])
	      ->get($this->collection);
    	}

    	

    	
	}

?>