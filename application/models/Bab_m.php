<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bab_m extends CI_Model {

	private $collection='bab';
	 var $field = '';
    var $query = '';
    
    public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

  	function get_all_file()
    {
      $this->mongo_db->addIndex('bab', array('_id' => -1));
      return
      $this->mongo_db
      ->get($this->collection);
    }

    function get_bab_1(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_1'])
      ->get($this->collection);
    }

    function get_bab_2(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_2'])
      ->orderBy(array('id_bab'=>1))
      ->get($this->collection);
    }

    function get_bab_3(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_3'])
      ->get($this->collection);
    }

    function get_bab_4(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_4'])
      ->get($this->collection);
    }

    function get_bab_5(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_5'])
      ->get($this->collection);
    }

    function get_bab_6(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_6'])
      ->get($this->collection);
    }

    function get_bab_7(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_7'])
      ->get($this->collection);
    }

    function get_bab_8(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_8'])
      ->get($this->collection);
    }

    function get_bab_9(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_9'])
      ->get($this->collection);
    }

    function get_bab_10(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_10'])
      ->get($this->collection);
    }

    function get_bab_11(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_11'])
      ->get($this->collection);
    }

    function get_bab_12(){
      $this->mongo_db->addIndex('bab', array('_id' => 1));
       return $this->mongo_db
      ->where(['_id' => 'bab_12'])
      ->get($this->collection);
    }

    // untuk form dinamis
    // function get_bab(){
    //   $id_kuesioner = 'kuesioner_1';
    //    $m = new MongoClient("192.168.0.201");
    //   $db = $m->ristek;
    //   $collection = $db->bab;
    //   $id_kuesioner = "kuesioner_1";
    //   $pertanyaan_kuesioner = array(
    //     '_id' => array('$in'=>array('bab_3','bab_4','bab_5','bab_6','bab_7','bab_8','bab_9','bab_10','bab_11')),
    //     'id_kuesioner' => $id_kuesioner);
    //   $question = $collection->find($pertanyaan_kuesioner);
    //   $question = $question->sort(array("_id",1));
    //   // var_dump($question);exit;
    //   return $question;
    // }

    // function get_bab_daftar(){
    //    $m = new MongoClient("l92.168.0.201");
    //   $db = $m->ristek;
    //   $collection = $db->bab;
    //   $id_kuesioner = "kuesioner_1";
    //   $pertanyaan_daftar = array(
    //     '_id' => array('$in'=>array('bab_2','bab_1')),
    //     'id_kuesioner' => $id_kuesioner);
    //   $identitas = $collection->find($pertanyaan_daftar)->sort(array("_id"=>1));
    //   return $identitas;
    // }

}
