<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Sub_penelitian_m extends CI_Model {
		private $collection='sub_penelitian';
		var $field = '';
	    var $query = '';

		public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}


	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('sub_penelitian', array('_id' => -1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
    	}

    	function get_data_by_id($id_bidpen){
    		$this->mongo_db->addIndex('sub_penelitian', array('_id' => 1));
	       return $this->mongo_db
	       ->orderBy(array('_id'=>1))
	      ->where(['id_bidpen' => $id_bidpen])
	      ->get($this->collection);
    	}
    	
	}

?>