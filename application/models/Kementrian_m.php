<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Kementrian_m extends CI_Model {
		private $collection='kementrian';
		var $field = '';
	    var $query = '';

		public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}


	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('kementrian', array('_id' => 1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
    	}

    	function get_name_by_id($id_kementrian){
    		  return
       			 $this->mongo_db
      		->where(['_id' => $id_kementrian])
      		->get($this->collection);
    	}

    	function count_kementrian(){
    		return 
    		$this->mongo_db
    		->count($this->collection);
    	}

    	function update_kementrian($string_id, $data){
         return $this->mongo_db
          ->where(['_id' => $string_id])
          ->set($data)
          ->update($this->collection);   
   		 }

       function delete_by_id($string_id){
          $id =array('_id' => $string_id);
          return $this->mongo_db
             ->where($id)
             ->delete('kementrian');
       }
	}

?>