<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Puslitbang_m extends CI_Model {
		private $collection='puslitbang';
		var $field = '';
	    var $query = '';

		public function __construct()
	  	{
	  		  parent::__construct();
	        $this->load->library('mongo_db');
	  	}


	  	function get_all_file()
	    {
	      $this->mongo_db->addIndex('puslitbang', array('_id' => 1));
	      return
	      $this->mongo_db
	      ->get($this->collection);
    	}

    	function get_data_by_id($id_lembaga){
    		$this->mongo_db->addIndex('puslitbang', array('_id' => 1));
	       return $this->mongo_db
	       ->orderBy(array('_id'=>1))
	      ->where(['id_lembaga' => $id_lembaga])
	      ->get($this->collection);
    	}

      function get_all_by_id($id_puslitbang){
        $this->mongo_db->addIndex('puslitbang', array('_id' => 1));
         return $this->mongo_db
         ->orderBy(array('_id'=>1))
        ->where(['_id' => $id_puslitbang])
        ->get($this->collection);
      }

    	function count_litbang(){
    		return
    		$this->mongo_db
    		->count($this->collection);
    	}


    	 function delete_by_id($string_id){
          $id =array('_id' => $string_id);
          return $this->mongo_db
             ->where($id)
             ->delete('puslitbang');
       }

       function update_puslitbang($string_id, $data){
   			  return $this->mongo_db
          ->where(['_id' => $string_id])
          ->set($data)
          ->update($this->collection);   
   		}
    	
	}

?>