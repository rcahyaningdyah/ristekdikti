<!-- css from template, used in all pages -->
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css'); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/theme.css'); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/ui.css'); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/ol.css'); ?>" >
<!-- end of css from template -->

<!-- css from plugin page style-->
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/plugins/metrojs/metrojs.min.css'); ?>" >
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/plugins/maps-amcharts/ammap/ammap.min.css'); ?>" >
<!-- end of css from plugin page style -->

<!-- javascript -->
<script src="<?php echo site_url('assets/plugins/jquery/jquery-1.11.1.min.js') ?>"></script>
 <script src="<?php echo site_url('assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') ?>"></script>

<!-- end of javascript -->

<script type="text/javascript">
	var base_apps_url = "<?php echo site_url(); ?>";
</script>

    
   
    

