
   
    <script src="<?php echo site_url('assets/plugins/jquery/jquery-migrate-1.2.1.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/plugins/gsap/main-gsap.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/plugins/jquery-cookies/jquery.cookies.min.js') ?>"></script> <!-- Jquery Cookies, for theme -->
    <script src="<?php echo site_url('assets/plugins/jquery-block-ui/jquery.blockUI.min.js') ?>"></script> <!-- simulate synchronous behavior when using AJAX -->
    <script src="<?php echo site_url('assets/plugins/translate/jqueryTranslator.min.js') ?>"></script> <!-- Translate Plugin with JSON data -->
    <script src="<?php echo site_url('assets/plugins/bootbox/bootbox.min.js') ?>"></script> <!-- Modal with Validation -->
    <script src="<?php echo site_url('assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js') ?>"></script> <!-- Custom Scrollbar sidebar -->
    <script src="<?php echo site_url('assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js') ?>"></script> <!-- Show Dropdown on Mouseover -->
    <script src="<?php echo site_url('assets/plugins/charts-sparkline/sparkline.min.js') ?>"></script> <!-- Charts Sparkline -->
    <script src="<?php echo site_url('assets/plugins/select2/select2.min.js') ?>"></script> <!-- Select Inputs -->
    <script src="<?php echo site_url('assets/plugins/icheck/icheck.min.js') ?>"></script> <!-- Checkbox & Radio Inputs -->
    <script src="<?php echo site_url('assets/plugins/backstretch/backstretch.min.js') ?>"></script> <!-- Background Image -->
    <script src="<?php echo site_url('assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>"></script> <!-- Animated Progress Bar -->
    <script src="<?php echo site_url('assets/plugins/bootstrap/js/jasny-bootstrap.min.js')?>"></script><!--input mask-->
    <script src="<?php echo site_url('assets/plugins/input-mask/jquery.maskMoney.js')?>"></script><!--input mask-->
    <script src="<?php echo site_url('assets/plugins/multidatepicker/multidatespicker.min.js')?>"></script> <!-- Multi dates Picker -->
   
    
	<script src="<?php echo site_url('assets/js/builder.js') ?>"></script> <!-- Theme Builder -->
    <script src="<?php echo site_url('assets/js/sidebar_hover.js') ?>"></script> <!-- Sidebar on Hover -->
    <script src="<?php echo site_url('assets/js/application.js') ?>"></script> <!-- Main Application Script -->
    <script src="<?php echo site_url('assets/js/plugins.js') ?>"></script> <!-- Main Plugin Initialization Script -->
    <script src="<?php echo site_url('assets/js/widgets/notes.js') ?>"></script> <!-- Notes Widget -->
    <script src="<?php echo site_url('assets/js/quickview.js') ?>"></script> <!-- Chat Script -->
    <script src="<?php echo site_url('assets/js/pages/search.js') ?>"></script> <!-- Search Script -->

     <!-- BEGIN PAGE SCRIPT -->
    <script src="<?php echo site_url('assets/plugins/step-form-wizard/plugins/parsley/parsley.min.js')?>"></script>
    
    <script src="<?php echo site_url('assets/js/pages/form_wizard.js') ?>"> </script>
    <script src="<?php echo site_url('assets/plugins/noty/jquery.noty.packaged.min.js') ?>"></script>  <!-- Notifications -->
    <script src="<?php echo site_url('assets/plugins/bootstrap-editable/js/bootstrap-editable.min.js') ?>"></script> <!-- Inline Edition X-editable -->
    <script src="<?php echo site_url('assets/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js') ?>"></script> <!-- Context Menu -->
    <script src="<?php echo site_url('assets/plugins/metrojs/metrojs.min.js') ?>"></script> <!-- Flipping Panel -->
    <script src="<?php echo site_url('assets/plugins/charts-chartjs/Chart.min.js') ?>"></script>  <!-- ChartJS Chart -->
   <script src="<?php echo site_url('assets/plugins/charts-highchart/js/highcharts.js') ?>"></script> 
 <script src="<?php echo site_url('assets/plugins/charts-highchart/js/modules/exporting.js') ?>"></script>
 <script src="<?php echo site_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
 <script src="<?php echo site_url('assets/js/ristek-main.js') ?>"></script>

    <!-- END PAGE SCRIPT -->

