 <div class="logopanel">
 	<h1>
        <a href="<?php echo site_url('') ?>">
        <img src="<?php echo site_url('assets/images/logo/logo-ristekdikti.png') ?>" style="width:69px;height:42px;margin-left:34px;">
        </a>
    </h1>
</div> 
<div class="sidebar-inner">
  	<div class="sidebar-top">
	    <form action="search-result.html" method="post" class="searchform" id="search-results">
	      <input type="text" class="form-control" name="keyword" placeholder="Pencarian">
	    </form>
	    <div class="userlogged clearfix">
	      	<!-- show avatar -->
	    			
	      		<img src="" class="img-responsive img-circle" alt="friend 8" style="
    				width: 85px;">
	      
	      		<i class="icon icons-faces-users-03"></i>
	      
	      	<div class="user-details">
	      		
		        <h4>UserName</h4> 
		        <br/>
		       
	      	</div>
	    </div>
  	</div>
  	<div class="menu-title">
   		 Menu 
  	</div>
  	<ul class="nav nav-sidebar">
	    <li id="navhome"><a href="<?php echo site_url('dashboard'); ?>"><i class="icon-home"></i><span data-translate="Dashboard">Dashboard</span></a></li>
	    <li id="navmap"><a href="<?php echo site_url('report');?>"><i class="icon-map"></i><span data-translate="Laporan">Laporan</span></a></li>
	    <li id="navtreeview"><a href="<?php echo site_url('kelola');?>"><i class="icon-layers"></i><span data-translate="Tata Kelola">Tata Kelola</span></a></li>
	    <li id="belanjalitbang"><a href="<?php echo site_url('kelola/belanja');?>"><i class="icon-basket"></i><span data-translate="Belanja Litbang">Belanja Litbang</span></a></li>
	    <li id="profil"><a href="<?php echo site_url('kelola/Profil');?>"><i class="icon-user"></i><span data-translate="Profil">Profil</span></a></li>
  	</ul>
  	<div class="sidebar-footer clearfix">
	    <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
	    	<i class="icon-size-fullscreen"></i>
		</a>
		<form  id="myform" action="" method="post" >
        	
	    	<a href="javascript: submitform()" class="pull-left btn-effect" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
	    		<i class="icon-power"></i>
	   	 	</a>
	   	</form>
 	</div>
</div>

<script type="text/javascript">
function submitform()
{
    document.forms["myform"].submit();
}
</script>