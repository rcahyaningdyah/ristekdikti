<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
         <!-- BEGIN CSS SCRIPT -->
        <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/ui.css'); ?>" rel="stylesheet">

         <!-- END CSS SCRIPT -->

	</head>

	<body class="sidebar-condensed error-page">
		 <div class="row">
            <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="error-container">
                    <div class="error-main">
                        <h1><span id="404"></span></h1>
                        <h3><span id="404-txt"></span></h3>
                        <h4><span id="404-txt-2"></span></h4>
                        <br>
                        <div class="row" id="content-404">                            
                            <div class="col-md-12 text-center">
                                <br><br><br>
                                <div class="btn-group">
                                    
                                    <a class="btn btn-white" href="<?php echo site_url(''); ?>">
                                    <i class="icon-login"></i> Home
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="copyright">© Copyright Gamabox Enterprise, 2015. <span>All rights reserved.</span></div>
        </div>
         <!-- BEGIN SCRIPT FUNCTION -->
          <script src="<?php echo site_url('assets/plugins/jquery/jquery-1.11.1.min.js') ?>"></script>
          <script src="<?php echo site_url('assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js') ?>"></script>
          <script src="<?php echo site_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
          <script src="<?php echo site_url('assets/plugins/typed/typed.js') ?>"></script>
              <script>
            $(function(){
                $("#404").typed({
                    strings: ["404"],
                    typeSpeed: 200,
                    backDelay: 500,
                    loop: false,
                    contentType: 'html',
                    loopCount: false,
                    callback: function() {
                        $('h1 .typed-cursor').css('-webkit-animation', 'none').animate({opacity: 0}, 400);
                        $("#404-txt").typed({
                            strings: ["It seems that this page doesn't exist or has been removed."],
                            typeSpeed: 1,
                            backDelay: 500,
                            loop: false,
                            contentType: 'html', 
                            loopCount: false,
                            callback: function() {
                                $('h3 .typed-cursor').css('-webkit-animation', 'none').animate({opacity: 0}, 400);
                                $("#404-txt-2").typed({
                                    strings: ["Go back to homepage. "],
                                    typeSpeed: 1,
                                    backDelay: 500,
                                    loop: false,
                                    contentType: 'html', 
                                    loopCount: false,
                                    callback: function() {
                                        $('#content-404').delay(300).slideDown();
                                    },
                                }); 
                            }
                        });  
                    }
                });  
            });
        </script>
         <!-- END SCRIPT FUNCTION -->
	</body>
</html>