<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title; ?> | GAMA-RISTEK</title>
        <?php $this->load->view('partial/head_script');?>
        <!-- css in selected pages -->
 		<?php //echo $content_css; ?>
 		<!-- end of css in selected pages -->
        
	</head>

	<body  class="fixed-topbar fixed-sidebar theme-sdtl color-default sidebar-top">
		<?php echo $content; ?>
	</body>
</html>

