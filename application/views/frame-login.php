<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title; ?> | GAMA-RISTEK</title>
        <?php $this->load->view('partial/head_script');?>
        <!-- css in selected pages -->
        <?php //echo $content_css; ?>
        <!-- end of css in selected pages -->
        
    </head>

    <body  class="fixed-topbar fixed-sidebar theme-sdtl color-default sidebar-top">
        <section>
            <!--README BEGIN SIDEBAR -->
            <div class="sidebar">
                 <div class="logopanel">
                        <!-- <img src="<?php echo site_url('assets/images/logo/logo-system.png')?>" ></img> -->
                </div> 
                <div class="sidebar-inner">
                    <div class="sidebar-top">
                        <form action="search-result.html" method="post" class="searchform" id="search-results">
                          <input type="text" class="form-control" name="keyword" placeholder="Pencarian">
                        </form>
                        <div class="userlogged clearfix">
                            <!-- show avatar -->
                                    
                                <img src="" class="img-responsive img-circle" alt="friend 8" style="
                                    width: 85px;">
                          
                                <i class="icon icons-faces-users-03"></i>
                          
                            <div class="user-details">
                                
                                <h4>UserName</h4> 
                                <br/>
                               
                            </div>
                        </div>
                    </div>
                    <div class="menu-title">
                         Menu 
                    </div>
                    <!-- <ul class="nav nav-sidebar">
                        <li id="navhome"><a href=""><i class="icon-home"></i><span data-translate="Home">Home</span></a></li>
                        <li id="navmap"><a href=""><i class="icon-map"></i><span data-translate="Map">Map</span></a></li>
                        <li id="navtreeview"><a href=""><i class="icon-layers"></i><span data-translate="TreeView">TreeView</span></a></li>
                        <li id="navproduction"><a href=""><i class="icon-graph"></i><span data-translate="Production Report">Production Report</span></a></li>
                        <li id="navupload"><a href=""><i class="icon-arrow-up"></i><span data-translate="Batch Upload">Batch Upload</span></a></li>
                        <li id="navdocument"><a href=""><i class="icon-doc"></i><span data-translate="Documents">Documents</span></a></li>
                        <li id="navinfo"><a href=""><i class="icon-info"></i><span data-translate="Info & Help">Info & Help</span></a></li>
                    </ul> -->
                    <div class="sidebar-footer clearfix">
                        <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
                            <i class="icon-size-fullscreen"></i>
                        </a>
                        <form  id="myform" action="" method="post" >
                            
                            <a href="javascript: submitform()" class="pull-left btn-effect" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
                                <i class="icon-power"></i>
                            </a>
                        </form>
                    </div>
                </div>
            </div>  
            <!-- END SIDEBAR -->

            <!-- BEGIN MAIN CONTENT -->
            <div class="main-content">
                <!-- README BEGIN TOPBAR -->
                <div class="topbar">
                   <div class="header-left">
                        <div class="topnav">
                          <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
                        </div>
                    </div>
                    <div class="header-right" >
                        <ul class="header-menu nav navbar-nav">
                            <!-- BEGIN USER DROPDOWN -->
                           
                         <!--    <li class="dropdown" id="user-header">
                                 <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                                   
                                        <span class="username"> &nbsp; Hi, UserName </span>
                                   
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href=""><i class="icon-user"></i><span>My Profile</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-settings"></i><span>Account Settings</span></a>
                                    </li>
                                    <li>
                                        <form  id="myform1" action="" method="post" >
                                           
                                            <a href="javascript: submitform()"><i class="icon-logout">
                                                </i><span>Logout</span>
                                            </a>
                                        </form>
                                    </li>
                                </ul>
                            </li>  -->  
                            <!-- END USER DROPDOWN -->
                           
                        </ul>
                    </div>
                </div>
                <!-- END TOPBAR -->

                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin" >
                    
                        <?php echo $content; ?>
                    
                    
                    <div class="footer">
                        <div class="copyright">
                            <p class="pull-left sm-pull-reset">
                                <span>Copyright <span class="copyright">©</span> 2015 </span>
                                <span>GAMABOX.</span>
                                <span>All rights reserved.</span>
                            </p>
                            <p class="pull-right sm-pull-reset">
                                <span><a href="" class="m-r-10">Support</a> | <a href="<?php echo site_url('dashboard/termofuse');?>" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
            <!-- END MAIN CONTENT -->
        </section>

     

        <!-- BEGIN PRELOADER -->
        <div class="loader-overlay">
          <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
          </div>
        </div>
        <!-- END PRELOADER -->

         <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 

        <!-- BEGIN SCRIPT ALL PAGES --> 
        <?php $this->load->view('partial/footer_script');?>
        <!-- END SCRIPT ALL PAGES -->
        <!-- BEGIN SCRIPT FUNCTION -->
        <?php // echo $content_js; ?>
        <!-- END SCRIPT FUNCTION -->
    </body>
</html>

