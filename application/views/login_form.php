<!DOCTYPE html>
<html>
<head>
	<title>GamaBox-ep SSO</title>
	<meta charset="utf-8">
   	<!-- BEGIN CSS SCRIPT -->
    <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo site_url('assets/css/ui.css'); ?>" rel="stylesheet">
   
    <!-- END CSS SCRIPT -->
</head>
<body class="account" data-page="login">
	 <!-- BEGIN LOGIN BOX -->
    <div class="container" id="login-block">
         <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="account-wall">
                    <i class="user-img icons-faces-users-03"></i>
                </div>
            </div>
        </div>
          <form class="form-signin" role="form">
            <div class="append-icon">
                <input type="text" name="name" id="name" class="form-control form-white username" placeholder="Username" required>
                <i class="icon-user"></i>
            </div>
            <div class="append-icon m-b-20">
                <input type="password" name="password" class="form-control form-white password" placeholder="Password" required>
                <i class="icon-lock"></i>
            </div>
            <button type="submit" id="submit-form" class="btn btn-lg btn-primary btn-block ladda-button" data-style="expand-left">Sign In</button>
        </form>
      

    	 <p class="account-copyright">
            <span>Copyright © 2015 </span><span>Gamabox Enterprise</span>.<span>All rights reserved.</span>
        </p>
    </div>
     <!-- BEGIN SCRIPT FUNCTION -->
   
   
    <!-- END SCRIPT FUNCTION -->
</body>
</html>