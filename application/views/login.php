<div class="row">
   <div class="col-md-12">
   		<div class="col-md-8">
   			<div class="panel">
   				<div class="panel-header">
   					<h3> <strong> Selamat Datang,</strong> </h3>
   				</div>
   				<div class="panel-content">
            <!-- <img id="img1" src="<?php echo site_url('./assets/images/logo/logo-ristekdiktimini.png')?>" /> -->
   					<h1 style="display:inline">Kuesioner Litbang Pemerintah-Kemenristekdikti</h1>
   					<p>Kuesioner online ini berisi pertanyaan yang berkaitan dengan seluruh kegiatan penelitian dan pengembangan
   					 (selanjutnya disebut litbang) yang dilaksanakan sepanjang tahun anggaran 2015, 
   					 terhitung mulai 1 Januari 2015 sampai dengan 31 Desember 2015. Yang dimaksud dengan 
   					 “seluruh kegiatan” adalah semua kegiatan baik dari anggaran sendiri atau yang didanai oleh instansi pemerintah lain, pihak industri dan luar negeri.</p> 
   					<p>Kegiatan penelitian yang didanai melalui skema insentif Ristek maupun insentif Dikti atau skema Grant lainnya dari berbagai pihak, termasuk ke dalam kegiatan penelitian yang dimaksud dalam kuesioner ini.</p>
   				</div>
   			</div>
   			<div class="panel">
   				<div class="panel-header">
   					<h3> <strong>Petunjuk Pengisian Kuesioner Online</strong> </h3>
   				</div>
   				<div class="panel-content">
   					<h3>Pengertian Penelitian dan Pengembangan</h3>

						<p>Kegiatan Penelitian dan Pengembangan adalah aktivitas kreatif yang dilakukan dengan sistematis untuk
						 menambah pengetahuan (stock of knowledge), termasuk pengetahuan tentang manusia, kebudayaan dan masyarakat 
						 (knowledge of man, culture and society), dan pemanfaatan pengetahuan ini untuk merancang penerapan baru (to devise new applications).</p>

						<p>Aktivitas berikut ini tidak dapat dikelompokkan ke dalam aktivitas litbang (kecuali apabila dilakukan sebagai bagian dari proyek litbang):</p>
						<p>a.	Pendidikan dan Pelatihan</p>
						<p>b.	Kegiatan IPTEK lainnya seperti:</p>
            <div style="margin-left:20px">
							<p>•	Pelayanan informasi IPTEK</p>
							<p>•	Pengumpulan data untuk tujuan umum, seperti ekspedisi identifikasi spesies</p>
							<p>•	Pengujian dan Standarisasi</p>
							<p>•	Studi Fisibilitas</p>
							<p>•	Patent and licence work</p>
            </div>
						<p>c.	Administrasi dan aktivitas pendukung lainnya</p>
            <div style="margin-left:20px">
							<p>•	Purely R&D-financing activities</p>
							<p>o	Pencarian dana, pengelolaan dan distribusi dana litbang oleh kementerian RISTEK atau lembaga litbang, tidak termasuk ke dalam litbang.</p>
							<p>•	Aktivitas pendukung yang tidak langsung terkait dengan litbang</p>
							<p>o	Contoh: transportasi, pergudangan, pembersihan, perbaikan, perawatan dan pengamanan.</p>
							<p>o	Administrasi dan pekerjaan kantor lainnya yang dilakukan tidak hanya untuk litbang, seperti aktivitas bagian keuangan dan bagian personil.</p>
						</div>
   				</div>
   			</div>
   		</div>

   		<div class="col-md-4">
   			<div class="panel">
   				<div class="panel-header">
   					<h3> <i class="icon-note"> </i> <strong>Login Page</strong> </h3>
   				</div>

   				<div class="panel-content">
              <?php
              $message = $this->session->flashdata('pesan_login');
              if(isset($message))
              {
                echo $message;
              } 
              $logout = $this->session->flashdata('logout');
              if(isset($logout)){
                echo $logout;
              }
            ?>
   					 <form action="<?php echo site_url('login/do_login') ?>" method="post" class="form-signin" role="form">
                  <div class="append-icon has-feedback">
                      <input type="text" name="email" id="email" class="form-control form-white username" placeholder="Masukkan Email Anda" required>
                      <i class="icon-user"></i>
                  </div>
                  <div class="append-icon m-b-20 has-feedback">
                      <input type="password" name="password" id="password" class="form-control form-white password" placeholder="Password" required>
                      <i class="icon-lock"></i>
                  </div>
                  <!-- <a href="<?php echo site_url('dashboard'); ?>" type="submit" id="submit-form" class="btn btn-lg btn-primary btn-block ladda-button" data-style="expand-left">
                  	Sign In
                  </a> -->
                  <button type="submit" class="btn btn-primary btn-block">Login</button>
                  <!-- <button type="submit" class="btn btn-primary btn-block">Register</button> -->
                  <div class="clearfix">
                      <p class="pull-right m-t-20"><a href="<?php echo site_url('daftar/step1'); ?>">Belum punya account login? Silahkan daftar</a></p>
                      <p> <a href="<?php echo site_url('daftar/forgot_pass') ?>"> Lupa password? </a> </p>
                  </div>
              </form>
                       

   				</div>
   			</div>
   		</div>
   </div>
</div>


 

     