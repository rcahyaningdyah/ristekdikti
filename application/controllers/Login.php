<?php 
	class Login extends CI_Controller
	{
		function __construct(){
			parent::__construct();
			$this->load->library(array('cart','form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file','text','date'));
			$this->load->model(array('users_m'));
			$this->form_validation->set_message('required', ' %s harap di isi.');
			$this->form_validation->set_message('valid_email', ' %s yang disikan harus valid.');
		}	


		function index(){
			$role = $this->session->userdata('role');			

			if ($role == "pegawai"){
				redirect('pegawai/Dashboard');   
			}
			elseif($role == "admin"){
				 redirect('dashboard');      
			}
			elseif($role == "superuser"){
				redirect('administrator/dashboard');
			}
			else{
				$data['title']	 = "Login";
				$data['navbar']	 = " ";
				$data['content'] = $this->load->view('login', $data, TRUE);
				$this->load->view('frame-login',$data);
			}	
		}

		function index1(){
			$data['title']	 = "Login";
			$data['navbar']	 = " ";
			$data['content'] = $this->load->view('login', $data, TRUE);
			$this->load->view('frame-login',$data);
		}

		function do_login(){
			
	    	$this->form_validation->set_rules('email', 'Email', 'valid_email');
	        $this->form_validation ->set_rules('password', 'Password', 'required');

	    	if ($this->form_validation->run() == FALSE) 
	        {
	            $this->index();
	    	} 
	        else 
	        {
	        	$email = $this->input->post('email');
           		$password = sha1($this->input->post('password'));
           		
           		$user_login = $this->users_m->cek_login($email,$password); 
           		 if (! $user_login){
           		 		// login failed
               			$this->session->set_flashdata('pesan_login',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Password atau Username yang anda masukkan salah!</div>');
                		redirect('login');
           		 }
           		 else{
           		 	//cek role
           		 	//echo "<pre>"; var_dump($user_login[0]['joined_at']); exit;

           		 	foreach($user_login as $data){
           		 		$role = $data['role'];
           		 	}
           		 	if ($role == 'admin'){
           		 		 $this->session->set_userdata([
	                            'is_login' => TRUE,
	                            'user_id'  => $user_login[0]['_id'],
	                            'username' => $user_login[0]['username'],
	                            'fullname' => $user_login[0]['q_1'],
	                            'role'     => $user_login[0]['role'],
	                            'lembaga'  => $user_login[0]['q_9']

	                            ]);
           		 		  redirect('dashboard');           		 		 
           		 	}

           		 	elseif($role == 'pegawai'){
           		 		 $this->session->set_userdata([
	                            'is_login' 		=> TRUE,
	                            'user_id'  		=> $user_login[0]['_id'],
	                            'username' 		=> $user_login[0]['username'],
	                            'fullname' 		=> $user_login[0]['q_1'],
	                            'role'    		=> $user_login[0]['role'],
	                            'tanggal_isi' 	=> $user_login[0]['joined_at']->sec,
	                            'lembaga'		=> $user_login[0]['q_9']
	                            ]);
           		 		 redirect('pegawai/Dashboard');           		 		
           		 	}
           		 	elseif($role == 'superuser'){
           		 		$this->session->set_userdata([
           		 				'is_login'   => TRUE,
           		 				'user_id'  		=> $user_login[0]['_id'],
	                            'username' 		=> $user_login[0]['username'],
	                            'fullname' 		=> $user_login[0]['q_1'],
	                            'role'    		=> $user_login[0]['role'],
	                            'lembaga'		=> $user_login[0]['q_9'],
	                            'tanggal_isi'   => $user_login[0]['joined_at']->sec
           		 			]);
           		 		redirect('administrator/dashboard');
           		 	}
           		 	else {
           		 		redirect('login');
           		 	}

           		 }	
	        }

		}


		function logout(){
			$this->session->sess_destroy();
             
            $this->session->set_flashdata('logout','Anda telah keluar dari sistem, silakan login untuk masuk ke sistem.');
            redirect('/login');
		}
	}

?>