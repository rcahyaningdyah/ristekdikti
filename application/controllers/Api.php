<?php
	class Api extends MX_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library(array('form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
			$this->load->model(array('kuesioner_m','pertanyaan_m', 'bab_m', 'index_m','jawaban_m','users_m','lembaga_m','bidang_penelitian_m','sub_penelitian_m'));
	        $this->form_validation->set_message('required', ' %s harap di isi.');
			$this->form_validation->set_message('valid_email','Harap memasukkan %s valid');
			$this->form_validation->set_message('numeric','Harap memasukkan %s berupa angka');
			$this->form_validation->set_message('valid_url', 'Harap memasukkan %s valid');
		}

		function belanja(){
			//get id lembaga frame-template-admin	

			$lembaga_id = $this->uri->segment(3);
			if ($lembaga_id){
				// $get_lembaga = $this->users_m->get_data_by_lembaga($lembaga_id);		
				$belanja_ini = $this->jawaban_m->get_data_belanja_byid($lembaga_id);
				$belanja_eksmud = $this->jawaban_m->belanja_kegiatan($lembaga_id);
				$data_belanja=array();
				foreach ($belanja_ini as $key) {
					$data_belanja = array(
						'id_lembaga' => $key['id_lembaga'],
						'q_35' => $key['q_35'],
						'q_36' => $key['q_36'],
						'q_37' => $key['q_37'],
						'q_38' => $key['q_38'],
						);
					}
				// echo "<pre>"; var_dump($data_belanja);exit();
				$data = $data_belanja + $belanja_eksmud;
				$belanja_json=json_encode($data);
				echo $belanja_json;
				return $belanja_json;
			}else{
				// $get_lembaga = $this->users_m->get_data_by_lembaga($lembaga_id);		
				$belanja_ini = $this->jawaban_m->all_belanja_kegiatan();
				
				$belanja_eksmud = $this->jawaban_m->all_belanja_kegiatan();
				$all_data = $this->jawaban_m->get_all_data();
				$data_belanja = array();
				foreach ($all_data as $key) {
					$data_belanja[] = array(
						'id_lembaga' => $key['id_lembaga'],
						'q_35' => $key['q_35'],
						'q_36' => $key['q_36'],
						'q_37' => $key['q_37'],
						'q_38' => $key['q_38']
					);
				}
				// echo "<pre>";var_dump($data_belanja);exit;
				$belanja = array_unique($data_belanja,SORT_REGULAR);
				// belanja lembaga yang dimasukkan oleh admin
				$belanja_lembaga = array("belanja_lembaga" => $belanja);
				$belanja_json=json_encode($belanja_lembaga + $belanja_eksmud);
				echo $belanja_json;
				return $belanja_json;
			}
		}

		function count(){
			$lembaga_id = $this->uri->segment(3);
			// var_dump($lembaga_id);exit;
			if ($lembaga_id){
				$data['januari'] = array(
					'dasar' => $this->jawaban_m->count_dasar_jan($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_jan($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_jan($lembaga_id)
				);

				$data['februari'] = array(
					'dasar' => $this->jawaban_m->count_dasar_feb($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_feb($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_feb($lembaga_id)
				);

				$data['maret'] = array(
					'dasar' => $this->jawaban_m->count_dasar_mar($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_mar($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_mar($lembaga_id)
				);

				$data['april'] = array(
					'dasar' => $this->jawaban_m->count_dasar_apr($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_apr($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_apr($lembaga_id)
				);

				$data['mei'] = array(
					'dasar' => $this->jawaban_m->count_dasar_mei($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_mei($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_mei($lembaga_id)
				);

				$data['juni'] = array(
					'dasar' => $this->jawaban_m->count_dasar_jun($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_jun($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_jun($lembaga_id)
				);

				$data['juli'] = array(
					'dasar' => $this->jawaban_m->count_dasar_jul($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_jul($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_jul($lembaga_id)
				);

				$data['agustus'] = array(
					'dasar' => $this->jawaban_m->count_dasar_aug($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_aug($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_aug($lembaga_id)
				);

				$data['september'] = array(
					'dasar' => $this->jawaban_m->count_dasar_sep($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_sep($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_sep($lembaga_id)
				);

				$data['oktober'] = array(
					'dasar' => $this->jawaban_m->count_dasar_okt($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_okt($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_okt($lembaga_id)
				);

				$data['november'] = array(
					'dasar' => $this->jawaban_m->count_dasar_nov($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_nov($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_nov($lembaga_id)
				);

				$data['desember']= array(
					'dasar' => $this->jawaban_m->count_dasar_des($lembaga_id),
					'terapan' =>$this->jawaban_m->count_terapan_des($lembaga_id),
					'eksperimental' =>$this->jawaban_m->count_eksperimental_des($lembaga_id)
					);

				$jml_kegiatan= json_encode($data);
				echo $jml_kegiatan;
				return $jml_kegiatan;

			}else{
				//all count kegiatan

				$data['januari'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_jan(),
					'terapan' =>$this->jawaban_m->all_count_terapan_jan(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_jan()
				);
				
				$data['februari'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_feb(),
					'terapan' =>$this->jawaban_m->all_count_terapan_feb(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_feb()
				);

				$data['maret'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_mar(),
					'terapan' =>$this->jawaban_m->all_count_terapan_mar(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_mar()
				);

				$data['april'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_apr(),
					'terapan' =>$this->jawaban_m->all_count_terapan_apr(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_apr()
				);

				$data['mei'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_mei(),
					'terapan' =>$this->jawaban_m->all_count_terapan_mei(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_mei()
				);

				$data['juni'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_jun(),
					'terapan' =>$this->jawaban_m->all_count_terapan_jun(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_jun()
				);

				$data['juli'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_jul(),
					'terapan' =>$this->jawaban_m->all_count_terapan_jul(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_jul()
				);

				$data['agustus'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_aug(),
					'terapan' =>$this->jawaban_m->all_count_terapan_aug(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_aug()
				);

				$data['september'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_sep(),
					'terapan' =>$this->jawaban_m->all_count_terapan_sep(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_sep()
				);

				$data['oktober'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_okt(),
					'terapan' =>$this->jawaban_m->all_count_terapan_okt(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_okt()
				);

				$data['november'] = array(
					'dasar' => $this->jawaban_m->all_count_dasar_nov(),
					'terapan' =>$this->jawaban_m->all_count_terapan_nov(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_nov()
				);

				$data['desember']= array(
					'dasar' => $this->jawaban_m->all_count_dasar_des(),
					'terapan' =>$this->jawaban_m->all_count_terapan_des(),
					'eksperimental' =>$this->jawaban_m->all_count_eksperimental_des()
					);

				$jml_kegiatan= json_encode($data);
				echo $jml_kegiatan;
				return $jml_kegiatan;
			}
			var_dump($data['januari']);exit;

		}

		
		function jenis_kegiatan(){

			$lembaga_id = $this->uri->segment(3);
			if ($lembaga_id){
				$dasar=$this->jawaban_m->kegiatan_dasar($lembaga_id);
				// echo "<pre>";var_dump($dasar);exit;
				$terapan=$this->jawaban_m->kegiatan_terapan($lembaga_id);
				$eksperimental=$this->jawaban_m->kegiatan_eksperimental($lembaga_id);

				//get bidang kegiatan per jenis penelitian di kegiatan dasar
				$bidang_dasar=array();
				foreach ($dasar as $data) {
					$bidang_dasar[]=$data['q_32'];
				}

				//get bidang kegiatan per jenis penelitian di kegiatan terapan
				$bidang_terapan=array();
				foreach ($terapan as $data) {
					$bidang_terapan[]=$data['q_32'];
				}

				//get bidang kegiatan per jenis penelitian di kegiatan eksperimental
				$bidang_eksperimental=array();
				foreach ($eksperimental as $data) {
					$bidang_eksperimental[]=$data['q_32'];
				}
				//count kegiatan per bidang kegiatan
				$kegiatan_struk=array(
					'dasar' =>(array_count_values($bidang_dasar)),
					'terapan' =>(array_count_values($bidang_terapan)),
					'eksperimental' =>(array_count_values($bidang_eksperimental))
				);
				$bidang_jenis_kegiatan = json_encode($kegiatan_struk);
				echo $bidang_jenis_kegiatan;
				return $bidang_jenis_kegiatan;
			}else{
				$dasar=$this->jawaban_m->get_all_kegiatan_dasar();
				// echo "<pre>";var_dump($dasar);exit;
				$terapan=$this->jawaban_m->get_all_kegiatan_terapan();
				$eksperimental=$this->jawaban_m->get_all_kegiatan_eksperimental();

				//get bidang kegiatan per jenis penelitian di kegiatan dasar
				$bidang_dasar=array();
				foreach ($dasar as $data) {
					$bidang_dasar[]=$data['q_32'];
				}

				//get bidang kegiatan per jenis penelitian di kegiatan terapan
				$bidang_terapan=array();
				foreach ($terapan as $data) {
					$bidang_terapan[]=$data['q_32'];
				}

				//get bidang kegiatan per jenis penelitian di kegiatan eksperimental
				$bidang_eksperimental=array();
				foreach ($eksperimental as $data) {
					$bidang_eksperimental[]=$data['q_32'];
				}
				//count kegiatan per bidang kegiatan
				$kegiatan_struk=array(
					'dasar' =>(array_count_values($bidang_dasar)),
					'terapan' =>(array_count_values($bidang_terapan)),
					'eksperimental' =>(array_count_values($bidang_eksperimental))
				);
				$bidang_jenis_kegiatan = json_encode($kegiatan_struk);
				echo $bidang_jenis_kegiatan;
				return $bidang_jenis_kegiatan;
			}
		}


		function cetak_by_lembaga($lembaga_id){
			//get id lembaga frame-template-admin			
			$lembaga_id = $this->uri->segment(3);

			$get_lembaga = $this->users_m->get_data_by_lembaga($lembaga_id);
			//echo "<pre>"; var_dump($get_lembaga); exit;
			//get data lembaga
			foreach ($get_lembaga as $data){
				if ($data['role'] == "pegawai"){
					$pegawai_id = $data['_id'];
					$data_jawaban = $this->jawaban_m->get_data_by_userid($pegawai_id);
					
					//echo "<pre>"; var_dump($data_jawaban);
					echo json_encode($data_jawaban);
					 exit;
				} // end if role pegawai
			} // end foreach get_lembaga
		}

		function cetak_by_kegiatan($id_kegiatan){
			$id_kegiatan = $this->uri->segment(3);

			$string_id = (string) $id_kegiatan;

			$get_kegiatan = $this->jawaban_m->kegiatan_byid($string_id);
			//get kegiatan
			$arr = array();
			foreach ($get_kegiatan as $data){
				$arr[] = $data;
				echo json_encode($arr);
			}
		}

		function get_pertanyaan(){
			$pertanyaan = $this->pertanyaan_m->get_all_file();
			echo json_encode($pertanyaan);
		}


		function tabel1(){
				$lembaga_id = $this->session->userdata['lembaga'];

				//get nama lembaga by id 
				$nama = $this ->lembaga_m->get_lembaga_by_nama($lembaga_id);
				// var_dump($nama);exit;
				$nama_lembaga = $nama[0]['nama_lembaga'];
				//get lembaga yang sama di collection users
				$get_lembaga = $this->users_m->get_data_by_lembaga($lembaga_id);
				//get user id yang punya role pegawai
				foreach ($get_lembaga as $data){
					if ($data['role'] == "pegawai"){
						$user_id = $data['_id'];
						$string_id = (string)$user_id;
						
						//get jawaban user yang ada ide user pegawai
						$data_jawaban = $this->jawaban_m->get_data_by_userid($user_id);
						echo "<pre>";var_dump($data_jawaban);exit;
						foreach ($data_jawaban as $tabel) {
							$nama_kegiatan = $tabel["q_27"]; 		
							$belanja_intramural = $tabel["q_38"]+$tabel["q_42"] + $tabel["q_35"] + $tabel["q_36"]	+ $tabel["q_37"];	
							$belanja_ekstramural = $tabel["q_45"];	

							$data_tabel= array(
								'nama_kegiatan' => $nama_kegiatan,
								'belanja_intramural' => $belanja_intramural,
								'belanja_ekstramural' => $belanja_ekstramural
								);
							$tabel1 = json_encode($data_tabel);
							echo $tabel1;
						}
						
					}
				}
		}


		function get_identitas_pengisi($id_user){
			$id_user = $this->uri->segment(3);
			$string_id = (string)$id_user;
			$identitas = $this->users_m->get_user_byId($string_id);
			foreach ($identitas as $data){
				$arr = array(
					"nama"		=> $data['q_1'],
					"jabatan"   => $data['q_2'],
					"nip"		=>$data['q_3'],
					"telp"      => $data['q_4'],
					"hp"        => $data['q_5'],
					"fax"		=> $data['q_6'],
					"email"		=>$data['q_7']

				);	
			}
			echo json_encode(array('identitas' => $arr));
		}


		function get_institusi_pengisi($id_user){
			$id_user = $this->uri->segment(3);
			$string_id = (string)$id_user;
			$identitas = $this->users_m->get_user_byId($string_id);
			foreach($identitas as $data){
				$arr = array(
					"kementrian" => $data['q_8'],
					"lembaga"    => $data['q_9'],
					"balai"      => $data['q_10'],
					"kepala_balai" =>$data['q_66'],
					"alamat"	 => $data['q_11'],
					"telp"		 => $data['q_12'],
					"website"     => $data['q_13'],
					"fax"        => $data['q_14']
				);
			}
			echo json_encode(array('institusi' => $arr));
		}

		function cetak_kegiatan($id_kegiatan){
			$id_kegiatan = $this->uri->segment(3);
			$id_lembaga = $this->uri->segment(4);
			$id_user = $this->uri->segment(5);

			if ($id_lembaga && $id_user){
				$data['id_kegiatan'] = $id_kegiatan;
				$data['id_user'] = $id_user;
				$data['id_lembaga'] = $id_lembaga;

				//get lembaga yang sama di collection users
				$data['get_lembaga'] = $this->users_m->get_data_by_lembaga($id_lembaga);
				$data['title']	 = "Laporan";
				
				$data['content'] = $this->load->view('cetak_kegiatan', $data, TRUE);
			
				$this->load->view('frame-template-cetak',$data);
			}
			else {
				$data['id_kegiatan'] = $id_kegiatan;
				//get id lembaga frame-template-admin
				$data['id_lembaga'] = $this->session->userdata['lembaga'];
				$data['id_user'] = $this->session->userdata['user_id'];
				//get lembaga yang sama di collection users
				$data['get_lembaga'] = $this->users_m->get_data_by_lembaga($this->session->userdata['lembaga']);
				$data['title']	 = "Laporan";
			
				$data['content'] = $this->load->view('cetak_kegiatan', $data, TRUE);
			
				$this->load->view('frame-template-cetak',$data);
			}

		}


	}
?>