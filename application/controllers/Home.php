<?php
	class Home extends CI_Controller
	{
		function __construct(){
			parent::__construct();
			$this->load->library(array('cart','form_validation','auth_lib', 'pagination','session'));
			$this->load->helper(array('form', 'url', 'file'));
		}	


		function index(){
			$data['title']	 = "Login";
			$data['navbar']	 = " ";
			$data['content'] = $this->load->view('login', $data, TRUE);
			$this->load->view('frame-login',$data);
		}


		

		function error(){
			$data['title']	= "404 Page Not Found";
			$this->load->view('frame-error',$data);
		}

		function tes(){
			$this->load->view('login_form');
		}

	}
?>