
/** bab indexes **/
db.getCollection("bab").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** bab indexes **/
db.getCollection("bab").ensureIndex({
  "title": NumberInt(-1)
},[
  
]);

/** counters indexes **/
db.getCollection("counters").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** daftar indexes **/
db.getCollection("daftar").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** index indexes **/
db.getCollection("index").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** jawaban indexes **/
db.getCollection("jawaban").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** jawaban_temp indexes **/
db.getCollection("jawaban_temp").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** kuesioner indexes **/
db.getCollection("kuesioner").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** kuesioner indexes **/
db.getCollection("kuesioner").ensureIndex({
  "title": NumberInt(-1)
},[
  
]);

/** lembaga indexes **/
db.getCollection("lembaga").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** pertanyaan indexes **/
db.getCollection("pertanyaan").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** pertanyaan indexes **/
db.getCollection("pertanyaan").ensureIndex({
  "title": NumberInt(-1)
},[
  
]);

/** pertanyaan indexes **/
db.getCollection("pertanyaan").ensureIndex({
  "id_bab": NumberInt(-1)
},[
  
]);

/** pertanyaan indexes **/
db.getCollection("pertanyaan").ensureIndex({
  "id_bab": NumberInt(1)
},[
  
]);

/** system.js indexes **/
db.getCollection("system.js").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** users indexes **/
db.getCollection("users").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** bab records **/
db.getCollection("bab").insert({
  "_id": "bab_3",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Detail identitas peneliti (fungsional peneliti, perekayasa, pranata nuklir, dll) yang berperan sebagai peneliti dalam kegiatan litbang",
  "topik": "Data personel litbang yang terlibat penelitian"
});
db.getCollection("bab").insert({
  "_id": "bab_1",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "identitas pengisi kuisioner",
  "topik": "asal pengisi"
});
db.getCollection("bab").insert({
  "_id": "bab_5",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Detail keseluruhan belanja dalam satu kegiatan litbang",
  "topik": "Data belanja litbang"
});
db.getCollection("bab").insert({
  "_id": "bab_4",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Detail kegiatan penelitian dan pengembangan (litbang) di tiap satuan kerja",
  "topik": "Data kegiatan litbang"
});
db.getCollection("bab").insert({
  "_id": "bab_2",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "detail institusi",
  "topik": "asal institusi"
});
db.getCollection("bab").insert({
  "_id": "bab_11",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Tanggapan dan saran untuk memperbaiki kuesioner yang akan datang",
  "topik": "Tanggapan dan saran kuesioner"
});
db.getCollection("bab").insert({
  "_id": "bab_10",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Perolehan Hak Kekayaan Intelektual (HKI) dalam litbang",
  "topik": "Data HKI litbang"
});
db.getCollection("bab").insert({
  "_id": "bab_9",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Buku teks yang digunakan selama litbang berlangsung",
  "topik": "Data buku teks litbang"
});
db.getCollection("bab").insert({
  "_id": "bab_8",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Luaran litbang yang berupa publikasi jurnal baik nasional maupun internasional",
  "topik": "Data luaran litbang"
});
db.getCollection("bab").insert({
  "_id": "bab_6",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Detail keseluruhan belanja kerjasama dalam satu kegiatan litbang",
  "topik": "Data belanja kerjasama"
});
db.getCollection("bab").insert({
  "_id": "bab_7",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Detail keseluruhan belanja ekstramural dalam satu kegiatan litbang",
  "topik": "Data belanja ekstramural"
});
db.getCollection("bab").insert({
  "_id": "bab_12",
  "id_kuesioner": "kuesioner_1",
  "deskripsi": "Konfirmasi kegiatan Litbang di setiap lembaga",
  "topik": "Konfirmasi Kegiatan Litbang"
});

/** counters records **/
db.getCollection("counters").insert({
  "_id": "userid",
  "seq": 3
});

/** daftar records **/
db.getCollection("daftar").insert({
  "_id": ObjectId("5653ed8c87201a709b00c014"),
  "email": "bond@n.bhaskara",
  "submitted_at": "24-11-2015"
});

/** index records **/
db.getCollection("index").insert({
  "_id": "index_kuisioner-1",
  "urutan": [
    "bab-1",
    "q-1",
    "q-2",
    "q-3",
    "q-4",
    "q-5",
    "q-6",
    "q-7",
    "q-8",
    "bab-2",
    "q-9",
    "q-10",
    "q-11",
    "q-12",
    "q-13",
    "q-14",
    "bab-3",
    "q-15",
    "q-16",
    "q-17",
    "q-18",
    "q-19",
    "q-20",
    "q-21",
    "q-22",
    "q-23",
    "q-24",
    "q-25",
    "bab-4",
    "q-26",
    "q-27",
    "q-28",
    "q-29",
    "q-30",
    "bab-5",
    "q-31",
    "q-32",
    "q-33",
    "q-34",
    "q-35",
    "q-36",
    "bab-6",
    "q-37",
    "q-38",
    "q-39",
    "q-40",
    "q-41",
    "bab-7",
    "q-42",
    "q-43",
    "q-44",
    "q-45",
    "bab-8",
    "q-46",
    "q-47",
    "q-48",
    "q-49",
    "q-50",
    "bab-9",
    "q-51"
  ]
});
db.getCollection("index").insert({
  "_id": 1,
  "name": "Sarah C."
});
db.getCollection("index").insert({
  "_id": 2,
  "name": "Sarah Cuk."
});
db.getCollection("index").insert({
  "_id": "coba3",
  "name": "Sarah C."
});

/** jawaban records **/
db.getCollection("jawaban").insert({
  "_id": ObjectId("565eb10896b353ecd0c80ac2"),
  "id_user": ObjectId("564e8f1d04154000bbe510e1"),
  "q_1": "Joko Merpati",
  "q_2": "Direktur Utama",
  "q_3": "123457890",
  "q_4": "021-8619773",
  "q_5": "08119703287",
  "q_6": "-",
  "q_7": "bondanbhaskara@gmail.com",
  "q_8": "Dasar",
  "q_9": "Kementerian Perdagangan",
  "q_10": "Badan Perdagangan",
  "q_11": "Pusat Perdagangan Badan Perdagangan",
  "q_12": "Jalan ini Blok itu RT berapa RW berapa",
  "q_13": "021-888888",
  "q_14": "http://www.websitenya.com",
  "q_15": "Joko Rajawali",
  "q_16": "Laki-laki",
  "q_17": "1234567890",
  "q_18": "S3",
  "q_19": "Peneliti",
  "q_20": "WNI",
  "q_21": "Indonesia",
  "q_22": "Industri",
  "q_23": "Industri Perdagangan",
  "q_24": "Technological Sciences",
  "q_25": "20",
  "q_26": "Kegiatan Memancing",
  "q_27": "Dasar",
  "q_28": "Inductive Logic",
  "q_29": "12/11/2015",
  "q_30": "12/31/2015",
  "q_31": "100000",
  "q_32": "10000",
  "q_33": "20000",
  "q_34": "30000",
  "q_35": "40000",
  "q_36": "LSM",
  "q_37": "LSM Asal aja",
  "q_38": "10000000",
  "q_39": "Pemerintah",
  "q_40": "Pemerintah Ulala",
  "q_41": "1500000",
  "q_42": "Jurnal Apa Aja",
  "q_43": "ISSN-123123123",
  "q_44": "2015",
  "q_45": "Nasional",
  "q_46": "A",
  "q_47": "Buku Asal",
  "q_48": "Penulis 1",
  "q_49": "2015",
  "q_50": "Penerbit Erlangga",
  "q_51": "Perekayasa",
  "q_52": "Judul HKI Entah",
  "q_53": "Hak Cipta",
  "q_54": "2015",
  "q_55": "Terdaftar",
  "q_56": "Saran saya adalah ini itu ini itu",
  "q_57": "fax-123-123",
  "q_58": "Ya"
});
db.getCollection("jawaban").insert({
  "_id": ObjectId("565eb68596b353ecd0c80ac3"),
  "id_user": ObjectId("564e8f1d04154000bbe510e1"),
  "q_1": "Budi Merpati",
  "q_2": "Direktur Keuangan",
  "q_3": "123457890",
  "q_4": "021-8619773",
  "q_5": "08119703287",
  "q_6": "-",
  "q_7": "bondanbhaskara@gmail.com",
  "q_8": "Dasar",
  "q_9": "Kementerian Perdagangan",
  "q_10": "Badan Perdagangan",
  "q_11": "Pusat Perdagangan Badan Perdagangan",
  "q_12": "Jalan ini Blok itu RT berapa RW berapa",
  "q_13": "021-888888",
  "q_14": "http://www.websitenya.com",
  "q_15": "Budi Rajawali",
  "q_16": "Laki-laki",
  "q_17": "1234567890",
  "q_18": "S1",
  "q_19": "Peneliti",
  "q_20": "WNA",
  "q_21": "Indonesia",
  "q_22": "Perguruan Tinggi",
  "q_23": "Perguruan Tinggi Bro",
  "q_24": "Philosophy",
  "q_25": "20",
  "q_26": "Kegiatan Memancing",
  "q_27": "Dasar",
  "q_28": "Genetics",
  "q_29": "12/11/2015",
  "q_30": "12/31/2015",
  "q_31": "400000",
  "q_32": "350000",
  "q_33": "4000000",
  "q_34": "7000000",
  "q_35": "Dalam Negeri",
  "q_36": "Perguruan Tinggi",
  "q_37": "Perguruan TInggi Asal aja",
  "q_38": "12000000",
  "q_39": "LSM",
  "q_40": "LSM Ulala",
  "q_41": "1500000",
  "q_42": "Jurnal Apa Aja",
  "q_43": "ISSN-123123123",
  "q_44": "2015",
  "q_45": "Nasional",
  "q_46": "A",
  "q_47": "Buku Asal",
  "q_48": "Penulis 1",
  "q_49": "2015",
  "q_50": "Penerbit Erlangga",
  "q_51": "Perekayasa",
  "q_52": "Judul HKI Entah",
  "q_53": "Hak Cipta",
  "q_54": "2015",
  "q_55": "Terdaftar",
  "q_56": "Saran saya adalah ini itu ini itu",
  "q_57": "fax-123-123",
  "q_58": "Ya"
});
db.getCollection("jawaban").insert({
  "_id": ObjectId("56653b4aef2e5eb004000029"),
  "id_user": ObjectId("564e8f1d04154000bbe510e1"),
  "q_27": "Kegiatan Tani",
  "q_28": "Dasar",
  "q_29": "Deductive logic",
  "q_30": "2015-12-09",
  "q_31": "2015-12-31",
  "q_16": [
    "Tanti Rahma",
    "Himawan"
  ],
  "q_17": [
    "Perempuan",
    "Laki-laki"
  ],
  "q_18": [
    "289713891237",
    "287163732"
  ],
  "q_19": [
    "D3",
    "S1"
  ],
  "q_20": [
    "Perekayasa",
    "Peneliti"
  ],
  "q_21": [
    "WNI",
    "WNI"
  ],
  "q_22": [
    "Andrean Region",
    "Indonesia"
  ],
  "q_23": [
    "LSM",
    "Pemerintah"
  ],
  "q_24": [
    "UPC",
    "ysudyu"
  ],
  "q_25": [
    "Chemistry",
    "Political Science"
  ],
  "q_26": [
    "72",
    "38"
  ],
  "q_36": [
    "Dalam Negeri",
    "Dalam Negeri"
  ],
  "q_37": [
    "Pemerintah",
    "Perguruan Tinggi"
  ],
  "q_38": [
    "Menpora",
    "UGM"
  ],
  "q_39": [
    "30000000",
    "20000000"
  ],
  "q_40": [
    "Perguruan Tinggi",
    "Pemerintah"
  ],
  "q_41": [
    "ugm",
    "Kehutanan"
  ],
  "q_42": [
    "10000000",
    "20000000"
  ],
  "q_43": [
    "Jurnal ini",
    "Jurnal itu"
  ],
  "q_44": [
    "2873821",
    "26392149"
  ],
  "q_45": [
    "2015",
    "2013"
  ],
  "q_46": [
    "Nasional",
    "Nasional"
  ],
  "q_47": [
    "Ada",
    "Ada"
  ],
  "q_48": [
    "Buku itu",
    "Buku ini"
  ],
  "q_49": [
    "Tatat",
    "cbdskcdc"
  ],
  "q_50": [
    "2014",
    "2014"
  ],
  "q_51": [
    "wefc n v",
    "wjkcbkjdbc"
  ],
  "q_52": [
    "Perekayasa",
    "Pranata Nuklir"
  ],
  "q_53": [
    "asfsaf",
    "sdvjbksvb"
  ],
  "q_54": [
    "Paten Sederhana",
    "Paten"
  ],
  "q_55": [
    "2012",
    "2012"
  ],
  "q_56": [
    "Terdaftar",
    "Terdaftar"
  ],
  "q_32": NumberInt(0),
  "q_33": NumberInt(0),
  "q_34": NumberInt(0),
  "q_35": NumberInt(0)
});
db.getCollection("jawaban").insert({
  "_id": ObjectId("5666bd9bc4365bc01900002b"),
  "id_user": ObjectId("564e8f1d04154000bbe510e1"),
  "q_27": "Penelitian Nusantara Gajdah Mada",
  "q_28": "Dasar",
  "q_29": "Methodology",
  "q_30": ISODate("2015-12-08T17:00:00.0Z"),
  "q_31": ISODate("2016-01-12T17:00:00.0Z"),
  "q_16": [
    "Haruki Murakami"
  ],
  "q_17": [
    "Perempuan"
  ],
  "q_18": [
    "4477575775"
  ],
  "q_19": [
    "S3"
  ],
  "q_20": [
    "Peneliti"
  ],
  "q_21": [
    "WNA"
  ],
  "q_22": [
    "American Samoa"
  ],
  "q_23": [
    "Perguruan Tinggi"
  ],
  "q_24": [
    "Tokyo University"
  ],
  "q_25": [
    "Life Science"
  ],
  "q_26": [
    "20"
  ],
  "q_36": [
    "Luar Negeri",
    "Luar Negeri"
  ],
  "q_37": [
    "Perguruan Tinggi",
    "Perguruan Tinggi"
  ],
  "q_38": [
    "KEMENRISTEK",
    "BAPPENAS"
  ],
  "q_39": [
    "500000000",
    "60000000000"
  ],
  "q_40": [
    "Perguruan Tinggi"
  ],
  "q_41": [
    "KEMENRISTEK"
  ],
  "q_42": [
    "5679606060"
  ],
  "q_43": [
    "Penelitian"
  ],
  "q_44": [
    "575757575775"
  ],
  "q_45": [
    "2012"
  ],
  "q_46": [
    "Internasional"
  ],
  "q_47": [
    "Ada"
  ]
});
db.getCollection("jawaban").insert({
  "_id": ObjectId("5666bf6ac4365bc01900002c"),
  "id_user": ObjectId("564e8f1d04154000bbe510e1"),
  "q_27": "PENELITIAN NUSANTARA GADJAH MADA",
  "q_28": "Dasar",
  "q_29": "Methodology",
  "q_30": ISODate("2015-12-18T17:00:00.0Z"),
  "q_31": ISODate("2016-01-27T17:00:00.0Z"),
  "q_16": [
    "HARUKI MURAKAMI"
  ],
  "q_17": [
    "Perempuan"
  ],
  "q_18": [
    "59599595"
  ],
  "q_19": [
    "S3"
  ],
  "q_20": [
    "Peneliti"
  ],
  "q_21": [
    "WNA"
  ],
  "q_22": [
    "Bermuda"
  ],
  "q_23": [
    "Perguruan Tinggi"
  ],
  "q_24": [
    "BERMUDA UNIVERSITY"
  ],
  "q_25": [
    "Life Science"
  ],
  "q_26": [
    "50"
  ],
  "q_36": [
    "Luar Negeri"
  ],
  "q_37": [
    "Perguruan Tinggi"
  ],
  "q_38": [
    "BERMUDA COLLEGE"
  ],
  "q_39": [
    "400000000"
  ],
  "q_40": [
    "Perguruan Tinggi",
    "Perguruan Tinggi"
  ],
  "q_41": [
    "BERMUDA COLLEGE",
    "TOKYO UNIVERSITY"
  ],
  "q_42": [
    "60000000",
    "6000900"
  ],
  "q_43": [
    "NUSANTARA GADJAH MADA"
  ],
  "q_44": [
    "44444444444444"
  ],
  "q_45": [
    "1545"
  ],
  "q_46": [
    "Internasional"
  ],
  "q_47": [
    "Ada"
  ],
  "q_48": [
    "NUSANTARA GADJAH MADA"
  ],
  "q_49": [
    "TERE LIYE"
  ],
  "q_50": [
    "1323"
  ],
  "q_51": [
    "TIGA SERANGKAI"
  ],
  "q_52": [
    "Peneliti"
  ],
  "q_53": [
    "NUSANTARA"
  ],
  "q_54": [
    "Hak Cipta"
  ],
  "q_55": [
    "4533"
  ],
  "q_56": [
    "Terdaftar"
  ]
});

/** jawaban_temp records **/
db.getCollection("jawaban_temp").insert({
  "_id": ObjectId("5663f5f3c4365bc81100002a"),
  "q_1": "Rachmasari Cahyaningdyah",
  "q_2": "Jabatan1",
  "q_3": "098493275",
  "q_4": "9843625",
  "q_5": "98478256",
  "q_6": "9283562",
  "q_7": "rachmaamma@gmail.com",
  "q_8": "2015-12-03",
  "q_10": "Lembaga1",
  "q_11": "Balai1",
  "q_12": "Jogja",
  "q_13": "194787365",
  "q_14": "website.com",
  "q_15": "9471285",
  "q_16": "346325",
  "q_9": "lalala",
  "q_59": "Ya"
});
db.getCollection("jawaban_temp").insert({
  "_id": ObjectId("566542c6c4365bd42700002b"),
  "q_1": "Rachmasari Cahyaningdyah",
  "q_2": "Direktur",
  "q_3": "01846",
  "q_4": "9182476214",
  "q_5": "74126412",
  "q_6": "917461",
  "q_7": "rachmaamma@gmail.com",
  "q_8": "2015-12-02",
  "q_10": "Lembaga Ilmu Pengetahuan Indonesia",
  "q_11": "Balai1",
  "q_12": "Jakarta",
  "q_13": "109476125",
  "q_14": "website.com",
  "q_15": "12746",
  "q_9": "Ristekdikti",
  "q_58": "Ya"
});
db.getCollection("jawaban_temp").insert({
  "_id": ObjectId("56654a6ec4365bf404000029"),
  "q_1": "Rachma Syailendra",
  "q_2": "Pegawai",
  "q_3": "4949949494949",
  "q_4": "089595959595",
  "q_5": "0985868686",
  "q_6": "027457575757",
  "q_7": "syailendra@gmail.com",
  "q_8": "7 Desember 2015",
  "q_10": "Bappenas",
  "q_11": "Bappenas",
  "q_12": "Jakarta",
  "q_13": "02156656565",
  "q_14": "bappenas.com",
  "q_15": "",
  "q_9": "Kementrian PAN",
  "q_58": "Ya"
});
db.getCollection("jawaban_temp").insert({
  "_id": ObjectId("5666629ac4365bb819000029"),
  "q_1": "eeeeeeeeee",
  "q_2": "dddddddddddddd",
  "q_3": "22222222222222222",
  "q_4": "4444444444444",
  "q_5": "44444444444444",
  "q_6": "555555555555",
  "q_7": "tadam@gmail.com",
  "q_8": "23122015",
  "q_10": "dffffffffffff",
  "q_11": "fffffffffffff",
  "q_12": "fffffffffffffffff",
  "q_13": "33333333333333",
  "q_14": "fffffff",
  "q_15": "44444444444444",
  "q_9": "rrrrrrrrrrrrr"
});
db.getCollection("jawaban_temp").insert({
  "_id": ObjectId("56668ea5c4365bdc1900002d"),
  "q_1": "Sherina",
  "q_2": "Wakil Direktur",
  "q_3": "201987466",
  "q_4": "9127648",
  "q_5": "98127645",
  "q_6": "91246",
  "q_7": "sherina@gmail.com",
  "q_8": "2015-12-04",
  "q_9": "Kementrian Ristek Dikti",
  "q_10": "Lembaga1",
  "q_11": "Balai1",
  "q_12": "Jakarta",
  "q_13": "1902746",
  "q_14": "website.com",
  "q_15": "018947654",
  "q_58": "Ya"
});
db.getCollection("jawaban_temp").insert({
  "_id": ObjectId("56665bf8c4365bdc1900002b"),
  "q_1": "Rachma Syailendra",
  "q_2": "Wakil Direktur",
  "q_3": "192746",
  "q_4": "182647254",
  "q_5": "981274621",
  "q_6": "821746314",
  "q_7": "rachma@gmail.com",
  "q_8": "2015-12-01",
  "q_10": "Lembaga1",
  "q_11": "Balai1",
  "q_12": "Jakarta",
  "q_13": "891246785314",
  "q_14": "website.com",
  "q_15": "194785",
  "q_9": "Kementrian1",
  "q_58": "Ya"
});

/** kuesioner records **/
db.getCollection("kuesioner").insert({
  "_id": "kuesioner_1",
  "created_by": "admin",
  "created_at": "",
  "masa_berlaku_awal": "01/01/2016",
  "masa_berlaku_akhir": "31/12/2016",
  "untuk": "semua kementerian"
});
db.getCollection("kuesioner").insert({
  "_id": "kuesioner_2",
  "created_by": "Admin",
  "created_at": "20-11-2015",
  "masa_berlaku_awal": "20-11-2015",
  "masa_berlaku_akhir": "20-11-2016",
  "untuk": "Menkominfo"
});

/** lembaga records **/
db.getCollection("lembaga").insert({
  "_id": "BAPETEN",
  "nama_lembaga": "Badan Pengawas Tenaga Nuklir",
  "alamat_lembaga": "Jl. Gajah Mada no. 8, Jakarta Pusat 10120 DKI Jakarta"
});
db.getCollection("lembaga").insert({
  "_id": "BATAN",
  "nama_lembaga": "Badan Tenaga Nuklir Nasional",
  "alamat_lembaga": "Jl. Kuningan Barat, Mampang Prapatan Jakarta, 12710"
});
db.getCollection("lembaga").insert({
  "_id": "BIG",
  "nama_lembaga": "Badan Informasi Geospasial",
  "alamat_lembaga": "Jl. Raya Jakarta - Bogor KM. 46 Cibinong 16911, INDONESIA"
});
db.getCollection("lembaga").insert({
  "_id": "BPPT",
  "nama_lembaga": "Badan Pengkajian dan Penerapan teknologi",
  "alamat_lembaga": "Jalan MH.Thamrin 8, Jakarta 10340"
});
db.getCollection("lembaga").insert({
  "_id": "BSN",
  "nama_lembaga": "Badan Standarisasi Nasional",
  "alamat_lembaga": "Jl. M.H.Thamrin No.8 Kebon Sirih, Jakarta Pusat 10340"
});
db.getCollection("lembaga").insert({
  "_id": "LAPAN",
  "nama_lembaga": "Lembaga Penerbangan dan Antariksa Nasional",
  "alamat_lembaga": "Jl. Pemuda Persil No.1 Jakarta 13220"
});
db.getCollection("lembaga").insert({
  "_id": "LIPI",
  "nama_lembaga": "Lembaga Ilmu Pegetahuan Indonesia",
  "alamat_lembaga": "Jl. Jend. Gatot Subroto Kav. 10, Mampang Prapatan, Jakarta Selatan, DKI Jakarta"
});

/** pertanyaan records **/
db.getCollection("pertanyaan").insert({
  "_id": "q_15",
  "id_bab": "bab_2",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Fax",
  "wajib": "N"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_18",
  "id_bab": "bab_3",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "NIP",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_27",
  "id_bab": "bab_4",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Kegiatan",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_38",
  "id_bab": "bab_6",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Instansi",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_39",
  "id_bab": "bab_6",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Jumlah Dana",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_41",
  "id_bab": "bab_7",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Instansi",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_42",
  "id_bab": "bab_7",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Jumlah Dana",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_43",
  "id_bab": "bab_8",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Jurnal",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_44",
  "id_bab": "bab_8",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "ISSN",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_45",
  "id_bab": "bab_8",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Tahun Terbit",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_48",
  "id_bab": "bab_9",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Judul Buku",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_49",
  "id_bab": "bab_9",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Penulis",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_50",
  "id_bab": "bab_9",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Tahun Terbit",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_51",
  "id_bab": "bab_9",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Penerbit",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_53",
  "id_bab": "bab_10",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Judul HKI",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_58",
  "id_bab": "bab_12",
  "bentuk_form": {
    "select": [
      "Ya",
      "Tidak"
    ]
  },
  "kalimat_tanya": "Ada Kegiatan Litbang",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_1",
  "id_bab": "bab_1",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_2",
  "id_bab": "bab_1",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Jabatan",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_3",
  "id_bab": "bab_1",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "NIP",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_4",
  "id_bab": "bab_1",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Telepon",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_5",
  "id_bab": "bab_1",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "HP",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_6",
  "id_bab": "bab_1",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Fax",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_7",
  "id_bab": "bab_1",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Email",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_8",
  "id_bab": "bab_1",
  "bentuk_form": {
    "date": [
      
    ]
  },
  "kalimat_tanya": "Tanggal Pengisian",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_9",
  "id_bab": "bab_2",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Kementrian",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_10",
  "id_bab": "bab_2",
  "bentuk_form": {
    "select": [
      
    ]
  },
  "kalimat_tanya": "Nama Lembaga/Badan",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_11",
  "id_bab": "bab_2",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Pusat/Balai/UPT",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_12",
  "id_bab": "bab_2",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Alamat",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_13",
  "id_bab": "bab_2",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Telepon",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_14",
  "id_bab": "bab_2",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Alamat Website",
  "wajib": "N"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_16",
  "id_bab": "bab_3",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Personil Litbang",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_17",
  "id_bab": "bab_3",
  "bentuk_form": {
    "select": [
      "Laki-laki",
      "Perempuan"
    ]
  },
  "kalimat_tanya": "Jenis Kelamin",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_19",
  "id_bab": "bab_3",
  "bentuk_form": {
    "select": [
      "D3",
      "S1",
      "S2",
      "S3"
    ]
  },
  "kalimat_tanya": "Tingkat Pendidikan",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_20",
  "id_bab": "bab_3",
  "bentuk_form": {
    "select": [
      "Peneliti",
      "Perekayasa",
      "Pranata Nuklir",
      "Teknisi",
      "Staf Pendukung"
    ]
  },
  "kalimat_tanya": "Jabatan Penelitian",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_21",
  "id_bab": "bab_3",
  "bentuk_form": {
    "select": [
      "WNI",
      "WNA"
    ]
  },
  "kalimat_tanya": "Warga Negara",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_23",
  "id_bab": "bab_3",
  "bentuk_form": {
    "select": [
      "Perguruan Tinggi",
      "Pemerintah",
      "Industri",
      "LSM"
    ]
  },
  "kalimat_tanya": "Jenis Institusi Asal",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_24",
  "id_bab": "bab_3",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Nama Institusi Asal",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_26",
  "id_bab": "bab_3",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Lama waktu penelitian setiap minggu (per jam)",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_28",
  "id_bab": "bab_4",
  "bentuk_form": {
    "select": [
      "Dasar",
      "Terapan",
      "Eksperimental"
    ]
  },
  "kalimat_tanya": "Jenis Penelitian",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_30",
  "id_bab": "bab_4",
  "bentuk_form": {
    "date": [
      
    ]
  },
  "kalimat_tanya": "Awal Penelitian",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_54",
  "id_bab": "bab_10",
  "bentuk_form": {
    "select": [
      "Paten",
      "Paten Sederhana",
      "Hak Cipta",
      "Merk Dagang",
      "Rahasia Dagang",
      "Desain Produksi Industri",
      "Indikasi Geografis",
      "Perlindungan Varietas Tanaman",
      "Perlindungan Topografi SIrkuit Terpadu"
    ]
  },
  "kalimat_tanya": "Jenis HKI",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_31",
  "id_bab": "bab_4",
  "bentuk_form": {
    "date": [
      
    ]
  },
  "kalimat_tanya": "Akhir Penelitian",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_32",
  "id_bab": "bab_5",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Gaji/Upah + Tunjangan Peneliti",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_33",
  "id_bab": "bab_5",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Gaji/Upah + Tunjangan Teknisi dan Staf Pendukung",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_34",
  "id_bab": "bab_5",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Belanja Modal (tanah, gedung dan bangunan, kendaraan, mesin, dan peralatan)",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_35",
  "id_bab": "bab_5",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Belanja sumber DIPA",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_36",
  "id_bab": "bab_6",
  "bentuk_form": {
    "select": [
      "Dalam Negeri",
      "Luar Negeri"
    ]
  },
  "kalimat_tanya": "Sumber Instansi",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_37",
  "id_bab": "bab_6",
  "bentuk_form": {
    "select": [
      "Pemerintah",
      "Perguruan Tinggi",
      "Swasta",
      "LSM"
    ]
  },
  "kalimat_tanya": "Jenis Instansi",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_40",
  "id_bab": "bab_7",
  "bentuk_form": {
    "select": [
      "Pemerintah",
      "Perguruan Tinggi",
      "Swasta",
      "LSM"
    ]
  },
  "kalimat_tanya": "Jenis Instansi",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_46",
  "id_bab": "bab_8",
  "bentuk_form": {
    "select": [
      "Nasional",
      "Internasional"
    ]
  },
  "kalimat_tanya": "Jenis Luaran",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_47",
  "id_bab": "bab_8",
  "bentuk_form": {
    "select": [
      "Ada",
      "Tidak"
    ]
  },
  "kalimat_tanya": "Akreditasi",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_52",
  "id_bab": "bab_10",
  "bentuk_form": {
    "select": [
      "Perekayasa",
      "Peneliti",
      "Pranata Nuklir"
    ]
  },
  "kalimat_tanya": "Nama Pemilik",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_55",
  "id_bab": "bab_10",
  "bentuk_form": {
    "text": [
      
    ]
  },
  "kalimat_tanya": "Tahun Pendaftaran/Perolehan",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_56",
  "id_bab": "bab_10",
  "bentuk_form": {
    "select": [
      "Terdaftar",
      "Granted"
    ]
  },
  "kalimat_tanya": "Status HKI",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_57",
  "id_bab": "bab_11",
  "bentuk_form": {
    "textarea": [
      
    ]
  },
  "kalimat_tanya": "Tanggapan dan Saran",
  "wajib": "N"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_22",
  "id_bab": "bab_3",
  "bentuk_form": {
    "select": [
      "Afghanistan",
      "Albania",
      "algeria",
      "American Samoa",
      "Andrean Region",
      "Andorra",
      "Angola",
      "Antigua and Barbuda",
      "Argentina",
      "Armenia",
      "Aruba",
      "Australia",
      "Austria",
      "Azerbaijan",
      "Bahamas, The",
      "Bahrain",
      "Bangladesh",
      "Barbados",
      "Belarus",
      "Belgium",
      "Belize",
      "Benin",
      "Bermuda",
      "Bhutan",
      "Bolivia",
      "Bosnia and Herzegovina",
      "Botswana",
      "Brazil",
      "Brunei Darussalam",
      "Bulgaria",
      "Burkina Faso",
      "Burundi",
      "Cabo Verde",
      "Cambodia",
      "Cameroon",
      "Canada",
      "Cape Verde",
      "Cayman Islands",
      "Central African Republic",
      "Chad",
      "Chile",
      "Chna",
      "Christmas Island",
      "Cocos (keeling) Islands",
      "Colombia",
      "Comoros",
      "Congo, Dem.Rep. (Kinshasa)",
      "Congo, Republic of (Brazzaville)",
      "Cook Islands",
      "Costa Rica",
      "Cote d'Ivoire",
      "Croatia",
      "Cuba",
      "Curacao",
      "Cyprus",
      "Czech Republic",
      "Denmark",
      "Djibouti",
      "Dominica",
      "Dominican Republic",
      "East Timor (Timor-Leste)",
      "Ecuador",
      "Egypt, Arab Rep.",
      "El Salvador",
      "Equatorial Guinea",
      "Eritrea",
      "Estonia",
      "Ethiopia",
      "Falkland Islands",
      "Faeore Islands",
      "Fiji",
      "Finland",
      "France",
      "French Guiana",
      "French Polynesia",
      "French Southern Territories",
      "Gabon",
      "Gambia, The",
      "Georgia",
      "Germany",
      "Ghana",
      "Gibraltar",
      "Grat Britain",
      "Greece",
      "Greenland",
      "Grenada",
      "Guadeloupe",
      "Guam",
      "Guatemala",
      "Guinea",
      "Guinea-Bissau",
      "Guyana",
      "Haiti",
      "Holy See",
      "Honduras",
      "Hong Kong SAR, CHina",
      "Hungary",
      "Iceland",
      "India",
      "Indonesia",
      "Iran (Islamic Republic of)",
      "Iraq",
      "Ireland",
      "Isle of Man",
      "Israel",
      "Italy",
      "Jamaica",
      "Jordan",
      "Kazakhstan",
      "Kenya",
      "Kiribati",
      "Korea, Democratic People's Rep. (North Korea)",
      "Korea, Republic of (South Korea)",
      "Kosovo",
      "Kuwait",
      "Kyrgyz Republic",
      "Lao, People's Democratic Republic",
      "Latvia",
      "Lebanon",
      "Lesotho",
      "Liberia",
      "Libya",
      "Liechtenstein",
      "Lithuania",
      "Luxemborg",
      "Macao SAR, China",
      "Macedonia, Rep. of",
      "Madagascar",
      "Malawi",
      "Malaysia",
      "Maldives",
      "Mali",
      "Malta",
      "marshall Islands",
      "Martinique",
      "Mauritania",
      "Maurituis",
      "Mayotte",
      "Mexico",
      "Micronesia, Federal States of",
      "Moldova",
      "Monaco",
      "Mongolia",
      "Montenegro",
      "Morocco",
      "Mozambique",
      "Myanmar, Burma",
      "Namibia",
      "Nauru",
      "Nepal",
      "Netherlands",
      "New Caledonia",
      "New Zealand",
      "Nicaragua",
      "Niger",
      "Nigeria",
      "Niue",
      "Northern Mariana Islands",
      "Norway",
      "Oman",
      "Pakistan",
      "Palau",
      "Palestinian Territories",
      "Panama",
      "Papua New Guinea",
      "Paraguay",
      "Peru",
      "Philippines",
      "Pitcairn Islands",
      "Poland",
      "Portugal",
      "Puerto Rico",
      "Qatar",
      "Reunion Island",
      "Romania",
      "Russian Federation",
      "Rwanda",
      "Saint Kitts and Nevis",
      "Saint Lucia",
      "Saint Martin (French Part)",
      "Saint Vincent and The Grenadines",
      "Samoa",
      "San Marino",
      "Sao Tome and Principe",
      "Saudi Arabia",
      "Senegal",
      "Serbia",
      "Seychelles",
      "Sierra Leone",
      "Singapore",
      "Sint Maarten (Ducth Part)",
      "Slovakia (Slovak Republic)",
      "Slovenia",
      "Solomon Islands",
      "Somalia",
      "South Africa",
      "South Sudan",
      "Spain",
      "Sri Lanka",
      "Sudan",
      "Suriname",
      "Swaziland",
      "Sweden",
      "Switzerland",
      "Syria, Syrian Arab Republic",
      "Taiwan (Republic of China)",
      "Tajikistan",
      "Tanzania",
      "Thailand",
      "Tibet",
      "Togo",
      "Tokelau",
      "Tonga",
      "Trinidad and Tobago",
      "Tunisia",
      "Turkey",
      "Turkmenistan",
      "Turks and Caicos Islands",
      "Tuvalu",
      "Uganda",
      "Ukraine",
      "United Arab Emirates",
      "United Kingdom",
      "United States",
      "United Kingdom",
      "United States",
      "Uruguay",
      "Uzbekistan",
      "Vanuatu",
      "Vatican City State (Holy See)",
      "Venezuela, RB",
      "Virgin Islands (British)",
      "Virgin Islands (U.S.)",
      "Wallis and Futuna Islands",
      "Western Sahara",
      "West Bank and gaza",
      "Yemen, Rep",
      "Zambia",
      "Zimbabwe"
    ]
  },
  "kalimat_tanya": "Negara Asal",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_25",
  "id_bab": "bab_3",
  "bentuk_form": {
    "select": [
      "Logics",
      "Mathematics",
      "Astronomy and Astrophysics",
      "Physics",
      "Chemistry",
      "Life Science",
      "Earth and Space Science",
      "Agricultural Sciences",
      "Medical Sciences",
      "Technological Sciences",
      "Anthropology",
      "Demography",
      "Economics Sciences",
      "Geography",
      "History",
      "Juridical Sciences and Law",
      "Linguistics",
      "Pedagogy",
      "Political Science",
      "Psychology",
      "Sciences of Art and Ethnic",
      "Sociology",
      "Ethics",
      "Philosophy"
    ]
  },
  "kalimat_tanya": "Bidang Ilmu",
  "wajib": "Y"
});
db.getCollection("pertanyaan").insert({
  "_id": "q_29",
  "id_bab": "bab_4",
  "bentuk_form": {
    "select": [
      "Application of logic",
      "Deductive logic",
      "General logic",
      "Inductive logic",
      "Methodology",
      "Other specialities relating to logic",
      "Algebra",
      "Analysis and functional analysis",
      "Computer sciences",
      "Geometry",
      "Number theory",
      "Numerical analysis",
      "Operations research",
      "Probability",
      "Statistics",
      "Topology",
      "Other mathematical specialities",
      "Cosmology and cosmogony",
      "Interplanetary medium",
      "Optical astronomy",
      "Planetology",
      "Radio‐astronomy",
      "Solar system",
      "Other astronomical specialities",
      "Acoustics",
      "Electro‐magnetism",
      "Electronics",
      "Fluid",
      "Mechanics",
      "Molecular physics",
      "Nuclear physics",
      "Nucleonics",
      "Optics",
      "Physical chemistry",
      "Solid state physics",
      "Theoretical physics",
      "Thermodynamics",
      "Units and constants",
      "Other physical specialities (tuliskan klasifikasinya",
      "pada kuesioner)",
      "Analytical chemistry",
      "Biochemistry",
      "Inorganic chemistry",
      "Macromolecular chemistry",
      "Nuclear chemistry",
      "Organic chemistry",
      "Physical chemistry",
      "Other chemical specialities",
      "Animal biology (zoology)",
      "Anthropology (physical)",
      "Biochemistry",
      "Biomathematics",
      "Biometrics",
      "Biophysics",
      "Cell biology",
      "Ethology",
      "Genetics",
      "Human biology",
      "Human physicology",
      "Immunology",
      "Insect biology (entomology)",
      "Microbiology",
      "Molecular biology",
      "Palaeontology",
      "Plant biology (botany)",
      "Radiobiology",
      "Symbiosis",
      "Virology",
      "Other biological specialities",
      "Atmospheric sciences",
      "Climatology",
      "Geochemistry",
      "Geodesy",
      "Geography",
      "Geology",
      "Geophysics",
      "Hydrology",
      "Meteorology",
      "Oceanography",
      "Soil science",
      "Space sciences",
      "Other earth, space, or environmental specialities",
      "Agricultural chemistry",
      "Agricultural engineering",
      "Agronomy",
      "Animal husbandry",
      "Fish and wildlife",
      "Forestry",
      "Horticulture",
      "Phytopathology",
      "Veterinary sciences",
      "Other agricultural specialities",
      "Clinical sciences",
      "Epidemology",
      "Forensic medicine",
      "Occupational medicine",
      "Internal medicine",
      "Nutrition sciences",
      "Pathology",
      "Pharmacodynamics",
      "Pharmacology",
      "Preventive medicine",
      "Pschiatry",
      "Public health",
      "Surgery",
      "Toxicology",
      "Other medical specialities",
      "Aeronautical technology and engineering",
      "Biochemical technology",
      "Chemical technology and engineering",
      "Computer technology",
      "Construction technology",
      "Electrical technology and engineering",
      "Electronic technology",
      "Environmental technology and engineering",
      "Food technology",
      "Industrial technology",
      "Instrumentation technology",
      "Materials technology",
      "Mechanical engineering and tenchnology",
      "Medical technology",
      "Metallurgical technology",
      "Metal products technology",
      "Motor vehicle technology",
      "Mining technology",
      "Neval technology",
      "Nuclear technology",
      "Petroleum and coal technology",
      "Power technology",
      "Railway technology",
      "Space technology",
      "Telecommunications technology",
      "Textile technology",
      "Transportation systems technology",
      "Unit operations technology",
      "Urban planning",
      "Other technological specialities",
      "Cultural anthropology",
      "Ethnography and ethnoogy",
      "Socia anthropology",
      "Other anthropological specialities",
      "Fertility",
      "General demography",
      "Geographical demography",
      "Historical demography",
      "Mortality",
      "Population characteristic",
      "Population size and demographic evolution",
      "Other demographic specialities",
      "Domestic fiscal policy and public finance",
      "Econometrics",
      "Economic accounting",
      "Economic activity",
      "Economic systems",
      "Economic of technological change",
      "Economic theory",
      "General economics",
      "Industrial organization and public policy",
      "International economics",
      "Organization and management of enterprises",
      "Sectorial economics",
      "Other economic specialities",
      "Economic geography",
      "Historical geography",
      "Human geography",
      "Regional geography",
      "Other geographical specialities",
      "Biographics",
      "General history",
      "History of countries",
      "History by epochs",
      "Sciences auxiliary to history",
      "Specialized histories",
      "Other historical specialities",
      "Canon law",
      "General theory and methods",
      "International law",
      "Legal organization",
      "National law and legislation",
      "Other juridical specialities",
      "Applied linguistics",
      "Diachronis linguistics",
      "Linguistic geography",
      "Linguistic theory",
      "Synchronic linguistics",
      "Other linguistic specialities",
      "Educational theory",
      "Organization and planning of education",
      "Teacher training and employment",
      "Other specialities",
      "International relations",
      "Policy sciences",
      "Political ideologies",
      "Political institution",
      "Political life",
      "Political sociology",
      "Political systems",
      "Political theory",
      "Public administration",
      "Public opinion",
      "Other political science specialities",
      "Abnormal psychology",
      "Adolescent and child psychology",
      "Counselling and guidance",
      "Educational psychology",
      "Evaluation and measurement in psychology",
      "Experimental psychology",
      "General psychology",
      "Geriatric psychology",
      "Occupational and personnel psychology",
      "Parapsychology",
      "Personality",
      "Psychological study of social issue",
      "Psychopharmacology",
      "Social psychology",
      "Other psychological specialities",
      "Architecture",
      "Literary theory, analysis and criticism",
      "Fine arts theory, analysis and criticism",
      "Other artistical specialities",
      "Cultural sociology",
      "Experimental sociology",
      "General sociology",
      "International disorganization",
      "Mathematical sociology",
      "Occupational sociology",
      "Social change and development",
      "Social communications",
      "Social groups",
      "Social problems – Social organizations",
      "Sociology of human seglements",
      "Other sociological specialities",
      "Classical ethics",
      "Ethics of individu",
      "Group ethics",
      "Prospective ethics",
      "Other sociological specialities",
      "Philosophy of knowledge",
      "Philosophical anthropology",
      "General philosophy",
      "Philosophical systems",
      "Philosophy of science",
      "Philosophy of nature",
      "Social philosophy",
      "Philosophical doctrines",
      "Other philosophical specialities"
    ]
  },
  "kalimat_tanya": "Bidang Penelitian",
  "wajib": "Y"
});

/** system.indexes records **/
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.system.js"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.bab"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "title": NumberInt(-1)
  },
  "name": "title_-1",
  "ns": "ristek.bab"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.counters"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.daftar"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.index"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.jawaban"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.jawaban_temp"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.kuesioner"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "title": NumberInt(-1)
  },
  "name": "title_-1",
  "ns": "ristek.kuesioner"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.pertanyaan"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "title": NumberInt(-1)
  },
  "name": "title_-1",
  "ns": "ristek.pertanyaan"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "id_bab": NumberInt(-1)
  },
  "name": "id_bab_-1",
  "ns": "ristek.pertanyaan"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "id_bab": NumberInt(1)
  },
  "name": "id_bab_1",
  "ns": "ristek.pertanyaan"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.users"
});
db.getCollection("system.indexes").insert({
  "v": NumberInt(1),
  "key": {
    "_id": NumberInt(1)
  },
  "name": "_id_",
  "ns": "ristek.lembaga"
});

/** system.js records **/
db.getCollection("system.js").insert({
  "_id": "getNextSequence",
  "value": function getNextSequence(name) {
    
    
   var ret = db.counters.findAndModify(
          {
      
      
            query:   {
        
         _id:   name 
      
      },
      
      
            update:   {
        
         $inc:   {
          
           seq:   1 
        
        } 
      
      },
      
      
            new:   true
          
    
    }
   );

   return ret.seq;

  
  }
});

/** users records **/
db.getCollection("users").insert({
  "_id": ObjectId("564eb2c404154000bbe510e2"),
  "joined_at": ISODate("2015-03-07T13:53:52.0Z"),
  "q_1": "Esti Mulyawati",
  "q_7": "estimulyawati@gmail.com",
  "q_10": "BATAN",
  "username": "esti",
  "password": "057e9f31d7020c13e90e4630f1d5f854b095ba49",
  "role": "admin",
  "status": "Sudah Dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("566542e4c4365bd42700002c"),
  "joined_at": ISODate("2015-12-07T08:27:16.0Z"),
  "q_1": "Rachmasari Cahyaningdyah",
  "q_2": "Direktur",
  "q_3": "01846",
  "q_4": "9182476214",
  "q_5": "74126412",
  "q_6": "917461",
  "q_7": "rachmaamma@gmail.com",
  "q_8": "2015-12-02",
  "q_9": "Ristekdikti",
  "q_10": "Lembaga Ilmu Pengetahuan Indonesia",
  "q_11": "Balai1",
  "q_12": "Jakarta",
  "q_13": "109476125",
  "q_14": "website.com",
  "q_15": "12746",
  "username": "Rachmasari",
  "password": "09fbd80a6d8afed8b206423e017fbbba7d6a5102",
  "role": "pegawai",
  "status": "Sudah Dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("56654ad9c4365bf40400002a"),
  "joined_at": ISODate("2015-12-07T09:01:13.0Z"),
  "q_1": "Rachma Syailendra",
  "q_2": "Pegawai",
  "q_3": "4949949494949",
  "q_4": "089595959595",
  "q_5": "0985868686",
  "q_6": "027457575757",
  "q_7": "syailendra@gmail.com",
  "q_8": "7 Desember 2015",
  "q_9": "Kementrian PAN",
  "q_10": "Bappenas",
  "q_11": "Bappenas",
  "q_12": "Jakarta",
  "q_13": "02156656565",
  "q_14": "bappenas.com",
  "q_15": "",
  "username": "Rachma",
  "password": "",
  "role": "pegawai",
  "status": "Belum dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("56654b2ac4365be422000029"),
  "joined_at": ISODate("2015-12-07T09:02:34.0Z"),
  "q_1": "Rachma Syailendra",
  "q_2": "Pegawai",
  "q_3": "4949949494949",
  "q_4": "089595959595",
  "q_5": "0985868686",
  "q_6": "027457575757",
  "q_7": "syailendra@gmail.com",
  "q_8": "7 Desember 2015",
  "q_9": "Kementrian PAN",
  "q_10": "Bappenas",
  "q_11": "Bappenas",
  "q_12": "Jakarta",
  "q_13": "02156656565",
  "q_14": "bappenas.com",
  "q_15": "",
  "username": "Rachma",
  "password": "",
  "role": "pegawai",
  "status": "Belum dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("564e8f1d04154000bbe510e1"),
  "joined_at": ISODate("2015-03-07T13:53:52.0Z"),
  "q_1": "Yulinri Tri Madya",
  "q_2": "Direktur Keuangan",
  "q_3": "123457890",
  "q_4": "021-8619773",
  "q_5": "08119703287",
  "q_6": "-",
  "q_7": "iinrabadi@gmail.com",
  "q_8": "11/14/2015",
  "q_9": "Kementerian Perdagangan",
  "q_10": "BATAN",
  "q_11": "Pusat Perdagangan Badan Perdagangan",
  "q_12": "Jalan ini Blok itu RT berapa RW berapa",
  "q_13": "021-888888",
  "q_14": "http://www.websitenya.com",
  "q_15": "9281466",
  "username": "iin",
  "password": "057e9f31d7020c13e90e4630f1d5f854b095ba49",
  "role": "pegawai",
  "status": "Sudah Dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("56665d04c4365bdc1900002c"),
  "joined_at": ISODate("2015-12-08T04:31:00.0Z"),
  "q_1": "Rachma Syailendra",
  "q_2": "Wakil Direktur",
  "q_3": "192746",
  "q_4": "182647254",
  "q_5": "981274621",
  "q_6": "821746314",
  "q_7": "rachma@gmail.com",
  "q_8": "2015-12-01",
  "q_9": "Kementrian1",
  "q_10": "Lembaga1",
  "q_11": "Balai1",
  "q_12": "Jakarta",
  "q_13": "891246785314",
  "q_14": "website.com",
  "q_15": "194785",
  "username": "Rachma",
  "password": "",
  "role": "pegawai",
  "status": "Belum dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("56665badc4365bdc1900002a"),
  "joined_at": ISODate("2015-12-08T04:25:17.0Z"),
  "q_1": "Rachmasari Cahyaningdyah",
  "q_2": "Direktur",
  "q_3": "92741854",
  "q_4": "9184634",
  "q_5": "87146513",
  "q_6": "1674561",
  "q_7": "rachmaamma@gmail.com",
  "q_8": "2015-12-02",
  "q_9": "Kementrian Ristek Dikti",
  "q_10": "Lembaga Ilmu Pengetahuan Indonesia",
  "q_11": "Balai Penelitian",
  "q_12": "Jakarta",
  "q_13": "192747547",
  "q_14": "website.com",
  "q_15": "98174852",
  "username": "Rachmasari",
  "password": "bf92c74a89ff8723d3cb9e51f914374a5e6e3128",
  "role": "pegawai",
  "status": "Sudah Dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("5666a992c4365bb01900002a"),
  "joined_at": ISODate("2015-12-08T09:57:38.0Z"),
  "q_1": "Sherina",
  "q_2": "Wakil Direktur",
  "q_3": "201987466",
  "q_4": "9127648",
  "q_5": "98127645",
  "q_6": "91246",
  "q_7": "sherina@gmail.com",
  "q_8": "2015-12-04",
  "q_9": "Kementrian Ristek Dikti",
  "q_10": "Lembaga1",
  "q_11": "Balai1",
  "q_12": "Jakarta",
  "q_13": "1902746",
  "q_14": "website.com",
  "q_15": "018947654",
  "username": "Sherina",
  "password": "",
  "role": "pegawai",
  "status": "Belum dikonfirmasi"
});
db.getCollection("users").insert({
  "_id": ObjectId("56677a73c4365bfc1100002a"),
  "joined_at": ISODate("2015-12-09T00:48:51.0Z"),
  "q_1": "qrq",
  "q_2": "rerqer",
  "q_3": "21412",
  "q_4": "12432",
  "q_5": "4214",
  "q_6": "1241",
  "q_7": "q@gmail.com",
  "q_8": "2015-12-09",
  "q_9": "4234",
  "q_10": "BATAN",
  "q_11": "arar",
  "q_12": "affa",
  "q_13": "213124",
  "q_14": "website.com",
  "q_15": "325235",
  "username": "qrq",
  "password": "",
  "role": "pegawai",
  "status": "Belum dikonfirmasi"
});
