// var base_apps_url = 'http://'+window.location.hostname+'/ristekdikti';

//README - buat tabel/grafik
function get_total_anggaran(id_lembaga){
    //api belom fix
    if (typeof id_lembaga == 'undefined') {id_lembaga = ''};
	address_total_anggaran = base_apps_url+'api/belanja/'+id_lembaga;
    console.log(address_total_anggaran);
	if (id_lembaga != '') {
        $.ajax({
            // url: base_apps_url+'/assets/totalAnggaran.json',
            url: address_total_anggaran,
            dataType: 'json',
            async: false,
            success: function(data) {
                console.log(data);
                dipa = [data.q_38[0], 0];
                // console.log(dipa);
                kerjasama = [data.result[0].BelanjaKerjasama, 0];
                // console.log(kerjasama);
                ekstramural = [0, data.result[0].BelanjaEkstramural];
                // console.log(ekstramural)
                upah = [0, data.q_35[0]+data.q_36[0]+data.q_37[0]];
                // console.log(upah)
            },
            error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
        });
    } else{
        $.ajax({
            // url: base_apps_url+'/assets/totalAnggaran.json',
            url: address_total_anggaran,
            dataType: 'json',
            async: false,
            success: function(data) {
                dipa = 0;
                upah = 0;
                console.log(data);
                $.each(data.belanja_lembaga, function( index, value ) {
                    console.log(index+' '+value);
                    dipa += parseInt(value.q_38);
                    upah += parseInt(value.q_35[0])+parseInt(value.q_36[0])+parseInt(value.q_37[0]);
                    // upah = upah.toString();
                });
                dipa = [dipa,0];
                upah = [0, upah];
                // console.log(dipa);
                // console.log(upah);
                kerjasama = [data.result[0].BelanjaKerjasama, 0];
                // console.log(kerjasama);
                ekstramural = [0, data.result[0].BelanjaEkstramural];
                // console.log(ekstramural)
            },
            error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
        });
    };
	this.dipa = dipa;
	this.kerjasama = kerjasama;
	this.ekstramural = ekstramural;
	this.upah = upah;
}
//README - buat tabel/grafik
function get_kegiatan_bulan(id_lembaga){
    if (typeof id_lembaga == 'undefined') {id_lembaga = ''};
	address_kegiatan_bulan = base_apps_url+'api/count/'+id_lembaga;
	$.ajax({
        // url: base_apps_url+'/assets/KegiatanPerBulan.json',
        url: address_kegiatan_bulan,
        dataType: 'json',
        async: false,
        success: function(data) {
            // console.log(data)
            // console.log(data.januari);
            dasar = [];
            terapan = [];
            eksperimental = [];
            average = [];
            jumlahDasar = 0;
            jumlahTerapan = 0;
            jumlahEksperimental = 0;
            $.each(data, function(key,val){
            	jumlahDasar = jumlahDasar + val.dasar;
            	jumlahTerapan = jumlahTerapan + val.terapan;
            	jumlahEksperimental = jumlahEksperimental + val.eksperimental;
            	dasar.push(val.dasar);
            	terapan.push(val.terapan);
            	eksperimental.push(val.eksperimental);
            	average.push((val.dasar+val.terapan+val.eksperimental)/3)
            });
        },
        error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
    }); 
    this.dasar = dasar;
    this.terapan = terapan;
    this.eksperimental = eksperimental;
    this.average = average;
    this.jumlahDasar = jumlahDasar;
    this.jumlahTerapan = jumlahTerapan;
    this.jumlahEksperimental = jumlahEksperimental;
}
//README - buat tabel/grafik
function get_bidang_jenis_kegiatan(id_lembaga){
    if (typeof id_lembaga == 'undefined') {id_lembaga = ''};
	address_bidang_jenis_kegiatan = base_apps_url+'api/jenis_kegiatan/'+id_lembaga;
	$.ajax({
        // url: base_apps_url+'/assets/BidangJenisKegiatan.json',
        url: address_bidang_jenis_kegiatan,
        dataType: 'json',
        async: false,
        success: function(data) {
            // console.log(data);
            jumlahDasar = 0;
            namaDasar = [];
            perDasar = [];
            $.each(data.dasar, function(key,val){
            	// console.log(key);
             //    console.log(val);
            	jumlahDasar = jumlahDasar + val;
            	namaDasar.push(key);
            	perDasar.push(val);
            });
            jumlahTerapan = 0;
            namaTerapan = [];
            perTerapan = [];
            $.each(data.terapan, function(key,val){
            	// console.log(val);
            	jumlahTerapan = jumlahTerapan + val;
            	namaTerapan.push(key);
            	perTerapan.push(val);
            });
            jumlahEksperimental = 0;
            namaEksperimental = [];
            perEksperimental = [];
            $.each(data.eksperimental, function(key,val){
            	// console.log(val);
            	jumlahEksperimental = jumlahEksperimental + val;
            	namaEksperimental.push(key);
            	perEksperimental.push(val);
            });

            jumlahDasarx = jumlahDasar / (jumlahDasar+jumlahTerapan+jumlahEksperimental) * 100;
			jumlahTerapanx = jumlahTerapan / (jumlahDasar+jumlahTerapan+jumlahEksperimental) * 100;
			jumlahEksperimentalx = jumlahEksperimental / (jumlahDasar+jumlahTerapan+jumlahEksperimental) * 100;
			
        },
        error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
    }); 
	this.jumlahDasar = jumlahDasarx;
	this.jumlahTerapan = jumlahTerapanx;
	this.jumlahEksperimental = jumlahEksperimentalx;
	this.namaDasar = namaDasar;
	this.namaTerapan = namaTerapan;
	this.namaEksperimental = namaEksperimental;
	this.perDasar = perDasar;
	this.perTerapan = perTerapan;
	this.perEksperimental = perEksperimental;
}
//README - buat ambil api di laporan berdasarkan id lembaga
function get_cetak_by_lembaga(idLembaga, jenis){
    this.id_lembaga = idLembaga;
    // console.log(this.id_lembaga)
    if (jenis == 'lembaga') {
        address_cetak_by_lembaga = base_apps_url+'api/cetak_by_lembaga/'+idLembaga;
    } else if(jenis == 'kegiatan') {
        address_cetak_by_lembaga = base_apps_url+'api/cetak_by_kegiatan/'+idLembaga;
    }
    
    // console.log(address_cetak_by_lembaga);
    $.ajax({
        url: address_cetak_by_lembaga,
        dataType: 'json',
        async: false,
        success: function(data) {
            // console.log(data);
            tabel1_1 = [];
            tabel1_2 = [];
            tabel1_3 = [];
            tabel1_3_1 = [];
            tabel1_3_2 = [];
            tabel1_4 = {'teknisi': {'laki': {'s2': 0, 's1': 0, 'd3': 0,'kd3': 0}, 'perempuan' : {'s2': 0, 's1': 0, 'd3': 0,'kd3': 0} }, 'staff_pendukung': {'laki': {'s2': 0, 's1': 0, 'd3': 0,'kd3': 0}, 'perempuan' : {'s2': 0, 's1': 0, 'd3': 0,'kd3': 0} }};
            tabel2_1 = [0,0,0];
            tabel3_1 = [];
            tabel3_2 = [];
            tabel4_1 = [];
            tabel5_1 = [];
            tabel5_2 = [];
            tabel5_3 = [];
            tabel5_4 = [];
            tabel5_5 = [];
            tabel6_1 = [];
            $.each(data, function(key,val){
                var tabel3_1_peneliti = 0;
                var tabel3_1_dipa = 0;
                var tabel3_2_dalam_negeri = [];
                var tabel3_2_luar_negeri = [];
                var tabel4_1_ekstramural = [];
                //q_20 : baca kewarganegaraan
                if (typeof val.q_20 != 'undefined') {
                    $.each(val.q_20, function(keyx,valx){
                        tabel3_1_peneliti++;
                        if (valx == 'WNI') {
                            tabel1_1.push({'nama': val.q_15[keyx], 'nip': val.q_17[keyx], 'jenis_kelamin': val.q_16[keyx], 'tingkat_pendidikan': val.q_18[keyx], 'bidang_ilmu': val.q_25[keyx], 'fungsional': val.q_19[keyx]})
                        } else if (valx == 'WNA') {
                            tabel1_2.push({'nama': val.q_15[keyx], 'kewarganegaraan': val.q_21[keyx],'nama_asal_institusi': val.q_24[keyx],'jenis_asal_institusi': val.q_23[keyx] ,'jenis_kelamin': val.q_16[keyx], 'tingkat_pendidikan': val.q_18[keyx], 'bidang_ilmu': val.q_25[keyx], 'fungsional': val.q_19[keyx]})
                        };

                        if (val.q_19[keyx] == 'Teknisi' && val.q_16[keyx] == 'Laki-laki') {
                            switch (val.q_18[keyx]) {
                                case '<D3':
                                    tabel1_4.teknisi.laki.kd3++;
                                    break;
                                case 'D3':
                                    tabel1_4.teknisi.laki.d3++;
                                    break;
                                case 'S1':
                                    tabel1_4.teknisi.laki.s1++;
                                    break;
                                case 'S2':
                                    tabel1_4.teknisi.laki.s2++;
                                    break;
                            }
                        } else if ( val.q_19[keyx] == 'Teknisi' && val.q_16[keyx] == 'Perempuan'){
                            switch (val.q_18[keyx]) {
                                case '<D3':
                                    tabel1_4.teknisi.perempuan.kd3++;
                                    break;
                                case 'D3':
                                    tabel1_4.teknisi.perempuan.d3++;
                                    break;
                                case 'S1':
                                    tabel1_4.teknisi.perempuan.s1++;
                                    break;
                                case 'S2':
                                    tabel1_4.teknisi.perempuan.s2++;
                                    break;
                            }
                        } else if (val.q_19[keyx] == 'Staf Pendukung' && val.q_16[keyx] == 'Laki-laki'){
                            switch (val.q_18[keyx]) {
                                case '<D3':
                                    tabel1_4.staff_pendukung.laki.kd3++;
                                    break;
                                case 'D3':
                                    tabel1_4.staff_pendukung.laki.d3++;
                                    break;
                                case 'S1':
                                    tabel1_4.staff_pendukung.laki.s1++;
                                    break;
                                case 'S2':
                                    tabel1_4.staff_pendukung.laki.s2++;
                                    break;
                            }
                        } else if(val.q_19[keyx] == 'Staf Pendukung'&& val.q_16[keyx] == 'Perempuan') {
                            switch (val.q_18[keyx]) {
                                case '<D3':
                                    tabel1_4.staff_pendukung.perempuan.kd3++;
                                    break;
                                case 'D3':
                                    tabel1_4.staff_pendukung.perempuan.d3++;
                                    break;
                                case 'S1':
                                    tabel1_4.staff_pendukung.perempuan.s1++;
                                    break;
                                case 'S2':
                                    tabel1_4.staff_pendukung.perempuan.s2++;
                                    break;
                            }
                        };
                        tabel1_3_1.push(val.q_26[keyx]);
                    });
                };
                //q_39 : luar negeri apa dalam negeri
                if (typeof val.q_39 != 'undefined') {
                    $.each(val.q_39, function(keyx, valx){
                        if (valx == 'Dalam Negeri') {
                            tabel3_2_dalam_negeri.push({'instansi': val.q_40[keyx], 'nama_instansi': val.q_41[keyx], 'jumlah_dana': val.q_42[keyx]});
                        } else if (valx == 'Luar Negeri') {
                            tabel3_2_luar_negeri.push({'instansi': val.q_40[keyx], 'nama_instansi': val.q_41[keyx], 'jumlah_dana': val.q_42[keyx]});
                        };
                    });     
                };               
                //q_44 : dana ekstramural 
                if (typeof val.q_44 != 'undefined') {
                    $.each(val.q_44, function(keyx, valx){
                        tabel4_1_ekstramural.push({'instansi': val.q_43[keyx], 'nama_instansi': val.q_44[keyx], 'jumlah_dana': val.q_45[keyx]});
                    });
                };
                
                // FIXME - Buat jurnal, kayanya bakal diubah dan harus dibenerin nunggu database
                if (typeof val.q_46 != 'undefined') {
                    $.each(val.q_46, function(keyx, valx){
                        if (val.q_53[keyx] == 'Internasional') {
                            tabel5_1.push({'judul_artikel': val.q_47[keyx], 'nama_jurnal': val.q_46[keyx], 'issn': val.q_51[keyx], 'tahun_terbit': val.q_52[keyx], 'jenis_luaran': val.q_53[keyx], 'akreditasi': val.q_54[keyx], 'nama_penulis': val.q_48[keyx], 'jk_penulis': val.q_49[keyx], 'pendidikan_penulis': val.q_50[keyx] });
                            console.log(tabel5_1);
                        } else if (val.q_54[keyx] == 'Ada'){
                            tabel5_2.push({'judul_artikel': val.q_47[keyx], 'nama_jurnal': val.q_46[keyx], 'issn': val.q_51[keyx], 'tahun_terbit': val.q_52[keyx], 'jenis_luaran': val.q_53[keyx], 'akreditasi': val.q_54[keyx], 'nama_penulis': val.q_48[keyx], 'jk_penulis': val.q_49[keyx], 'pendidikan_penulis': val.q_50[keyx]});
                            console.log(tabel5_2);
                        } else {
                            tabel5_3.push({'judul_artikel': val.q_47[keyx], 'nama_jurnal': val.q_46[keyx], 'issn': val.q_51[keyx], 'tahun_terbit': val.q_52[keyx], 'jenis_luaran': val.q_53[keyx], 'akreditasi': val.q_54[keyx], 'nama_penulis': val.q_48[keyx], 'jk_penulis': val.q_49[keyx], 'pendidikan_penulis': val.q_50[keyx]});
                            console.log(tabel5_3);
                        };
                    });
                };
                //FIXME - Buat buku, juga mungkin ada yang diubah
                if (typeof val.q_55 != 'undefined') {
                    $.each(val.q_55, function(keyx, valx){
                        tabel5_4.push({'judul_buku': val.q_55[keyx], 'nama_penulis': val.q_56[keyx], 'tahun_terbit': val.q_57[keyx], 'penerbit': val.q_58[keyx]});
                    });
                };
                //FIXME - Buat HKI, juga mungkin ada yang diubah
                if (typeof val.q_59 != 'undefined') {
                    $.each(val.q_59, function(keyx, valx){
                        tabel5_5.push({'nama_pemilik': val.q_59[keyx], 'judul_hki': val.q_60[keyx], 'jenis_hki': val.q_61[keyx], 'tahun_pendaftaran': val.q_62[keyx], 'status': val.q_63[keyx]});
                    });
                };
                //KRITK SARAN
                if (typeof val.q_64 != 'undefined') {
                    tabel6_1.push({'judul_penelitian': val.q_27, 'kritik_saran': val.q_64});
                };
                
                //tabel 2.1 - baris 1
                if (typeof val.q_35 != 'undefined') {
                    $.each(val.q_35, function(keyx,valx){
                        tabel2_1[0] = valx;
                    });    
                };
                //tabel 2.1 - baris 2
                if (typeof val.q_36 != 'undefined') {
                    $.each(val.q_36, function(keyx,valx){
                        tabel2_1[1] = valx;
                    });    
                };
                //tabel 2.1 - baris 3
                if (typeof val.q_37 != 'undefined') {
                    $.each(val.q_37, function(keyx,valx){
                        tabel2_1[2] = valx;
                    });    
                };
                
                //tabel 3.1 - dana DIPA
                if (typeof val.q_38 != 'undefined') {
                    $.each(val.q_38, function(keyx,valx){
                        tabel3_1_dipa += valx;
                    });    
                };
                
                tabel3_1.push({'judul_penelitian': val.q_27, 'koordinator': val.q_28, 'jumlah_peneliti': tabel3_1_peneliti, 'jenis_penelitian': val.q_29, 'bidang_penelitian': val.q_32, 'jumlah_dana': tabel3_1_dipa, 'tanggal_isi': val.created_at.sec});
                tabel3_2.push({'judul_penelitian': val.q_27, 'koordinator': val.q_28, 'jenis_penelitian': val.q_29, 'bidang_penelitian': val.q_32, 'sumber_dana_dalam_negeri': tabel3_2_dalam_negeri, 'sumber_dana_luar_negeri': tabel3_2_luar_negeri});
                tabel4_1.push({'judul_penelitian': val.q_27, 'jenis_penelitian': val.q_29, 'bidang_penelitian': val.q_32, 'belanja_ekstramural': tabel4_1_ekstramural});

                var difference = new Date(val.q_34.sec * 1000)-new Date(val.q_33.sec * 1000);
                var daysDifference = Math.floor(difference/1000/60/60/24);
                tabel1_3_2.push(daysDifference);
            });
            tabel1_3.push(average_array(tabel1_3_1));
            tabel1_3.push(average_array(tabel1_3_2));

            // README BAB 1 PERSONIL LITBANG
            // ----------------------------
            // console.log(tabel1_1);
            // console.log(tabel1_2);
            // console.log(tabel1_3);
            // console.log(tabel1_4);
            
            // README BAB 2 GAJI UPAH DAN BELANJA MODAL
            // console.log(tabel2_1);

            // README BAB 3 BELANJA LITBANG
            // console.log(tabel3_1);
            // console.log(tabel3_2);

            // README BAB 4 BELANJA LITBANG EKSTRAMURAL
            // console.log(tabel4_1);

            // README BAB 5 LUARAN LITBANG
            // console.log(tabel5_1); //jurnal internasionl
            // console.log(tabel5_2); //jurnal nasional terakreditasi
            // console.log(tabel5_3); //jurnal nasional tidak terakreditasi
            // console.log(tabel5_4); //buku dengan ISBN
            // console.log(tabel5_5); //HKI

            // README TANGGAPAN DAN SARAN
            // console.log(tabel6_1);
        },
        error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
    }); 
    this.tabel1_1 = tabel1_1;
    this.tabel1_2 = tabel1_2;
    this.tabel1_3 = tabel1_3;
    this.tabel1_4 = tabel1_4;
    this.tabel2_1 = tabel2_1;
    this.tabel3_1 = tabel3_1;
    this.tabel3_2 = tabel3_2;
    this.tabel4_1 = tabel4_1;
    this.tabel5_1 = tabel5_1;
    this.tabel5_2 = tabel5_2;
    this.tabel5_3 = tabel5_3;
    this.tabel5_4 = tabel5_4;
    this.tabel5_5 = tabel5_5;
    this.tabel6_1 = tabel6_1;
}
//README - buat ambil api di laporan berdasarkan id user
function get_cetak_by_user(idUser){
    address_identitas_pengisi = base_apps_url+'api/get_identitas_pengisi/'+idUser;
    $.ajax({
        // url: base_apps_url+'/assets/totalAnggaran.json',
        url: address_identitas_pengisi,
        dataType: 'json',
        async: false,
        success: function(data) {
            // var identitas;
            identitas = data.identitas;
        },
        error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
    }); 

    address_institusi_pengisi = base_apps_url+'api/get_institusi_pengisi/'+idUser;
    console.log(address_institusi_pengisi)
    $.ajax({
        // url: base_apps_url+'/assets/totalAnggaran.json',
        url: address_institusi_pengisi,
        dataType: 'json',
        async: false,
        success: function(data) {
            // var institusi;
            institusi = data.institusi;
        },
        error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
    }); 
    this.identitas = identitas;
    this.institusi = institusi;
    // console.log(identitas);
}
//README - buat tabel di halaman cetak (daftar kegiatan aja)
function get_daftar_kegiatan_by_lembaga(idLembaga){
    address_cetak_by_lembaga = base_apps_url+'api/cetak_by_lembaga/'+idLembaga;
    $.ajax({
        // url: base_apps_url+'/assets/totalAnggaran.json',
        url: address_cetak_by_lembaga,
        dataType: 'json',
        async: false,
        success: function(data) {
            // var identitas;
            kegiatan = data;
        },
        error: function(jqXHR, textStatus, errorThrown) { console.log('gagal')},
    }); 
    this.kegiatan = kegiatan;
}
//README - cetak yang ke function cetaknya
function cetak_laporan(id_user, id_laporannya, jenis_laporan){
    if (jenis_laporan == 'lembaga') {
        cetak_laporan = new get_cetak_by_lembaga(id_laporannya, 'lembaga');
        laporan_kegiatan = '';
    } else if (jenis_laporan == 'kegiatan') {
        cetak_laporan = new get_cetak_by_lembaga(id_laporannya, 'kegiatan');
        laporan_kegiatan = 'LAPORAN PER KEGIATAN \n '+cetak_laporan.tabel3_1[0].judul_penelitian+'\n'
    };
    // cetak_user = new get_cetak_user(id_user);
    cetak_laporan_user = new get_cetak_by_user(id_user);
    // console.log(cetak_laporan_user);
    

    $('#idlembaga').html(cetak_laporan_user.institusi.balai);
    // console.log('ini di page');
    // console.log(cetak_laporan.tabel6_1);
    //inisiasi cetak tabel
    cetak_tabel1_1 = [];
    cetak_tabel1_1.push([ { text: 'Nama', style: 'tableHeader' }, { text: 'NIP', style: 'tableHeader' }, { text: 'Jenis Kelamin', style: 'tableHeader' }, { text: 'Tingkat Pedidikan', style: 'tableHeader' }, { text: 'Bidang Ilmu', style: 'tableHeader' }, { text: 'Fungsional', style: 'tableHeader' } ])
    for (var i = cetak_laporan.tabel1_1.length - 1; i >= 0; i--) {
        cetak_tabel1_1.push([cetak_laporan.tabel1_1[i].nama, cetak_laporan.tabel1_1[i].nip, cetak_laporan.tabel1_1[i].jenis_kelamin, cetak_laporan.tabel1_1[i].tingkat_pendidikan, cetak_laporan.tabel1_1[i].bidang_ilmu, cetak_laporan.tabel1_1[i].fungsional]);
    };
    cetak_tabel1_2 = [];
    cetak_tabel1_2.push([ { text: 'Nama', style: 'tableHeader' }, { text: 'Kewarganegaraan', style: 'tableHeader' }, { text: 'Asal Institusi', style: 'tableHeader' }, { text: 'Jenis Kelamin', style: 'tableHeader' }, { text: 'Tingkat Pendidikan', style: 'tableHeader' }, { text: 'Bidang Ilmu', style: 'tableHeader' }, { text: 'Fungsional', style: 'tableHeader' } ])
    for (var i = cetak_laporan.tabel1_2.length - 1; i >= 0; i--) {
        cetak_tabel1_2.push([cetak_laporan.tabel1_2[i].nama, cetak_laporan.tabel1_2[i].kewarganegaraan, cetak_laporan.tabel1_2[i].nama_asal_institusi, cetak_laporan.tabel1_2[i].jenis_kelamin, cetak_laporan.tabel1_2[i].tingkat_pendidikan, cetak_laporan.tabel1_2[i].bidang_ilmu, cetak_laporan.tabel1_2[i].fungsional]);
    };
    cetak_tabel1_3 = [];
    cetak_tabel1_3.push([ { text: 'No', style: 'tableHeader' }, { text: 'Rata-rata waktu yang digunakan', style: 'tableHeader' }, { text: 'Rata-rata', style: 'tableHeader' }, { text: 'Satuan', style: 'tableHeader' }])
    cetak_tabel1_3.push(['1','Rata-rata peneliti menghabiskan waktu untuk kegiatan litbang dalam seminggu',(Math.round(cetak_laporan.tabel1_3[0] * 100) / 100).toString(), 'Jam']);
    cetak_tabel1_3.push(['2','Rata-rata lamanya kegiatan litbang dalam setahun',(Math.round(cetak_laporan.tabel1_3[1] * 100) / 100).toString(), 'Hari']);
    cetak_tabel1_4 = [];
    cetak_tabel1_4.push([ { text: 'No', style: 'tableHeader' }, { text: 'Jabatan', style: 'tableHeader' }, { text: 'Jenis Kelamin', style: 'tableHeader' }, { text: 'Pendidikan\nS2', style: 'tableHeader' }, { text: 'Pendidikan\nS1', style: 'tableHeader' }, { text: 'Pendidikan\nD3', style: 'tableHeader' }, { text: 'Pendidikan\n<D3', style: 'tableHeader' }])
    cetak_tabel1_4.push([{rowSpan: 2, text:'1'},{rowSpan: 2, text:'Teknisi'}, 'Pria', cetak_laporan.tabel1_4.teknisi.laki.s2.toString(),cetak_laporan.tabel1_4.teknisi.laki.s1.toString(),cetak_laporan.tabel1_4.teknisi.laki.d3.toString(),cetak_laporan.tabel1_4.teknisi.laki.kd3.toString()]);
    cetak_tabel1_4.push(['','', 'Wanita', cetak_laporan.tabel1_4.teknisi.perempuan.s2.toString(),cetak_laporan.tabel1_4.teknisi.perempuan.s1.toString(),cetak_laporan.tabel1_4.teknisi.perempuan.d3.toString(),cetak_laporan.tabel1_4.teknisi.perempuan.kd3.toString()]);
    cetak_tabel1_4.push([{rowSpan: 2, text:'2'},{rowSpan: 2, text:'Staff Pendukung'}, 'Pria', cetak_laporan.tabel1_4.staff_pendukung.laki.s1.toString(),cetak_laporan.tabel1_4.staff_pendukung.laki.d3.toString(),cetak_laporan.tabel1_4.staff_pendukung.laki.s2.toString(),cetak_laporan.tabel1_4.staff_pendukung.laki.kd3.toString()]);
    cetak_tabel1_4.push(['','', 'Wanita', cetak_laporan.tabel1_4.staff_pendukung.perempuan.s2.toString(),cetak_laporan.tabel1_4.staff_pendukung.perempuan.s1.toString(),cetak_laporan.tabel1_4.staff_pendukung.perempuan.d3.toString(),cetak_laporan.tabel1_4.staff_pendukung.perempuan.kd3.toString()]);
    cetak_tabel2_1 = [];
    cetak_tabel2_1.push([ { text: 'No', style: 'tableHeader' }, { text: 'Belanja', style: 'tableHeader' }, { text: 'Jumlah Dana', style: 'tableHeader' }])
    cetak_tabel2_1.push(['1', 'Gaji/upah +tunjangan (Peneliti)',format_currency(cetak_laporan.tabel2_1[0], 'Rp. ')]);
    cetak_tabel2_1.push(['2', 'Gaji/upah+tunjangan (teknisi dan staf pendukung)',format_currency(cetak_laporan.tabel2_1[1], 'Rp. ')]);
    cetak_tabel2_1.push(['3', 'Belanja modal (tanah, gedung dan bangunan, kendaraan, mesin dan peralatan)',format_currency(cetak_laporan.tabel2_1[2], 'Rp. ')]);
    cetak_tabel3_1 = [];
    cetak_tabel3_1.push([ { text: 'Judul Penelitian', style: 'tableHeader' }, { text: 'Nama Ketua Peneliti / Koordinator', style: 'tableHeader' }, { text: 'Jumlah Anggota Peneliti', style: 'tableHeader' }, { text: 'Jenis Penelitian', style: 'tableHeader' }, { text: 'Bidang Penelitian', style: 'tableHeader' }, { text: 'Jumlah Dana', style: 'tableHeader' }]);
    for (var i = cetak_laporan.tabel3_1.length - 1; i >= 0; i--) {
        cetak_tabel3_1.push([cetak_laporan.tabel3_1[i].judul_penelitian, cetak_laporan.tabel3_1[i].koordinator, cetak_laporan.tabel3_1[i].jumlah_peneliti.toString(), cetak_laporan.tabel3_1[i].jenis_penelitian, cetak_laporan.tabel3_1[i].bidang_penelitian, format_currency(cetak_laporan.tabel3_1[i].jumlah_dana, 'Rp. ') ]);

    };
    var tanggal_isi = new Date(cetak_laporan.tabel3_1[0].tanggal_isi*1000)
    var day = tanggal_isi.getDate()
    var month = tanggal_isi.getMonth() + 1
    var year = tanggal_isi.getFullYear()
    tanggal_pengisian = day+'/'+month+'/'+year;
    
    cetak_tabel3_2 = [];
    cetak_tabel3_2.push([ { colSpan: 4, text: ' ', style: 'tableHeader' },'', '','', { colSpan: 2, text: 'Sumber Dana', style: 'tableHeader' },'']);
    cetak_tabel3_2.push([ { text: 'Judul Penelitian', style: 'tableHeader' }, {text: 'Nama Ketua Peneliti', style: 'tableHeader' },  { text: 'Jenis Penelitian', style: 'tableHeader' },  { text: 'Bidang Penelitian', style: 'tableHeader' }, {text: 'Dalam Negeri', style: 'tableHeader' }, {text: 'Luar Negeri', style: 'tableHeader' }]);
    for (var i = cetak_laporan.tabel3_2.length - 1; i >= 0; i--) {
        str_dalam_negeri = '';
        str_luar_negeri = '';
        for (var j = cetak_laporan.tabel3_2[i].sumber_dana_dalam_negeri.length - 1; j >= 0; j--) {
            str_dalam_negeri += 'Instansi : '+cetak_laporan.tabel3_2[i].sumber_dana_dalam_negeri[j].nama_instansi+'\n Jumlah :'+format_currency(cetak_laporan.tabel3_2[i].sumber_dana_dalam_negeri[j].jumlah_dana, 'Rp.')+'\n';
        };
        for (var j = cetak_laporan.tabel3_2[i].sumber_dana_luar_negeri.length - 1; j >= 0; j--) {
            str_luar_negeri += 'Instansi : '+cetak_laporan.tabel3_2[i].sumber_dana_luar_negeri[j].nama_instansi+'\n Jumlah :'+format_currency(cetak_laporan.tabel3_2[i].sumber_dana_luar_negeri[j].jumlah_dana, 'Rp.')+'\n';
        };
        cetak_tabel3_2.push([cetak_laporan.tabel3_2[i].judul_penelitian, cetak_laporan.tabel3_2[i].koordinator, cetak_laporan.tabel3_2[i].jenis_penelitian, cetak_laporan.tabel3_2[i].bidang_penelitian, str_dalam_negeri,str_luar_negeri ]);
    };
    cetak_tabel4_1 = [];
    cetak_tabel4_1.push([ { text: 'Instansi', style: 'tableHeader' }, { text: 'Nama Instansi', style: 'tableHeader' }, { text: 'Bidang Penelitian', style: 'tableHeader' }, { text: 'Nama Kegiatan', style: 'tableHeader' }, { text: 'Jumlah Dana', style: 'tableHeader' }]);
    for (var i = cetak_laporan.tabel4_1.length - 1; i >= 0; i--) {
        for (var j = cetak_laporan.tabel4_1[i].belanja_ekstramural.length - 1; j >= 0; j--) {
            if (cetak_laporan.tabel4_1[i].belanja_ekstramural[j].jumlah_dana != 0) {
                cetak_tabel4_1.push([cetak_laporan.tabel4_1[i].belanja_ekstramural[j].instansi, cetak_laporan.tabel4_1[i].belanja_ekstramural[j].nama_instansi, cetak_laporan.tabel4_1[i].bidang_penelitian, cetak_laporan.tabel4_1[i].judul_penelitian, format_currency(cetak_laporan.tabel4_1[i].belanja_ekstramural[j].jumlah_dana,'Rp.')]);
            };
            
        };      
    };
    cetak_tabel5_1 = [];
    cetak_tabel5_1.push([ { text: 'Judul Artikel', style: 'tableHeader' }, { text: 'Penulis', style: 'tableHeader' }, { text: 'Jenis Kelamin', style: 'tableHeader' }, { text: 'Tingkat Pendidikan', style: 'tableHeader' }, { text: 'Nama Jurnal', style: 'tableHeader' }, { text: 'ISSN', style: 'tableHeader' }, { text: 'Tahun Terbit', style: 'tableHeader' }]);
    if (typeof cetak_laporan.tabel5_1.length !== 'undefined' && cetak_laporan.tabel5_1.length > 0) {
        for (var i = cetak_laporan.tabel5_1.length - 1; i >= 0; i--) {
            if (cetak_laporan.tabel5_1[i].judul_artikel!="") {
                cetak_tabel5_1.push([cetak_laporan.tabel5_1[i].judul_artikel, cetak_laporan.tabel5_1[i].nama_penulis, cetak_laporan.tabel5_1[i].jk_penulis, cetak_laporan.tabel5_1[i].pendidikan_penulis, cetak_laporan.tabel5_1[i].nama_jurnal, cetak_laporan.tabel5_1[i].issn, cetak_laporan.tabel5_1[i].tahun_terbit])
            };          
        };
    }
    
    cetak_tabel5_2 = [];
    cetak_tabel5_2.push([ { text: 'Judul Artikel', style: 'tableHeader' }, { text: 'Penulis', style: 'tableHeader' }, { text: 'Jenis Kelamin', style: 'tableHeader' }, { text: 'Tingkat Pendidikan', style: 'tableHeader' }, { text: 'Nama Jurnal', style: 'tableHeader' }, { text: 'ISSN', style: 'tableHeader' }, { text: 'Tahun Terbit', style: 'tableHeader' }]);
    if (typeof cetak_laporan.tabel5_2.length !== 'undefined' && cetak_laporan.tabel5_2.length > 0) {
        for (var i = cetak_laporan.tabel5_2.length - 1; i >= 0; i--) {
            if (cetak_laporan.tabel5_2[i].judul_artikel!="") {
                cetak_tabel5_2.push([cetak_laporan.tabel5_2[i].judul_artikel, cetak_laporan.tabel5_2[i].nama_penulis, cetak_laporan.tabel5_2[i].jk_penulis, cetak_laporan.tabel5_2[i].pendidikan_penulis, cetak_laporan.tabel5_2[i].nama_jurnal, cetak_laporan.tabel5_2[i].issn, cetak_laporan.tabel5_2[i].tahun_terbit])
            };
        };
    }

    cetak_tabel5_3 = [];
    cetak_tabel5_3.push([ { text: 'Judul Artikel', style: 'tableHeader' }, { text: 'Penulis', style: 'tableHeader' }, { text: 'Jenis Kelamin', style: 'tableHeader' }, { text: 'Tingkat Pendidikan', style: 'tableHeader' }, { text: 'Nama Jurnal', style: 'tableHeader' }, { text: 'ISSN', style: 'tableHeader' }, { text: 'Tahun Terbit', style: 'tableHeader' }]);
    if (typeof cetak_laporan.tabel5_3.length !== 'undefined' && cetak_laporan.tabel5_3.length > 0) {
        for (var i = cetak_laporan.tabel5_3.length - 1; i >= 0; i--) {
            if (cetak_laporan.tabel5_3[i].judul_artikel!="") {
                cetak_tabel5_3.push([cetak_laporan.tabel5_3[i].judul_artikel, cetak_laporan.tabel5_3[i].nama_penulis, cetak_laporan.tabel5_3[i].jk_penulis, cetak_laporan.tabel5_3[i].pendidikan_penulis, cetak_laporan.tabel5_3[i].nama_jurnal, cetak_laporan.tabel5_3[i].issn, cetak_laporan.tabel5_3[i].tahun_terbit])    
            };
        };
    }

    cetak_tabel5_4 = [];
    cetak_tabel5_4.push([ { text: 'Judul Buku', style: 'tableHeader' }, { text: 'Nama Penulis', style: 'tableHeader' }, { text: 'Tahun Terbit', style: 'tableHeader' }, { text: 'Penerbit', style: 'tableHeader' }]);
    for (var i = cetak_laporan.tabel5_4.length - 1; i >= 0; i--) {
        if (cetak_laporan.tabel5_4[i].judul_buku!="") {
            cetak_tabel5_4.push([cetak_laporan.tabel5_4[i].judul_buku, cetak_laporan.tabel5_4[i].nama_penulis, cetak_laporan.tabel5_4[i].tahun_terbit, cetak_laporan.tabel5_4[i].penerbit]);
        };
    };

    cetak_tabel5_5 = [];
    cetak_tabel5_5.push([ { text: 'Nama Pemilik', style: 'tableHeader' }, { text: 'Judul HKI', style: 'tableHeader' }, { text: 'Jenis HKI', style: 'tableHeader' }, { text: 'Tahun Pendaftaran', style: 'tableHeader' }, { text: 'Status', style: 'tableHeader' }]);
    for (var i = cetak_laporan.tabel5_5.length - 1; i >= 0; i--) {
        if (cetak_laporan.tabel5_5[i].judul_hki!="") {
            cetak_tabel5_5.push([cetak_laporan.tabel5_5[i].nama_pemilik, cetak_laporan.tabel5_5[i].judul_hki, cetak_laporan.tabel5_5[i].jenis_hki, cetak_laporan.tabel5_5[i].tahun_pendaftaran, cetak_laporan.tabel5_5[i].status]);
        };
    };

    cetak_tabel6_1 = [];
    cetak_tabel6_1.push([ { text: 'Nama Kegiatan', style: 'tableHeader' }, { text: 'Kritik dan Saran', style: 'tableHeader' }]);
    for (var i = cetak_laporan.tabel6_1.length - 1; i >= 0; i--) {
        if (cetak_laporan.tabel6_1[i].kritik_saran!="") {
            cetak_tabel6_1.push([cetak_laporan.tabel6_1[i].judul_penelitian, cetak_laporan.tabel6_1[i].kritik_saran]);
        };
    };

    var currentDate = new Date()
    var day = currentDate.getDate()
    var month = currentDate.getMonth() + 1
    var year = currentDate.getFullYear()
    var tanggal_hari_ini = day+'_'+month+'_'+year;

    var docDefinition = {
        footer: function(currentPage, pageCount) { return {text: 'halaman '+currentPage.toString() + ' dari ' + pageCount+' - di buat pada : '+tanggal_hari_ini+' / '+cetak_laporan_user.institusi.balai, italics: true, margin: [72,0,0,40], fontSize: 9 } },
      content: [
        {
            table: {
                    body: [
                            [{text: 'RAHASIA', fontSize: 20, bold: true,}],
                    ]
            },
            margin: [0, 0, 0, 40]
        },
        {
            columns: [
                {
                    width: '*',
                    alignment: 'center',
                    stack: [
                        {
                            image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAtAAAAKgCAMAAACFq7pvAAAC8VBMVEUAAABKY6N9k60AcLwsLJJFiLYAbbuLlKoAbrxfX5r+89Fgd6YqKpEoKJAAb74AbbxGkcP//+r347mmqKl/ma5hhKP/+H0Abb77z5r8+dPy68cpKZO9t7Dy6sn29NhjmsATdLYmJpKcnrLxxJzx4Kh4d6URd7xXi65Oj7s8PJVvbqMce7vSzb8Acb5JSZk/jcJISJgrgbyGha6GsMysrbdTia6yraoxfbFPTpuOjqt7nbdhYJ6MlqdFRZRVVZ2sopkfe7pmjah2pcVfXp1QT5xShKaIiZxDQ5YgebSfnqGGnbBRi7NYV5smebFJSJVzcqV2daJYV5a8rqKKq8Irf7mYpK6ZlJK5trE1hLtrap5raqqPkZ1ul7KZnbQud6pYV55mZaFPTpZQjbYtgLlWjLGmrLpcW6A4hbo8grJQT5hYlL2Fh6RpkKyAhJCak49/f6VYgZ6UlLhfX59NTJdgk7ZgmL5BdptpaKiIj5k8eqV3jZ1QfZx9fKpgYJyHhqtEhrRZWIk4gbNhYJhHgqw3hr1/fptMS4x2dZxfXp9Ij8CSkay9r6Sno504eKR4eYxqhpppaKFhhp+WlZhkY6Rsao5kmLuqqKZVmMZFirmlnJVsa48mJpAygLZ2nbgpgr5hYIpGirh5ipdRjreMm6dnZp9zc6lxlKx9fKQUdbhdXIpqaaNHirlGeJo0gbdphZlsao5HgqqWmrF2eIpzm7dlhpx3j6BcfpZ7eqRumLWJiY+Kia6EpLmRkZZbjbB8e48/i8CbnqJ5eYhcg5+GhYyEiJNrgZJLibN0laxTj7gtfLFdXmFxcnQuLpIAcLswMJMAbrwpKZECcLoAbLksLJIadbQoKJAAb702NpQJc7sefsAXer8zM5NAQJhERJoOc7cQdbsmerQ5OZUXdbYRd74NcbU1f7FAP5IGcrwYeLoFbLM8PJc8PJQ4OJIhc6w0NIw8PIsXcbAObrBOTZNEQ4ssLIwWbakvc6JQT4ghbqQ3cJc/R0zu4s9qAAAAzHRSTlMAAgX9/AX6A/sEDQP7+/v98QMODoANA/oNBQv7ExIHgPL7SBMKf/KAgPOB8RX+/PLw8YCARvKA8vBHgLcU/u5K/vKAfv2AgPPygEvxt4B9tUrxRkn8f0w8/LfxgEk9/vvt8ku3tkrwtfK8tEy2toI/837sh39L/biz/YD9tId5tf2B9PLwf/3wwLV0TD7ytbaOgrX47rR68ex4/PjDs4P++/GKfXbAgnH68sJ98+36vbqKgD3suLaMdcC0efHswb6y9MH0jvG/7sF3/vwEmcdMAAB8WElEQVR42uzdO4gTQRgH8NldvI0ezOKDrBGN4gZsTOcDQYgWChY2AbU4SGVhoTGFLCZXiVhplLMQ0imCNuFQieKBha8rVJZlBhGxUBFFtghkiywXK2fy8P28bJL1/P84QpJLc/DPd998M5sQAAAAAAAAAAAAAACAf5jasVghPcoitUMnAP8EXVUV8pV4cklPknxJQa4hslR1US/IyWUrJauSl0pT5y71nJsqiScmKluycfGCZLdqI9YQJYosyr0gxzP5q0emy9VqudxqCxoN3/e6/Eaj1WoELdsuV8v2rYlMXMQatRqio9tfJKwLux8d3FkoVKt2vem5bWYbpenPKBVPiCcNV/Bz1WqhcG2qYiVkr4JMwyjJJkOWZSue3VI5Pl2sv2Kcc8ZlgjVNc7qY+Pms89ARxEuoaTDO2Rt7+uaebCJFCOo0jIaqqkRaZVUOX9yaywVNPmbGNKkd2D/EHE2gMe7ncodunkmKPC8mAEOld7O8esWVqVo58FzXME2qOX3QNNN45hdrV7bFdaKoBGAYeh1zSjTMN2bLTxu+55q9otwn2YB4jVxhQvbT6Dxg8HSZ5uSaeOZhrbPwo7JZZk5YmJOmBp+bPFfZiEjDYOlqZ5RROX+pYNc9d8ykaWcQNOr69qW8hWYaBkRW5s7yr3RrtjjncYNqmuwxBkS2HoHoPIiCKg2ha0/mUolM/kEhaHieKMwizM6ApSmvF3aLSCsEINxGQ4+fzt8q2i2fU6qlRZiHgDFNRjq/Cq00hGZ8XNxYlanaZOC5hpnWRJcxPDLS9rkUQY2GUCyWjUb2oegzODfbgzlnyJhm+if3k8WINITRaSSOHr6Ym+Nyx2RkqJE7HEfbAf3QZX5Wbi8V6p5rUI05oyOLdEO0Hdg5hHlS1U6ncXJdg8c0Oc8YMUb96QwSDfOiy07jzM1Ctc5dqjmRwKhX3oNpB8wnzcS68LBmN7gRlTRLTBsr3l2KTRb4273AVdbErD3nGTQCncY3jXTxPBINf3WCLmHlZ8t1b4z+cOuE9Xxx3xkiM0Ci4c8HzkTsBdotz/0uzfKhpjlpSmnv8ilT3m8b5ppRJpoA/ElxtipHJgOZ5l7j3L2QJCbQmMuY6zfnmm11qXvfc5lB6VgspmlDmO2Zwd0EAfhdcU5YpZO5Jje/SHP7Uj/OOXv//mWjXhUuzdw4MnPnzszxUxMTE7tvHLkjzcqrYf3n7xnn7hB2X8ziHkzv4Bf0cUI23j74riHiKBrjzvl6Ghsbc7kf2OWTm/fu3XuidNkSH5+xLEkSq5YsWdWtkckl0rKV8czE1Im9ezefLAdvXWNssHNrjVctAvDzze3E6su1p9xtp1kTTO69ffvmxeutm6f2ZOPLlpDPFHVcEbfjaptCvrJkpVXZVc7teNU7Ke0MAtP8czipBD+k63ID5X5R7gYyseYzx7hQn7y0a8OxtfusFGlbJKjqr98Wi4T2vXj2+obD0+Um5+aAxn7sWbCfjBOA7zpnhWTFBspbg6bT1DRcL5gsFAo7H2bjevcVqj6PqwBIKnVmZmfBbg4q07y4CTuG8KPifLNY55RSw2s2Wna1lr9giUZZ/PY3Nfk31VpeQivaj5lCq+mZNPxIa+x+nAB81TknxQ5K8a1pPvP8wK4deZjPxJeRNlUP56ReQswBp+3mAPbPzXoJkw74sjgnz8zcCzyD+0F5diqfjaeWds/YhbzvuFHMttsdergl+lkZkw7oxVkhm/L3Hn/4MFevTt+sdJZ+imyXQ6cqnTa9HvKZPUYx6YBPzYYozo+FJw/ymUTqh0u/sLcgd59sGOH2Hc/q2zDpAEIUcmDmiQzz7j2bEjoZxsfHyfeQVZp8G2qkNX4QJRp0cvTGgwdPzuY3JZZ2Vn/DsVi8jw6/82IhRtp4vR6ju/+dTq4/ePJgy5pkt88YIlVE+v5TU2Mo0RCajQcu7LmwcTRf+6DrZFWpzGl4JbqYRYn+vy0/kOzU6dHQVbK/MGeEVaRpo4SPcfzv6apKRkclqfP2WEiJ1ngNs+j/3Mj/Q+sK2T/p0ZBKdJAf/V8E/zdFF2vDeiiJZmn/0ioCMFo6iR+vm04INF5AzwEjpytLQ0p0zK7giBKMnBJSohltzhCAkeskmjn9SnuzywjAyClK/EgYK0O3upIAjF4n0azvQNsZDO4gCkSiz3l977CYrTxWhRAJCrE2c83pC6ONGwg0RINOMpO8z0Cnm2cJQDSMk/25Z85fwJgDIm2c3GxSpx+aW8aYA6LDqvG00w/XxploiIxxkikaTj/MFja/ITpUsrtOGQINC4Wy9JbXx+yOYRANkaKSStBHiWY0QKAhUqxZr491IW1MkUUEICoWk3xA+wi0j0BDlOgkO91Hidb8mwQgQhRyfo4yBBoWCFGiJzlaDlgwFHK8mWYINCwYVtnVEGhYKBRy30OgYcFQSalBGebQsGDIngOBhgVjySV/nstC+haBhqhZTErN+fUcjOLr3SByVHL5acz5FZy2g3+KVX7m/A4+xwD+Hbd8zfk5HPCHf4pK8jlzfoG2tyDQEDU6Of278xy4SBY+sneurzLEYRx/ZnYYvzOzq+O+61IO0ortJInckrvkVgi5vaDkTrlTSETupfBKSCEhl5JXLnnza86ssfbF7mKFOudQKOKd+S1CbjPDzsyzns9/sGe/59nvc/k9DybaXf79FiVaY0CgosVQT4LuVX+ZFs0QIaTqrodxDrHx/BJddiPCB4PuD5t5EfQr6qsQIUSGE49/XuagMjSBEifzSXQ1iEBDO0/bDOrO0V03IpS0OO5B0BG6sUKElSvuyxyiakeCJsJIExjx1O2+GbHA/0YVEET4aAIHbEG7pe27mVSGJsKIN0Hf20CTHEQo8SJocbyeLDRq5G+pqNDkUdBDaZIDJ5ImS/ADrHJ6Cj8KmnLCCkX9LOVOHat6rviGi/PjqpB6ZYjak6Dbzl5KFhoTqsykkpRbsEXb18yYNK/wLamVPXYNTHQC0CrgS/VkOZpuoOcqiJAZAHSN998+YOu8ZLLRymQy0WhULxG1Mc3s8+Twcad7xitA0l4E3av+eEcgcCBLIEGfa+Onb00+tzKmZemKYRj8OwxFt0yrMXVqFn7j4UXQbWePIceBAmE04h02b5rUmOVmeyFl/guEqE1j+KlZ2L9ZD53CSN1sqkJjgAHEe54ekMpnTV0RYv4TSudcbbcESIAYDxE6cm8bFe3Cjip8c4eTa+Y0clvNBneIciy/sVrCrGgvgq7fB0SoUWWAlgOH1WatjFCzC4xjxQnVgBjmXtDNZk+hGkeYUTWAPvv7JbOmYqvZJYaenR7HHKLb2fPQbh0HDfeHGE2ToObagpxpfVWzuxjd0A3z5FmXcy73GDRLb8H8eSscBhLMOjW8GNUN7hEjk6pBm/TL0PfZ949k6cE3XmQJ1E7LFg43PsnZu6Lnou0ziFffTV0JOpLe0wGIECJr0KlDtwUvrKjC/w49ex2Q0gTOPvpO0LRiBimqBi12DUi9MBWF/y1GZs58tKZjscsnhTFqE4YQtUlJzoqpGPwfoBfR5oUt7rrLCSN1VOMIH8zOBHcWlKgt53+CwscBUtwua4w8pa5K2GCSXXauNY4p/F9hmLVYEyW363SbrTtMNY5QoWrQsVttzs4E/yH6880oPYfbA/bi8RU5jlDBINF7QVGYjX+JkhuPUtAyzHR3kqLZ/VYoP2ilotnm+Uw+oxv836JYWE30EjeHCsXifgrQ4UHToMOwfrnov5ZzyUS3BIy0W+7GQqdjr5fgnpatKCQJJvYT5rkMWMnWKL/pgaPd3ClMP3g3hIrQIUGWYNnC4ufCMwn6S+P7pYv9/WLBDI32hwNVg67TB1lfzDMJuoQM61+6GIZOx97fppQwFMh2baOWf3UbJOgSDC65ygmb03HCUKCqMO1M3vxGziToz1x1McmR7lV/JwFE4DC7kzKYRw1ePgxrVGtAhwo1s93khLEPS8lxBI4kw/xDWdttlBWUgrYt9OuYm5TwPBWhA0eFRLd55o/JINWhAZibnRxiEpqK0IHDIDEhb7uNMqNYCwEhiW1uTrq1XUen3AJGY9D/kKGXW89oZzk+3yikjXZI0KDrsFGmwsuPnkc4bafCkXcuinYPZu8mxxEkErNbg1kfwnOpyIFyHvrKa+eCjty7QHoOEk2CiSnRGvQBw1yJryWswm43s9APHvcHDYigYJAY0Kgr3BcUYwKgQ4YxLmahI2kK0AEiMZg4h1sG9wc93w1f/i/BwfeOHYdYL4MvS6gYRPE5aSrcLzLD+yCccehyub6XcwdN62WCQ1MT44q2e/YLw9yB7ygUg6WzY86bKi9HUoAOCtHrFr1B39CLvRF+21U33vZKOx+Epq53UAg9ZxTuI5kCxm2N7e7U93IeoOm2d1AwGDvHXz0r1gB8BQAVJq+uc/FShQahg0GVqq8XotxX9EaEbUIGI53XOJq9PIHvE1YEKlRvbIga3E8Ms19PwEeV7TicB2ia4ggEZq+sa/RZz1zJ9cAXvkqOgxx0yJGhZo5odvuL+QZhSshgpu04qMQRahgsEumgryBNCQFa2BtmKECHGUmKb8xHue9YjRPxDe2oUPO4zvEc9B1y0P4jSdUTirZ99huFL0QYoCW48jRCT2NDTFB65tECwpqdnRKuu0cPVUJMSc96AHo2zBkIv24G65/EHDrotnRTxRGVEZ+5WRiL0HFAF8evY3vVX6YSh98IPWcD0bPC1yDUM4OZjlfa1a2jfaM+I/Q8PRg9GzgHoaGL45pd5PU+hP+xuCnpOQj/LKY4MJ5zYzDyZSxNATqkfNIzDwLFWliNMHxJcLM+4rCn8vQANIHyIoHEGLoHbE7w7p91HghmHmFPBVTo86y506a3H1c2qzqBgDENYXSwqZT4zBUDZdObwYHXMYcXKF5OLfd/rAo1O8aNX9WtQwJKSP97rA7KP4uMcAHGsVGxAMxhShhJ3yz7f6wMUwZ1zhnFeTtWbZrYpmXLUrRmsvyfGncpHpjf4GZhEULDIXbovnKYEjZfN9WPjHBC8Ziim2YumxxVO3zArp6JKhAwWUL4+/d3NIFuDccMHggKn4DRcIjbsQ4DdLMna335hHGxH9bgiq5blslT8xbM3dT7Yk1c/SxrFeFf2fv8cyoTjJ6F4UDY83azXiZSt83rJ/Q+9msoUdMO1vlCauX+zQNbt6wCgcTwFUc9zvObBg8GswGl4QDoctzh06u278aADH4gw/xvBtkNw1D0aDSTfZ5Mjpq7amR/1vGTsa704h6DGvFnCAbFmo7RcIgVuqvbOrsQ9HpvNTigXF+loQgHkjGeDxp1dMD4mZ+rIFLlpotakHrm0UMoe94AXW697pV2dsLNxx6hDDULfraYXgTr9raxzg4aNXdTjx6be8ZBoKH8bfxjeyAwPYsZjvkoDYcKY961DeNVb03t+qt5SaFq21lbuYaG/PA1p+10sWRBWGX5alWqXmMFFp/14nWUhgMkWOIwQNdt8BqgvS+g+N0EcKkKomd4Q6Ewb86pZZ2qQFBBrrr6elHnAaFYazDOcHxaXuCs6x17K0p2fiJJv1d0iVK6aBvr5Ly5q8afHJgAAcP4W/lDQ+W62CcTDEYGq4GWYJ+zIxSRey7G+v1+pSEsiC1quxGzctPpDi0TFdBalGFF4Vhgejbn4DTQIu+YXefwQFAAU6MiRjs9wSdErVhmrqG2dsH+/p+aMHhbMKWEMDg9pxYhHIIWSLDY2VvvSP3iAHIEoejrLo5KfintfWTvWmPaKsPwe3oOnJaetrYzINg2ypgLiBXJzEa4TB0KKjq8O+cYU4cSL0M0bniZd52bOnSJMWpMdN6yqNF4jbpEY+IlOXw9bdd2kYKwzOm4KBqd+s/zHW5FgXNK23O+78wnRpeYkDAe3j7v877v88X7Orsr391YyFMrqhl3k2iYwfF8ohJygEbYYWVLUOPWqBe0wWBGT4xhpLg8Wrzqo2ILna2iDSqNawiF+OpCWj/ZFu8Ns5q2RvcZNAXF5tVCmiPcK0rSSGlD522vez15lA3LZcHRJ4kGgUNr3EAnLHDnvoIQ4XeEvKyjFxi3iQQhIqHhhgcevU0ewFBUqQ0VHNFWH50ONPCw8g5t2QWuuwxMN+eZNAKRERbVktR7qH7Nqq1eSkw9BjYjw/gsBah9fVJzR1jQlfJSEgGqYwrKIqokDiYqOsu2l+flKfKDYE/PDs0Jh2gMkKOWxiPClF6qD53yxxZjmwReHjKklxo0KarjfRvaruquAgbzhtCDRTtUBQwS0Lg+N1Nq2OECvUUeetMRBm2DykT6YwbF/ohKvYn2C2WnugQAGAIrtSygDVvhcFBrQKeQ2J/b8ozhj+LyKu6ddmBTzxGRZ4ob9mwq9PoBGMImijYoM8yxcwzRy2cAN95K0rJk9wERLo76Xod2cLhTRKUV9U1lG4EhavmUxzdXoiHABjS9fLbAudr2+sN3EfF+G89kkNEYiv0RGapfvfVWbOkxNjLG5Ix7m1GCQ0DV1A5UlHe9w6ym7NxbDBccSYwWMwlM6ijq68PLp4QoagbuNiq0QOhtonNjVPtlrCI4dpLyXeJNpSyoS87hiKLS+qZVVRa/4aTmYVn9iUg0AkJvtZvelAgluoAiwTGViZWdgQMnSJF4aaBpSbHifRjXJjKg+Tv8n8//ulP5Q0uBjrVcTpDNzoCnM0tHdsqWXmS4Yf2asmWFvEGkxhZ0rSEWNKKcz/hFlZiWM8IxsrKgs3oGrZzfSuJQoPWjqsI8ANA/KpKBakN2OJAwSDWfgXHv/JUNaUiyeyefrO/SnuXgFYSw+hgJXCRvnhbyegdF8lBsTIGWSleVEFW5UrfsWgq0BCW1PE6MgNYxGgtxkiTFA63dzRY/6EhqrKCNsOyQg+J594Tg6NDSETr3vUbe4UIOVOoQXojHidHe0tr2171FfrDrJai9DQbkfiFHopm8n3NKcN/0K6tlh+NMw3c45oqX1ePHjjguEjlUsX7zVo8+STYMrDZAQXOR0lV+iuUGgA3OHSgIaRAcN2wkU1fp90ADUkbkfe2VGwv9Wd+jxh60/kNvFKVcb2h88QoL6PMIcuz+/USDbpVM2WVKBK6Sh4nZbRJxgdbfg+ai9TdSrjdy4Kkxp7pjR6SAHgej974DXjoVaztvay70Z+/ghYdi/Qs0h3YVU16feXi8JcaqL/UfPpPcsGsbVNXr2z0pdl7vSKBabhKzxWn9CzQS4muKGCI/hrWDyf/wMKtl5L2DNMcuCXa4sVbnI6WpJvHR7qosONS4QOt9p4Ic8Wo3wT9lrUtJGp7YDDmPPE1mQzhlRz98SBB1hzJ2Gayt667y+wEyunBqg1VxXQs0tuvW5NM8Hpw89HZpGXnfTMqO3ZzmXbUxWzxIFBxSvGHX5tdrFB5mrtK06ztU4aLraT5PmRqpvB1Wf37ilHAHOTt2cyG/ybBTfzx1EYcr7qmyeQCYDAnqmtKIjm0BEsT1xWQsuqd5F6tlC9r1JvnPeSumABINwnjaWGlp3RKvPzNDF33XktDzg3uuoJ/PWHDENMRwjK6l4Hu1Q/OwJBoIxB0fkQbrq8uqgEn/s3uRnlNv9Hzf3dS3gxi+99UdjhB7mJgjFRVDfVWvIBoLxEloqG/bOj8PjC29RejBiKgXOGlFZZEJ+GyBtaMFIQ2PEZK4wkFYAtwUcICNGLiqrEY5tE3D4+jV61tBDrRrIwUfwargYUeXhqQk18DVZI68CVl+mAWc7Hv0tb+COQ0LO0lkQLdwGe7ExN1FZuAzgO9r9Qc2Q84BerLNxkPgSADipEh8uP31mhJcpwmeqiCBq/+k0ARyQ4Ybj1TUHegPqOGzcvRvuIyeojSHbxLruouBAcaSclqSPooDCb3tVwChoX4pX6l05WpILfiQsKMrCmT0JJRl07i871FemKqVl9epi8fBybdWZugGNS+NssGOxUSPvIl6W2cWICRI4lA95nRK6eqeNh0IjRxi241gfJhOhuSmhrNYNniEpNQCrbGkhIiOSXCCPEas37apkNfOaU9d9gnNOfpWF1E/7J5eTxuLqQvoI+dQ9w0z7mpyRMcEEM5XH6m4basPQFuB8G7INqGREJFX+WnfFU167arLpb4D/etLlDjQSeChigzv7l/guAhKdHZv9Wly8hZVZJnQyDHcvg5sNMnJebH49DCrvtP/9g6qBPRkt1tJiHc3E0qdHkzsanZj6aE2Ifgtq7+USJAqlpRQpibn3bXU8v6x6w7yV5LmOAPvJU10TBHJIQ23dePMGrvdOEJzjviujeS/lZRCDbulpSCknpt7Lq3zI18nSU7HTCBB6g20Kkt5Nt4QyYEcaEWZj7rmKM2k0ZDz8E20pl3LHe9QRCQWiHNIgxv2KFfjNt0rNOKiiVfXAU/jZ+88QXbOkKqAfp++hjAp4zBCqOiYZJUkyodbONfUZteT0IiTRmTvmaH0o3dW2OVoc6s6nzvoFNAT8K6QiBUd4+A4aTDQWunD3rlukoNz9FYs8YGd4p/sLAJ6pSw4TM5nOzQfIp3RWE1Lh+q6NxXOsjvtzQahkQNVrPKaxnseB4/5rPrYlatrB60N4dR2T5ywgeEcnO49VPfweIeY5UkhEqTEnnXAmKcZnAwaVedzQcvn1HuU5XXkOh1JQFxEGsH7SzNniEVtGb6QRbLCqas0zaQ7KVZm50FVB9o5+jStBseM1WhJpAJ4d3q4DpvT071aXltmfx05h1i3xGMu8TxxdNViVTXs/iI9hEMLGNhMttORBGWGGNjVnHRce3cGZ0OYzvWri8ymNsYfcGhxqTaE4bfJD+HQAh8dokOBMkOsVZQHY8nwFiznQIE9y8xl1U3Zm6cHWbNOvGdZJB2moC9M5nRkqE45rrXxmTtZUei8DmiP+JoNPD4iVOVzQctuug2OafhaKSrRGFh51LZvLwewMxkJTkIyneVQBWBMJ54xmEL1I8JQwegW6hvCqRK9tpbItbt5GRgRE60fLctItB0SoiOBbZtMSmdg4PMuVQHtHL3ZDaYBgbv+Gigt4Pil7XBO7fFiOuAc0lAb/tWw0+7Azr2SpLrT7/zjSeoDVWfEDwUose7+XVnFvge+fERa+FeQvwQaH6yblM5KsPmvKhOVkLn4rFh3iLoSrYBzRH9f8MIgHtYk1r+K88gsphQbGDn5O8ecKnxmwy/RdeOtTujiAGV94RTQgtnMSfGGzspyP5huLJgEBj5Q43MPS/azEws8xxqkyrpLF8rLLxeV1RSB+aaCM1B07GhMnc8rqd/g+A98bbSW6JShxEQO1clvc/GmLs4ycuDYgVzVgcqRy81iQE/DdrSUaITTPxJ92zb5Tc9mrDe8TwRZVQP6TlLfIUyzRBO/GJ0ukOCISJGh+s2VxcCY6PZ1LjBM/rUhVT6PfWCCjaTZSvQS05ZohDiOO16ISIMNFUramIlNupl8/vAgqxoyujMfzInyTrOVaJnHgvyPIEUjvSOHaks7u4tt+EaAMa9JNwNu2eBQDxk1lQE98wS8j8bpytyQouLI8MjIcGLDA00Xlq3dji0NMOFu6NwbHEfUDOhTUuCzRUEOA5CTY8EgvZFkYDWl05U58Mh9TQ9jbC/P84MCG2/OYjRHqMwduT1qAxXV0NwJEk/Dn5cHU8DcJvbvlJiHKjKFPz/zwQQYm434gpJZ2PFLsSoTb9bVNa8BbcmZJLHnuOO8u+/E+Pbe8/e+eL/yx93e4xRuE+sWySWavh2leXBi7epFAEzO0cXkKT7fFVbn8+Vgn5fKJTWX7V5757fvnN7R8cS+MYz9PeHwj/1jGPue6Nh7wdlriy0eILXL9jaYqi+U0Ar8YD5zdLSAyWCUlFFVPs86UGHsmM0eT8ni3Wc/2DgwMLpvfygcCgWtTgwry7LWiT8Gw+H9/aONjXtf83r8JFKagTXUnBdqAocfF2827WLofIbdvWoGdM9sfOZzAMP36cvHnPrQ8oH+YCy3wGq1snN8LZbNtVpdsfBPy0/95j1giJMePBTXmsvowBeIQ3vWHWWUlvm8s98ZSnXgreiMvEU5V7/81hn7gzIKrD2awFqDMfaMr97z86RRmoEmc5VohdOiPOg+qijNuLeMWlUvVLYk62fegv+Vv+zlZ08+6Y39wVwr5mlPCmBdoZ8eun0xYXFTNnjFhONCznF0UZop3DJaoMI/6x87py4Ix9VviffjC/b+0B8OygV3diqr1+n+B5d6CdsLKb9I16U7fcJ8MaUT2zYRKPKyATt8jvmsOvBmJkqz/N+Swotf29t4YH/YpcJlNUq7+h+6GnKIKhyfxHXUHNIv9+nC6ckqTZ7IyzwssLJLA5/dDDPRA/r9m745bfmBcMyay/aki9xY49JFBC1X6ztcifz8xZ+6NaG4Srfe6jP76NsCl6uF5uIFDjfDQw4DfH7xsTfdcEIo6LL2ZAastf+FdQSdc+l5XSj8/OVzepoqnBBNdJZ5TU1pzGeVgYqS0Y+VLgNVLz/4w4GDQewt92QM1tDANeQIaTsU9+lUornIo5+N6OsSIiEa32BmSuMQaJWByuSjbSU1H78zIKtmJyZzRsHm7nuNHEYzoNMj4EJvk69V98Ek4qT4CtNSmocdHZr47F98+1uNB3pc06qZnUbaDGddb5BTo3m4VZcnkznUBEuMyNRDghSvK/OB3XyU5uHiMw+rDLxjMp/dHz925RvBmJUNsYrqtVpzXcFkuHKtzrREdZAgRusTRsqJ7YalniqUriwicfkg3QHhdaqh5rG/P/t6+U+hIGaz1SqTNxY6eLD/u0tvSMal331/8KD8/xbs4uEaTYrXYYN7st8WomhFDWw2arcPa+m+plcWmYvSMp9vVs9klOl7OJybay2Qmfz9D8vfOvWYx85eevtZl+R7ppG/8azbl8rbHG8tH+iJLYzUrOuMqwlhtB2qsh/p70g0Q/OQJBoG5BCG2001PcQXhGNWdaaxTmcw9P3AQOOl9y59ZnGJx+OH2eHxuHcce/2ljQdwOU+Z1Naea/MJMe90aAuFxCfgNThmHQliwkQD8X/Yu9KYOMow/M7Bzg57ILSysrIedPFAqZXU2FptPfBuLJ5EsXi12EpVvFqg1UbRWA801mg0GhNj9IdXSAyNGjWmRhOT7ewBa4ll60KsCi1JManpT+cbrmG7s9/M7gy8s8vzs3+6LQ/vPt/7Pu/zCkzpw8e9VDrHZMdn/6GeX5uuv3RRBShgisjaCaghsPKfAUGQrXpyZ1LhtFFGD2xA0o623tDBR7b54SoykpxX8HllW5L57IvSdMCf3V07f11cWTHJZIFS2pS5OFTIDb5Nw4YpXXL5DUhEh+r4nyWQIveeDw2DCHYJlIF4PphLBWj8hlqfnbHfXwoqCoMp0r2Qxghko7BiySfvDBtV0+HvkdxYFuEqS28XxhMbATbj8KnmhxOPgSXfHae8BznH3xc+BUxWK2mE00suuXG4xBCjncmPsFhnzrfyYLJ7aAdAwxEsV/OJx8Pmz8MJ/RzN1NuIco7kQ/4cLsiIDAQWb4oaKtLhi5CUaLDypj0v3VkBgccRRUPybkl+Htr3+pXMZzmBI3Ozjgt3X+/PTdKyDAQ3JDkDAxdnEskZIhbqLJsWSpE3lwNU47rHTJ6Htr2GLPP5x2Yf7TnY3KoIB8iR0l+9P6Cf0Vy0B1BAdf3PbEjx+o0ge1QRFWgCIjyurQkAlme5SXyOTskQhc+mDCnKd/6h3zYdvrgSUECEGouaau8N1kER7DiA4kWYQumh1dWVNhweUuqzmXwmUrr8od8degnt7V+MQ3MwoFz/Mx/u/esqRKhCmdBELB4trXa77cZU0Pgc5Xz//Qgu06jhP/26sE5COwaexeJRKrZkjsf3rS0l6R9IA5pkSl+2a72tXocsNNH4XOvr2G3mSc0iuDtZolNEx7rKAAVEeNoKVRCpXwksNPyFNvxD8oSWfmyf1yEjQmtzCaU+x7qb/GAmiqAz6dMpopNVOK4RCbDysPmaw5NoBZHBMlNJD94jLauxyeahAOW7aXyuHWkz/YKKnMuks9fhHL4Eh4gGKLvZ7NkKEdAkh6RhCMtMRYvSR1rqypAlpqSFEHy42cdR+PzlKxZQqvyXKKfrVfjnG1hEtPkxd3zfqgoGmFLUBZpA4iNyw6MM+703AW75ieLfiHot4TP5qzeV2IvQItSYvB8lRZYpq3w1NshmktyRoZZW3LqDYUpfo/J53AI+E7jg7v6wrtHKTsABkttoruboO9JABPTKpRhbdukonfh4PeJLWQzFvzGZvxG0KEuYgQ0xTgehw5uQtDkAysxdyeZDa4EBKL4LgWtUFyT5dXiVH6vukPncS6/Pe8qt6zFcel1YF6FPASww8+IKWSKsVOr+UrQtuxPAe/ZvfhTnoIUE5h6l9J9JnoxlfCYlOsrZidAsPGOmQcmdqAOWzFTsdJdIcoeGdjUi7HcI9P0U5T1oHZ9JbbqtxE6ENtUUTZauSpX/BJtdjuPdofqPG4HFRWkXE9xw1El/D1o602DgrQGnnTS0mZpDWboCxENvbfAe6U1kBg8WXrnCQdXPexspfDajRNupQptoipbi92wEQVlWtMuTcBalB3dhmoazsKYtRtm38pGDxwJYi7KTwvYi9GMJT8gUeBKPAavsKtpJQU9DcvctxWP/V/JFOTqfLb/9xcKV/zhsRGjzMsH4A+sqGLBrgSbgSQsPh1daB585hc8MWI7AFzHOThrapKwuKdISAAI7n17mPXJoaXDejTYC+B/5nMZn53+9c8LnIvjgb6eNKjQLz4yaIKKl+NIqcE1UfGyLVwYg8Z6hzfOsO0j7ub3Docoz17qnOSefsgiePOi0VYWuvNYECylJLWAUd0jdoCdkY0ju0LXVAWAYmC8w5F6bj6PzmYW5waVnhGlejg2ACMUmhA2QJRUGCAL32lRBT0Mu0i11fuvoQq/PveS+FRo+Q1lXjLOJ245AhCdyFr1SZOlKECYK9Ch+mx0Fkjs+uG6+/B0CU94zjorPUPy9zQh9U86E5g9cNSno/I/bvUATSJ4DJCt9HpS0AOU7D3pp61ZzymcRnqWIaOfBT+b9IT0r4y7XxUIpsqJyaqcL7yahEZA5y6eNc+8rFaGxZ4A27o51rJlL/rDQedSbeaewH00CqTkxpH2HG8A1uXU77/G5JkHxlQZN1x309nOU0lLwxrrmlM/AwjVjXtt07RSsyq0TzUurgAGw9VAlDSZ9pYKplKaPU7wUPh+l2Dcs+FQf9TszxxjcWAyIIIvonDSHFF9dOa1eEIYlZQsyDP9L1h3MHNGHkccpW2McAvtGKqFvTzopb0IsgboKcl0A9IwSwaGgeEX+FGgC3qP4SkUGrAcZp2x11NLG3b0W2DfojejfMkeBdSJ6E04uFubwQ5e2ATN1iGgUd3aBUcz4SsFqiGQ7hZJWwDnGekvnfOTDwMmZCe3sR5KnO42Tr82eh1Lf5lJm2hZiOyM0HbwnscNv+eTQBaU9tG0rLra11ZrvdjqhbZE+Oo1FOQy/ieAQJ//hN9hsU0UfJI/U0mqxkmZh/ftRb5TC5+1ZtJ+tJzQX+x5ZkwOKs56GkKT+ScFhp11vYyBN6TstLdJFcH8yykUp7eft2bTrrNfQzoNXo3oTAog5bK3E71kODORfz242pFOlFguVtBLGSGs/j1jIZ3qXww4nKdRR/v9mmQvjGawDceqnUm1/G4cm+EhinTVFmrg3tozR8vK9x/Zk1362vg/9GzoJDeBfnVVtJR2OUmbmNK19nf10SJ4QKdLmlyIWaO0N0q4be8269jN9UtjvzSShr8AmoQECb2ZH6Hj9chBAgQtzILQp4OMJCzx4AjTuHXdGaXxut7D9TPdyZBp9O/84G5mEJtiWVVaoe6gGxJnw9HyaEqYB8f6vfsxcozQD/ut/Hqe2N5o7/fN0UZtuTgrfhiTtXAUWqrOZFfKkBa2SLfn6JJwBH1eyHU3jFsuU7+4YofC5ltKusxwkDswu2yo5hHOoTHYTU8LD+TYlPBHEKL3CvG0WAchzsDZKaW+0rZlfPpddFOMyKI4n8SkOmdAJw4QmBRoYKBzFMQH+TLLNwpjD5/U9spmfwufjH5Lw5/nEKV1hTts6ettGdIojy7SZyLQpiaDYzsveRiB5pBXVfhDN6D4filGeg/t8xx/OIfzZ+iVZ58A5iLavZnD+fQbpOBUFPQVy8rAwCE0Gh6N3lgKbc/e5vZl6JN7X3F46zwVQzBhjED7rUmAAH4rvNUpoKVJfpVYc1UcKQnEokNwHduX4NhRA5rOPo7iRCJ+td9fRg2Ycmopj3wMo+ZyFDcO9v3rW1+5a7EeCTAUfIm9DVw4ir3EvMddRlq2af5x3Pst4cB+nrTg+Q6k4jD/ppPjjAXW92YjuUL2lIH6ltZSNQ2q0KI3P4213YGggBDZpNjk4fM7RCbDwgsFrWJFB9aIva/e8JMMg+1nLGoARso6uo3Sf9/n+ez2I4OtcgFv/CWfIL8DwK5c24M4YoXlpHTCgwo589nGkB+/ev6sRXIxxitB3rcgp+vZyHP2wt7THKr8ll+P4jKlQmhQh/VCuEaoRaCksxaFA6gvVf1sBglF59+reYz6Odjpl++4KBPJZRtkFYe0eBzpr/8xSghFCq00cBCy8nCgwxTEB3nN4lTFTKSNORBXQT6fg+C4XYPltmmMVR/J2ZE5oFaGNtJGlyOrKlDfl/vy1QmeC5Ha3VAEjGJAbvc0OL22aMr4Hg3wmYOGS3x2aNo5fygEnDBJaXaAVFL9YAD6O9FB2aAWXXnoE9xC5QTGLDu9BIp8JM3b+rVWgS5I3YPmYJ2KR/vUpMiMsTZn235z/TjstSB7pnlZg9ZCDhTu6R2hyg/Mlt5TjkM8ALrjh8rD2hW8kXyPpUPyc/lddPFEFQkqwR357+zODj9e3gujS063rDnNRinyOdn/iR0OUInhWMzvSOfw1Dp2fK6H5PpIso4Iod7ELU0JPgncv3VEKLqrc2NLs4KK06LqeVzHR5JQvNAt0+IvzAS8MEDq1QJPI9MLrQs+CRHIdM5o7BAHWfDnupZo3xl4vR9U40E4wcB5EaIQ27k5KLdAKFuVZpF0WkI8cNmTIOWBhSVN3jCY3ah1bZTMSoneWCJ8MOzWn3ksALwwU2cjgSnCltkgKtskxDd5NWtKi1mtwzYfHS6jyObYdhXlDrTg2hTmtAo1xN9bwXQpVVJL6TZjn+956QOaGKxrSyg4B/O+2jfg4SnvDd+zLNajkBulxDHNaBfrQLXh7dkqkrk4/c7x+DQipuQ1/FfSbcAp85NyaNJ5SFoLtHSO1lPLMecd+aMTFZyiCcwYcNlolTCHlqSEq1K4kdXUvjHVCKiT30NpU2SHIcuOKYz4an2tj3Z1BZCUvw9ibCz+PWUEbIHQ8QRR0CjYXlLk/I6P7yJBFUJMi2NmtBBXoSGLExedMy1eO309Haew3SGhVurkaiwpnnZAGIjuq/TOVtki21tGuwipHHa54BZncICh7R6tA73uwFFBDH6Gne9ALTQ5NSJ7BjxuBnZYb22NOjjrsHu9pRCY3Jh77Sa2pynk4F1UMEjptD5pEzAwtEFrNaOnNjcBOvAY7m2NUucE5mnvRmJFUYGCLxtjbGX0Wt+DQJjS9QJMGSb7cJjQJfIRs0JIYxtfHyGyQJjfarvcj5LMAVYfCdpyp6Ce0ukCrsW3hTaiGIjtkb0dx0+fj1ObzPt+xvQjlxkSBjnIariSkm4RGCZ06JMz3SxTZQ3rv37dh91bFyU9dHcRl3pjBEq0EsPA76Au0LkKrs5LUCCwQOhV8/N9X4bvjpPlMuzPxIyrzhlpJLu73avj6cV32zp7QkcEGcKU7OVSAC7KZwHtCq+tOIw0OLpqxPJegWR1Mg8AXMS69iWMLGrt2boRWTnqnGyfVx0MLmAHPJ7b5BYaBqq4Rb8buRkc7Hit/Corg9D+c6S9QvBMA/NBDaPdoNYhpk0sXnBwzkNzS5vXKLRYBlnw47s2wmtJxB+CUGwSB59MXaMfllyDV/EYJTVa9NcLSFwg9Dd49uqp8Upi5mCA5B6R1tXvvq8Airc8ZCnR0A/IZoW5Cu4eqQVwgdEZI7tCyBhDF6c6XcrAtmvZq9+sYhynTqLwwfYsjnMR3USU7QkvxFQFYqNAZwXuG1s4K1GBEaO1QZoWpXqStTRiHKSrfaPoC7RzuxD4j1Eto/kANiAuE1gaZeS+rCab0gVg5KGk85ewE5zvWdgdmIeqCu9OHF3DhLvwtaJ0G/0hC67tmXYFvyE6CHAQPnhgL5hKCe8Z8tbO9Gw8HMfMZoPyB9JHQjuRitG1GgytYfN9ajQ5TccvCXEXZ+15NzmNphI2Oe1XDlM+bsFn5T5yppBccA1uwZn8ZXpJ1H34BRFChoO4T/s/elQY3UYbh90s23Ww3IdNWS690bKEenVorU7UIPRyrRWQQL7CeoAUP8AId0eHwoniPYr0PvK8ZBfWHOt464zETv2yahjKmhRZpqS0Wp8Xjp/ttKjTtt/l2c9BvC8/4R6VhA0/ePO/1vCxgl7ta9xQ4kuDhV/8iK7Ka3Pj3g1p+qxsxm972wDVW4TPTxgAHiX+uDqEP+/F+QWl/cm6MO+BI2yqcYg8E7K13ruZcboADbqNmhPbWugs5f3TjRjNCeJWeeMo83CM0dvmqSrwxTXVFpNrM/JE2RT2i+TTP1Y2R1u8l1ADt3Mn52pUZQrs61oFN5ydXHt6EVpPBWdNZLT+EVKeZW/7+W7XJ5cWHUQ9Iz7Lfb4met0FCBw+eCDoy3z8amhsHRW3QgvTDN92Uw/13tgzFXR4qn3uW8jp4Yt5Od8QQ+khjZQyw4B5oNGjhryWCEvdzlzDtWT99huMtCwkOluG5myiOI4QeC8HdTsKzEZKKNrjgk40beBfQ4IAnqG52zj03ZFsoPjMIjZVlXoAjhI4GdgcrSgsMhmfIO+f8QXfvFl7XUw5g2mnUAO15hdt7V3EQ2tU/A6QjhI6G4O584Apj4RlJ8MhXzUUCfmFwC58LhAeJ0ETNCD0XXcj/lophbw0cWuaNtRzceRhaNQpuvEztDEpGD22+O1iEieYebDmVY9khweVtfuqWylNWSgg1b42BGIRWLk0HfaSff9itYAluXzmxk5GNsWT6jc1uAfsI8vdv3sTpFmFkiMNvp7UIt2ZYis9EN8Q4KiHgtQdD0ZElWeJg1/FeZCuFDVGChd/uI+E5AlzUvCmD0xiNMh6kWst4emo5fWJ9QpfEGLYLlS8d9X4OexsDQelQ708gWQRj2eDZd+/Px74DwEXDn/EppNWeN/UmoafrcksJaJZZjOAj9jJHCB2B4A4T8WyMzqIMj1w/7I7+48FFg89z5myuAcHcLg+15f2StQQ0SzYEByL2MkdahdoKbPl5Kp1lg6lJ5kdLgvl47IuoQprHAX91yI7uNGqlCjR72A4HG7IAxfRA6z9MzBqxEOp+IEo8s+5snr/XTfuwC/uHFvDGaAfdDdruP7nAcgFahKXlut4axC8pGoerna7aGCxfk2tU/toQLBwKuahxgqSGnMVoGZZS7fr9dbdbT0DH8tbAwYEFjHfkrTkc6naC29dQWpaLJDConvNubCbFDTpw6O5z+HKZ8VJnODzvXG09Psfs9ml2HAw0Tn77UZXOnaRUZ5CCotrqbhmMlSxjd/PqDH5GSVH2DXsogiOtzUojSYYILfgamT+9brJfwcIqnY2LZ0ASLPqqmahnfZDy3Y3cMNoBp9el0UaS1lswIQSQYK3uHStXfymzwTt7cotoQd0Y1CrPssHwjGDh86TVHRuE0Zy0WESo7PFYfIlwTJFD99vR3bmOSeisJZO4Eq1G54EadU/bKJ0RgumPNbu1Yh2b0dMRDwoVZW8N2Gkdwkq+Mlcm2FYE2uQoE7MmrYjGrlDnrIfKCJ2NFzfUSSQfG5EWCw/FDrViR/Mt8PRZsMARQabu9Xmhna04bLBhYHKKaCwo7dq9FAmMQZSjwjMT+SEOWiwy3P6On2b7ZcEO4QjKdBew3J23GvgDL6iYlJqDbKSUFoBsM65F8y4jg6JmfovNE90GR1B2P6UC7Qw0WTIhjJQp9IocWFlSZkSybJyEI9GC26dtpEiGmSGqxY1htyn1hTVGSzCBQNnXBOyW31GJxjyfzt8CDi1ON1IlKemfbJpDGKnUSQDG1XMtrbjBjtELJ5LRMqynzYy2vrLUunzOqtDbvyKH3IzAO8ncZrQ+CoXOjNbgkNt0ckxi9N3LJ47RDrh6JzUhnDvh2j5+HN2gV0d2DcwAm6EQPZkm7gQ3rn/AROE5cqT+7PMH4xsBwEr1RI0qkYSwzkPpEPa9ZNUChzYuV60ngZWOYhANvUT5ZLkchNXo3PHAInN0RpEt2Hh/y/zqBRMTo4kTtIc2Ar0i16IJYWRdZUA3Jzw/E4wATZZzslgowkPvmYvOIEHe6s2DZI4/fkavzp2ANjiCade12ml8tmqBY8QbOszMCdlbtr2ToNCBXUp7/ZuPsPeroi3Nofb7fYzwzABpGmYfekaj7PUBO+X6eFM2L0MmcXpD6xFawMR01AjQJOgWCi6lu+qTSnPRWZTBu+HdwfwEc2JctO+xQz6qhLJXUBJC++6tluZzrLaIEJ4fTehJbM8huILdVaVeoKwLsgaR9imJf5Zx/iFnNILVf0ylrKicO82yHcKRtopu49rVezbYwGiI9lk4RGOX0H/92wUANlN0luCRLcOh/8OztRitJoSUpVi7p8daJklmJLRP6SVFDqMq2rohGrtCKp2zACST9aGMG++OtFKsx2gbfWTU32OB0/Tx3gvCwYosE19ga9qt2S5U6dxdUzIdZJN0RrBoy14tGbQgoxHK/iFgp5jYzY1eUUERWEhUx7gXhJUWLxiHd7EFdwuxy610L5vhNU1nGaavjljIWJHRKp+bKAlhWt/p//PZZhsvNh02K3QP02v0h6E3ppvyfq+3muhQ6dxevrjES2lys8eeWyK1OisyGqkFOwqfnTufykDEzBpFmJGZk3P0/8jKycnUSM35hIcIxR10GlJOBbEYvbzf7bMOsKAG5yWPz8wVzbbpZJs2V0cNz1ZgNInPFBMOz44bMpAWoAtzZr9+2V1fnnLLo8f8j9NuOeXLe+66uABA5lxjr9G9UCi0zwfJlHppDFum0oEFxVdd83glILN0FiP+uPm+5IPUo1O/OiuiDBqfnX+s0E7QFTz9+opX5nT98Wdra6t/28j/3Ob3t7Zub+t6/7VaQDLPUVrXNMk8oQHlzrNI7U4Nzu3ljepuFUiiWTojtVbX7E4JnyMxOtV9DZRxw87xO94n/v7qqZBX/Nq5c3q6Av5tU6dOsdtHZ43qvzmdU/07ul78GBC3WlqEynJdQrt6N5ibAkNQUGWBOVJVOYdV5VxQaJ7OgJCqNv7xFaXsXaZedaDsl3aN53Ng21/ffvfMdXN2+P1TnYTIOnB6Art+WMTtbKmuaRJBUfU5Jh9chgXlnDMaC9rAxhWFIjXOsEt1n909yBx75pnRMnxzZRqNqoHff/f7PU5ywzkm7M7AaafzZftkyGOGTmg2ozu5ZrQQxB0rP6EoZ+NjdUUpVVWpZrQDlvd46EQ9URUVU5zOKWl2O4PSaXUvcevYsUY/j3MPmSY0SLBgwMUpo7HgCu6tWLWOOrDBhizB8o3DBunMK6MdMJfG5zSn06/mgIEdXV1tbdsDrX57WprTHoPRfSsygEt4l+lJ6PgIDQ4obX+BR0Zjl4LDVQuyRR3lzO6k5GxoUAx/WPlktAMuvITCZ8+eHbuOve+UU9af9frrr9/18/pT5ryzfc8ef5o+paf2cel6Z4OHOvVKx3ESGlDuHf1u3hiNyWxozao3CgAk+lti1zZuvn6fqcozh4zW+GwfP2A35+XbPp+dk5lZCBoKvdmXv/byy+/3qZJaL0b7TysA/iBBqe4Ahh6h2UgvaVB4qt5hMrjfoc2Gxjl4g0iprjdoks/cMVrVGzQ+ey6ZCwgIkMOmwoFAQ9mnX1+03WPXYXRgPY95YfrGGD6Lbr2kkK2j67kZ9yexWQ3Oa9eR4CxBPBC1Ut1gvvm3xBejST5I5fOF4HCM+YuWHQ4HIKj8qSugo6X9XXM5NDrIOV+3JhF/hAYHXHF9mAfZgYWgEhxYsrG0ACDuNVQEeZueJ7WNQ4gU9AxJfKbw2dl631w9NeyQEVyxdRc9SNt3rwDewFgFjJvQIEPuedWhlFOAHZvDJ1w16+2yPKKc45dltS37yOHMQwycn1xG26h6wxk442GwxUiFEVzd02qnq+gy4AwSrNX302AQmrnLsXzZRAZpMhba3lnz+M2FJNGREukT3/hPIlOivDBaosbnKdtZXT/ZAbX3UWO0s+9T7hqGeoMcBCxCs7+oz+tMUUuNzeZ8pbu6RhukS3DuAMFje1PV52arjuHVGUhMWn/QSVnw3poNIvNHy+4LUC8m/wS8YXZDMFWEBhnSZ9TgQ05pLKi6ub16ZYTNkpwoD2qbU8tnNqNTlw9OCagL3qIBsVLbQ7sotO3+HOAKNri4Wr8KnSihAUlwxXvl+BDqDqzJ5oETksFmW2Rd1tuy3zdxIIzORSnLB9POuABEQx/rq9/xU0R0D2cGpawjsGxCs/c6Fr3ZETpErXDsUkLB9opVC2yZkBibDwyxS7BhX75v4kAYvQnk1PQHnTu3TpeRwRc4jjJAndZzKmciOj3WhUHzhKYnycuX9SupFh5YcLncoe7yS+8gvUA2m9l0zlv3ySMgQnrLBO9J4qKhhWBLRT8lbWdTtvEAO+398aboUzkjdNT2FQ1FDEIbdRaasbgcp7DuRXSGr7+/c9knlYWFhMwowUeWSJt7oJzcLlg4PNFrkjhRj38E02+b46fw+QYTiwQSOZzMe4SWoGRvDOMBoz4zbAEmFqpaOqzkJ53TRDPnBxVfR33jGyW3lkV0e4IQQaOzEJoHAJn3cjALG3qeVIoT2R/sS6PojRvMlQQL5mzjndBMPzqh/UyQkvLJQTDz8ZresBJ0JYvUmKiMYCg4UN1wb+PbZbmaZBchUYiymFv5eENQUCoKQOIgQKsQ3Cqjxbj5nNG0c9zUnH3qDpOGjAiu/cXOt+QQYWl9LFNnxk6hWVGakTFzbcVAezCkcjohUmPCZXdIwQO9ahuwpFjyFpLInBTLCAnB0kvL293Y1U0cmzO+GuRgJAUnwGgRZVP2B+1puyh8ZqWFO518E1qCGYzLKAyvRrPT8SDmLi+dVz/QrijueFiNI1RWFGXvQOeSxas2PDQ7vRCA0ToxR+cr3ixXH84n4FmARDh1Mx+T3UJooxdQXEErW43PlKb1UdMBmU24usZqDidfhIb0SxkCkXXn23w7HCC3bF3p4qqObp8SVIyzGguCq0gJqnK5u6Oi5tJSVTDnQYTLIkreJ67gzCVaJ0jwXU/6GelfDHJy8zl/MK7ROxss+mG7kzou6gCzyDl3bCnafz9PI9EiVDIUhw8r92ZCMoFkCQFAXtm6NxqvqmgYCKscDear4Zogit0YCxpcbndQhRJsHxiqqKq6Sk3+CvLSgYCIjOQBETpXhLViDFY6byZLWhds5sbbLH/4sYw4+Fx7SoDC5554+AyZP46dUdp2El+dwvOYrjDBE2ZD0mHTVExhgVhcunblCSqqu8NhjMNYUYIHEQqS/xbure4lv6R+1vzSpwuysjILGVyOvwOkKvxwMJKyukbO525Smyq8IF9tgpsuP1/e05o0PjvgtTEi2h5oAp6QxbbQcFdfnBqRJEkj7mlHHy3N/LBkPsGqxRUnHEDDk7Pmr5o//8yzL3toNjFWKzzQTpdS8DQI8taUj9BZO8ThBRXeb/f7uAF+ofkysJnk80U66ymO+A6C/xqtxp27j+NorVCGBd1MgSjsJaEqRUDiGHKmZx19AFnewjFzIYyB5oToXFBao4mNCEKdS0nxHJ77p8jHD3BRs6k2lwRXU/js9KgHNW0QH6F/G0PovtM5ygklWMtSHOwbK4kDISTapAhgDIUJiKkry5w4cTpXtSvCqDe9BhCo/2zhoWZ3EDg0VAuyYQ1FKW+oHLQz+MwgNM8D/kZMu7CyOB0OIVAq7LXZqaBKZzyKN0u8QOB9l5uUcAT7nyfDcUYgQ/b6HePbKWkMEy9TkoNsyfIDCd4w4LePlQqeCjNJhmiD3Eh0Hk1cV7cmsyS4bJiflDACYX+LsXK0Axa9uCeN5g5DxpHiJfTx0Umhc9dbHCkOBPOM7GW7utdx9NBJhSyDmgrWR9OZCI55WjVh4ufsKMgfVHeyRCP3u3so5brWObflQQKrlcftjnpNf92FPE1Dl1WwFUfkeP2khCwjKH68KkzoHAWlfCaI2vpwNQdjHGOA3cObmIxGKp/rPE5KOlicUHDKOaPVHiWhn00HbiDBjG6Xjw0cquFK+CcHIpkwvfmBoXF0JgH6PECR3Qe+UkKCyLx/Bks+Z6xoc9rHbw+euxQcCfXhotewnDuOAwm4QfpKQ1+oRHPw9NhJgRQZEPXRRrRD9cURlerlUHFojG7elItiTz9fu5syXbf7u0UMgcDMCaOrJs6ui/lRo8bvvgq+WTwaPiVWp6ssUensogXgYPcCrTImwtNDPBWhDwK/QMrRsbop19nHy2dnGysdZBP6nu1pUYrjvmnADajLhHQonTMtfohxLJ29JVX9QRqdScowDyTQ8BiPiiOiAjfrFu9sMrxURzNjPFZNB0VIkNBReaaz7S6evrq9Kw1uYpC5Bp4ePOFxv6zSxf1FLjpZsdIxcyRA1w7xlxKOQBjcojN5J0Lebe/QuoN9Vyfeo865LkpCbzuNowAtwjnlRu+vYaVmkpSibSNdFH2LZyE8sqFj47AIfRDaLCl9WPRrWnfwlzrC54RFatu26MEkrqToLCwYDgiTI0RLxEpzbaQpSEV0G6mRV8Xh00odlMQQSVD8yi+Ubsr2Fz9OmM9k1i5qSdbTVcxPEVqEShMnX7FS5QWLg2iNsrcb68Mx6EzU1RoYQWYLB8uxusDuf74BNG435aUeiuO+s60pg8ioJCgO++gAvYKjAC1ByYCJM/NCeA1HDx8HJJI0zFjZgWPR+eDUKCHHwmaeCa3G6H8WgjymWte0azyfnf5j3poOtiTEwAv7/FE37nnyTEJwvSk78lA5R18vcWxWkT3uqg7FxXjP7k5ixEEgwUf7+FUcBDjUMg3QaIfQua/snjI+PAe6iHxOHDZ4a5dzdIB+iqMYJ8LMDjMpPOl/c3rxyNDABvG5CbPtyHDwAfIuuZ3jiEJ0qUOG7BVdHnsi8pltYjCq7233X1TJEaEpk9AMuDtmWDIvlIBsVtV0h4zYNoWqtSGOkclRrhUHAfGOPvA2P35xh5Min+uaspPTQ5Dh8l3+US+8/XiOVlX+Y+/qY9oow/hz5dxxvTtJUdH2aGMd0zAJUTN1cwM1LCCTTFh0U5nO+TGdbhiHRk38IBhAp1GjSJRp0PiJiZnuH6Pxa2qM0fPlxbOypGXSqaiArUoT4n/ee4PrAS39usKLud+S7Y8R6MGPp7/n6/cAlJK5pEyAAmuXX+lO0B2bHq0KpneJjVWuiAedYvoJjfyjH+qlDoasWmm1ufny+bKTPeCwSKQ+ZjaIHniHJjoYc0npg1goLbMQLZDbZKt3DaftqYfjiQIHW6YCCu1AB6qawUHkxu2/JwrPP45shxMsK4tdWWSSMse2U9U89u7LWCAiHGqk6hlSNrg5352v7xgiiWB6YNX4zIoD6qnaJkwIo9RRdiuZsZhvXWeRfCZgYM+RQtMFZaruufFwV0hSMgWr7nZR9BCpOoK6hy/KxJdaDMWnCh3QQZF/wUJKsO9xuPHswflyo4BYi1oWgTjY8muRebCfrgCt+yVlDDa4flmIDk4AcG94bfcoCc7pA+F9XiCgfDJpFpAcO3zpWwMFCfg88pyL4a2LgWZbuxU/3U1TRmh0CTOFGG6kntGcA8CtdQQrh1QVZfZ0Y+Y6jrM7RntOqEGORdv+JXSef3nwwlc9VqqClRcOmBa5LqeqcczoYxxZAEmVd1DNaI3NpKzx+saQgtkMHxHhaq+5zdtGfZFDQVKk974XbvsxgZPM0UuJQ7plOGGW1/nAW5toUtACNI5hJTvgSnpjNMczpCH4fhNpoWRuQS0OrQLeRGi6G98E7IGpD7a54BZD3Jrkxkuk2W0hSi8cKIgvXtHklkTkUGswW3mIqGW0ngauef+GyqHsjrmgwEZ9NGH5VO38ke56J3lT2jQyMKf6rK3CGmu01tegB26jaAx6epc56+CDpKpGqn49dfAADJxRs37HUABneR6ANFU0LJeqHWLx1CfbwKEvp+85eqJ51eqYtgrrYCylzKZLBkyze69SFdN4aEVZ5+806miOJ+a8+xuqwmog62MXSJ1lp+OgzNRuLpAcadpqmLyvNJkLFHx35t1eq0tqKy83fYGBj6kK0AAVa3NShwjXneekJifgBCLwVl27c1ghRbqsoTv4LZcIjeRY56FSEIyuJjnyappF0v/Dyozwjd/jGWHR1WdQ89OfHoQeyq3AiqTQehdDQ5AWeAE8Ps0rZgznegWRDb62bAiNcLR3KwicWRKQtjTh88+3llitCAXYbhIcRSObqGqpcLBmR65jkehAsMG15LKDeKbr91DKyYYgypUjdaWwTDS0HNvZ4QV+dgy9iMTQgh9Pv9tjeYYjwE3fFcTb6c9RxWcNDwRz74CJaPedwCzdg3Ek5+FKGjc3VCrYiitxrLZ5tSwIjVgp2nUxcHNEBeN+TCPd4NX3WE5ncuP7aJzPA7eW0KQ3NFRUWzG3zqp1NRXALMmz6Qm8u2J/69rRIVVikQWPQ1ZjlwWhWX+k7z4P8IluYg4M3lTG5IHP20eK4hW7ke10LS7x8NCwaMl3Vgw+uyntW2qWNk/A577lvCt2DAdVlWUVS6BfcJtN6B4KCY3kSNvBJ4DhEiZuf/7ZnIeCKuP6NN5SKfqBrpkk3UEXWUMCJErDD3hg8ZJD4vJ/PDS//uw5IQVbeThcHFs1N+qV0tcp1NTGwW3AMIl7/vXffvOB9WxzwEXxm8orfqarRah3vcdVxSIgHFy7unRxKC3w5G/OvW71+h0hJWDoZqueZL6/6knUEdof6e/wAMclvBULW9siMX022joY9RNj6+ph2gQ0Aw0Kq1gGFg+trTkJ8qk8jBNwxfwZ77c2jYaNs8oWwuyjQ+kGFpJjbR/oncH5YATY9sEUVlGgzbiebNkU9MsDpvMTJUAXBH3X20IgjdLn1JT6IE9FPOZ4BljsXvf0DXVV4wjLJDRbDcO8wAznYYrGRzU6T3ZdnCRwcOD+rDcqI3Ku4rAXrATj2mMM2RVoIxxUdVSOz42qrGIpCKUrH32zQl9IzUdkdpc+9OC+6sohrEp5IbOS2NGdh0P0bKyw/mh3izdJA5ABz95OP3mtuuOdm7FSQF81YgjoordoSwgNR2hLQSithKpXlbpB4HnOIsXM88dPcm7Yf8XGkEZmkbRO8gVDcdA5zIEkrVRXBonFBM/D1u6IH01/qDzZAg7r+Lzm6u+NhPDYG5QlhBpc96qsYj0QK+LwMGm16BeLcy1lMEDg2fBQa3V5eUgxztvnDerYugRsWUnHhD8rB7Sp52RpCgcle/8xdf01Gb3FKhnNMK6bjKHRwt+oSwiJd0FIUvIDxGJ1fPfmB9e5XJAlq0n3T0eZe92Dm1v3lY8irKpEZuQZCK/1wHw42ylwTkKyUtXUnMyonOPhiYNRv/llsrrNriUgfC40+HwTbQmhcVMlX2BZNThUtevB/aVlTkj/vLxDD8k6fMWlJWver7lmY/lQEGHC5UVhFLHsT4SDCqssLZAsTTbsr0g2PecA133/KDJSzJAma8FhDZ+fO2IkhIM3lTC0BWiyeaUq+QTLirI6VHlO9bXvP1ThdDrN940dszD7DLLPq31wxV2r1u+oPuef0JBCuEzIvChI5KBDh985kgPR7loPLBSeJ/2sMhvkeLIVooMctlgxE59p5LNxBzm/QKyEsaaod+669toHOx4qdTq9PkgCjcaaUu547d5rr21q2jk8pjEZY8JlZVGhjs+5IEOFuR2S8WTvfR4QkohnDjy1bUZ4NiPQa4GMPgHu+b3IWLkdOYOeY1dxBV0TZJXFAEJIlLCG4ESorqn92YbNrZtramubm5trZ7B6s4bWG9rbd5aHJibUAMYBLBGJsQQMQmrdKYm/YYcmlkpzkOg82d3sdSfjEceQ8Jx4BpxVDuYsox3azkCcz5fcSFvBDoAxNgkXB0gDK4pYo6oaRGhodBYmEAHWoIokJBMoSwSE24sTFxAunvIv/qsyovMzHgAm2YgWaQ3G5CSvTrtWAUyOeuPiR74vmOHz/duXfPg9i7ve1sPgNYEomyCyOpAGCgoJuMuZ5Fu2d0nMk9gU0Rk4cGmtweQrOsifY+1OgDVmPl9MXwFaNzgXFRsJQM6KJiH0lqbFr9yxklKl0Zl8+eRq45POA/JCnyOWk+hgoPRyY2S06JJNFPJ5uqdiIwHEpDbBPHRMLHIUQFipOninjwM+abcDPM+0RVJoIXnyM2ByKNjdanhNF/5+I4185uGuYUmxkQg4RPqE87H4x74R649VkQF+4CF5eL6jO+pPFZtymrsjPh8Gn4/toS8fJPDeQEHbi0og3FS8QG60iJm07I90dm0FJvmUF6MPIkly6peEYl3erAt2N/4wzefBU4/toa7hPd30thV0MqjJf/QkL1ysmTt/bLLvkyd8HCMstEy5tT1C6JwacvQZYLLk81tFRnx+iko+E8N+KgZtqITa5VxowL13Ed7akByITHV/uQUY4GABtVH2ZGea9iNEdGTTMGSYkpd+MPTGX3tobBDq3jITdoDOhtAgwNZ8D90h2a+ZPH/RUwwgLEBnAG9tfyTdwniWlQ5Gm+j/4dTpAoemN1xU8pmcvLIDdDKw6ApYAFx+GY2QLEVJcHZz4OAWIBp4e7pGcSaFKnmyGRwZ8/nWmXyw4NRjlMZnveltl+ySQQwvfJffAVv7pDwxGslYU857XyAinlvwNXjqv9A7gxkASZmKDg5Kbj1SGLeYppTPdoBeAKkJDbwWo/PCaNYfiLYdfmYbMAsFZ53qZXunIv6MP7+6PiPRwUDZTUdWGP3BrRT2u3Uw7nuRHaCTQhxLQWgSo9sClq9iinoeeLHPB5wjBc+KO/qj2ayDydH7gMmgvHHLy0WFxjzSJvrm647DYfdUksIgdMrRs3bF0iAtB5TxNlKkA+C4VHFza28UZ3cSx6+NRgtpt1Mqnh+I8/keGvuDM26jdk8lN0KDwHmerPJbM6eNFCKcd1zzZoUbII2lYpfRSckCuHtl+vPP564omC7XFf1K5/wGAQNX2IIjKVIT2qgBN+5EOW+BI8T6JTza2VXv9aWMzYabQg4zrHJkLzBptlOMY4cFg1dvp5bPApxRhRUbBrIjNDBk36kzJucQGzQy40BsqreL1OjSNpvy5jbwhzu3ApdOue6l3434PPjiNlr1s90jTAmjypE6NHjquzsj/mw64YiVZb+WBPb3HWx5XGNzGhwTHJyu3mtzWmwk7ZU03n/AxOcV2n43nfNI00McQ3aPcEGwwSvSvebC+Z44OBWNSbKSdowgCwyEy9Gpqd69L6x0+ghLuXSuenFuXUEfjOUmGPFkSgNHB3huPWbswx7VHM2pjc8crKm0M8IUiL3iTPvbycDFX3a3TcYC/lT7j/p/q7JfisUmO3sPHmrp8RKGMo60hLPLfeeXn1liu45Srsw6zOW6H08/q4w2AzsTGLh3yb0lqIfa5czk5pYWpp/5or8zgqUDkiwr82mNyBKVLElYxZHJvt7uL56pf5xwJF3ZzDPgPW/XeJS8LOcrMSVHsJG9rgXpXPbqhQNGeeMSKsf5TU4cITsjTAGEdxVneE3c597S8mRvf1/bZDSGMZZFE2SMY5FItLOtr7+/+6OWC7a4SEGDIZ47aUEQoHjVrnF8QJf2xd05Exr523qAW6DbvWckng4+Qm95Q0dptS04UgGpdaUZCjlO9yvbckFLx6HD/f2dowTRaHRUR2d/9xd7D7XU96w8rdgJYMiMtCCcAJ7z9o1hEaEGF9k777PgBxjr9iav1l331dFTZ/h85MVttLa7Z8xz7RJ0aoijHeDI1ofPWVyy5r1azXCko6ODWI/UvremxBu31uENLqerNu7cHSJ0FkONIPDQYoV5kzxZm5inzAmw6WrDvm7FkYcpLm/YgiNtsMHWrOISRyzNICGIwnBkXpHS6Nxap0pEhKN7gdFN15XcgXBiVwMBys66zOh2F15yewlD1YGrBE4cdkaYEnN8ObIjthlZckJgwLOhphzp551RYKN+fnilJWZkxJEUmETDde/+tMKw57+SRjcZMwS4Y1xVbKQEwjtLYYkhCEQ7lwenzYPF8Hng0P5cYNEkNu6cNxnNnQDbzx0sNNLBCylPBzV4q+0eYVoQQ/uXttXLC5rYeHsYqzqd9TOgFcdvJVpEaBSYmxeeACW3X1JUMDPM/9O7ZRzN8lmfgraHktIEG3xwCd9tOR70VNA0n8oi3frGAS1WXcVg0QPAzCqmXPfikUKDzyPP0Z0O2lPQGQEFqr2wRBAEgIo3d48poumsBD7+ehzW3S1C2GwazYHn7svi5vzfX70JeHq7g/EpaMVGehDHzluaEE2C85r3qydUE52NdwxrD3HFjMlogdHC89EVhhfjLx/fQvFw3QxctuBIHyiwa5FDtNFGv/PRjRN4zsVyXLUOBIsJbaxjcQKU7RkZLDT4/MOeMoZyuaEbcdhOSemDhOjFDlIOAXzayEYVmrMIYz6k7/woYuEd634vMCQ833Pb0SKDzgW3bQK6q8/Tdqh1ds87AyC8qwIWE0RrXPf6jjGcwAhJqtoPnOWEVtjIA4QZ277+vajQsOb//TEP0OpVYALjtlsqmYENXrN4ab7Ak0Rw/UYFiwmiDvnlyssxWzzZXFGy/erB+C36wnPvWQbh2V4jzAY4dEf+GW3UNUpXrw2rSVzq2OADkA9CI1b+6/xH3oqH58IjN5Uth/BMDiBXSrbgyBDSxjPyzGij6Lzu6eowFtlkLyT0EPAAxv6VRWClSNXfA9qdCUNuHKV8FskA47IFR8ZAbOAGkjTlFTyj1zWGh3ByRwQUWFth9BJ62iy6WoT8kbYnX/j8SMHgDJ0Hr9RG+ZdBeAbgYXXYrnBkDGLamE+vTY5ngIEznt43jKTkdNb9u8CAVcNJ/tjUJ08ArJxZTSksGnmY5k0rMwS4s9JuEWYDMZw3RpM0ENwbXmvYGMQpDJ7F8YeMCqI146OsFp0PXwyMcArcfaxQ9xU9ctt2oL/4PCM4dtvXgbKDFL7GZXXSb6xwr1ndUBVUcSr3JaSWb4AZWDHgT+isHwbgSMQ/f7BgUAvPNy+PbNAWHLkBiUPXuCzOkwSBsPnOp7sqx1XSEUwFhJuc5vsBfYGcnug4nWcOA/DQ/MOJA0fPf7VseWSDGnitwmHPcGQNMfz2NuvmdBiiNHzOitXV5WFVFtOyxkOBGwxC662VHDQHkqfpzHGGKL/0j+tfWj7hmQiOR23BkQNEtKMRGN4ioUFqGm8/uzaMRVFJEywy74Mx8HjWW7JIxtG+2XQmPh+HW4CnfxTJNARtGyXlBFYKrS8Dgc+RzCTKOx2NrU1jagCLGVCSRZtNhAYe6icPZMNoJEuTTV9uMdOZPNUd7ZFr1yyT8oYGBj4M2xWO3IBE5ZwaDwCXtWrWF8J9pat2lYeDWGRRBnycR2hgXNdkcU6ElfFEe713tsGpg4HGSuwfWk357mAcAtxS6bdbhLmClcK736wA4AXIFDqXwV26qqahchyrbOaO0uzQa7P4xsG2R5GMMmOzFOnsP1Q820SPZ+C6R8clsn+7fEK09wZ7i9ACIFEJ77vLC5C+2mTAwRMe+rzudauu2DgWJoNHKIsvrdZtmHc4sCGStupArByLRLVM0M0BN7vY4l19DmJ1h4SGZUJoTXDYx66sASvi4X1Pr9NYkcrFi3hzzJRFPBX77722ejikkVnM0u0f4fbieV7rJU92HkgrSBOb087uQ7XkgCc/d6e8Ojzd0hHDjcuiaifAHbavjGVAEg6GqltXlTp9M94xvGMWTNU9X/EpG1Ztbt23c1gJYMlMZgsIDQx4mvujC1MaKcSAOtrZ9+QLFTqDZ9GZqI1QYCY3ZdXdLqAf9i0Vi4FYEaNwXfv6mub6DWWE13Pg8zqdpfXNtQ9uXt9eVz6OEDZSwCxhlhxm8LDtg94olhN+doSI12ksOtnW+8wLFe659mOcwIA2D6WaSi3issgL7Rk7y0E4rQbUoaqquqaua2pqOnrqTXjtiq6urp1VVaNDQSWgqiJL6JYzxNFEHiEOKO75oG8yEsN+jdUGWFb2E7NTQuYPaokBddyy1xydqxTJTA0U0Gw/aAcPNXYFOg/QWSOpKlbC4dA/ZowhrGIsyTKrU9kisBMJoyfHgbti/97D/W3RSCAQwBq0f9RIdKqt//DeQ7U9JDQTNs+n89tkvE8xgxgz0b7nzUOjvRWbPyCksKIo+s0QWV27Wgy9Dp2sIuhzruypfXBfe5OO9vaG1zpe2FLh8SX27OWA0LlyHp1JiN5Be+nOnrH7n8BofSfmNIHT6SzW4HTqrr3J6jAcz/m06GyIDTPImhfdhGbcDfYW4f8C/7F3/q5phGEcf0+uHGe8IZGWaq1QqYMiIqWI0sTFEmkpth0bKdGhgdKhpBm61A6BkJKpIHRox6ZLt0yF/g3y8sIhDqeDS4aYqX9Bn+c8jTGRS6Lx3lzeD+gWOM033zw/3ud9rMNJYxm+wReVrChj9rRJJLsWQ3c+FbzdlOPpWIl8MkTA4QooS4WmcIUNzsZUxs8t4ngOxxY9R7JiKtYtyJCwqRNfYRN8nmyCnMfDkg+5tWiJJNZEBdot0MaHu5OYs3WFDbU54ycbVW5r0dK8WKXiInzdi9fUMG1M7DxK1pmdIChLOX7L+9iE8JWoQLsIqKm9IcrFpr0wdM60sLJhi2zk+bRolXwSenYXDG8IOb83Y+vl2dKhmQraQxmf7UIPHEkSCaG7gCrxuabPJQVnF8Pvfz1eqrMzn4ySuTzRMUciq+KIndvwHfydJ56zX8eEgfOvWqyO5nxW+Dzpr5CIOGLnPqgG0+dYfrPFg94ciHxNtWgD1HwO+GwXwsSZOGLnQqiPZqJBSPPs571Cu8+SseaFDq42HvNWi/aQaFckhK5E1g6+vCYSnjkaq+VwcDFazXR0cx3cBZANztqFMBT7WAQcLgVMOvn7dThsHT4aoKrWcEFi5/lq4aDJ5IsOyHDXLoQZlZoYinUvskYLtY/rbwL3yTG8XpxdLGfalOmTjHvx1i5UpPmP4sioq5E1Vm8XNp/ll0vLFlswuriZatUbjGkTzhVw1y6c/ySuZXQ5lMqaBkNg+wO6Bm00zBSQ1ieFs3Zh4G9XdFSuATgE5jtCnkDLPFu0Sh60fwg9CyYCpwv5sGiJ3M2JAodgMqzDqhwgiRlCwVTwdfmwaHEJh2AqUMaDRYuZWMG08LWct+gb4lIZwZRAi/YSZ5kjL8QJDsG00J2+jFQl79ui4y2YFrjpUyLnRBQ4BLzibHMF9SwSwvNDbbm+//Oc7H+jnh+JS5KQsSKVT+DzacwezSef4FpIn7JMgjjGNdXzkF59AIqUjaF5OEq33a68tKPSbh2OYrBTwQfAl1skDuvFnbJoKXB99Iw32Jrueg/Q+7D6AdBsHrSSlT652soQ8egJ1tcTXjsS6w+io1RXhlirWKRa8AAdeLHBQ8ETavhnZg5CXTl5O5cW3iBxNxag0eKGQwWfjoDXIp2lf0Ahlemx+mg3Go3n4/E/kfkFC+8xyBTxDrPQIxj6jA8AT1HO9EgV4AmXOk0ERY5rkC2uhoGzdtaRyp1K3rXctVcTAwhLvg2qU0oNpBsrAC+3q3mklE6nvy36+4SJDSeW++AbscfjwZ/Et6N3G7n7LRa/pdM/80i1XEC6+EEo0DAFbgZJHMtarjth0bjmKnmlC9AUsbK0HszodvdjSKpcfLZR7BFdvAncPt1rJdWjIh6FOAXqXYW300V+069G/hR7bJQzMWB//9CgTANQ2gBvtn02ixZ6Bo5KD1ap4RBotS3WqltbD7IqyNfvJeH5E16LeADOF4KQnqkjo/pegM+2uLO8ld/ItREz9cQBKqbJcp2XiMSJW5QkEspcoRPQWI6QYSkJ03UGGEYHs7gc5GwQfT5YN9mLBIY0rM6pFh4c3+dqvv4c4LMroO5RfSuByN46Eo3vPq/lKpVC02AAfj+QUTobkVA9EyKzReJ+bVvPiH13NE27Y5Yjms3Wv8eYzOVyGA7/iQQwfyPHQP+du8rytQWXQ5j+TY4IQ4IZyH6tbq/lcplMATPKOigbvjoznXTAtOXmjJsr2FDhce9xP5wAIK9rUGN/CShATPwdFvstp7+F/MDCkIzRgftwdCfEDFAGN2tIg4AkGPT7lYfpUv7VJuSS/2DroWHoDYZp5EyFTVlmgcwO1DNPl0D3WxyaBiJmlOJUciy2Wd7eKG6VSk/2IupN/0J4JBy+ZvK1RTlu2gs31cheqbRVLD76/jJmjnezBtO0GVX+9O4M00LU83Me9EytfX3MxIDcLrW5Wd4o5peXQcQj8YSkqvxnczwgKbg4fDggCfbSSFhImGwfDhrzl6pquf5xhpU7h/3ZdGPE7HYctNqVSq1WW4lDbheChsPJGrDi3pD4EpEUMx450rb37nxkL7pSgxQy1cIo2wdclq4b7TMservaSyf69TaMjutNaPbGIMPbhiLF50TQCwyFE0LEU8aU9qBbid3J3VeroOpOx2C6fhmJo6z/mpVFq2Tm8QYd9J8pNazV7fmv2Vt+/5Afi4ji8lGO2jdhUHU8nq9uV2JNqoOsMW+c4q+crc1oWlYlT2e0dKLvyObtU9jAKxQy0L3bWQx6w0LGzjJUGrmVrRbfJgvQgDR0DVU9HbP2daIzqdx5SGT1P3vn75pIEMXxWQmIMSkSIbC5dSGSLRJEFrkioqZRzkbMWSZXaIoErrykSBUL4TiwDVxx13oErrMK3N+wDAPLskXWwiaFsfIvuDerO/HC5Ydmh0SdD/kP8hjfzOf79vGuZwwMTJ5l9HrddrtSbH7IgL6LrLMbHhAWXcXrEmCuRo2AYG81P4N5pN7Rj4/dYXKyivgTQKm8ZXADG6DziOtBOu3D3erBWZN6vLg80iCLA/mNEfaOawnFWsnLs4vjvEPFDHnJac090MGWXOWJ/0Jl0Fq8A6VHcLffh8senMlw1xtZ8B+SZljfzQJUsEsIUKPykbZf2dnpO7ZB4J86YVnT/d8cYeezn4Ekz+sRy7x2pR5c95qt90GweaySpyALJPBgT9jqKhjHqxIIR5CNpmVOcFhjUo4jvoR9W3LFumTLxL2bbPlHugRSrxVbi6p3XTISTCnBgIRcNuUUyMZ0fcsVjWOuElrKNu49Rb/Feh5IkYHZS5TL9WJpu5VagTpmhSyekGcD1oOAa8zUIItdzt8a4zhGmvPniYSiVWvS/pl9KJte9xxHPzwGswcH8kiTPHfpoLngzqMvykfJ9MVuwbGhqp/TgWBSUBAXWCBpsoDdIMdpElfv6Rd7Ta2mrHtmTxIxoXmAbcuSU1fbJ7pDpx2fPKoXemM9RfMP2DHDRyDHqX/bL+VymsKkiCjkecNrrTeVGp12hAbEfGRv1hjj37wDdizQSUyYMAVOTks1VslSSDxbzDGePV9XLk910IuG9aCFwUSPIz5IEuy4WsbPbTC8QGciXaIjesPEhSSsiOCfDiQC841nlfaDS+EWrjnNFtLzGer5GZqPej5iOHDnA3IZr70QlSz436Z7QI43ftG1nTSOer//wKS6iDggSWvQP+Mn/MiySWy72y/sVPYvB4FO8QgneBwJBaSRxcod454uJw7T377fB/GD/QU0GDC918vqMHuqfVfYtNOrrxYQTAV3q+9z6cSNbbKn6tGnaP73wYHqsyx8e6O7c0+NDMvCif5CMFFVy8on8IqFjulaRXdYVkEUbvdB1isTQmwYfqpXz782FO9ROSQeMAQvDVlvyjEtXdcdbMFJvdRt0h96bvdBTPP2JnE/tlkF1bcSXRSxToGPBN2iVsEq7lXaXWPD/I0Qn/4ZQ77TJNd2J6tXz7VkTB6WsjiUBQw/BYwc/aKl8/0/yF+gnhfgXAbbZ3ayMAIFo3zyqlB9Ap6wolZpjthfivZP8H29ra1EcTsVigxLWeSIBHxh7Qf8+UcAJTsb3Uqx9BF0nzrslcNIIJhOgih2cKBF1aG5Fo7kL/t2TAMAAMMwTLvHn++OoWhl04hCvO99a+ijwuh9AAAAAAAAx94dvDYNxXEA/708XpIZMNg6V0d1RUcVKkhRFEfB6WlWvNiKHvQiSqV41LMMe/HoRT2oiIcx6EXxIgzxL3iElBHaHhapjubon+Be2nSvM8lrllZU9rlslO23b37vu5LD1u7Zsyc+aWDvj5z+PO3feE9GReLABEgjQIwwKXBEX40YRRLhYsahgABySQFgWMTiBI8SXb+GXDASxH8e3CLYdWYpBkL8oyEYgxgTiQIh0mf6Ls/dDhmMFIKix5wcpJEJZtHGMQhJI6S6fdnbfwlQrPWhCZwA8j6UzgwsLMzC2KXXDgqtFaa3JFMJt9RaQOBSrd3qcTpOFfwpbnmOptjAqeLHgwIF6CscjGHlWNB5aL3freQ0UwhaRRp4Se/hhfviY9aGF3xjnlsFFMNjfyxOsVj9d8pFBEI9sZ1W3+JD4PE/aS0NQugwt4mVlRenREURq8+lwHWo2trmOM9gvBQ4+66zLpbJZDPZpadfnl84lADQfCs9+9KQPaqRT4EfApCQissfylk2cnVdoFNL9mIeO8/FjOxnvgB+2IUcTU5deJvLMpmgFK9SwKkMHt+8ARKEIlCscclXn17hvuOhKPeqmyq7dPXe50LyKJsWBMF0xpI9jVxyR9k729dyXPzKQ08cLsSPdzdB2SrKgzgn0HTewBYJ1hZljs6CjhOBRxszqpjhajQ6mfOX3t8G39u6xGkD0z7TyPsl1RCU3r/KfbOpwVCshpOtzDQgFrPelNXdk5t1ID51RvB4uVJeyjSNHtWfxa1dgZLjbWymcSkl7HPZ4HLMbF4ExRt0fVMXXBSm7pqMhrm+VK7U04CksEKbtAfTLFsbF7msDyI4n4EI/nO6usHvrltxL+SEHesE9FrCHTP3XabbjBwfND6Wc0M16Ygw3o+NRtN5/QmQ5ldoOmD5FRrBkZM5W7d0FWM6CnNQ6Gtdle6e3H372ykqGoJPXx3bNCwsh8YxKVdoCRY63sawqNAESrf07dlYXnzDDTq3KY+8eNWyqJ0/mQaEggtNPcOFluBMSx6s4nsdiKDPNrcPtVs54J7AclOmMRi12V6h28OFTk6g0DQSLOt48dk8oKiFRgQen7Z1blliuFMEhcV82420TnGh3Tq3sDpKHGtnoSlX6Ah9NrNz+4YKPRNt8YadvzwLSsxCnwQSer9R5Z9DVbt6GMFYCp36Swu9BavmrcdAwgr9+y2HAgeeRaozo67fAGkShZbgztc21gVp4hVa2tFnvX0XpkKeocVUvV2dBzK5QrM+21wqubkMBP77QrPDcd4cQlGeoZEyX2nKmEbj3nJMoNAEfrV3Pi+NXVEcP/dd3g990GhsM1EyWjpiB7SItGVoySIdKMiU2VSnzWKmC3VQpNCNbroZhrqxdNOFHRfTUiyUARFapCAMxU3pj8XjkjCE6MIEnZJ0Ueif0NzY3pz3zPO++G5GJ8kXhlkYzb33fXJy7jnnnns77XhHoxhoDW5gnq0IMgJSoH2tNE3fBl0V0HKeK/4GaQegHWqUv+6VA403Juls4++UO7wDpnIfmvuJZUPyB8MC7fU3nG/vgg6hgOaibHJoAIhioLH/jJ5xYZU/4+cZaPqCj5wT27gK0UACA01i8695FiTAxpDmXk4eD3OTW/ezihp58RSFHaLeF/n/+lmA1rw8P/wQBeyC+9D05GT2F3tVW2jMs5BVrvjPRIDCl+zMoucCNI1knvqoGtOi7iGOJgJbaAL38m6LaBhMrkhusu+/xECRReq/xMGKsLrK7k3hvRTn+cROl/kqy7YkQEvts5WZjfER+ANNmY8M4wTRhcUYCQm03N+gkUcztVea8NafDqsvNxZ+T7LnHIDOfvTeu/X15c/Xrk0XMq5NFM3eCwq0DctlF3kWK+xd2+qRa+Z4vgRmfF6QzjhIxfW6Lxof6sdfFnNuni0r5+xPb437jWFrfBlIY0DrXv/52zdAg1OAZv+s9NTTyrW9AmMe22jsL4GuGmjiWhe+S+I8ozf45Pee+kM8xMGAJwv1X8RDvc8a6NyfH/SaUF/R+JVbr879SS08mkPxoGUWOrHFXJ/i8vrGzlg82i1VbT3r/jjaO5VitQEVb/TW/5tA8JTdPEdKo+nVobu9vqOJRgEkFlqyH+T+sw1cPj60lf94uLueBsYeb7w3euTxPCLFG2CrBZrnB108pzjPSAQGu+tqeC4jRkdLc/XnEeUW5RkDTTNzQHQfHU/pi4clC3O53h3IQutw1bWjOFh/+woEky2t8Zl3qODrvaifR+tOm7kiYalftwcHJaNoDGgbbky7/A3hP/sBzVLcJ/LRQGJnvuw20mwrodZCc/857+LZE5f1KXjwMGrsXQfNp8rgmQP9wtN3T80hmboNsXlMNHvCDYUcaOieQI4WezTTa4KuKZrAzbxRA/qyvGy1d7ZEcWBq/AYQILoJEvkALd8PGpVcm6C1vstB2bWo38g1vVpJN80s/DEsrQFRBjTeD2KeZZIDjXXhgOZLQ2LzeZz6+j2Ihbbh9lEEPYv7gmYlQJcQ0N3yIsbVkoW90bUBYXoUAU1IbJdZLp6JBnKgT5Guw52Hrl212OaqAJrzvHZ0gufnHugDKdD8Yc3W/DnKtqKBfOhF/DsrwoyqttByoG2Y2nMFpr4WxlMR0HyJ1gqY51mem5ACHZcNvBI6x+6eswpEDdA4voF5fv6B5hY6QHnekVWD86P3wZQDPbbCaI2h76ELzgtoiO0i54fm1zhsCoHGwQLMc2igQSO9u3hrGKmYaFsJ0F5/wzreD7YJ0CbcTde4NYq8vFcCtAbX9w30gwTAeQHdBW+nDLypvQIEFALt5ZlW/Q0CoYDGnjlOS+0CUQK01392qva5XYCuCEVprMIG6HILvYq2Yezn+PkBDfF1NEZj723QQSnQ3BnF9rkgeA4NNN+KlBky0anbYIcCGg8Zh2S2QYf2AboLR65p5mOpD41/wNdzA/TzAtqGmQJFDvRaL4BSoDkc5Zp9ZiOi7EIB0GDDvdfQ8PN8JUMAjetFsYsUAxvaCGgdlgo1oLO/DTQIdOoxaOdnofGO1tntBaVAczgwz8IXVQS0Bo+nDXcsWoGF9rr8MaJDOwGNPWIOdDQg0PjJnRvQyVE0xKNl0MIB7SCgsf+MeVYGNFf/vQwy0akXQQsLNOl18ZyfjxECHaAlQK/g7PQrYJ4T0BrsHCJviY8wJNBsIS7hWSXQ/L1R6I4686EttA5LZWyfqzy3HdB7DQOdc9fVnJuFXnWoq0Q6DNBiljhYYHlqe1QCzTU2kaWYhpBA66IOAJXztxnQOlwvG41tCuMIaJqZOz+XA+PADpbBDm2h34xj//nI8PCsGGhcucLFj/OEAZrXuDIvz+0GtCvKYR18DCSQy4FPLpwP0HxPVbbch7sUbAolPKsFmld5WQjoZAigvTWuxv4i9zfaDmiC49CX/vxKlljx/oCVb4N9TkB/f2ihD1YUQBXQXv+ZCf9ZLdAm4GpZi3cbOTPQXp7zi9w+tx3QJrxSRDGLP4Okvgd6mIO0lQDtPIDW4X6ptvK8REoZ0N794DTPtYUEWh54tErfhACa+xuY51XOc/sBbQsDLS1OQt+TrpNqmcUY2PY5+NDdDxjKcfLJqohyiGQbiuUOmzaoBRoXF6JAzQCcCWg/ntsPaB1mDhna4S2CFGjuu+7hSdDC73eqJcheaU0G+vJ6tuYs8SqU8EC/F8f5QZxrawrQ3n4OI8mzAe3l2TreD7Yd0KYOt1MRVE7Ag8oyoDlJK5wkIYtOP9weHoT6EmQrBzp6jdFaPPwOaIGWkoBmn+ZycH/DwzNiQznQS7juojAE+lmA9vrP5ep+sM2A1kj1DBajnppcCdBob45Plz9auPfHzZv3XXoxGY/zjr1cmnKgTZjawwmeLxpI8JBTgDaF/4xybc0CWoNbRQu3Gzkb0Drcno5Qj8vfgkC/erJSGc10uP+NjckMddd7yYHmSo6caCbGMvlS3q0nk5+988479zevPk4MAOhKgfZWsdKDXz9P9AVS4u1lIP5A2xKelQJNIMm5RRblLC7HS3DDxTPvxdSCQDuZ98YGur2C7qoGrnz+a3q6gPsYGAURNZIATWDWsbxIU8MrK8ey2Wz+6Cg1MX79MhBbKdC8czAeRKk4GVCp/VVfC30F+sX5FMGzeqB9uGUPus8CNPefPTy3JNBOZuXBCX1T+ddT0UIxkxOtFlGf1SBA28L1lokec55j+yubfaCHAFreJMpgAWXwZfctTkLxDSr2Vk0FOrTLgfeDrMpziwLtsJMqicdKHSyLb+aDN5qZ4WasAVmsNIo8O/VAyyVvBebkit/h+HOpGvu62EDTzNxjdAafpTjPLQt0HVl1e6xR8eUatBXYfKPtQw02sjl8IYB2fIF2Mk/QdpeNjwGBZgMdyofmOjiMoFoxHidpK6DriVpWQfActFnjbKPtFmm1WuZiA+1YFHu0x801LrQPzZ8eRQeIOkA71IrkJyvGkzTUwZ93sG2w3TmthpguNtBZvDcozID+TC302YDGO3uWWm4zl+Ok2OHCfTGMwEBD9UIKFmno3XgDTPsiA/30s2mcaCqv9hNywV0O9mQhH3ER3cKbwgCimYUXrwDpgqBA4yuDhtanS7xBLKUBTTWbSJy/hc7kRl/ySX1/dkVk3Gop5Iu9KSzN9S96uiS1bNguiF74+xeeaYLGgQYbzME7v6cnU0eZbIblcsYlQ+hSJMf/s6j34zMPpFlhu1yEj0CqSwafjE8cOipiYKjI5yIDzd24QW+fpBZNrPBOycwlyzKoF7HsZzdAtxsFWiS0ByuZtz/m3nnnzcnRItbky8VCqVTKepCOHC2D3QygWb748mQxoNb8U9/E3WnUqO6WL/CmkAOt9666O422Zuq7Qs/Tl7EqN6selpys4+khzyZf7QO7YaBxV9XBaDzel7yF9OMrL9169ebNm+uPPF3VHW6i1QNN8z989dIrP96Si4/ttNS3LohGxUkXHGgSu3eC6BYEOvLkp239JaTkd9//MZ8e2Sswy0GyDh7egS450H7SdN85mP3bu2LGqM2mcqDFfRqhi5NQ5RrqZnehXQ5ddDfHRLcc0DRzLwYecWOafH1jooI0xdcUFmdAlwItk4ZlEk2vCAhMfcvwpye/psyHfmrh8RFiasFknn5iRff40RlOdBOBVlA+Wu03anmIbjWgq+WjxCXtP2OaWFp/VMK+Nj8EaocA2l+2DnenGcWnai8rAhqvZW40CcrOFGKisY1WD7QJNw5zCgr8OdGreS/RrQh0fReBuwK/PrEc9wLYaoHGzU8s3FLxMWjK66EvFb8CTQHQUeCS+NGKgPYeZ6PZhehZgebHbEQJpOiL0yZAV9TF76EqMte9cwnFFhqfleJ/CT8BNSdWFnCb6lcVHpKFasW8I7HRio9gcR8xRKMZTRT5o84LbQN01RW4XYzgart7ioHGsz6ykKO4oQzoLDoVelMV0MJGe4nWmnxI1ghx6htfbYSJbh+gq1bIZaMr8yeKgcbOAe7ORNSc+u5hqPforBofGmX29w030WA2o41BhuI9oaai0Qwmuo2ABhu+zlu4nWtfk1wOAqsZivrndSvqy7GBgq/s5aRSoMFGpf6cNnEXsjqguVHFPc/5FEK2AjtJdBsB7Tm+ze/hJk0BWoMhd/88BUDj5qNcFr/+WyXQQPoX3UTz2+rVAs3vPLfc2IZv1ugluo2A5o0aERLFW6CFBVo+bevgvhofmkByxJOCVAq0985lK8KJVgo0cTVrZBMDodvpevxoNrLU30ZAA/RiF+7gXehqFtAFS7GF5hrYylJ3ClIV0Ph2NGTw0sugKwTahLvTWcfVzSwM0CI8g4l+NNRN2gdo1x3HlG0NqPWh5U3V5UDLm9vhriLqr6Q4chFd5kQrA5rAGkpvscIy2CGAru9HG/uVn7cN0CZMHTJXooo8R0CLfrrIRKsEGt+Qhp1SVUDzK/Vwaf56PEwHf18/unrTUZsADRB/k1FRGn24A5pKoHE8ohkuBySu4bgjr7lQALScaEVAE1gsWYpvwcJEi5wTr4BtE6C74NNaBtx6KrJtaoGGxApz97NRBDTMOa5OOejZKr94ExOt6J7C5TLO3ezvKAEa7wxxnrM9gDbhjSJDmSq1QOPrl1WnvvHVlbiXWZcCoOVEKwDa5hUBFHscV9Tc9Y13hpjotgAaoG9CrJvVJKAhsYX+krU/pApoHvbKURy6m42RLlB+eb2P1xEKaB165x2KDfR10EMALSW6PSw0QPTnLG2qheZjwm2WItNvgBkeaNyTDLczi4GuAGi5Hx0OaI3A8DzOqVDWcxlCWGg50cOa3Q5A63A/b+FT9KqB1nW4Ibwa8SYKgMZJdbSzergNRDeJQqA50ffc8PHAgVkHaCMY0LoNl4dGM5aDxGN26oDmRKcw0ZazngDSFkBvHtUAWu9WCrTG3z62lqKO+6tVUwQ019gKcxHtjG6OcSB0TRnQQNCZPS5qpL7uJeapQNfvIkZMnfCM0PX1smvYvLaKhLHQcqJZpRbFbAega1ePUnGRqhTooJeqDCR2ZpG/wZlZGVNooXmooMDczdcLExtT2qCE4oBAY6ItTHQZEY1dDgHC634gDPYld2b3PA16eMbGVgo0v23ETXR1Z9jyQGvwlY/nJ7HQg1GZ4smdb1ZGyu5O0kZ+CHQVQOO0uuHpbFYY2Zv943qyL+qnoBYaE73q+mAKousDbeUXh6N11DW1ND89su/wEbtWhWe91QLNC2A50a6zkWC2OtA4OREcaALbv74p08LIYSmbMai7C8j4ZQCVQIOIfiH7Gck4pfLINb+RvbfUHxBoTPQuM9xEi3QFAlro4LO6S/Ko4GQdsSYINQKqgeY2+sjTMXnYtFse6PgZgIa+LScrE2MWf3Ker9YpsNUCTeDutxF68j4BizHfkYnJyIDGvE6lDcdLtOkG2hULydaRE8GdfsS6f8sDP8qB5kS7WsRSXi+otTzQtSweu5aQAI1Wm0pVp/1oYRGIagttwyvp+j0jqY94bzvSINBAtOFZx/D3OrCF5pItiZDBjw7YoBBoXHx2hIdMGSe6xYGOrgtuLVHMIQfaaVzUOFqMEVANNGgVG808RMu7jzYIND6FiokmbqAbFhWHu5QDzTVw9WXmuGz0G6C1NtDwcYaiYo6uoBbaaVQWG706AKAeaNDgw92yhRa2KUCDDdsPS9TrdWihgDZoahUdv1UOtA3LexFMNI/e6a0MNP+NF2r1BAPQHKAptfLjU6A3BWgwIfZ1Oi+stGKg8VfBuOEmei3m40NLJAIyD7eRu6EeaNCBpznxg0gvg9biQFu15hxXgm0Kkw0BTQ3KMqm54VCXBsnuxK1cuphnzKC0caDxXd+S0Y15LtEt//4hkP+Bnm4AaEoN5uQnrg6DDc0EGohoTYrynM8R0O8+FXj+9TGQYMX3VGTA3gZNDrQJdw4pDfzgIk7+0Si/D9wM2KgiONCYAOi7PzFScLLSizIyuUnXpvDAoqITep9ktXitsUWxB7zeV6splLwzDixmnUJqdjPBM1RnApqKfcmmxE7wxD0aGOV5TkkXFfHXRcnU+QG9k7rEjnXp8HuwAeQFpN/W+kfvL4GOgKa+YbtJFlSZ/b3Zzdv96A4XKdD/j0akGwIaaRhObu6mK3kLdqoieDImTKXET3I/x6Uhlak0Q3IuPXkRtGPMbqUNFkzO/kh6dSfRD2J+MqCTnj4nWe8j8xcvRSlQVhONTMRPrQXYE/Mwph83aKH5J0+phq/2/C/u38lF4M6u+JWrwy6XwxCKeBIryz3jPYH0YPX6lDxXjuvnHoi/vGpCQ9L5/PunhlYfnDqk8fGreGX6h8Qb7m4Dkb7H9i6e+jj/6vlv6W8FW5QHfE36BwGIfnpbUkOozLnCQ+4ZF3PpC7CoM3jM4z1L/acjNC5ezb0TmTlHioivEoXq/l+DQXdUg+JXAGtt76imeZ93kely9cHZpPHx8wk0LJ0AiPn4ys0OWrHAy1V/kMHXRNDsj9U6Wv2VhHfIQoHMhGfMwR+B3P6kj5AKPQOgWvgYTzChdKjuWrWpjc2rx9pc6vPOJKiI1pidJSCZgNz3IPI1wtIClVz5vcjGuZWAIroZIAFaW/2NKc9aaHguDY9ZnlsJPCUMCR/oXSBwYYWHdpHH2dEz1nPLha3XBB09Y5l6TSZcWFWG+XwMtKOOOuqoo4466qijjjrqqKOOOuro2epfsCcuD98dJp4AAAAASUVORK5CYII=',
                            width: 200,
                            margin: [0, 0, 0, 40]
                        }
                    ]
                }
            ]
        },
        {
            table: {
                    widths: ['*'],
                    body: [
                            [{text: laporan_kegiatan+'SURVEI PENELITIAN DAN PENGEMBANGAN\nDI SEKTOR PEMERINTAH TAHUN 2016', fontSize: 22, alignment:'center'}],
                    ]
            },
            margin: [0, 0, 0, 90],
                layout: {
                    hLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 2 : 1;
                    },
                    vLineWidth: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                    },
                    hLineColor: function(i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                    },
                    vLineColor: function(i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                    },
                    paddingLeft: function(i, node) { return 4; },
                    paddingRight: function(i, node) { return 4; },
                    paddingTop: function(i, node) { return 2; },
                    paddingBottom: function(i, node) { return 2; }
                }
        },
        {
            table: {
                    widths: ['*'],
                    body: [
                            [{text: 'Kementerian Riset, Teknologi dan Pendidikan Tinggi\nContact Person:\n\n\nPertanyaan berkenaan dengan kuesioner ini dapat ditujukan ke alamat di atas.', fontSize: 14, alignment: 'center'}],
                    ]
            }           
        },
        {
            text:' ',
            pageBreak: 'after',
        },
        { text: 'TUJUAN SURVEI', style: 'header' },
        { text: 'Tujuan umum dari survei ini adalah mengumpulkan data litbang, sektor pemerintah untuk mengetahui gambaran aktivitas dan kinerja litbang sebagai bahan masukan dalam penyusunan kebijakan pembangunan Iptek Nasional. Tujuan khususnya adalah untuk mengembangkan basis data Iptek di Kementerian/Lembaga Pemerintah Non-Kementerian.', style : 'paragraph'},
        { text: 'Kegiatan Litbang yang dimaksud dalam kuesioner ini adalah seluruh kegiatan litbang yang dilakukan di lingkungan unit kerja Saudara dengan sumber pendanaan baik yang berasal dari pemerintah maupun dari pihak lain melalui kontrak. Di samping itu, survei ini juga mendata tenaga peneliti yang terlibat kegiatan litbang, tenaga teknisi dan penunjang serta gaji/upah di unit kerja Saudara. Pengertian beberapa hal yang dimaksud dalam kuesioner ini dapat dilihat dalam Petunjuk Pengisian Kuesioner.', style : 'paragraph'},
        { text: 'KERAHASIAAN', style: 'header' },
        { text: 'SEMUA INFORMASI DAN ISIAN YANG TERKANDUNG DALAM KUESIONER INI AKAN DIPERLAKUKAN DENGAN PRINSIP KERAHASIAAN, DAN HANYA AKAN DIGUNAKAN UNTUK KEPENTINGAN ANALISIS.', style: 'paragraph'},
        { text: 'INFORMASI SPESIFIK TENTANG IDENTITAS RESPONDEN ATAU UNIT KERJA TIDAK AKAN DIINFORMASIKAN KEPADA UMUM ATAU DIPUBLIKASIKAN DALAM BENTUK APAPUN. IDENTITAS INI HANYA AKAN DIGUNAKAN UNTUK KEPENTINGAN KORESPONDENSI.', style: 'paragraph'},
        { text: 'HAL-HAL YANG PERLU DIPERHATIKAN', style: 'header' },
        {
            ul: [
                'Daftar pertanyaan ini diisi oleh pimpinan unit kerja atau pihak yang ditunjuk dan karenanya bertanggungjawab atas semua isian dalam kuesioner ini.',
                'Survei ini mengacu dan berdasarkan atas realisasi anggaran dan aktivitas tahun 2016.',
                'Agar kuesioner ini dapat terisi dengan benar, mohon definisi dan batasan istilah sebagaimana termuat dalam “Lembar Petunjuk Pengisian Kuesioner” dibaca terlebih dahulu.',
                'Jika ada saran dan/atau komentar, silakan tulis pada halaman yang telah disediakan.'
            ],
            style: 'paragraph'
        },
        {
            text:' ',
            pageBreak: 'after',
        },
        { text: 'IDENTITAS INSTITUSI', style: 'header'},
        {
            columns: [
                {
                    width: 'auto',
                    ul: [
                        'Nama Kementerian/Lembaga  ',
                        'Alamat Kementerian/Lembaga  ',
                        'Nama Pusat/Balai/UPT  ',
                        'Nama Kepala Pusat/Balai/UPT  ',
                        'Alamat Website  ',
                        'Telp/Fax ' 
                    ],
                    style: 'paragraph'
                },
                {
                    width: '*',
                    text: ': '+cetak_laporan_user.institusi.kementrian+'\n: '+cetak_laporan_user.institusi.alamat+'\n: '+cetak_laporan_user.institusi.balai+'\n: '+cetak_laporan_user.institusi.kepala_balai+'\n: '+cetak_laporan_user.institusi.website+'\n: '+cetak_laporan_user.institusi.telp+' Fax: '+cetak_laporan_user.institusi.fax,
                    style: 'paragraph'
                }               
            ],
            columnGap: 10
        },
        
        { text: 'DATA IDENTITAS PENGISI', style: 'header'},
        {
            columns: [
                {
                    width: 'auto',
                    ul: [
                        'Nama',
                        'NIP',
                        'Jabatan',
                        'No. Telepon',
                        'HP',
                        'E-Mail',
                        'Tanggal Pengisian'
                    ],
                    style: 'paragraph'
                },
                {
                    width: '*',
                    text: ': '+cetak_laporan_user.identitas.nama+'\n: '+cetak_laporan_user.identitas.nip+'\n: '+cetak_laporan_user.identitas.jabatan+'\n: '+cetak_laporan_user.identitas.telp+' Fax: '+cetak_laporan_user.identitas.fax+'\n: '+cetak_laporan_user.identitas.hp+'\n: '+cetak_laporan_user.identitas.email+'\n: '+tanggal_pengisian,
                    style: 'paragraph'
                }               
            ],
            columnGap: 10
        },
        {
            text:' ',
            pageBreak: 'after',
            pageOrientation: 'landscape' 
        },
        { text:'KUESIONER LITBANG DI SEKTOR PEMERINTAH', style:'header'},
        { text:'1. PERSONIL LITBANG', style:'header_2'},
        { text:'Tabel 1a.Sebutkan peneliti (fungsional peneliti, perekayasa, pranata nuklir, dan lain-lain) yang berperan sebagai peneliti dalam kegiatan litbang berdasarkan tingkat pendidikan dan bidang ilmu di unit kerja Saudara tahun 2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', 'auto', 'auto', 'auto', '*', 'auto' ],
            body: cetak_tabel1_1
          }
        },
        { text:'Tabel 1b. Sebutkan peneliti asing berdasarkan tingkat pendidikan dan bidang ilmu pada unit kerja Saudara tahun 2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', 'auto', 'auto', '*', 'auto', '*','*' ],

            body: cetak_tabel1_2
          }
        },
        { text:'Tabel 1c. Berapa rata-rata peneliti menghabiskan waktu dan lamanya kegiatan litbang di unit kerja Saudara', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 'auto', '*', 'auto', 'auto' ],

            body: cetak_tabel1_3
          }
        },
        { text:'Tabel 1d. Sebutkan jumlah tenaga teknisi dan staf pendukung di unit kerja Saudara berdasarkan tingkat pendidikan tahun 2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 'auto', '*', '*', 'auto', 'auto', 'auto', 'auto' ],

            body: cetak_tabel1_4
          }
        },
        { text:'2. GAJI UPAH DAN BELANJA MODAL', style:'header_2'},
        { text:'Tabel 2a. Sebutkan total realisasi belanja pegawai dan belanja modal di unit kerja Saudara tahun 2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 'auto', '*', 'auto' ],

            body: cetak_tabel2_1
          }
        },
        { text:'3. BELANJA LITBANG', style:'header_2'},
        { text:'Tabel 3a. Sebutkan belanja kegiatan litbang dari sumber dana DIPA tahun 2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 'auto', 'auto', 50, 'auto', 'auto', 120 ],

            body: cetak_tabel3_1
          }
        },
        { text:'Tabel 3b. Sebutkan belanja litbang dari kerjasama tahun 2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 'auto','*', '*', 'auto', 200, 200 ],

            body: cetak_tabel3_2
          }
        },
        { text:'4. BELANJA LITBANG EKSTRAMURAL', style:'header_2'},
        { text:'Apakah unit kerja Saudara mendanai kegiatan litbang yang pelaksanaannya dilakukan oleh unit kerja/institusi lain (belanja litbang ekstramular) selama tahun 2015?', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', 'auto', '*', '*', '*' ],

            body: cetak_tabel4_1
          }
        },
        { text:'5. LUARAN LITBANG', style:'header_2'},
        { text:'Tabel 5a. Sebutkan publikasi pada jurnal internasional tahun 2014-2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', 150, '*', '*', '*','auto','auto' ],

            body: cetak_tabel5_1
          }
        },
        { text:'Tabel 5b. Sebutkan publikasi pada jurnal nasional terakreditasi tahun 2014-2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', 150, '*', '*', '*','auto','auto' ],

            body: cetak_tabel5_2
          }
        },
        { text:'Tabel 5c. Sebutkan publikasi pada jurnal nasional tidak terakreditasi (memiliki ISSN )tahun 2014 -2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', 150, '*', '*', '*','auto','auto' ],

            body: cetak_tabel5_3
          }
        },
        { text:'Tabel 5d. Sebutkan buku teks (memiliki ISBN) tahun 2014 -2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', '*', 'auto', '*' ],

            body: cetak_tabel5_4
          }
        },
        { text:'Tabel5e. Perolehan Hak Kekayaan Intelektual (HKI) tahun2014 -2015', style:'header_tabel'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ '*', 'auto', '*', '*', '*' ],

            body: cetak_tabel5_5
          }
        },
        'Catatan : ',
        {
            ul: [
                '*Pilih salah satu (Paten, Paten Sederhana, HakCipta, MerekDagang, RahasiaDagang, DesainProdukIndustri, Indikasi Geografis,Perlindungan Varietas Tanaman, Perlindungan Topografi SirkuitTerpadu)',
                '** Pilih salah satu : terdaftar atau granted',
                '***Unggah dokumen HKI dalam file PDF',
                '**** Perekayasa, PranataNuklir, Peneliti',
            ]
        },
        { text:'Kritik dan Saran', style:'header'},
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 'auto', '*'],

            body: cetak_tabel6_1
          }
        },
        { text:'- TERIMA KASIH ATAS KESEDIAAN SAUDARA MENGISI KUESIONER INI -', style:'header_2'}
      ],
      styles: {
         header: {
           fontSize: 24,
           bold: true,
           italic: true,
           margin: [20, 25, 10, 10]
         },
         header_2: {
           fontSize: 14,
           bold: true,
           margin: [0, 30, 0, -10],
           alignment: 'center'
         },
         header_tabel: {
            fontSize: 9,
            italics: true,
            margin: [0,30,0,10]
         },
         paragraph: {
            fontSize: 12,
            margin: [0,5,0,10]
         },
         tableHeader: {
            bold: true,
            fontSize: 13,
            color: 'black',
            alignment: 'center'
        }
       }
    };
    
    filename = 'report-kuesioner-'+cetak_laporan_user.institusi.balai+'-'+tanggal_hari_ini+'.pdf'
    
    if (jenis_laporan != 'kegiatan') {
        pdfMake.createPdf(docDefinition).download(filename);
    } else {
        pdfMake.createPdf(docDefinition).download(filename);
    }
}



//README - function buat dipake
//README - nilai rata rata di dalam array
function average_array(array){
    var total = 0;
    for(var i = 0; i < array.length; i++) {
        total += parseInt(array[i]);
    }
    // console.log(array);
    var avg = total / array.length;
    return avg;
}
//README - buat nampilin nilai mata uang
function format_currency(n, currency) {
    return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}
