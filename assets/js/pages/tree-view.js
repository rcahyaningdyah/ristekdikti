//Content Generator
function content_well(uwi){
    // console.log("masuk content well : "+uwi);
    $( ".panel-main" ).addClass("hide");
    var well = new Well(uwi);
    $( "#panel-well" ).removeClass("hide");
    well.print_head_properties('#TitleWell');
    well.print_properties('#tab1_1');
    well.print_production('#chart-wellproduction', '#table-wellproduction-container');
    well.print_table_document('#table-welldocument');
    well.print_avatar('#avatar-well');
    well.print_well_activity('#table-wellactivityx');
}
function content_warehouse(warehouse_id){
    // console.log("masuk content warehouse : "+warehouse_id);
    $( ".panel-main" ).addClass("hide");
    var warehouse = new Warehouse(warehouse_id);
    $( "#panel-warehouse" ).removeClass("hide");
    warehouse.print_head_properties('#TitleWarehouse');
}
function content_field(field_id){
    // console.log("masuk content field : "+field_id);
    $( ".panel-main" ).addClass("hide");
    var field = new Field(field_id);
    $( "#panel-field" ).removeClass("hide");
    field.print_head_properties('#TitleField');
}
function content_area(area_id){
    // console.log("masuk content area : "+area_id);
    $( ".panel-main" ).addClass("hide");
    var area = new Area(area_id);
    $( "#panel-area" ).removeClass("hide");
    area.print_head_properties('#TitleArea');
}
function content_seismic(seismic_id, seismic_type){
    // console.log("masuk content seismic : "+seismic_id, seismic_type);
    $( ".panel-main" ).addClass("hide");
    var seismic = new Seismic(seismic_id, seismic_type);
    $( "#panel-seismic" ).removeClass("hide");
    seismic.print_head_properties('#TitleSeismic');
    seismic.print_properties('#seismic-tab1');
    seismic.print_map('#seismic-tab2');
}

$(function() {
    //Get Global Variable
    get_array_cart();

    // README - Bagian TREE VIEW 
    treeview_area();
    treeview_well();
    treeview_warehouse();
    treeview_doctype_well();
    treeview_seismic();

    //README Panel Header - 
    $(document).on("click", ".panel-header .panel-maximize", function(event) {
        var panel = $(this).parents(".panel:first");
        if (panel.hasClass("maximized")) {
            map.invalidateSize();
        }
        else {
            map.invalidateSize();
        }
    });
    $(document).on("click", "#switch-rtl", function(event) {
        map.zoomControl = generateZoomControl();
        map.validateData();
        $('#listdiv').mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            autoHideScrollbar: false,
            scrollInertia: 150,
            theme: "light-thin",
            set_height: 440,
            advanced: {
                updateOnContentResize: true
            }
        });
    });

    /* Progress Bar  Widget */
    if ($('.widget-progress-bar').length) {
        $(window).load(function() {
            setTimeout(function() {
                $('.widget-progress-bar .stat1').progressbar();
            }, 900);
            setTimeout(function() {
                $('.widget-progress-bar .stat2').progressbar();
            }, 1200);
            setTimeout(function() {
                $('.widget-progress-bar .stat3').progressbar();
            }, 1500);
            setTimeout(function() {
                $('.widget-progress-bar .stat4').progressbar();
            }, 1800);
        });
    };
   
    //Hide Panel
    $( ".panel-main" ).addClass("hide");

    //Sidebar Collapsed
    $('body').addClass('sidebar-collapsed');

    //Sidebar active indicator
    $('#navtreeview').addClass('nav-active active');
});